<?php 
    require_once 'connection.php';
    header('Content-Type: application/json');
    Class Latest_fmc
    {
        private $db;
        private $connection;
        public function __construct()
        {
            $this->db = new DB_Connection();
            $this->connection = $this->db->get_connection();
        }

        public function get_latest_fmc()
        {
            $fmc = array();
            $query = "SELECT users.name, jobs.* FROM jobs JOIN users ON users.id = jobs.user_id ORDER BY created_at DESC LIMIT 5";
            $all_fmc= "SELECT COUNT(*) FROM users Where role_id = 3 AND parent_id = 0";
            $all_oam= "SELECT COUNT(*) FROM users Where role_id = 2 AND parent_id = 0";
            
            
            $jobs = mysqli_query($this->connection, $query);
            $all_oam_result = mysqli_query($this->connection, $all_fmc);
            $all_user_result = mysqli_query($this->connection, $all_oam);
           
            $data['count_fmc'] = $all_fmc_result ->num_rows; 
            $data['count_oam'] = $all_oam_result ->num_rows;
            $data['count_users'] = $data['count_oam'] + $data['count_fmc'];
            if($jobs->num_rows > 0)
            {
                $data['jobs'] = $jobs->fetch_array();
            } 
            echo json_encode($data);
        }
    }
    
    $all = new Latest_fmc();
    $all->get_latest_fmc();
?>
<?php 
    require_once 'connection.php';
    header('Content-Type: text/html');
    Class Latest_oam
    {
        private $db;
        private $connection;
        public function __construct()
        {
            $this->db = new DB_Connection();
            $this->connection = $this->db->get_connection();
        }

        public function get_latest_oam()
        {
            $oam = array();
            $query = "SELECT * FROM users Where role_id = 2 ORDER BY created_at DESC LIMIT 5";
            $count = "SELECT COUNT(*) FROM users Where role_id = 2";
            $oam_result = mysqli_query($this->connection, $query);

            $counter = mysqli_query($this->connection, $count);
            $oam['count_oam'] = $counter->num_rows; 
            if($oam_result->num_rows > 0)
            {
                $oam['oam'] = $oam_result->fetch_array();
            } 
            return $oam;
        }
    }
    
    $all = new Latest_oam();
    $all->get_latest_oam();
?>
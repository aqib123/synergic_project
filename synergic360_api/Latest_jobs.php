<?php 
    require_once 'connection.php';
    header('Content-Type: application/json');
    Class Latest_fmc
    {
        private $db;
        private $connection;
        public function __construct()
        {
            $this->db = new DB_Connection();
            $this->connection = $this->db->get_connection();
        }

        public function get_latest_fmc()
        {
            //$fmc = array();
            $query = "SELECT users.name, jobs.* FROM jobs JOIN users ON users.id = jobs.user_id WHERE job_status != 1 AND job_status != 5 ORDER BY created_at DESC LIMIT 10";
            $all_fmc= "SELECT COUNT(*) FROM users Where role_id = 3 AND parent_id = 0";
            
            $jobs = mysqli_query($this->connection, $query);
            $all_fmc_result = mysqli_query($this->connection, $all_fmc);
            if($jobs->num_rows > 0)
            {
               while($row = mysqli_fetch_array($jobs,MYSQLI_ASSOC)){
                     $json_array[] = array(
                         'id' => $row['id'],
                         'title' => $row['job_title'],
                         'comp_name' => $row['name'],
                         'date' => $row['created_at'],
                         'status' => $row['admin_approval'],
                         'description' => $row['description']
                     );
               }
            } 
            echo json_encode($json_array);
        }
    }
    
    $all = new Latest_fmc();
    $all->get_latest_fmc();
?>
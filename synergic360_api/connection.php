<?php
require_once 'config.php';

Class DB_Connection
{
    private $connect;
    function __construct()
    {
        $this->connect = mysqli_connect(hostname, username, password, db_name) or die("Database connection error");
    }

    public function get_connection()
    {
        return $this->connect;
    }
}

?>
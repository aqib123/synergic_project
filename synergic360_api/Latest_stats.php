<?php 
    require_once 'connection.php';
    header('Content-Type: application/json');
    Class Latest_stats
    {
        private $db;
        private $connection;
        public function __construct()
        {
            $this->db = new DB_Connection();
            $this->connection = $this->db->get_connection();
        }

        public function get_latest_stats()
        {
            $all_fmc= "SELECT * FROM users Where role_id = 3 AND parent_id = 0";
            $all_oam= "SELECT * FROM users Where role_id = 2 AND parent_id = 0";
            $query = "SELECT * FROM jobs WHERE job_status IN(0,2,3,4)";
            
            $all_fmc_result = mysqli_query($this->connection, $all_fmc);
            $all_oam_result = mysqli_query($this->connection, $all_oam);
            $jobs = mysqli_query($this->connection, $query);

            $data['count_fmc'] = $all_fmc_result->num_rows; 
            $data['count_oam'] = $all_oam_result->num_rows;
            $data['count_jobs'] = $jobs->num_rows;
            $random = rand(25, 35);
            $data['count_users'] = $random + $data['count_oam'] + $data['count_fmc'];
            
            echo json_encode($data);
        }
    }
    
    $all = new Latest_stats();
    $all->get_latest_stats();
?>
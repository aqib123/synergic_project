@extends('layouts.app')
@section('content')
<!-- Add user section starts from here -->
@php
$name = '';
$description = '';
$total_amount = '';
if(isset($edit_record)){
    $name = $edit_record['package_title'];
    $description = $edit_record['package_description'];
    $total_amount = $edit_record['total_amount'];
}
$i = 0;
$url = '/subscription/packages';
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url($url)}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Subscription List</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">

    </ul>
</div>
@include('flash_message')
<div class="row ">
    <div class="col-sm-12">
        <div class="card-box add_staff add_user_role">
                <form id="package_form" action="{{url('/save/record/subscription')}}" method="POST">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Package Name </label>
                            <span style="color:red;">*</span>
                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                            <input name="name" placeholder="Name" class="form-control" type="text" value="{{$name}}">
                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Description </label>
                            <textarea name="description" placeholder="Description" class="form-control" type="text">{{$description}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Amount </label>
                            <span style="color:red;">*</span>
                            <input name="amount" placeholder="Amount" class="form-control" type="text" value="{{$total_amount}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="bottom_border">
                            <h6 class="m-t-0 m-b-10 font-13"><b>Select the features you want to enable for this Package</b></h6>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="bottom_border add_user_role_detail m-b-10">
                            <p class="m-b-10 text-muted font-13">Listings for Services</p>
                            <div class="card-box add_user_role_box m-b-10">
                                <dl class="row bottom_border m-b-10">
                                    <dt class="col-sm-2">Select Feature <span style="color:red;">*</span></dt>
                                </dl>

                                @foreach($system_services as $key => $value)
                                    <dl class="row bottom_border m-b-10">
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <div class="checkbox checkbox-success checkbox-single">
                                                    <input id="{{$i}}" type="checkbox" name="services[{!! $value['id'] !!}]"
                                                    @php
                                                    if(isset($edit_record['services'])){
                                                        echo GeneralFunctions::checkValueInAssociativeArray($value['id'], $edit_record['services']) == true ? 'checked' : '';
                                                    }
                                                    @endphp
                                                    >
                                                    <label for="{{$i}}"></label>
                                                </div>
                                            </span>
                                        </dt>

                                        <dd class="col-sm-4">{{ $value['service_name'] }}</dd>
                                    </dl>
                                @endforeach
                            </div>
                        </div>

                        </div>
                        <div class="col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</a>
                            <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                        </div>
                </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
            var url_path = "{{ generate_url('/add_package/validation') }}";
        $(document).on('click', '.save_button', function(){
            data = $('#package_form').serialize();
            $('.loading_gif').show();
            $.ajax({
                type: 'POST',
                url: url_path,
                data: data,
                success:function(data){
                    if(data.status == 'success'){
                        $('#package_form').submit()[0];
                    }
                    else{
                        var errorArray = data.msg_data;
                        var list = '';
                        errorArray.forEach(function(e){
                            list = list +'<li>'+e+'</li>';
                        });

                        $('#msg-list').html(list);
                        $('.msg-box').addClass("alert-danger").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                    $('.loading_gif').hide();
                }
            });
        });
    })
</script>
@endsection

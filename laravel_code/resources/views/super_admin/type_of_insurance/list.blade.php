@extends('layouts.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('screen/type_of_insurance')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add Type of Insurance</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title of Insurance</th>
                        <th>Remark</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($record) && count($record)>0)
                        @foreach($record as $key => $row)
                            <tr>
                                <td>{!! ++$key !!}</td>
                                <td>{{$row['name']}}</td>
                                <td>{{$row['remarks']}}</td>
                                <td><?php echo date_format(new DateTime($row['created_at']), 'g:ia jS F Y'); ?></td>
                                <td>
                                    <div class="btn-group user_action_btn">
                                        <a class="custm_btn" data-toggle="dropdown" aria-expanded="true"><i class="zmdi zmdi-more"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url('screen/type_of_insurance?id='.$row['id'])}}" title="Edit">
                                                <i class="fa fa-edit text-info"></i> Edit
                                            </a>
                                                <a class="dropdown-item delete_cat_btn" href="javascript:void(0)" id="{{$row['id']}}" title="Delete">
                                                    <i class="fa fa-trash text-danger"></i> Delete
                                                </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade md_delete_confirmation" id="md_delete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '.delete_cat_btn', function(){

            var uuid = $(this).attr('id');
            $('#user_uuid').val(uuid);
            /* Show Confirmation alert box before deletion */
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click','.md_ok_delete_btn', function(){
            var user_uuid = $('#user_uuid').val();
            var url = "{{url('/delete/type/insurance')}}/"+user_uuid;
            window.location.href = url;
        });
    });
</script>
@endsection
@extends('layouts.app')
@section('content')
<!-- Add user section starts from here -->
@php
$name = '';
$remarks = '';
if(isset($edit_record)){
    $name = $edit_record['name'];
    $remarks = $edit_record['remarks'];
}
$i = 0;
$url = '/type/insurance/list';
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url($url)}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Insurance Type List</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">

    </ul>
</div>
@include('flash_message')
<div class="row ">
    <div class="col-sm-12">
        <div class="card-box add_staff add_user_role">
                <form id="package_form" action="{{url('/save/type/insurance')}}" method="POST">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Insurance Name </label>
                            <span style="color:red;">*</span>
                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                            <input name="name" placeholder="Insurance Name" class="form-control" type="text" value="{{$name}}">
                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="remarks">Remarks </label>
                            <textarea name="remarks" placeholder="Remarks" class="form-control" type="text">{{$remarks}}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</button>
                    </div>
                </div>
        </form>
        </div>
    </div>
</div>
@endsection

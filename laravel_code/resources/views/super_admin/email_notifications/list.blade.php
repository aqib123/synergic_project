@extends('layouts.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@if(\Session::has('status'))
    <div class="alert alert-success">
        <p>{{ \Session::get('status')}}</p>
    </div>
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Heading</th>
                        <th>Sub Heading</th>
                        <th>Short Description</th>
                        <th>Status</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($record) && count($record)>0)
                        @foreach($record as $key => $row)
                            <tr>
                                <td>{!! ++$key !!}</td>
                                <td>{{$row['title']}}</td>
                                <td>{{$row['heading']}}</td>
                                <td>{{$row['sub_heading']}}</td>
                                <td>{{$row['short_description']}}</td>
                                <td>
                                    @if($row['active_status'] == 1)
                                        <span class="label label-success">Active</span>
                                    @else
                                        <span class="label label-danger">InActive</span>
                                    @endif
                                </td>
                                <td><?php echo date_format(new DateTime($row['created_at']), 'g:ia jS F Y'); ?></td>
                                <td>
                                    <div class="btn-group user_action_btn">
                                        <a class="custm_btn" data-toggle="dropdown" aria-expanded="true"><i class="zmdi zmdi-more"></i></a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url('edit/email/content?id='.$row['id'])}}" title="Edit">
                                                <i class="fa fa-edit text-info"></i> Change Content
                                            </a>
                                            @if($row['active_status'] == 1)
                                                <a class="dropdown-item" href="{{url('change/status/email/content?id='.$row['id'].'&status=0')}}" title="Status">
                                                    <i class="fa fa-edit text-info"></i> InActive
                                                </a>
                                            @else
                                                <a class="dropdown-item" href="{{url('change/status/email/content?id='.$row['id'].'&status=1')}}" title="Status">
                                                <i class="fa fa-edit text-info"></i> Active
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade md_delete_confirmation" id="md_delete_confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade md_status_modal" id="md_status_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{url('subscription/change/status/')}}">
                <div class="modal-header alert alert-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="success-message">Are you sure you want to change status of Package </p>
                    <input type="hidden" name="package_id" id="package_id">
                    <input type="hidden" name="status_id" id="status_id">
                    {{ csrf_field() }}
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success status-confirm md_ok_delete_btn">Ok</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '.delete_cat_btn', function(){

            var uuid = $(this).attr('id');
            $('#user_uuid').val(uuid);
            /* Show Confirmation alert box before deletion */
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click','.md_ok_delete_btn', function(){
            var user_uuid = $('#user_uuid').val();
            var url = "{{url('/delete/record/subscription/')}}/"+user_uuid;
            window.location.href = url;
        });

        $(document).on('click', '.status_event', function(){
            $('#status_id').val($(this).attr('id'));
            $('#package_id').val($(this).attr('data-id'));
            $('.md_status_modal').modal('show');
        });
    });
</script>
@endsection
@extends('layouts.app') @section('content')
@include('flash_message')
<div class="row">
    <div class="col-sm-12">
        <div class=" pull-right m-t-15">
            <button type="button" id="pending-job-button" class="btn btn-dark-outline  waves-effect waves-light active">Pending</button>
            <button type="button" id="approved-job-button" class="btn btn-dark-outline  waves-effect waves-light">Approved</button>
            <button type="button" id="rejected-job-button" class="btn btn-dark-outline  waves-effect waves-light">Rejected</button>
            <button type="button" id="review-job-button" class="btn btn-dark-outline  waves-effect waves-light">Reviewed Jobs</button>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@if(isset($jobs) && $jobs != '')
    <!-- All Pending Jobs -->
    <div class="row" id="pending_jobs">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <p class="text-muted font-13 m-b-30">
                    All Pending for approval Jobs are listed
                </p>
                <table id="datatable2" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Post Date</th>
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Type</th>
                            <th>Focal Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $row) @if($row['admin_approval'] == 0)
                        <tr>
                            <td>
                                <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                            </td>
                            <td>{{$row['companies']['name'] }}</td>
                            <td>{{ $row['job_title'] }}</td>
                            <td>{{ $row['job_type'] }}</td>
                            <td>
                                @foreach(json_decode($row['contact_name']) as $name) {{$name}}
                                <br /> @endforeach
                            </td>
                            <td>
                                <a href="{{ url('job_preview?id='.$row['id']) }}" class="btn btn-sm btn-dark-outline  waves-effect waves-light apply" id="{{ $row['id'] }}">Show Detalis</button>
                            </td>
                        </tr>
                        @endif @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Pending Jobs -->

    <!-- All Approved Jobs -->
    <div class="row" style="display: none;" id="approved_jobs">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <p class="text-muted font-13 m-b-30">
                    All Approved for approval Jobs are listed
                </p>
                <table id="datatable3" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Type</th>
                            <th>Post Date</th>
                            <th>Focal Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $row) @if($row['admin_approval'] == 1)
                        <tr>
                            <td>{{$row['companies']['name'] }}</td>
                            <td>{{ $row['job_title'] }}</td>
                            <td>{{ $row['job_type'] }}</td>
                            <td>
                                <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                            </td>
                            <td>
                                @foreach(json_decode($row['contact_name']) as $name) {{$name}}
                                <br /> @endforeach
                            </td>
                            <td>
                                <a href="{{ url('job_preview?id='.$row['id']) }}" class="btn btn-dark-outline btn-sm waves-effect waves-light apply" id="{{ $row['id'] }}">Show Detalis</button>
                            </td>
                        </tr>
                        @endif @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Approved Jobs -->

    <!-- All Rejected Jobs -->
    <div class="row" style="display: none;" id="rejected_jobs">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <p class="text-muted font-13 m-b-30">
                    All Rejected for approval Jobs are listed
                </p>
                <table id="datatable4" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Type</th>
                            <th>Post Date</th>
                            <th>Focal Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $row) @if($row['admin_approval'] == 2)
                        <tr>
                            <td>{{$row['companies']['name'] }}</td>
                            <td>{{ $row['job_title'] }}</td>
                            <td>{{ $row['job_type'] }}</td>
                            <td>
                                <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                            </td>
                            <td>
                                @foreach(json_decode($row['contact_name']) as $name) {{$name}}
                                <br /> @endforeach
                            </td>

                            <td>
                                <a href="{{ url('job_preview?id='.$row['id']) }}" class="btn btn-dark-outline btn-sm waves-effect waves-light apply" id="{{ $row['id'] }}">Show Detalis</button>
                            </td>
                        </tr>
                        @endif @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Rejected Jobs -->

    <!-- All Review Jobs -->
    <div class="row" style="display: none;" id="review_jobs">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <p class="text-muted font-13 m-b-30">
                    All Jobs for Review by client are listed
                </p>
                <table id="datatable5" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Job Title</th>
                            <th>Job Type</th>
                            <th>Post Date</th>
                            <th>Focal Name</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($jobs as $row) @if($row['admin_approval'] == 3)
                        <tr>
                            <td>{{$row['companies']['name'] }}</td>
                            <td>{{ $row['job_title'] }}</td>
                            <td>{{ $row['job_type'] }}</td>
                            <td>
                                <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                            </td>
                            <td>
                                @foreach(json_decode($row['contact_name']) as $name) {{$name}}
                                <br /> @endforeach
                            </td>

                            <td>
                                <a href="{{ url('job_preview?id='.$row['id']) }}" class="btn btn-dark-outline btn-sm waves-effect waves-light apply" id="{{ $row['id'] }}">Show Detalis</button>
                            </td>
                        </tr>
                        @endif @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- End Rejected Jobs -->
@endif 
@section('js')
<script>
    $(document).ready(function () {
        $("#pending-job-button").click(function () {
            $("#pending_jobs").show();
            $("#approved_jobs").hide();
            $("#rejected_jobs").hide();
            $("#review_jobs").hide();
            $("#approved-job-button").removeClass("active");
            $("#rejected-job-button").removeClass("active");
            $("#review-job-button").removeClass("active");
            $("#pending-job-button").addClass("active");
        });

        $("#approved-job-button").click(function () {
            $("#pending_jobs").hide();
            $("#approved_jobs").show();
            $("#rejected_jobs").hide();
            $("#review_jobs").hide();
            $("#pending-job-button").removeClass("active");
            $("#rejected-job-button").removeClass("active");
            $("#review-job-button").removeClass("active");
            $("#approved-job-button").addClass("active");
        });

        $("#rejected-job-button").click(function () {
            $("#pending_jobs").hide();
            $("#approved_jobs").hide();
            $("#rejected_jobs").show();
            $("#review_jobs").hide();
            $("#approved-job-button").removeClass("active");
            $("#pending-job-button").removeClass("active");
            $("#review-job-button").removeClass("active");
            $("#rejected-job-button").addClass("active");
        });

        $("#review-job-button").click(function () {
            $("#pending_jobs").hide();
            $("#approved_jobs").hide();
            $("#rejected_jobs").hide();
            $("#review_jobs").show();
            $("#approved-job-button").removeClass("active");
            $("#pending-job-button").removeClass("active");
            $("#rejected-job-button").removeClass("active");
            $("#review-job-button").addClass("active");
        });

        $('#datatable2').DataTable();
        $('#datatable3').DataTable();
        $('#datatable4').DataTable();
        $('#datatable5').DataTable();
    });
</script> @stop @endsection
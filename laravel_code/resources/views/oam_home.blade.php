@extends('oam.app') @section('content') @section('calendarCss')
<!--calendar css-->
<link href="{{ URL::to('assets/plugins/fullcalendar/dist/fullcalendar.css') }}" rel="stylesheet" /> @endsection @section('css')
<style>
    .fill_prof {
        font-size: 12px;
        font-weight: 800;
        margin-top: 13px;
    }

    .stat_boxes h6 {
        font-size: 11px;
    }

    .image_box {
        padding: 16px 20px;
    }

    .dashboard_grid_header {
        padding: 0.7rem 0.6rem;
        background-color: #fff;
        border-bottom: 1px solid #ccc;
        /* font-weight: 200; */
    }

    .dashboard_grid_header h4 {
        font-weight: 400;
        font-size: 17px;
        font-family: ProximaNova, arial, sans-serif !important;
        color: #000;
    }

    .list_detail {
        border: none;
        padding: 7px 6px 30px 6px;
    }

    .list_desgin {
        margin-bottom: 5px;
        background-color: #F9F9F9;
        padding: 10px 5px !important;
        border: 1px solid #ccc !important;
        border-radius: 2px;
    }

    .job_description_title {
        font-size: 13px;
        font-weight: 600 !important;
        color: #000 !important;
    }

    .text_dark_blue {
        color: #0087e0;
    }

    .job_description_detail {
        color: #515151 !important;
    }

    .font-11 {
        font-size: 11px;
    }

    .job_stats {
        margin-right: 15px;
    }

    .task_nav .active {
        font-weight: 700;
        font-family: ProximaNova, arial, sans-serif !important;
        color: #191919;
        background: #fff;
        border-left: 1px solid #BBB;
        border-right: 1px solid #BBB;
        border-top: 1px solid #BBB;
        z-index: 2;
    }

    .task_nav .nav-link {
        padding: 5px 6px;
        font-family: ProximaNova, arial, sans-serif !important;
        margin: 0 2px;
        color: #767676;
        text-decoration: none;
        border: 1px solid #DADADA;
        border-bottom: 0;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        background: linear-gradient(-180deg, #fff 75%, #E8EAED 100%);
        cursor: pointer;
        font-size: 12px;
    }

    .dashboard_balance {
        font-size: 24px;
        margin-left: 5px;
        border-right: 1px solid #DADADA;
        padding: 5px 5px 5px 0px;
        vertical-align: middle;
    }

    .outstanding_balances {
        padding: 8px;
        vertical-align: middle;
    }

    .main_balances {
        margin-bottom: 5px;
    }

    .balance_detail {

        position: relative;
        display: inline-block;
        width: 100%;
        padding: 12px 10px;
        background-color: #F4F4F4;
        background: linear-gradient(180deg, rgba(240, 240, 240, .2) 0, rgba(209, 209, 209, .2) 100%);
        background-image: linear-gradient(rgba(240, 240, 240, 0.2) 0px, rgba(209, 209, 209, 0.2) 100%);
        border: 1px solid #BBB;
        border-radius: 3px;
        margin-bottom: 0px;
    }

    .balance_detail:hover {

        background-image: linear-gradient(-180deg, #e8eaed 0, #fbfcfd 99%);

    }

    .custom_calendar .fc-button {
        padding: 2px 5px !important;
    }

    .custom_calendar .fc-toolbar h2 {
        font-size: 14px;
    }

    .custom_calendar .fc-view {
        margin-top: 10px;
    }

    .custom_calendar .fc-toolbar {
        margin-top: 5px;
    }

    .custom_calendar table {
        width: 99% !important;
    }

    .top_boxes {
        min-height: 0px;
    }

    .bg_cards .bg-icon {
        height: 60px;
        width: 60px;
        text-align: center;
        -webkit-border-radius: 50%;
        border-radius: 50%;
        -moz-border-radius: 50%;
        background-clip: padding-box;
        border: 1px dashed #818a91;
        background-color: #f7f7f9;
        margin-right: 0px;
    }

    .bg_cards .bg-icon i {
        line-height: 60px;
        font-size: 26px;
        color: #818a91;
    }

    .custom_padding {
        padding: 19px 2px;
    }

    .dashboard_icons .bg-icon i {
        font-size: 16px !important;
        line-height: 50px !important;
    }

    .dashboard_icons .bg-icon {
        height: 50px;
        width: 50px;
        margin-top: 10px;
    }
</style>
@endsection

<div class="m-t-10">
    <div class="col-sm-12">
        <h4 class="header-title m-b-10">Welcome,
            <strong>{{isset($title)?$title:''}}</strong>
        </h4>
        <!-- <div class="pull-right m-b-10">

            <a href="{{url('oam/job_form')}}" class="btn btn-sm btn-dark-outline waves-effect waves-light">Post a job</a>
    </div> -->
        <div class="clearfix"></div>
    </div>

    <div class="col-xs-12 col-lg-9 col-xl-9 dashboard_icons padding-left-0 stat_boxes top_boxes padding-right-0 ">
        <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
            <a href="{{url('oam/jobs?tab=active')}}" class="text-dark">
                <div class="card-box  m-b-10 bg_cards tilebox-three">
                    <div class="bg-icon pull-xs-left">
                        <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    </div>
                    <div class="text-xs-right">
                        <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Active Ads</h6>
                        <h3 class="m-b-10">
                            <span data-plugin="counterup ">{{isset($jobs)?$jobs->count():0}}</span>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
            <a href="{{url('oam/jobs?tab=active')}}" class="text-dark">
                <div class="card-box m-b-10 bg_cards tilebox-three">
                    <div class="bg-icon m-b-10 pull-xs-left">
                        <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    </div>
                    <div class="text-xs-right">
                        <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Applied Jobs</h6>
                        <h3 class="m-b-10">
                            <span data-plugin="counterup">{{isset($job_apply)?$job_apply:0}}</span>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
            <a href="{{url('oam/jobs')}}" class="text-dark">
                <div class="card-box m-b-10 bg_cards tilebox-three">
                    <div class="bg-icon pull-xs-left">
                        <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    </div>
                    <div class="text-xs-right">
                        <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Pending Approval</h6>
                        <h3 class="m-b-10">
                             <span data-plugin="counterup">{{isset($pending_jobs)?$pending_jobs->count():0}}</span>
                        </h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
            <a href="{{url('oam/show_assigned_jobs')}}" class="text-dark">
                <div class="card-box bg_cards m-b-10 tilebox-three">
                    <div class="bg-icon pull-xs-left">
                        <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    </div>
                    <div class="text-xs-right">
                        <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Assigned Jobs</h6>
                        <h3 class="m-b-10">
                              <span data-plugin="counterup">{{isset($assigned_jobs)?$assigned_jobs->count():0}}</span>
                        </h3>
                    </div>
                </div>
            </a>
        </div>



    </div>
    <div class="col-xs-12 col-lg-3 col-xl-3">
        <div class="card-box custom_padding m-b-10">

            <div class="  col-lg-4 col-md-4 col-xl-4">
                <div class="  widget-user m-b-0 ">
                    <div style="text-align: center;">
                        @if(Auth::user()->image != null)
                            <img src="{{asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image)}}" class="img-responsive img-circle"
                            alt="user">
                        @else
                            <img src="{{asset('laravel_code/storage/app/public/user_image/default_user.png')}}" class="img-responsive img-circle"
                            alt="user">
                        @endif    
                        <div class="wid-u-info">
                            <h6 class="text-muted  m-b-0 font-11" style="margin-top: 5px;">{{Auth::user()->name}}</h6>

                        </div>
                    </div>
                </div>
            </div>

            <div class="  col-lg-8 col-md-8 col-xl-8 ">
                @if(Auth::user()->profile_strength == "100%")
                <span class="text-success font-11 m-t-10 pull-xs-right fill_prof" data-toggle="tooltip" title="Congratulations! Your profile is now completed">
                    <i class="ion-checkmark-round"></i>&nbsp;Completed</span>
                @else
                <a href="{{url('oam/oam_profile')}}" class="btn btn-sm btn-primary-outline waves-effect waves-light pull-xs-right m-t-10">Complete Profile</a>
                @endif
                <h6 class="text-dark m-t-10 m-b-10">Profile Status</h6>
                <p class="font-600 text-muted m-b-10">Set up your account
                    <span class="text-primary pull-right">
                        <b>{{isset($strength->profile_strength)?$strength->profile_strength:'0%'}}</b>
                    </span>
                </p>
                <progress class="progress progress-striped progress-sm progress-primary m-b-0" value="{{str_replace('%','',$strength->profile_strength)}}"
                    max="100"></progress>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="col-xs-12  m-t-0 m-b-10">

        <a href="{{url('oam/job_form')}}" class="btn btn-sm btn-dark-outline waves-effect waves-light">Post New Job Ad</a>
    </div>
    <div class="col-xs-12 col-md-4">
        @if(GeneralFunctions::check_view_permission('/oam/jobs'))
        <div class=" dashboard_grid_header  ">
            <h4 class="header-title m-t-0 m-b-0">Active Job Ads</h4>
        </div>
        <div class="card-box list_detail m-b-10">
            <div class="inbox-widget nicescroll" style="height: 309px;">
                @php $i = 0; @endphp @if(isset($jobs) && count($jobs)>0) @foreach($jobs as $row) @php $is_assigned =0; @endphp @if($is_assigned
                == 0) <a href="{{url('oam/oam_job_details/'.$row->id)}}">
                    <div class="inbox-item list_desgin">
                        <!-- <div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg" class="img-circle" alt=""></div> -->
                        <p class="inbox-item-author job_description_title">
                            {{substr($row->job_title,0,20)}}....
                        </p>
                        <p class="inbox-item-author text-info font-11">
                            <b>{{$row->job_type}}</b>
                        </p>
                        <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                        <div class="job_stats">
                            <ul class="list-inline m-b-0  text-xs-left">
                                <li style="display: inline;" class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                    @if(GeneralFunctions::days_remaining_in_expiration($row->job_expiry_days, $row->admin_approval_date) == 'Job Post Expired')
                                    {!! GeneralFunctions::days_remaining_in_expiration($row->job_expiry_days, $row->admin_approval_date)
                                    !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->job_expiry_days,
                                    $row->admin_approval_date) !!} @endif
                                </li>
                                <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">{!! GeneralFunctions::short_list($row->id) !!}</li>

                                <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                    Applied {{ $row->jobApply->count()}}
                                </li>
                                <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{ $row->count_views}} </li>
                            </ul>

                        </div>
                        <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y'); @endphp</p>
                    </div>
                    </a>
                    @php $i++; @endphp @endif @endforeach @endif @if($i
                    < 1) <div class="card-block page_box_shadow">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted">
                                    You have not listed any job please post a job.
                                </p>
                                <a href="{{url('oam/job_form')}}" class="btn btn-dark-outline waves-effect waves-light">Post a job</a>
                            </div>
                        </div>

                        <div class="clearfix"></div>
            </div>
            @endif

        </div>

        <div class="pull-right" style="margin-top: 5px;">
            <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  ">
                <strong>View all</strong>
            </a>
        </div>
    </div>
    @endif

</div>
<div class="col-xs-12 col-md-4">
    <div class=" dashboard_grid_header  ">
        <h4 class="header-title m-t-0 m-b-0">Jobs Ads Status</h4>
    </div>
    <div class="card-box list_detail m-b-10">
        <ul class="nav task_nav nav-tabs m-b-10" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pending-approval-tab" data-toggle="tab" href="#pending_approval" role="tab" aria-controls="pending-approval-tab"
                    aria-expanded="true">Pending Approval</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="expired-jobs-ads-tab" data-toggle="tab" href="#expired_jobs_ads" role="tab" aria-controls="expired-jobs-ads-tab">Expired Jobs Ads</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="reviewed-job-ads-tab" data-toggle="tab" href="#reviewed_job_ads" role="tab" aria-controls="reviewed-job-ads-tab">Reviewed Job Ads</a>
            </li>
        </ul>
        <div class="inbox-widget nicescroll" style="height: 270px;">

            <div class="tab-content" id="myTabContent">

                <!-- pending approval Tab -->
                <div role="tabpanel" class="tab-pane fade in active" id="pending_approval" aria-labelledby="pending-approval-tab">
                    @if(isset($pending_jobs) && count($pending_jobs) > 0) @foreach($pending_jobs as $pjobs)
                    <a href="{{url('oam/oam_job_details/'.$pjobs->id)}}">
                        <div class="inbox-item list_desgin">
                            <p class="inbox-item-author job_description_title">
                                {{substr($pjobs->job_title,0,20)}}....
                            </p>
                            <p class="inbox-item-author text-info font-11">
                                    <b>{{$pjobs->job_type}}</b>
                                </p>
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($pjobs->description,0,120)) }}</p>

                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($pjobs->created_at), 'jS F Y'); @endphp</p>
                        </div>
                    </a>
                    @endforeach @else
                    <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no pending approval.
                                </p>
                                <h1 class=" display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif

                </div>

                <!-- expired jobs ads Tab -->
                <div role="tabpanel" class="tab-pane fade  " id="expired_jobs_ads" aria-labelledby="expired-jobs-ads-tab">
                    @if(isset($expired_jobs) && count($expired_jobs) > 0) @foreach($expired_jobs as $row)
                    <a href="{{url('oam/oam_job_details/'.$row->id)}}">
                        <div class="inbox-item list_desgin">
                            <p class="inbox-item-author job_description_title">
                                {{substr($row->job_title,0,20)}}....
                            </p>
                            <p class="inbox-item-author text-info font-11">
                                    <b>{{$row->job_type}}</b>
                                </p>
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>

                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y'); @endphp</p>
                        </div>
                    </a>
                    @endforeach @else
                    <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no expired jobs ads.
                                </p>
                                <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
                <!-- reviewed job ads Tab -->
                <div role="tabpanel" class="tab-pane fade  " id="reviewed_job_ads" aria-labelledby="reviewed-job-ads-tab">
                    @if(isset($reviewed_jobs) && count($reviewed_jobs) > 0) @foreach($reviewed_jobs as $rjobs)

                    <a href="{{url('oam/oam_job_details/'.$rjobs->id)}}">
                        <div class="inbox-item list_desgin">
                            <p class="inbox-item-author job_description_title">
                                {{substr($rjobs->job_title,0,20)}}....
                            </p>
                            <p class="inbox-item-author text-info font-11">
                                    <b>{{$rjobs->job_type}}</b>
                                </p>
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($rjobs->description,0,120)) }}</p>

                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($rjobs->created_at), 'jS F Y'); @endphp</p>
                        </div>
                    </a>
                    @endforeach @else
                    <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no rejected job ads.
                                </p>
                                <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
            </div>

        </div>

        <!-- <div class="pull-right" style="margin-top: 5px;">
                <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  "><strong>View all</strong></a>
            </div>  -->
    </div>

</div>
<div class="col-xs-12 col-md-4">
    <div class=" dashboard_grid_header  ">
        <h4 class="header-title m-t-0 m-b-0">Incoming Requests</h4>
    </div>
    <div class="card-box list_detail m-b-10">
        <ul class="nav task_nav nav-tabs m-b-10" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active " id="site-visit-requests-tab" data-toggle="tab" href="#site_visit_requests" role="tab" aria-controls="site-visit-requests-tab"
                    aria-expanded="true">Site visit requests</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="quote-submitted-tab" data-toggle="tab" href="#quote_submitted" role="tab" aria-controls="quote-submitted-tab">Quote Submitted</a>
            </li>
        </ul>
        <div class="inbox-widget nicescroll" style="height: 270px;">

            <div class="tab-content" id="myTabContent">

                <!-- Incoming requests Tab -->
                <div role="tabpanel" class="tab-pane fade in active" id="site_visit_requests" aria-labelledby="site-visit-requests-tab">
                    @if(isset($site_visit) && count($site_visit) > 0) @foreach($site_visit as $row)
                    @php
                        if($row['oam_staff_id'] != 0){
                            $creator = User::where('id', $row['oam_staff_id'])->first(['name']);
                            $creator = $creator->name;
                        }
                    @endphp
                    <a href="{{url('oam/oam_job_details/'.$row->job_id)}}">
                        <div class="inbox-item list_desgin">
                            <p class="inbox-item-author job_description_title">
                                {{substr($row->job_title,0,20)}}....
                            </p>
                            <p class="inbox-item-author text-info font-11">
                                <b>{{$row->name}}</b>
                            </p>
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->inspection_date), 'jS F Y g:ia'); @endphp</p>
                        </div>
                    </a>
                    @endforeach @else
                    <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no site visit requests.
                                </p>
                                <h1 class=" display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    @endif

                </div>

                <!-- Assigned to me Tab -->
                <div role="tabpanel" class="tab-pane fade  " id="quote_submitted" aria-labelledby="assigned-to-me-tab">
                    @if(isset($qoute_submitted) && count($qoute_submitted) > 0) @foreach($qoute_submitted as $row)
                    @php
                        if($row['oam_staff_id'] != 0){
                            $creator = User::where('id', $row['oam_staff_id'])->first(['name']);
                            $creator = $creator->name;
                        }
                    @endphp
                    <a href="{{url('oam/oam_job_details/'.$row->job_id)}}" >
                        <div class="inbox-item list_desgin">
                            <p class="inbox-item-author job_description_title">
                                {{substr($row->job_title,0,20)}}....
                            </p>
                            <p class="inbox-item-author text-info font-11">
                                <b>AED {{$row->quotation_amount}}</b>
                            </p>
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp</p>
                        </div>
                    </a>
                    @endforeach @else
                    <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no quote submitted.
                                </p>
                                <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>

            </div>

        </div>

        <!-- <div class="pull-right" style="margin-top: 5px;">
                <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  "><strong>View all</strong></a>
            </div>  -->
    </div>

</div>

<div class="col-xs-12 col-md-4">
    <div class=" dashboard_grid_header  ">
        <h4 class="header-title m-t-0 m-b-0">Activity Planner </h4>
    </div>
    <div class="card-box list_detail m-b-10">

        <div class="inbox-widget nicescroll" style="height:320px;">
            <div class="custom_calendar" id="calendar"></div>

        </div>
    </div>

</div>

<div class="col-xs-12 col-md-4">
    @if(GeneralFunctions::check_view_permission('/oam/jobs'))
    <div class=" dashboard_grid_header  ">

        <h4 class="header-title m-t-0 m-b-0">Assigned Jobs </h4>
    </div>
    <div class="card-box list_detail m-b-10">

        <div class="inbox-widget nicescroll" style="height: 320px;">

            @if(isset($assigned_jobs) && count($assigned_jobs)>0) @foreach($assigned_jobs as $row)
            <a href="{{url('oam/oam_job_details/'.$row->id)}}">
                <div class="inbox-item list_desgin">
                    <!-- <div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg" class="img-circle" alt=""></div> -->
                    <p class="inbox-item-author job_description_title">
                        {{substr($row->job_title,0,20)}}....
                    </p>
                    <p class="inbox-item-author text-info font-11">
                            <b>{{$row->job_type}}</b>
                        </p>
                    <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                    <div class="job_stats">
                        <ul class="list-inline m-b-0  text-xs-left">
                            <li style="display: inline;" class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                @if(GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date) == 'Job Post Expired')
                                {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date)
                                !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date)
                                !!} @endif
                            </li>
                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">{!! GeneralFunctions::short_list($row->id) !!}</li>

                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                Applied {{ $row->jobApply->count()}}
                            </li>
                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{ $row->count_views}} </li>
                        </ul>

                    </div>
                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y'); @endphp</p>
                </div>
            </a>
            @endforeach @else
            <div class="card-block  ">
                <div class="col-lg-12">

                    <div class="text-xs-center">
                        <p class="text-muted">
                            You have not any assigned job.
                        </p>
                        <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                            <i class="zmdi zmdi-mood"></i>
                        </h1>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            @endif

        </div>
        @if(isset($assigned_jobs) && count($assigned_jobs)>0)
        <div class="pull-right" style="margin-top: 5px;">
            <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  ">
                <strong>View all</strong>
            </a>
        </div>
        @endif
    </div>
    @endif

</div>

<div class="col-xs-12 col-md-4">
    @if(GeneralFunctions::check_view_permission('/oam/jobs'))
    <div class=" dashboard_grid_header  ">

        <h4 class="header-title m-t-0 m-b-0">Staff </h4>
    </div>
    <div class="card-box list_detail m-b-10">
        <div class="inbox-widget nicescroll" style="height: 320px;">
            @if(isset($staffData) && count($staffData)>0) @foreach($staffData as $row)
            <a>
                <div class="inbox-item list_desgin">
                    <!-- <div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg" class="img-circle" alt=""></div> -->
                    <p class="inbox-item-author job_description_title">
                        {{$row['name']}} @if($row['status'] == 0)
                        <span >
                            <i class="text-danger fa fa-circle" data-toggle="tooltip" title="Inactive" style="font-size: 10px;"></i>
                        </span>
                        @else
                        <span>
                            <i class="text-success fa fa-circle" data-toggle="tooltip" title="Active" style="font-size: 10px;"></i>
                        </span>
                        @endif
                    </p>
                    <p class="inbox-item-text job_description_detail">{{$row['staff_members']['user_roles']['name']}}</p>
                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia'); @endphp</p>

                </div>
            </a>
            @endforeach @else
            <div class="card-block page_box_shadow">
                <div class="col-lg-12">

                    <div class="text-xs-center">
                        <p class="text-muted">
                            You have not any staff member.
                        </p>
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
            @endif

            <div class="clearfix"></div>
        </div>
        @if(isset($staffData) && count($staffData)>0)
        <div class="pull-right" style="margin-top: 5px;">
            <a href="{{url('oam/staff/member/list')}}" class="  font-11" style="margin-left:5px;  ">
                <strong>View all</strong>
            </a>
        </div>
        @endif

    </div>

    @endif

</div>

<!-- <div class="col-xs-12 col-md-4">
    <div class=" dashboard_grid_header  ">
        <h4 class="header-title m-t-0 m-b-0">Pending Jobs list </h4>
    </div>
    <div class="card-box list_detail m-b-10">

        <div class="inbox-widget nicescroll" style="height:320px;">
            <div class="main_balances">
                <span class="dashboard_balance">
                    <strong>AED 49,707.57</strong>
                </span>
                <span class="outstanding_balances">Outstanding balances</span>
            </div>
                <a href="#">
                    <div class="inbox-item list_desgin">
                        <p class="inbox-item-author job_description_title">
                            Title area
                        </p>

                        <p class="inbox-item-text job_description_detail">Description area</p>

                        <p class="inbox-item-date text-success">21st June 2018</p>
                    </div>
                </a>
        </div>

            <div class="pull-right" style="margin-top: 5px;">
                    <a href="#" class="  font-11" style="margin-left:5px;  "><strong>View all</strong></a>
                </div>
    </div>

</div> -->
<!-- <div class="col-xs-12 col-lg-9 col-xl-9 padding-left-0 stat_boxes padding-right-0 ">
    <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
        <div class="card-box image_box widget-user m-b-0 text-xs-center">
            <div>
                <img src="{{asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image)}}" class="img-responsive img-circle"
                    alt="user">
                <div class="wid-u-info">
                    <h6 class="text-muted m-t-10 m-b-0 font-13">{{Auth::user()->name}}</h6>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
        <a href="{{url('oam/jobs')}}">
            <div class="card-box  m-b-0 bg_cards tilebox-two">
                <i class="icon-graph pull-xs-right text-muted"></i>
                <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Total Job Posted</h6>
                <h3 class="m-b-10">
                    <span data-plugin="counterup">{{isset($total_jobs)?$total_jobs:0}}</span>
                </h3>
            </div>
        </a>
    </div>
    <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
        <div class="card-box   m-b-0 bg_cards tilebox-two">
            <i class="icon-user pull-xs-right text-muted"></i>
            <h6 class="text-pink font-13 text-uppercase m-b-15 m-t-10">Companies Applied </h6>
            <h3 class="m-b-10" data-plugin="counterup">{{isset($job_apply)?$job_apply:0}}</h3>
        </div>
    </div>
    <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
        <a href="{{url('oam/staff/member/list')}}">
            <div class="card-box  m-b-0 bg_cards tilebox-two">
                <i class="icon-people pull-xs-right text-muted"></i>
                <h6 class="text-info font-13 text-uppercase m-b-15 m-t-10">Total Staff</h6>
                <h3 class="m-b-10">
                    <span data-plugin="counterup">{{isset($users)?$users:0}}</span>
                </h3>
            </div>
        </a>
    </div>
    <div class="col-xl-12 m-t-10">
        @if(GeneralFunctions::check_view_permission('/oam/jobs'))
        <div class="card_box_job_list m-b-20">

            <div class="card page_box_shadow">
                <div class="card-header sidebar_header dashboard_sidebar">

                    <div class="pull-left">
                        <h4 class="header-title job_listing_heading m-b-0"> Posted Jobs list </h4>
                    </div>
                    <div class="pull-right">
                        <a href="{{url('oam/jobs')}}" class="btn btn-sm btn-dark-outline waves-effect waves-light right show_datails" style="margin-left:5px; ">View All Jobs</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @if(isset($jobs) && count($jobs)>0)
                @foreach($jobs as $row)
                    @php $i = 0;

                        $is_assigned = \App\JobApply::where('job_id',$row->id)->where('status', 4)->get();
                        $is_assigned = count($is_assigned); @endphp @if($is_assigned <1)
                        <div class="card-block page_box_shadow">
                        <div class="col-lg-8">
                        <p class="lead m-b-0">
                            <a class="custom_links" href="{{url('oam/oam_job_details/'.$row->id)}}">{{$row->job_title}}</a>
                            <span class="text-muted font-13">{{$row->created_at}}</span>
                        </p>
                        <p class="m-b-0">{{ strip_tags(substr($row->description,0,180)) }}
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <div class="pull-right">
                            <ul class="list-inline m-b-0  text-xs-right">
                                <li style="display: inline;" class="lead text-xs-right font-13 text-danger m-b-0 list-inline-item manage_stats">
                                    @if(GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date) == 'Job Post Expired')
                                    {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date)
                                    !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                    $row->admin_approval_date) !!} @endif
                                </li>
                                <li class="lead text-xs-right font-13 text-primary m-b-0 list-inline-item">{!! GeneralFunctions::short_list($row->id) !!}</li>
                            </ul>
                            <ul class="list-inline m-b-0 text-xs-right">
                                <li class="lead text-xs-right font-13 text-primary m-b-0 list-inline-item manage_stats">
                                    Applied {{ $row->jobApply->count()}}
                                </li>
                                <li class="lead text-xs-right font-13 text-primary m-b-0 list-inline-item">Viewed {{ $row->count_views}} </li>
                            </ul>

                        </div>
                    </div>
                    <div class="clearfix"></div>
            </div>
            @php $i++; @endphp @endif @endforeach @endif @if($i
            < 1) <div class="card-block page_box_shadow">
                <div class="col-lg-12">

                    <div class="text-xs-center">
                        <p class="text-muted">
                            you have not listed any job please post a job.
                        </p>
                        <a href="{{url('oam/job_form')}}" class="btn btn-dark-outline waves-effect waves-light">Post a job</a>
                    </div>
                </div>

                <div class="clearfix"></div>
        </div>
        @endif


    </div>

</div>
@endif -->
</div>


</div>
<!-- <div class="col-xs-12 col-lg-3 col-xl-3">
    <div class="card-box m-b-10">
        @if(Auth::user()->profile_strength == "100%")
        <span class="text-success  m-t-10 pull-xs-right fill_prof" data-toggle="tooltip" title="Congratulations! Your profile is now completed">
            <i class="ion-checkmark-round"></i>&nbsp;Completed</span>
        @else
        <a href="{{url('oam/oam_profile')}}" class="btn btn-sm btn-primary-outline waves-effect waves-light pull-xs-right">Complete Profile</a>
        @endif
        <h6 class="text-dark m-t-10 m-b-10">Profile Status</h6>
        <p class="font-600 text-muted m-b-10">Set up your account
            <span class="text-primary pull-right">
                <b>{{isset($strength->profile_strength)?$strength->profile_strength:'0%'}}</b>
            </span>
        </p>
        <progress class="progress progress-striped progress-sm progress-primary m-b-0" value="{{str_replace('%','',$strength->profile_strength)}}"
            max="100"></progress>
    </div>
    <div class="card-box fixed_height">
        <h4 class="header-title m-t-0">Jobs</h4>
        <div class="p-20">
            <div id="pie-chart"></div>
        </div>
    </div>
</div> -->
<!-- <div class="col-xs-12 col-lg-12 col-xl-6">
            <div class="card-box fixed_height">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                <div class="p-20  m-b-10 bg_cards tilebox-two">
                    <i class="icon-rocket pull-xs-right text-muted"></i>
                    <h6 class="text-success text-uppercase m-b-15 m-t-10">Total Job Posted</h6>
                    <h2 class="m-b-10">
                        <span data-plugin="counterup">{{isset($total_jobs)?$total_jobs:0}}</span>
                    </h2>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                <div class="p-20  m-b-10 bg_cards tilebox-two">
                    <i class="icon-layers pull-xs-right text-muted"></i>
                    <h6 class="text-pink text-uppercase m-b-15 m-t-10">Applied Jobs</h6>
                    <h2 class="m-b-10" data-plugin="counterup">{{isset($job_apply)?$job_apply:0}}</h2>
                </div>
            </div>

            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                <div class="p-20  m-t-10 bg_cards tilebox-two">
                    <i class="icon-paypal pull-xs-right text-muted"></i>
                    <h6 class="text-info text-uppercase m-b-15 m-t-10">Total Users</h6>
                    <h2 class="m-b-10">
                        <span data-plugin="counterup">{{isset($users)?$users:0}}</span>
                    </h2>
                </div>
            </div>

        </div>
    </div>

    </div> -->
</div>
<!-- <div class="col-xs-12 col-lg-6 col-xl-6">
    <div class="card-box fixed_height">
        <div class="pull-left">
            <h4 class="header-title m-t-0 m-b-30">Staff</h4>
        </div>
        <div class="pull-right">
            <a href="{{url('/oam/staff/member/list')}}" class="btn btn-sm btn-dark-outline waves-effect waves-light">All Staff</a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered m-b-0">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Role Description</th>
                        <th>Status</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($staffData) && count($staffData)>0) @foreach($staffData as $key => $value)
                    <tr>
                        <th class="text-dark lead font-13">{{ucwords($value['name'])}}</th>
                        <td>{{ $value['staff_members']['user_roles']['name']}}</td>
                        <td>
                            @if($value['status'] == 1)
                            <span class="label label-success">Active</span>
                            @endif @if($value['status'] == 2)
                            <span class="label label-danger">Deactive</span>
                            @endif
                        </td>
                        <td>{{date_format(new DateTime($value['created_at']), 'jS F Y g:ia')}}</td>
                    </tr>
                    @endforeach @else
                    <tr>
                        <td colspan="4">
                            <div class="card-block">
                                <div class="col-lg-12">
                                    <div class="text-xs-center">
                                        <a href="{{url('oam/staff/form')}}" class="m-b-10 btn btn-primary-outline waves-effect waves-light">Add staff</a>
                                        <p class="text-muted">
                                            you have not added any staff member please add a staff.
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div> -->
<!-- end col-->
<!-- <div class="col-xs-12 col-lg-6 col-xl-6">
    @if(GeneralFunctions::check_view_permission('/oam/jobs'))
    <div class="card-box fixed_height">
        <div class="pull-left">
            <h4 class="header-title m-t-0 m-b-30">Approved Jobs</h4>
        </div>
        <div class="pull-right">
            <a href="{{url('/oam/jobs')}}" class="btn btn-sm btn-dark-outline waves-effect waves-light">
                All Jobs
            </a>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered m-b-0">
                <thead>
                    <tr>
                        <th>Jobs</th>
                        <th>Post Date</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($jobs) && !empty($jobs)) @foreach($jobs as $job)
                    <tr>
                        @if($job->admin_approval == 1)
                        <th>
                            <a class="text-dark lead   font-13" href="{{url('oam/oam_job_details/'.$job->id)}}"> {{$job->job_title}}</a>
                        </th>
                        <td>@php echo date_format(new DateTime($job->created_at), 'jS F Y g:ia'); @endphp</td>
                        <td>
                            <span class="label label-success">Approved</span>
                        </td>
                        @endif
                    </tr>
                    @endforeach @else
                    <h1>hello</h1>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    @endif
</div> -->
<!-- end col-->
<div class="col-xs-12 col-lg-6 col-xl-6 ">


    <!-- BEGIN MODAL -->
    <div class="modal fade none-border" id="event-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">Add New Event</h5>
                </div>
                <div class="modal-body p-20"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                    <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Add Category -->
    <div class="modal fade none-border" id="add-category">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h5 class="modal-title">Add a category</h5>
                </div>
                <div class="modal-body p-20">
                    <form role="form">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="control-label">Category Name</label>
                                <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Choose Category Color</label>
                                <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                    <option value="success">Success</option>
                                    <option value="danger">Danger</option>
                                    <option value="info">Info</option>
                                    <option value="pink">Pink</option>
                                    <option value="primary">Primary</option>
                                    <option value="warning">Warning</option>
                                    <option value="inverse">Inverse</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL -->
</div>
<!-- end col-12 -->

<!-- After login terms and condition popup-->
<div class="modal fade" id="login_term_condition">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Synergic360 Fair Usage Terms and conditions</h4>
            </div>
            <div class="col-sm-12">
                <div class="tc_msg_box alert" style="display: none;">
                    <ul style="text-decoration: none;" id="tc_msg_list">
                    </ul>
                </div>
            </div>
            <!-- Modal body -->
            <form class="user_term_condition_form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-body">
                    @php if(isset($user_tc->description)){ echo $user_tc->description; } else{ echo "";} @endphp
                    <div class="checkbox checkbox-primary">
                        <input id="agree_term_condition" type="checkbox" name="agree_term_condition">
                        <label for="agree_term_condition">Agree with terms & conditions</label>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary user_term_condition">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection @section('js')
<!-- Jquery-Ui -->
<script src="{{ URL::to('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- BEGIN PAGE SCRIPTS -->
<script src="{{ URL::to('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ URL::to('assets/plugins/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ URL::to('assets/pages/jquery.fullcalendar.js') }}"></script>

<script type="text/javascript" src="{{ URL::to('assets/plugins/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('assets/plugins/c3/c3.min.js') }}"></script>
<script>
    $(document).ready(function () {

        var synergic_rules = '{{Auth::user()->synergic_term_condition}}';

        if (synergic_rules === '0') {
            $('#login_term_condition').modal({
                backdrop: 'static',
                keyboard: false,
            });
            $('#login_term_condition').modal('show');
        }

        $(document).on('click', '.user_term_condition', function () {
            var params = "";
            if ($("#agree_term_condition").prop('checked') == true) {
                params = 1;
            } else {
                params = 0;
            }
            $('.tc_msg_box').hide();
            $('.tc_msg_box').removeClass("alert-success").removeClass("alert-danger");
            $.ajax({
                type: 'POST',
                url: "{{ generate_url('/submit_user_term_condition') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "agree_term_condition": params
                },
                success: function (data) {
                    $("#tc_msg_list").empty();

                    if (data.flag === 0) {
                        $('#tc_msg_list').append(data.response);
                        $('.tc_msg_box').addClass("alert-danger").show();
                    } else if (data.flag == 1) {
                        $('#tc_msg_list').append(data.response);
                        $('.tc_msg_box').addClass("alert-success").show();
                        setTimeout(function () {
                            $('#login_term_condition').modal('hide');
                        }, 2000);
                    }

                }
            });
        });

        $('[data-toggle="tooltip"]').tooltip();
    });
    $('#calendar').fullCalendar('option', 'height', 443);

    ! function ($) {
        "use strict";

        var ChartC3 = function () {};
        ChartC3.prototype.init = function () {

                //Pie Chart
                c3.generate({
                    bindto: '#pie-chart',
                    data: {
                        columns: [
                            ['Approved jobs', '{{isset($total_jobs)?$total_jobs:0}}'],
                            ['Pending Jobs', '{{isset($not_approved_jobs)?$not_approved_jobs:0}}'],
                        ],
                        type: 'pie'
                    },
                    color: {
                        pattern: ["#3db9dc", "#ff5d48"]
                    },
                    pie: {
                        label: {
                            show: false
                        }
                    }
                });

            },
            $.ChartC3 = new ChartC3, $.ChartC3.Constructor = ChartC3

    }(window.jQuery),

    //initializing
    function ($) {
        "use strict";
        $.ChartC3.init()
    }(window.jQuery);

    $(document).on('click', '.all_staff_btn', function () {
        window.location.href = '{{url("/oam/staff/member/list")}}';
    });
    $(document).on('click', '.job_rejected_modal', function () {

    });
</script> @endsection

@extends( Auth::user()->role_id == 2 ? 'oam.app' :  'fmc.app') @section('content')

@section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet">
<!--venobox lightbox-->
<link rel="stylesheet" href="{{ URL::to('assets/plugins/magnific-popup/css/magnific-popup.css') }}" />
<style>
.profile_img {
    border: 4px solid #822767;
    padding: 40px;
    font-size: 25px;
    font-weight: 700;
    color: #822767;
    border-radius: 50%;
    letter-spacing: 0px;
}
</style>
@endsection
@php
$insurance_type_array = [];
$policy_no_array = [];
$upload_certfication_array = [];

if(isset($company->id)){ $policy_no_array = ($company->policy_number != null) ? json_decode($company->policy_number
, true) : array();

$insurance_type_array = ($company->insurance_type != null) ? json_decode($company->insurance_type
, true) : array();
$upload_certfication_array = ($company->certification_path != null) ? json_decode($company->certification_path
, true) : array();}
$getSubscriptionAccountType = $company_details->subscription_account;
$getNoOfDays = GeneralFunctions::difference_bwt_two_dated($company_details->account_expiration_date,time());
$previous_sub_details = [];
if($company_details->previous_subscription_dates){
$previous_sub_details = json_decode($company_details->previous_subscription_dates, true);
}
$count = 1;
$save_company_details_url = Auth::user()->role_id == 3 ? 'fmc/company_details/update' : 'oam/company_details/update';
$contact_number = '';
$contact_alter = '';
if(isset($company->contact_no)){
$contact_number = explode('-',$company->contact_no);
if(is_array($contact_number)){
if(count($contact_number) > 1){
$contact_number = $contact_number[1];
}
else{
$contact_number = $contact_number[0];
}
}
}
if(isset($company->contact_alter)){
$contact_alter = explode('-',$company->contact_alter);
if(is_array($contact_alter)){
if(count($contact_alter) > 1){
$contact_alter = $contact_alter[1];
}
else{
$contact_alter = $contact_alter[0];
}
}
}
//dd($contact_number);
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <!-- <a href="#" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Staff List</a> -->
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>

@include('flash_message')
<div class="row">
    <div class="card-box bg_grey">
        <div class="col-md-9">
            <div class="p-10">
                <div class="col-md-10">
                    <div class="card-box fmc_bg_shadow">
                        <div class="fmc_company_detail">
                            <div class="wid-u-info description-font fmc_profile fmc_application_section">

                                <div class="col-md-12">
                                    <h4 class="m-t-0">
                                        {{$company_details->name}}
                                    </h4>
                                    <hr class="m-t-0 m-b-10">
                                </div>
                                <div class="col-md-6">

                                    <p class="text-muted m-b-0 font-13">
                                        <b class="text-dark font-13">Email:</b> {{$company_details->users->email}}</p>
                                        <p class="text-muted  font-13 ">
                                            <b class="text-dark font-13">Contact:</b> {{$company_details->contact_no}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="m-t-0">
                                                <p class="text-muted  font-13 ">
                                                    <b class="text-dark font-13">Licence Number:</b> {{$company_details->licence_number}}</p>
                                                    <p class="text-muted  font-13 ">
                                                        <b class="text-dark font-13">Licence Expiry:</b> {{$company_details->license_expiry}}</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    @if(Auth::user()->role_id == 3)
                                    <div class="card">
                                        <div class="card-header font-16 ">
                                            <b>Previous Subscription Dates Details</b>
                                        </div>
                                        @if(isset($previous_sub_details) && !empty($previous_sub_details))
                                        <table class="table table responsive">
                                            <tr>
                                                <th>Subscription Dates</th>
                                            </tr>
                                            @foreach($previous_sub_details as $key => $value)
                                            <tr>
                                                <td>{!! $count !!}) {!! date_format(new DateTime($value), 'jS F Y g:ia') !!}</td>
                                            </tr>
                                            @php
                                            $count++;
                                            @endphp
                                            @endforeach
                                        </table>
                                        @else
                                        <div class="card-block">
                                            <div class="col-md-12">
                                                <div class="text-xs-center">
                                                    <div class="p-20">
                                                        <p class="card-title lead">You have no reviews yet</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if(Auth::user()->role_id == 3)
                        <div class="col-sm-3 col-lg-3 col-xs-12">
                            <div class="card fmc_profile_sidebar fmc_bg_shadow page_box_shadow">
                                <h4 class="card-header font-16 sidebar_header m-b-0">
                                    <b>Details</b>
                                </h4>

                                <ul class="list-group list-group-flush  ">
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Subscription Account Type
                                        </p>
                                        <span>{!! $getSubscriptionAccountType == 1 ? '3 Months Subscription' : '1 Month Subscription' !!}</span>
                                    </li>
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Days Left For Account Expiration
                                        </p>
                                        <span><b>{!! $getNoOfDays !!}</b> No of Days Left</span>
                                    </li>
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Expiration Date
                                        </p>
                                        <span><b><u>{!! date_format(new DateTime($company_details->account_expiration_date), 'jS F Y g:ia') !!}</u></b></span>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row ">

                    <form action="{{url($save_company_details_url)}}" method="POST" enctype="multipart/form-data">
                        <div class="col-sm-12">
                            <div class="card-box add_staff ">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="m-t-0 m-b-10">Company Information</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <label class="form_label" for="userName">Company Name (required)
                                                    <span class="text-danger">*</span>
                                                </label>
                                                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                                <input name="name" placeholder="Name" class="form-control" type="text" value="{{isset(Auth::user()->name)?Auth::user()->name:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label class="form_label" for="primaryemail">Email (required)
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class=" form-group phone_input input-group">
                                                <span class="input-group-addon">
                                                    <i class="zmdi zmdi-email"></i>
                                                </span>

                                                <input class="form-control" placeholder="Email" type="email" name="email" value="{{isset(Auth::user()->email)?Auth::user()->email:''}}">
                                                <input class="form-control" type="hidden" name="email_hidden" value="{{isset(Auth::user()->email)?Auth::user()->email:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label class="form_label" for="primaryemail">Alternative Email (required)
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class=" form-group phone_input input-group">
                                                <span class="input-group-addon">
                                                    <i class="zmdi zmdi-email"></i>
                                                </span>

                                                <input class="form-control" placeholder="Email" type="email" name="email_alter" value="{{isset($company->email_alter)?$company->email_alter:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label class="form_label" for="primaryemail">No of Employees (required)
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class=" form-group">
                                                <input class="form-control" placeholder="No of Employees" type="number" name="no_of_emp" value="{{isset($company->no_of_emp)?$company->no_of_emp:''}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <label class="form_label" for="primaryemail">About Company
                                            </label>
                                            <div class=" form-group">
                                                <textarea class="form-control" name="about_company">{{isset($company->about_company)?$company->about_company:''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <b>
                                                <a href="#myModal" data-toggle="modal">Update Password</a>
                                            </b>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4 class="m-t-0 m-b-10">Company Logo</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div clss="col-sm-12">
                            <!-- <div class="left company_image" style="margin-right: 20px;">
                                        <div class="profile_img">FS</div>
                                    </div> -->

                                    <div class="col-sm-3">
                                        <div class="thumb m-t-0">
                                            @if(isset(Auth::user()->image) && Auth::user()->image != '' && file_exists(('laravel_code/storage/app/public/user_image/'.Auth::user()->image)))
                                            <a href="{{ asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image) }}" class="image-popup" title="Screenshot-1">
                                                <img height="100px" width="135px" src="{{ asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image) }}" class="img-thumbnail" alt="work-thumbnail">
                                            </a>
                                            @else
                                            <a href="{{ asset('laravel_code/storage/app/public/user_image/default_user.png') }}" class="image-popup" title="Screenshot-1">
                                                <img height="100px" width="135px" src="{{ asset('laravel_code/storage/app/public/user_image/default_user.png') }}" class="img-thumbnail" alt="work-thumbnail">
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div style="position:relative;">
                                    <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                        Choose File..
                                        <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        name="user_image" size="40" onchange='$("#upload-file-info").html($(this).val());'>
                                    </a>
                                    &nbsp;
                                    <span class='label label-info' id="upload-file-info"></span>
                                </div>
                                <div class="m-t-10">
                                    <p class="text-muted font-13 m-t-0"> Upload a PNG or JPEG file to add a personal touch to your details.</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <label class="form_label" for="primaryemail">Company Website
                                    </label>
                                    <div class=" form-group">
                                        <input class="form-control" placeholder="www.company.com" type="text" name="company_website" value="{{isset($company->company_website)?$company->company_website:''}}">
                                        <input class="form-control" placeholder="www.company.com" type="hidden" name="company_website_hidden" value="{{isset($company->company_website)?$company->company_website:''}}">
                                    </div>
                                </div>
                            </div>

                            <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-default" id="showInformation">
                                   <h4 class="modal-title" id="myLargeModalLabel">More Information</h4>
                               </a>
                           </span>
                       </div>
                       <div class="clearfix"></div>
                   </div>




                   <div class="card-box add_staff ">

                    <div class="card-box add_staff ">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Buisnes Insurance Details</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>
                        
                        @if(count($policy_no_array) > 0 || count($insurance_type_array) > 0 || count($upload_certfication_array) > 0  ) 
                        

                        @foreach($policy_no_array as $in_key => $in_value)
                        @if($in_key == 0)
                        <div class="row buisness_ins_div">
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Insurance Type
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <select class="form-control" name="insurance_type[]">
                                        <option value="">Select Option</option>
                                        @if(isset($insurance_type_array[$in_key]))
                                            @foreach($type_of_insurance as $key => $value)
                                            <option value="{{$value['id']}}" {!! isset($insurance_type_array) ? ($insurance_type_array[$in_key] == $value['id'] ? 'selected' : '') : '' !!}>{{$value['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Policy No
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="Name" value="{{$policy_no_array[$in_key]}}" name="policy_no[]" 
                                    class="form-control">
                                    {{ csrf_field() }}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Upload Certification
                                </label>
                                <div class="m-t-10">
                                    <div style="position:relative;">
                                        <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                            Choose File..
                                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                            name="upload_certfication[]" size="40" onchange='$("#upload-file-info-cert").html($(this).val());'>

                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info-cert"></span>
                                    </div>
                                    <div class="m-t-10">
                                        <p class="text-muted font-13 m-t-0">Upload Certification copy in PDF, PNG or JPEG format max size 2MB</p>
                                    </div>
                                </div>

                                @if(isset($upload_certfication_array[$in_key]))
                                    @if($upload_certfication_array[$in_key] != '' && file_exists(('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key])))


                                    <div class="img-thumbnail m-t-0">
                                        <a href="{{ asset('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key]) }}" class="image-popup"
                                        title="Screenshot-1" download>
                                        <img height="100px" width="100px" src="{{ asset('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key]) }}" class="" alt="work-thumbnail">
                                         </a>
                                    </div>
                                    @else
                                    <b class="text-danger">No Crtification</b>
                                    @endif
                            @endif
                            </div>
                            <div class="col-sm-3 padding-left-0 padding-right-0">
                                <label for="inputEmail3" class="col-sm-12 form-control-label">
                                </label>

                                <div class="col-sm-12">
                                    <a  class="custom_btn btn btn-secondary waves-effect m-t-20 add_buisness_detaial">
                                        <span class="btn-label">
                                            <i class="fa fa-plus"></i>
                                        </span>Add More</a>

                                </div>
                            </div>
                        </div>
                        @else
                        <div class="row buisness_ins_div">
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Insurance Type
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <select class="form-control" name="insurance_type[]">
                                        <option value="">Select Option</option>
                                        @if(isset($insurance_type_array[$in_key]))
                                            @foreach($type_of_insurance as $key => $value)
                                            <option value="{{$value['id']}}" {!! isset($insurance_type_array) ? ($insurance_type_array[$in_key] == $value['id'] ? 'selected' : '') : '' !!}>{{$value['name']}}</option>
                                            @endforeach
                                        @else
                                            @foreach($type_of_insurance as $key => $value)
                                            <option value="{{$value['id']}}">{{$value['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Policy No
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="Policy Number" name="policy_no[]" value="{{$policy_no_array[$in_key]}}"
                                    class="form-control">
                                    {{ csrf_field() }}
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label class="form_label" for="primaryemail">Upload Certification
                                </label>
                                <div class="m-t-10">
                                   
                                    <div style="position:relative;">
                                        <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                            Choose File..
                                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                            name="upload_certfication[]" size="40" onchange='$("#upload-file-info-cert").html($(this).val());'>

                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info-cert"></span>
                                    </div>
                                    <div class="m-t-10">
                                        <p class="text-muted font-13 m-t-0">Upload Certification copy in PDF, PNG or JPEG format max size 2MB</p>
                                    </div>
                                </div>
                                @if(isset($upload_certfication_array[$in_key]))
                                    @if($upload_certfication_array[$in_key] != '' && file_exists(('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key])))


                                    <div class="img-thumbnail m-t-0">
                                        <a href="{{ asset('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key]) }}" class="image-popup"
                                        title="Screenshot-1" download>
                                        <img height="100px" width="100px" src="{{ asset('laravel_code/storage/app/public/company_image/'.$upload_certfication_array[$in_key]) }}" class="" alt="work-thumbnail">
                                         </a>
                                    </div>
                                    @else
                                    <b class="text-danger">No Crtification</b>
                                    @endif
                            @endif
                        </div>
                        <div class="col-sm-3 padding-left-0 padding-right-0">
                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                            </label>
                            <div class="col-sm-12">
                             <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_buisness_ins">
                                <span class="btn-label">
                                    <i class="fa fa-trash"></i>
                                </span>Clear</a>
                            </div>
                        </div>
                    </div>
                    @endif @endforeach @else
                    <div class="row buisness_ins_div">
                        <div class="col-sm-3">
                            <label class="form_label" for="primaryemail">Insurance Type
                            </label>
                            <div class=" form-group phone_input input-group">
                                <select class="form-control" name="insurance_type[]">
                                    <option value="">Select Option</option>
                                    @foreach($type_of_insurance as $key => $value)
                                        <option value="{{$value['id']}}">{{$value['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="form_label" for="primaryemail">Policy No
                            </label>
                            <div class=" form-group phone_input input-group">
                                <input type="text" parsley-type="text" placeholder="Name" name="policy_no[]" 
                                class="form-control">
                                {{ csrf_field() }}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <label class="form_label" for="primaryemail">Upload Certification
                            </label>
                            <div class="m-t-10">
                                <div style="position:relative;">
                                    <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                        Choose File..
                                        <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        name="upload_certfication[]" size="40" onchange='$("#upload-file-info-cert").html($(this).val());'>
                                    </a>
                                    &nbsp;
                                    <span class='label label-info' id="upload-file-info-cert"></span>
                                </div>
                                <div class="m-t-10">
                                    <p class="text-muted font-13 m-t-0">Upload Certification copy in PDF, PNG or JPEG format max size 2MB</p>
                                </div>
                            </div>

                            @if(isset($company->certification_path) && $company->certification_path != '' && file_exists(('laravel_code/storage/app/public/company_image/'.$company->certification_path)))
                            <div class="img-thumbnail m-t-0">
                                <a href="{{ asset('laravel_code/storage/app/public/company_image/'.$company->certification_path) }}" class="image-popup"
                                    title="Screenshot-1" download>
                                    <img height="100px" width="100px" src="{{ asset('laravel_code/storage/app/public/company_image/'.$company->certification_path) }}" class="" alt="work-thumbnail">
                                </a>
                            </div>
                            @else
                            <b class="text-danger">No Crtification</b>
                            @endif
                        </div>
                        <div class="col-sm-3 padding-left-0 padding-right-0">
                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                            </label>

                            <div class="col-sm-12">
                                <a  class="custom_btn btn btn-secondary waves-effect m-t-20 add_buisness_detaial">
                                    <span class="btn-label">
                                        <i class="fa fa-plus"></i>
                                    </span>Add More</a>

                            </div>
                        </div>
                    </div>
                  
                        @endif


                        <div class="clearfix"></div>
                    </div>
                    <div class="card-box add_staff ">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Value Added Tax (VAT)</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">TRN number
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="number" parsley-type="text" placeholder="xxxxxxxxx" name="trn_number" value="{{old('trn_number')}}{{isset($company->trn_number)?$company->trn_number:''}}"
                                    class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="card-box add_staff ">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Bank Details</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Bank Name
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="Bank Name" name="bank_name" value="{{old('bank_name')}}{{isset($company->bank_name)?$company->bank_name:''}}"
                                    class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Account Title
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="Account Title" name="bank_account_title" value="{{old('bank_account_title')}}{{isset($company->bank_account_title)?$company->bank_account_title:''}}"
                                    class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Account No.
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="xxxxxxxxxxxxxx" name="bank_account_no" value="{{old('bank_account_no')}}{{isset($company->bank_account_no)?$company->bank_account_no:''}}"
                                    class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Branch / Address
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <textarea  placeholder="Branch Address" name="bank_branch_address"
                                    class="form-control">{{old('bank_branch_address')}}{{isset($company->bank_branch_address)?$company->bank_branch_address:''}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">IBAN No.
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="xxxxxxxxxxxxxx" name="bank_iban_no" value="{{old('bank_iban_no')}}{{isset($company->bank_iban_no)?$company->bank_iban_no:''}}"
                                    class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Swift Code.
                                </label>
                                <div class=" form-group phone_input input-group">
                                    <input type="text" parsley-type="text" placeholder="xxxxxxxxxxxxxx" name="bank_swift_code" value="{{old('bank_swift_code')}}{{isset($company->bank_swift_code)?$company->bank_swift_code:''}}"
                                    class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if(Auth::user()->role_id == 3)
                    <div class="card-box add_staff ">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Buisness Activities</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>

                        @foreach($new_categories as $key => $value)
                        <div class="row">
                            <div class="col-md-12 categories_chkboxes">
                                <div class="checkbox checkbox-primary">
                                    <input name="category[{{$value['id']}}]" data-id="{!! 'sub_category_section_'.$value['id'] !!}" class="category_name" {!! in_array($value['id'], $parentCategory) ? 'checked' : '' !!} type="checkbox">
                                    <label for="checkbox2">
                                        {{$value['name']}}
                                    </label>
                                </div>
                                <div class="{!! 'sub_category_section_'.$value['id'] !!}" {!! in_array($value['id'], $parentCategory) ? '' : 'style="display:none;"' !!}>
                                @foreach($value['sub_categories'] as $key => $sub_category_values)
                                <div class="col-md-3">
                                    <input class="{!! 'sub_category_name' !!}" data-id="{!! 'category_name_'.$value['id'] !!}" type="checkbox" name="category[{{$value['id']}}][sub_category][{{$sub_category_values['id']}}]" {!! in_array($sub_category_values['id'], $sub_category) ? 'checked' : '' !!}>
                                    <label for="checkbox2">
                                        <b>{{$sub_category_values['sub_category']}}</b>
                                    </label>
                                    <ul class="list-unstyled">
                                        @foreach($sub_category_values['job_categories'] as $key => $job_category_values)
                                        <li>
                                            <div class="checkbox checkbox-primary">
                                                <input class="service_name"  type="checkbox" name="category[{{$value['id']}}][sub_category][{{$sub_category_values['id']}}][service_name][{{$job_category_values['id']}}]" {!! in_array($job_category_values['id'], $serviceName) ? 'checked' : '' !!}>
                                                <label for="checkbox2">
                                                    {{$job_category_values['service_name']}}
                                                </label>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endforeach
                </div>
                @endif
            </div>
            <div class="col-sm-12">
                <button type="submit" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</button>
            </div>


                   <!-- /.modal-dialog -->  <!-- /.modal-dialog -->  <!-- /.modal-dialog -->
<div class="modal fade show_information in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">More Information</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" id="error-msg-list">

                    </ul>
                </div>

                                <!-- Terms and condition model -->
                <div class="card-box add_staff ">
                    <div class="modal fade" id="myModal1">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>

                            </div>
                        </div>
                    </div>
                    @if($license_expiry_left != null)
                        @if($license_expiry_left < 7 && $license_expiry_left > 0)
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <p>Trade License information is getting expired kindly update and upload new information</p>
                                 </div>
                            </div>
                        @endif
                    @if($license_expiry_left <= 0)
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <p>Trade License information is expired kindly update and upload new information</p>
                        </div>
                    </div>
                    @endif
                    @endif
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="form_label" for="companyName">Phone (Required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class="phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-smartphone"></i>
                                </span>
                                <span class="input-group-addon" style="font-size: 14px;">
                                +971
                                </span>
                                <input class="form-control" placeholder="+971-506401812" type="text" name="contact_no" 
                                            value="{{$contact_number}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="form_label" for="companyName">Office phone (Required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class=" phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-smartphone"></i>
                                </span>
                                <span class="input-group-addon" style="font-size: 14px;">
                                    +971
                                </span>
                                <input class="form-control" placeholder="+971-506401812" type="text" name="contact_alter" value="{{$contact_alter}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="form_label" for="primaryemail">Billing Address (required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class=" form-group phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-email"></i>
                                </span>
                                <input class="form-control" placeholder="Billing Address" type="text" name="billing_address" value="{{isset($company->billing_address)?$company->billing_address:''}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="form_label" for="primaryemail">Office Address (required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class=" form-group phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-email"></i>
                                </span>

                                <input class="form-control" placeholder="Office Address" type="text" name="office_address" value="{{isset($company->office_address)?$company->office_address:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="form_label" for="primaryemail">Trade License Number (required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class=" form-group phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-book"></i>
                                </span>
                                <input class="form-control" placeholder="License Number" type="text" name="licence_number" value="{{isset($company->licence_number)?$company->licence_number:''}}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="form_label" for="primaryemail">Trade License Expiry (required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class=" form-group phone_input input-group">
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-calendar-note"></i>
                                </span>

                                <input type="text" class="form-control" placeholder="mm/dd/yyyy" name="license_expiry" id="datepicker-autoclose" value="{{isset($company->license_expiry)?$company->license_expiry:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="form_label" for="primaryemail">Upload Trade licence (required)
                                <span class="text-danger">*</span>
                            </label>
                            <div class="m-t-10">
                                <div style="position:relative;">
                                    <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                        Choose File..
                                        <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        name="licence_image" size="40" onchange='$("#upload-file-info-trade").html($(this).val());'>
                                    </a>
                                    &nbsp;
                                    <span class='label label-info' id="upload-file-info-trade"></span>
                                </div>
                                <div class="m-t-10">
                                    <p class="text-muted font-13 m-t-0">Upload Trade License copy in PDF, PNG or JPEG format max size 2MB</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            @if(isset($company->license) && $company->license != '' && file_exists(('laravel_code/storage/app/public/company_image/'.$company->license)))
                            <div class="img-thumbnail m-t-0">
                                <a href="{{ asset('laravel_code/storage/app/public/company_image/'.$company->license) }}" class="image-popup"
                                    title="Screenshot-1">
                                    <img height="100px" width="100px" src="{{ asset('laravel_code/storage/app/public/company_image/'.$company->license) }}" class="" alt="work-thumbnail">
                                </a>
                            </div>
                            @else
                            <b class="text-danger">No trade license image</b>
                            @endif
                        </div>
                        <div class="col-sm-12" style="text-align: center">
                            <button href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</button>
                        </div>
                    </div>
                </div>


             </div>
                        <!-- End terms and condition model -->

                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
        </form>

        <!-- Copy of Buisness Insurance -->
        <div class="row buisness_ins_div_copy" style="display: none;">
            <div class="col-sm-3">
                <label class="form_label" for="primaryemail">Insurance Type
                </label>
                <div class=" form-group phone_input input-group">
                    <select class="form-control" name="insurance_type[]">
                        <option value="">Select Option</option>
                        @foreach($type_of_insurance as $key => $value)
                        <option value="{{$value['id']}}">{{$value['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <label class="form_label" for="primaryemail">Policy No
                </label>
                <div class=" form-group phone_input input-group">
                    <input type="text" parsley-type="text" placeholder="Name" name="policy_no[]" value="{{old('policy_number')}}"
                    class="form-control">
                    {{ csrf_field() }}
                </div>
            </div>
            <div class="col-sm-3">
                <label class="form_label" for="primaryemail">Upload Certification
                </label>
                <div class="m-t-10">
                    <div style="position:relative;">
                        <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                            Choose File..
                            <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                            name="upload_certfication[]" size="40" onchange='$("#upload-file-info-cert").html($(this).val());'>
                        </a>
                        &nbsp;
                        <span class='label label-info' id="upload-file-info-cert"></span>
                    </div>
                    <div class="m-t-10">
                        <p class="text-muted font-13 m-t-0">Upload Certification copy in PDF, PNG or JPEG format max size 2MB</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 padding-left-0 padding-right-0">
                <label for="inputEmail3" class="col-sm-12 form-control-label">
                </label>
                <div class="col-sm-12">
                 <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_buisness_ins">
                    <span class="btn-label">
                        <i class="fa fa-trash"></i>
                    </span>Clear</a>
                </div>
            </div>
        </div>

        <!-- End of Buisness Insurance -->
        <!-- The Modal -->
        <div class="modal  bs-example-modal-lg" id="myModal">
            <div class="modal-dialog  modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Update Password</h4>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <form method="POST" id="oam_password">
                            <div class="row">

                                <div class="msg-box alert" style="display: none;">
                                    <ul style="text-decoration: none;" id="msg-list">

                                    </ul>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="form_label" for="userName">Old Password (required)
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                        <input name="old_password" id="old_password" placeholder="Old Password" class="form-control" type="password">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="form_label" for="userName">New Password (required)
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input name="new_password" id="new_password" placeholder="New Password" class="form-control" type="password">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="form_label" for="userName">Confirm New Password (required)
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" class="form-control" type="password">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <a href="javascript:void(0)" class="btn btn-success" id="save_oam_password">Save</a>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('js')
    <script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
    <!-- Magnific popup -->
    <script type="text/javascript" src="{{ URL::to('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            @if(Auth::user()->role_id == 3)
            var update_password_url = '{{ generate_url("fmc/update_user_password") }}';
            @else
            var update_password_url = '{{ generate_url("oam/update_user_password") }}';
            @endif

            $("#save_oam_password").click(function () {
                var params = $("#oam_password").serialize();
                $.ajax({
                    type: 'POST',
                    url: update_password_url,
                    data: params,
                    success: function (data) {
                        $("#msg-list").empty();
                        if (data.status == 'success') {
                            $('#msg-list').append('<li>Record has been saved successfully</li>')
                            $('.msg-box').addClass("alert-success").show();
                        }
                        else {
                            var errorArray = data.message;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });
                            $('#msg-list').append(list);
                            $('.msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });

            $('.image-popup').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                mainClass: 'mfp-fade',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
            $('#datepicker-autoclose').datepicker({
                autoclose: true,
            });

            $(document).on('click', '.category_name', function(){
                var sub_category_section = $(this).attr('data-id');
                if($(this).prop("checked") == true){
                    $('.'+sub_category_section).show();
                }
                else{
                    $('.'+sub_category_section).hide();
                    $(this).parent().find('input:checkbox:first').removeAttr('checked');
                    $(this).siblings('ul')
                    .find("input[type='checkbox']")
                    .removeAttr('checked');
                    $(this).parent().next('.'+sub_category_section).find('input:checkbox:first').removeAttr('checked');
                    $(this).parent().next('.'+sub_category_section).find('input:checkbox:first').siblings('ul').find("input[type='checkbox']").removeAttr('checked');
                }
            });

            $(document).on('click', '.sub_category_name', function(){
             if($(this).prop("checked") == true){
                $(this).siblings('ul').find("input[type='checkbox']").prop('checked', 'checked');
            }else{
                $(this).siblings('ul').find("input[type='checkbox']").removeAttr('checked');
            }
        });

            $(document).on('click', '.service_name', function(){
                if($(this).prop("checked") == true){
                 $(this).parent().closest('ul').siblings('input:checkbox:first').prop('checked', 'checked');
             }
         });

            $(".add_buisness_detaial").click(function () {
                var maxGroup = 10;

                if ($('body').find('.buisness_ins_div').length < maxGroup) {
                    var fieldHTML = '<div class="row buisness_ins_div">' + $(".buisness_ins_div_copy").html() + '</div>';
                    $('body').find('.buisness_ins_div:last').after(fieldHTML);
                } else {
                    alert('Maximum ' + maxGroup + ' Insurance are allowed.');
                }
            });

            $("body").on("click", ".remove_buisness_ins", function () {
                $(this).parents(".buisness_ins_div").remove();
            }); 
        });
$(document).on('click', '#showInformation', function () {
    $('.show_information').modal('show');
});
</script> @endsection

<!-- end col-->


@endsection

@extends('layouts.app') @section('content')

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="" type="button" class="btn btn-dark-outline  waves-effect waves-light">Go Back</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <form role="form" data-parsley-validate novalidate method="POST" action="{{url('save_termCondition')}}">
        {{ csrf_field() }}
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="card-box">
                <!-- <div class="col-md-2"></div> -->
                <div class="col-md-12">
                    <div class="p-10">
                        <h4 class="header-title m-t-0 m-b-15">Term and Conditions</h4>
                        <div class="form-group row">
                            <div class="col-sm-6">
                            <textarea id="edit" class="form-control" name="description" placeholder="Terms And Conditions"></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-2"></div> -->
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group row">
                <div class="col-sm-8">
                    <button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                        Save
                    </button>
                    <button type="reset" class="btn btn-danger waves-effect m-l-5">
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    </form>
    @endsection
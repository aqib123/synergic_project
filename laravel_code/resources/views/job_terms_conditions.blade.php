@extends('layouts.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="  pull-right m-t-15">
            <!-- <a href="{{url('/oam/terms_and_conditions')}}" type="button" class="btn btn-dark-outline waves-effect waves-light">Add Term & Conditions</a> -->
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Posted By</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($t_c) && count($t_c) > 0) @foreach($t_c as $tc)
                    <tr>
                        <td>{{$tc['id']}}</td>
                        <td>{{ucwords($tc['title'])}}</td>
                        <td>
                            <?= substr($tc['description'],0,200) ?>
                        </td>
                        <td>
                            @if($tc['status'] == 1)
                            <span class="label label-success">Active</span>
                            @else
                            <span class="label label-danger">InActive</span>
                            @endif
                        </td>
                        <td>{{$tc['posted_by']}}</td>
                        <td>
                            <?php echo date_format(new DateTime($tc['created_at']), 'jS F Y g:ia');?>
                        </td>
                        <td>

                            <div class="btn-group user_action_btn">
                                <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{url('/settings?status=3&id='.$tc['id'])}}" title="Edit">
                                        <i class="fa fa-edit text-info"></i> Edit
                                    </a>
                                    <a class="dropdown-item delete_terms_cond_btn" href="javascript:void(0)" id="{{$tc['id']}}" title="Delete">
                                        <i class="fa fa-trash text-danger"></i> Delete
                                    </a>
                                    @if($tc['status'] == 0)
                                    <a class="dropdown-item" href="{{url('/change_job_tc_status?status=1&id='.$tc['id'])}}" title="Active">
                                        <i class="fa fa-check text-info"></i> Active
                                    </a>
                                    @else
                                    <a class="dropdown-item" href="{{url('/change_job_tc_status?status=0&id='.$tc['id'])}}" title="Inactive">
                                        <i class="fa fa-check text-info"></i> Inactive
                                    </a>
                                    @endif
                                </div>
                            </div>

                        </td>
                    </tr>
                    @endforeach @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->

<!-- Modal Box for Deletion confirmation -->
<div class="modal fade md_delete_confirmation" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Box for Set to Default confirmation -->
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this term and condition? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm confirm_delete_tc">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<script src="{{ URL::asset('assets/tinymce/js/tinymce/tinymce.min.js') }} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        
        $(".delete_terms_cond_btn").click(function () {
            var uuid = $(this).attr('id');
            $('#user_uuid').val(uuid);
            $('#delete_confirm').modal('show');
        });

        $(".confirm_delete_tc").click(function () {
            var id = $('#user_uuid').val();
            var url = '{{url("delete_job_terms_conditions/")}}/'+id;
            window.location.href = url;
        });

    });
</script>
<script>
</script> 
@endsection

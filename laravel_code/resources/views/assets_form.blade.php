@extends( Auth::user()->role_id == 1 ? 'layouts.app' : 'oam.app' ) @section('content')
<!-- Page-Title -->
<style type="text/css">
.form-check-inline input[type="radio"]{
    margin-right: 10px !important;
}
</style>

<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-6 col-xs-12 col-md-6">
        <div class="card-box">
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="1">Manual Data Entery
              </label>
            </div>
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="2">Bulk Import
              </label>
            </div>
        </div>
    </div>
</div>
<div class="row bulk_importing" style="display: none;">
    <div class="col-sm-6 col-xs-12 col-md-6">
        <div class="card-box">
            <form enctype="multipart/form-data" role="form" data-parsley-validate novalidate method="POST" action="{{ url('bulk/import/assets') }}">
                <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }} row">
                    <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                    <div class="col-md-6">
                        <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-4">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="header"> File contains header row?
                            {{ csrf_field() }}
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            Parse CSV
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row manual_adding" style="display: none;">
    <form role="form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('save_assets') }}">
        {{ csrf_field() }}
        <div class="col-sm-12 col-xs-12 col-md-4">
            <div class="card-box">
                <div class="form-group row">
                    <div class="col-sm-12 padding-left-0 padding-right-0">
                        <div class="col-sm-12">
                            <h5 class="m-t-0 m-b-15">Asset Name</h5>
                        </div>
                        <div class="col-sm-12">
                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                            <input type="text" class="form-control" placeholder="Asset Name" name="assets" value="{{old('name')}}{{isset($assets->name)?$assets->name:''}}">
                        </div>
                    </div>
                    <!-- <div class="col-sm-6 padding-left-0 padding-right-0">
                        <div class="col-sm-12">
                            <h5 class="m-t-0 m-b-15">Asset Code</h5>
                        </div>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" placeholder="Asset Code" name="assets_code" value="{{old('assets_code')}}{{isset($assets->assets_code)?$assets->assets_code:''}}">
                        </div>
                    </div> -->
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
       @if(GeneralFunctions::check_add_permission('/assets_form'))
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group row">
                <div class="col-sm-8">
                    <button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                        Save
                    </button>
                </div>
            </div>
        </div>
        @endif
</div>
</form>
@if(isset($csv_data))
    <div class="col-sm-12 col-xs-12 col-md-12">
        <div class="card-box">
            <form class="form-horizontal" method="POST" action="{{ url('import_process') }}">
                {{ csrf_field() }}
                    <table class="table">
                        @foreach ($csv_data as $row)
                            <tr>
                            @foreach ($row as $key => $value)
                                <td>{{ $value }}</td>
                            @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            @foreach ($csv_data[0] as $key => $value)
                                <td>
                                    <select name="fields[{{ $key }}]">
                                        @foreach (config('app.db_fields') as $db_field)
                                            <option value="{{ $loop->index }}">{{ $db_field }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            @endforeach
                        </tr>
                    </table>
                <button type="submit" class="btn btn-primary">
                    Import Data
                </button>
            </form>
        </div>
    </div> 
@endif   

<div class="modal fade md_category_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Category</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_category_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="md_category_name">Category Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control md_category_name" id="md_category_name" name="md_category_name" placeholder="Enter Catgory Name">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_category_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Box for Adding Job Sub Category -->
<div class="modal fade s_category_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Sub Category</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_sub_category_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name"> Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_category_name"></p>
                          <input type="hidden" name="category_uuid" id="category_uuid" class="category_uuid">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Sub Category Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control s_sub_category_name" id="s_sub_category_name" name="s_sub_category_name" placeholder="Enter Sub Catgory Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_sub_category_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Box for Adding Service Code -->
<div class="modal fade s_service_code_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Service Code</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_service_code_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_category_name"></p>
                          <input type="hidden" name="service_category_uuid" id="service_category_uuid" class="service_category_uuid">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Sub Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_sub_category_name"></p>
                          <input type="hidden" name="service_ub_category_uuid" id="service_ub_category_uuid" class="service_ub_category_uuid">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Service Code</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control s_service_code" id="s_service_code" name="s_service_code" placeholder="Enter Service Code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for=""></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_service_code_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            @if(Request::input('id'))
                var req_id = "{{Request::input('id')}}";
            @else
                var req_id = null;
            @endif
            function getSubcategories(){
                var category_option = $('#parent_category').val();
                $.ajax({
                    type: 'GET',
                    url: "{{ generate_url('/sub/category') }}",
                    data: {'category_uuid' :category_option,
                            'req_id' : req_id},
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.sub_category_class').html(data.result);
                        }
                    }
                });
            }
            function getServiceCodes(){
                var category_option = $('#parent_category').val();
                var sub_category_option = $('.sub_category_class').val();
                console.log('comming here');
                $.ajax({
                    type: 'GET',
                    url: "{{ generate_url('/service_code/category') }}",
                    data: {'category_uuid' :category_option,
                            'sub_category_uuid' :sub_category_option,
                            'req_id' : req_id},
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.service_code_class').html(data.result);
                        }
                    }
                });
            }
            $(document).on('change','#parent_category', function(){
                getSubcategories();
            });

            $(document).on('change','.sub_category_class', function(){
                getServiceCodes();
            });

            $(document).on('click','.add_category_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                $('.md_category_details').modal('show');
            });

            $(document).on('click','.add_category_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_category_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/category/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('#parent_category').html(data.result);
                            $('.md_category_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });

            $(document).on('click','.add_sub_category_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var category_option = $('#parent_category').val();
                if(category_option == ''){
                    alert('Please Select the Category first.');
                }
                else{
                    $('.selected_category_name').text($("#parent_category option:selected").text());
                    $('#category_uuid').val(category_option);
                    $('.s_category_details').modal('show');
                }
            });

            $(document).on('click','.add_sub_category_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_sub_category_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/sub/category/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('.sub_category_class').html(data.result);
                            $('.s_category_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });
            getSubcategories();
            setTimeout(function() {
                getServiceCodes();
            }, 1000);
            // New Implementation for Job Category
            $(document).on('click','.add_service_code_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var category_option = $('#parent_category').val();
                var sub_category_option = $('.sub_category_class').val();

                if(category_option == '' || sub_category_option == ''){
                    alert('Please Select the Category first And Sub Category First');
                }
                else{
                    $('.selected_category_name').text($("#parent_category option:selected").text());
                    $('.selected_sub_category_name').text($(".sub_category_class option:selected").text());
                    $('.service_category_uuid').val(category_option);
                    $('.service_ub_category_uuid').val(sub_category_option);
                    $('.s_service_code_details').modal('show');
                }
            });
            $(document).on('click','.add_service_code_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_service_code_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/service_code/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('.service_code_class').html(data.result);
                            $('.s_service_code_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });

            $(document).on('click', '.data_entry_type', function(){
                if($(this).val() == 1){
                    $('.bulk_importing').hide();
                    $('.manual_adding').show();
                }else{
                    $('.bulk_importing').show();
                    $('.manual_adding').hide();
                }
            });
        });
    </script>
@endsection

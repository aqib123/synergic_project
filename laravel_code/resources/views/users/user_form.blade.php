@extends('layouts.app') 
@section('content')
@section('css')
<link href="{{ URL::to('assets/plugins/multiselect/css/multi-select.css') }}"  rel="stylesheet" type="text/css" />
<link href="{{ URL::to('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<!--Date Picker CSS-->
<link href="{{ URL::to('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
<!--venobox lightbox-->
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/magnific-popup/css/magnific-popup.css') }}"/>
<style>
    .profile_img{
        border: 4px solid #822767;
        padding: 25px;
        font-size: 25px;
        font-weight: 700;
        color:#822767;
        border-radius: 50%;
        letter-spacing: 0px;
    }
</style>
@stop
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('users?status=OAM')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back to Companies</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<form role="form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('user_save') }}">
    <div class="row">
        <div class="col-sm-6 col-xs-12 col-md-6">
            <div class="card-box">
                <div class="p-10">
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <h4 class="header-title">Personal Information</h4><hr>
                        </div>
                    </div>
                    @include('flash_message') {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Name
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" required parsley-type="text" placeholder="Name" name="name" value="{{old('name')}}{{isset($user->name)?$user->name:''}}"
                                class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Email
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="email" required parsley-type="email" name="email" value="{{old('email')}}{{isset($user->email)?$user->email:''}}"
                                class="form-control" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Password
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <div>
                                <input type="password" required parsley-type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Roles
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <fieldset class="">
                                <select class="form-control" name="role_id">
                                    <option value=''>Select Role</option>
                                    @if(isset($roles) && count($roles) > 0) @foreach($roles as $row)
                                    <option @if(isset($user->role_id) && $user->role_id == $row->id) selected="selected" @endif value="{{$row->id}}">
                                        {{$row->name}} </option>
                                    @endforeach @endif
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <!-- Copmany detail for edit starts from here -->
                    
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Profile Image
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="file" name="user_image" class="new-license"> @if(isset($user->image) && !empty($user->image))
                            <a href="{{ asset('laravel_code/storage/app/public/user_image/' . $user->image) }}" class="image-popup" title="{{ $company->license }}">
                            
                            <img class="img-thumbnail thumb-img" width="150" height="150" src="{{ asset('laravel_code/storage/app/public/user_image/' . $user->image) }}"
                                alt="{{$user->image}}" />
</a>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($user) && $user != '')
        @if($user->role_id == 2 || $user->role_id == 3)
        <div class="col-sm-6 col-xs-12 col-md-6">
            <div class="card-box">
                <div class="p-10">
                    <div class="company-detail">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <h4 class="header-title">Company Detail</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Contact
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" required parsley-type="contact_no" value="{{isset($company->contact_no)?$company->contact_no:''}}" name="contact_no"
                                        class="form-control" placeholder="contact No">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Alternative Contact
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" required parsley-type="contact_no" value="{{isset($company->contact_alter)?$company->contact_alter:''}}"
                                        name="contact_alter" class="form-control" placeholder="Alternative Contact">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Alternative Email
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="email" required parsley-type="email" value="{{isset($company->email_alter)?$company->email_alter:''}}" name="email_alter"
                                        class="form-control" placeholder="Alternative Email">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Billing Address
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" required parsley-type="text" name="billing_address" value="{{isset($company->billing_address)?$company->billing_address:''}}"
                                        class="form-control" placeholder="Billing Address">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Office Address
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" required parsley-type="text" name="office_address" value="{{isset($company->office_address)?$company->office_address:''}}"
                                        class="form-control" placeholder="Office Address">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">License Number
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" required parsley-type="text" name="licence_number" value="{{isset($company->licence_number)?$company->licence_number:''}}"
                                        class="form-control" placeholder="License Number">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">License Expiry
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <div>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" name="license_expiry" id="datepicker-autoclose" value="{{isset($company->license_expiry)?$company->license_expiry:''}}">
                                </div>
                            </div>
                        </div>
                        @if(isset($user->role_id) && $user->role_id == 3)
                        <div id="fmc_div" class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">Job Categories
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <select class="select2 form-control select2-multiple" style="width:100%" name="job_categories[]" multiple="multiple" data-placeholder="Job Categories">
                                    <?php 
                                     if(isset($company->job_categories) && !empty($company->job_categories)) {
                                        //job categories IDs in string separating with comma
                                        $selected_categories = explode(',', $company->job_categories); //Make categories an array
                                        foreach(\App\JobCategory::all() as $category){?>
                                    <option @if(in_array($category->id, $selected_categories)) selected='selected' @endif value="{{$category->id}}">{{$category->title}}</option>
                                    <?php }
                                    }else{
                                        foreach(\App\JobCategory::all() as $category):?>
                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                    <?php  
                                        endforeach; 
                                    } ?>
                                </select>
                            </div>
                        </div>
                        @endif
                        <div class="form-group row">
                            <label for="hori-pass1" class="col-sm-4 form-control-label">License
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="file" name="licence_image" class="new-license"> @if(isset($company->license) && $company->license != '')
                                 <a href="{{ asset('laravel_code/storage/app/public/company_image/' . $company->license) }}" class="image-popup" title="{{ $company->license }}">
                            <img class="img-thumbnail thumb-img" width="150" height="150" src="{{ asset('laravel_code/storage/app/public/company_image/' . $company->license) }}"
                                    alt="License" />
                                </a>
@endif
                            </div>
                        </div>
                    </div>
                   
                    <!-- Company detail for edit ends here -->
                </div>
            </div>
        </div>
        @endif
        @endif
        @if(GeneralFunctions::check_edit_permission('/users'))
        <div class="col-sm-12">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                Save
            </button>
            
            <div class="clearfix"></div>
        </div>
        @endif
    </div>
</form>
@section('js')
<script type="text/javascript" src="{{ URL::to('assets/pages/jquery.formadvanced.init.js') }}"></script>
<script src="{{ URL::to('assets/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<!-- Date Picker -->
<script src="{{ URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Magnific popup -->
<script type="text/javascript" src="{{ URL::asset('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        }
    });
}); 
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
    });
</script>
@endsection
@endsection
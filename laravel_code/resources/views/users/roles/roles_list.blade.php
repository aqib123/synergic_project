@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
<!-- Page-Title -->
@section('css')
<style>
.user_action_btn{
    float: none !important;
}
</style>
@endsection
@php
    if(Auth::user()->role_id == 1){
        $url = '/staff/roles';
    }
    else if(Auth::user()->role_id == 2){
        $url = '/oam/staff/roles';
    }
    else{
        $url = '/fmc/staff/roles';
    }

@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">

            <a href="{{url($url)}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add Roles</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    @include('flash_message')
    <div class="col-sm-9">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($roles) && count($roles)>0) 
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role['id']}}</td>
                                <td>
                                    {{ucwords($role['name'])}}
                                </td>
                                <td>
                                    <?php echo date_format(new DateTime($role['created_at']), 'jS F Y g:ia');?>
                                </td>
                                <td  style="text-align: center;">
                                     @php
                                        if(Auth::user()->role_id == 1){
                                            $edit_url_path = '/staff/roles?id='.$role['id'];
                                            $delete_url_path = '/delete_role/'.$role['id'];
                                        }
                                        elseif(Auth::user()->role_id == 2){
                                            $edit_url_path = '/oam/staff/roles?id='.$role['id'];
                                            $delete_url_path = '/oam/delete_role/'.$role['id'];
                                        }
                                        else{
                                            $edit_url_path = '/fmc/staff/roles?id='.$role['id'];
                                            $delete_url_path = '/fmc/delete_role/'.$role['id'];
                                        }
                                    @endphp
                                    <div class="btn-group user_action_btn">
                                        <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                            <i class="zmdi zmdi-more"></i>
                                        </a>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{url($edit_url_path)}}" title="Edit">
                                                <i class="fa fa-edit text-info"></i> Edit
                                            </a>
                                            <!-- <a class="dropdown-item" href="{{url($delete_url_path)}}" title="Delete">
                                                <i class="fa fa-trash text-danger"></i> Delete
                                            </a> -->
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif  
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12">
            <div class="card page_box_shadow m-t-0">
                <h4 class="card-header sidebar_header m-b-0">
                    Job Stats
                </h4>
                <ul class="list-group list-group-flush sidbar_icon">
                    <li class="list-group-item">
                            <!-- <i class="fa fa-check-square"></i> -->
                            <span> Applied</span> 0
                    </li>
                    <li class="list-group-item">
                            <span> Shortlisted</span> 0
                    </li>
                    <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                </ul>
                
        
            </div>
            <div class="m-t-10">
                <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
            </div>
        </div>
</div>
@endsection
@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
<!-- Add user section starts from here -->
@php
$name = '';
$description = '';

if(count($role_info) > 0){
    $name = $role_info['name'];
    $description = $role_info['description'];
}

if(Auth::user()->role_id == 1){
    $url = '/roles/list';
}
else if(Auth::user()->role_id == 2){
    $url = '/oam/roles/list';
}
else{
    $url = '/fmc/roles/list';
}
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url($url)}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Roles</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">
        
    </ul>
</div>
@include('flash_message')
<div class="row ">
    <div class="col-sm-9">
        <div class="card-box add_staff add_user_role">
            @if(Auth::user()->role_id == 1)
                <form id="permission_form" action="{{url('/add/role_permissions')}}" method="POST">
            @elseif(Auth::user()->role_id == 2)
                <form id="permission_form" action="{{url('oam/add/role_permissions')}}" method="POST">
            @else
                <form id="permission_form" action="{{url('fmc/add/role_permissions')}}" method="POST">
            @endif    
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="form_label" for="userName">Name </label>
                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                            <input name="name" placeholder="Name" class="form-control" type="text" value="{{$name}}">
                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <label class="form_label" for="userName">Description </label>
                            <textarea name="description" placeholder="Description" class="form-control" type="text">{{$description}}</textarea>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="bottom_border">
                            <h6 class="m-t-0 m-b-10 font-13"><b>Select the features you want to enable for this type of user</b></h6>
                        </div> 
                    </div> 
                </div>
                <div class="row">
                        <div class="col-md-8"> 
                            <div class="bottom_border add_user_role_detail m-b-10">
                                <p class="m-b-10 text-muted font-13" style="margin-left: 0px;">Listings for admin screens</p>
                                <div class="card-box add_user_role_box m-b-10">
                                    <dl class="row bottom_border m-b-10">
                                        <dt class="col-sm-2">Add</dt>
                                        <dt class="col-sm-2">View</dt>
                                        <dt class="col-sm-2">Edit</dt>
                                        <dt class="col-sm-2">Delete</dt>
                                         <dt class="col-sm-2">Screens</dt>
                                    </dl>
                                    @php $i=0; $j = 11; $k = 21; $l = 31; @endphp
                                    @foreach($screens as $key => $value)
                                    <dl class="row bottom_border m-b-10">
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <div class="checkbox checkbox-success checkbox-single">
                                                    <input id="{{$i}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][add]" 
                                                    @php
                                                    if(isset($roles_details[$value['code']])){
                                                        if(isset($roles_details[$value['code']]['add'])){
                                                            echo 'checked';
                                                        }
                                                    }
                                                    @endphp
                                                    >
                                                    <label for="{{$i}}"></label>
                                                </div>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <div class="checkbox checkbox-success checkbox-single">
                                                    <input id="{{$j}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][view]" @php
                                                    if(isset($roles_details[$value['code']])){
                                                        if(isset($roles_details[$value['code']]['view'])){
                                                            echo 'checked';
                                                        }
                                                    }
                                                    @endphp
                                                    >
                                                    <label for="{{$j}}"></label>
                                                </div>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <div class="checkbox checkbox-success checkbox-single">
                                                    <input id="{{$k}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][edit]"
                                                    @php
                                                    if(isset($roles_details[$value['code']])){
                                                        if(isset($roles_details[$value['code']]['edit'])){
                                                            echo 'checked';
                                                        }
                                                    }
                                                    @endphp
                                                    >
                                                    <label for="{{$k}}"></label>
                                                </div>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <div class="checkbox checkbox-success checkbox-single">
                                                    <input id="{{$l}}" type="checkbox" name="permissions[<?php echo $value['code']; ?>][delete]"
                                                    @php
                                                    if(isset($roles_details[$value['code']])){
                                                        if(isset($roles_details[$value['code']]['delete'])){
                                                            echo 'checked';
                                                        }
                                                    }
                                                    @endphp
                                                    >
                                                    <label for="{{$l}}"></label>
                                                </div>
                                            </span>
                                        </dt>
                                        <dd class="col-sm-4">{{ $value['name'] }}</dd>
                                    </dl>
                                    @php $i++; $j++; $k++; $l++; @endphp
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</a>
                            <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                        </div>
                </div>
        </form>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12">
            <div class="card page_box_shadow m-t-0">
                <h4 class="card-header sidebar_header m-b-0">
                    Job Stats
                </h4>
                <ul class="list-group list-group-flush sidbar_icon">
                    <li class="list-group-item">
                            <!-- <i class="fa fa-check-square"></i> -->
                            <span> Applied</span> 0
                    </li>
                    <li class="list-group-item">
                            <span> Shortlisted</span> 0
                    </li>
                    <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                </ul>
              
        
            </div>
            <div class="m-t-10">
                <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
            </div>
        </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        @if(Auth::user()->role_id == 1)
            var url_path = "{{ generate_url('/roles/permission_validation') }}";
        @elseif(Auth::user()->role_id == 2)
            var url_path = "{{ generate_url('/oam/roles/permission_validation') }}";
        @else
            var url_path = "{{ generate_url('/fmc/roles/permission_validation') }}";
        @endif
        $(document).on('click', '.save_button', function(){
            data = $('#permission_form').serialize();
            $('.loading_gif').show();
            $.ajax({
                type: 'POST',
                url: url_path,
                data: data,
                success:function(data){
                    if(data.status == 'success'){
                        $('#permission_form').submit()[0];
                    }
                    else{
                        var errorArray = data.msg_data;
                        var list = '';
                        errorArray.forEach(function(e){
                            list = list +'<li>'+e+'</li>'; 
                        });

                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                    $('.loading_gif').hide();
                }
            });
        });
    })
</script>    
@endsection
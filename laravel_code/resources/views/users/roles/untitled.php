@extends('layout.with-side-menu')
@section('content')
<!-- Add user section starts from here -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url('/oam/jobs')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To User roles</a>
        </div>
        <h4 class="page-title">Add user role</h4>
    </div>
</div>
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">
        
    </ul>
</div>
<div class="row ">
    <div class="col-sm-12">
        <div class="card-box add_staff add_user_role">
                <form id="permission_form" action="{{url('fmc/add/role_permissions')}}" method="POST">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Name </label>
                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                            <input name="name" placeholder="Name" class="form-control" type="text" value="{{$name}}">
                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="form_label" for="userName">Description </label>
                            <textarea name="description" placeholder="Description" class="form-control" type="text">{{$description}}</textarea>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                        <div class="bottom_border">
                            <h6 class="m-t-0 m-b-10 font-13"><b>Select the features you want to enable for this type of user</b></h6>
                        </div> 
                    </div> 
                </div>
                <div class="row">
                        <div class="col-md-8">
                            <div class="bottom_border add_user_role_detail">
                                <p class="text-muted font-13">Listings for admin screens</p>
                                <div class="card-box add_user_role_box">
                                    <dl class="row bottom_border m-b-10">
                                        <dt class="col-sm-2">Add</dt>
                                        <dt class="col-sm-2">View</dt>
                                        <dt class="col-sm-2">Edit</dt>
                                        <dt class="col-sm-2">Delete</dt>
                                         <dt class="col-sm-2">Screens</dt>
                                    </dl>
                                    <dl class="row bottom_border m-b-10">
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <input id="leasing" type="checkbox" name="permissions[][add]" 
                                                >
                                                <label for="leasing"></label>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <input id="leasing" type="checkbox" name="permissions[][view]" @php
                                                if(isset($roles_details[$value['code']])){
                                                    if(isset($roles_details[$value['code']]['view'])){
                                                        echo 'checked';
                                                    }
                                                }
                                                @endphp
                                                >
                                                <label for="leasing"></label>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <input id="leasing" type="checkbox" name="permissions[<?php echo $value['code']; ?>][edit]"
                                                @php
                                                if(isset($roles_details[$value['code']])){
                                                    if(isset($roles_details[$value['code']]['edit'])){
                                                        echo 'checked';
                                                    }
                                                }
                                                @endphp
                                                >
                                                <label for="leasing"></label>
                                            </span>
                                        </dt>
                                        <dt class="col-sm-2">
                                            <span class=" m-b-0 m-t-10">
                                                <input id="leasing" type="checkbox" name="permissions[<?php echo $value['code']; ?>][delete]"
                                                @php
                                                if(isset($roles_details[$value['code']])){
                                                    if(isset($roles_details[$value['code']]['delete'])){
                                                        echo 'checked';
                                                    }
                                                }
                                                @endphp
                                                >
                                                <label for="leasing"></label>
                                            </span>
                                        </dt>
                                        <dd class="col-sm-4">{{ $value['name'] }}</dd>
                                    </dl>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-12">
                            <a href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save user role</a>
                            <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                        </div>
                </div>
        </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        var url_path = "{{ generate_url('/fmc/roles/permission_validation') }}";
        $(document).on('click', '.save_button', function(){
            data = $('#permission_form').serialize();
            $('.loading_gif').show();
            $.ajax({
                type: 'POST',
                url: url_path,
                data: data,
                success:function(data){
                    if(data.status == 'success'){
                        $('#permission_form').submit()[0];
                    }
                    else{
                        var errorArray = data.msg_data;
                        var list = '';
                        errorArray.forEach(function(e){
                            list = list +'<li>'+e+'</li>'; 
                        });

                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                    $('.loading_gif').hide();
                }
            });
        });
    })
</script>    
@endsection
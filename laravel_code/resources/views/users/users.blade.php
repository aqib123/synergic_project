@extends('layouts.app') @section('content')
<style>
    .short_profile_image {
        border: 3px solid #822767;
        padding: 10px;
        font-size: 16px;
        font-weight: 700;
        color: #822767;
        border-radius: 50%;
        letter-spacing: 0px;
        width: 25px;
        display: block;
        text-align: center;
    }
</style>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <!-- <div class="btn-group pull-right m-t-15">
            <a href="{{url('user_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Company</a>
        </div> -->
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs association_nav m-b-0" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {{ Request::input('status') == 'OAM' ? 'active' : '' }}" id="oam-tab" data-toggle="tab" href="#oam" role="tab" aria-controls="oam" aria-expanded="true"><b>OAM</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::input('status') == 'FMC' ? 'active' : '' }}" id="fmc-tab" data-toggle="tab" href="#fmc" role="tab" aria-controls="fmc"><b>FMC</b></a>
                </li>
            </ul>
        </div>
    </div>
<div class="row">
    <div class="col-md-12 col-xs-12 m-t-0">
        <div class="card-box ">
            @if(isset($users) && count($users)>0)
            <div class="tab-content m-t-20" id="myTabContent">
                <!-- OAM Tab -->
                <div role="tabpanel" class="tab-pane fade {{ Request::input('status') == 'OAM' ? 'in active' : '' }}" id="oam" aria-labelledby="oam-tab">
                    <!-- OAM Content -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="  custom_table">
                                <table id="datatable2" class="table table-striped table-bordered">
                                    <thead>
                                        <tr> 
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Created At</th> 
                                            <th>Account Status and Settings</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $row) @if($row->role_id == 2)
                                        <tr>
                                            <td>{{$row->id}}</td>
                                            @if(isset($row->image))
                                            <td class="text-center">
                                                <img class="img img-circle" width="50" height="50" src="{{ asset('laravel_code/storage/app/public/user_image/'.$row->image) }}" alt="Profile" /> 
                                            </td>
                                            @else 
                                            <td class="text-center" style=" padding: 20px 5px;">
                                                @php $words = explode(" ", $row->name); $acronym = ""; foreach ($words as $w) { $acronym .= $w[0]; } @endphp
                                                <span class=" short_profile_image">{{$acronym}}</span>
                                            </td>
                                            @endif
                                           
                                            <td>
                                                <a class="custom_links user_name_link" href="show_profile?id={{$row->id}}">{{ucwords($row->name)}}</a>
                                            </td>
                                            <td>{{$row->user_role_name}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>
                                                @php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp
                                            </td> 
                                            <td style="text-align: center;">
                                                @if($row->status == 0)
                                                <span class="label label-warning">Pending</span>
                                                @elseif($row->status == 1)
                                                <span class="label label-success">Active</span>
                                                @elseif($row->status == 2)
                                                <span class="label label-danger">Deactive</span>
                                                @elseif($row->status == 3)
                                                <span class="label label-danger">Suspended</span>
                                                @else
                                                <span class="label label-success">Renew</span>
                                                @endif
                                                <!-- <a class="btn btn-success" href="javascript:void(0)" class="account_settings" id="{{$row->id}}">Settings</a> -->
                                                <div class="btn-group user_action_btn">
                                                    <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        
                                                            <a class="dropdown-item" href="{{url('user_form?id='.$row->id)}}" title="Edit">
                                                            Edit
                                                            </a>
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <!-- <a class="dropdown-item" href="{{url('delete_user/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"
                                                            title="Delete">
                                                            Delete
                                                        </a> -->
                                                        
                                                        @if($row->status != 1)
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Approval" id="{{$row->id}}">
                                                         Active
                                                        </a>
                                                        @endif
                                                        @if($row->status != 3)
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Suspend" id="{{$row->id}}">
                                                         Suspend
                                                        </a>
                                                        @endif
                                                        @if($row->status != 2)
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Reject" id="{{$row->id}}">
                                                         Deactive
                                                        </a>
                                                        @endif
                                                        <!-- <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Renew" id="{{$row->id}}">
                                                         Renew
                                                        </a> -->
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End OAM Content -->
                </div>
                <!-- End OAM Tab -->

                <!-- FMC Tab -->
                <div class="tab-pane fade {{ Request::input('status') == 'FMC' ? 'in active' : '' }}" id="fmc" role="tabpanel" aria-labelledby="fmc-tab">
                    <!-- FMC Content -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class=" custom_table">
                                <table id="datatable3" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Referred By</th>
                                            <th>Created At</th> 
                                            <th>Account Status and Settings</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $row) @if($row->role_id == 3)
                                        <tr>
                                            <td>{{$row->id}}</td>

                                            @if(isset($row->image))
                                            <td class="text-center">
                                                <img class="img img-circle" width="50" height="50" src="{{ asset('laravel_code/storage/app/public/user_image/'.$row->image) }}" alt="Profile" /> 
                                            </td>
                                            @else 
                                            <td class="text-center" style="padding: 20px 5px;">
                                                @php $words = explode(" ", $row->name);
                                                $acronym = ""; 
                                                $i = 0;
                                                foreach ($words as $w){
                                                    if($i < 2){ 
                                                        $acronym .= $w[0]; 
                                                    }
                                                    $i++;
                                                }
                                                @endphp
                                                <span class=" short_profile_image">{{$acronym}}</span>
                                            </td>
                                            @endif

                                            
                                            <td>
                                                <a class="custom_links  user_name_link" href="show_profile?id={{$row->id}}">{{ucwords($row->name)}}</a>
                                            </td>
                                            <td>{{$row->user_role_name}}</td>
                                            <td>{{$row->email}}</td>
                                             <td>
                                               <?php echo GeneralFunctions::getRefernceCompany($row->reference_url); ?>
                                            </td>
                                            <td>
                                                @php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp
                                            </td> 
                                            <td style="text-align: center;">
                                                @if($row->status == 0)
                                                <span class="label label-warning">Pending</span>
                                                @elseif($row->status == 1)
                                                <span class="label label-success">Active</span>
                                                @elseif($row->status == 2)
                                                <span class="label label-danger">Deactive</span>
                                                @elseif($row->status == 3)
                                                <span class="label label-danger">Suspended</span>
                                                @else
                                                <span class="label label-success">Renew</span>
                                                @endif
                                                <!-- <a class="btn btn-success" href="javascript:void(0)" class="account_settings" id="{{$row->id}}">Settings</a> -->
                                                <div class="btn-group user_action_btn">
                                                    <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </a>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="{{url('user_form?id='.$row->id)}}" title="Edit">
                                                            Edit
                                                        </a>
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <!-- <a class="dropdown-item" href="{{url('delete_user/'.$row->id)}}" onclick="return confirm('Are you sure you want to delete this item?');"
                                                            title="Delete"> 
                                                            Delete
                                                        </a> -->
                                                        @if($row->status != 1)
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Approval" id="{{$row->id}}">
                                                         Active
                                                        </a>
                                                        @endif
                                                        @if($row->status != 3)
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Suspend" id="{{$row->id}}">
                                                         Suspend
                                                        </a>
                                                        @endif
                                                        @if($row->status != 2)
                                                        <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Reject" id="{{$row->id}}">
                                                         Deactive
                                                        </a>
                                                        @endif
                                                        <!-- <div class="dropdown-divider m-t-0 m-b-0"></div>
                                                        <a class="dropdown-item account_settings" href="javascript:void(0)" title="Renew" id="{{$row->id}}">
                                                         Renew
                                                        </a> -->
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endif @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- End FMC Content -->
                </div>
                <!-- End FMC Tab -->
            </div>
            @endif
        </div>
    </div>
</div>
<!-- Modal for Account Settings -->
<div class="modal fade md_account_settings in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header" style="text-align: center;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title lead" id="md_heading"></h4>
            </div>
            <div class="modal-body">
                <form action="{{url('update/account/status')}}" method="POST" id="md_account_setting_form">
                    {{csrf_field()}}
                    <input type="hidden" name="account_status" id="md_account_status">
                    <input type="hidden" name="user_id" id="md_user">
                    <div class="form-group row" style="display: none" id="md_reason">
                        <label for="inputEmail3" class="col-sm-12 form-control-label">Reason
                        </label>
                        <div class="col-sm-12">
                            <textarea class="form-control" name="reason" id="md_reason_text"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-12 form-control-label">
                        </label>
                        <div class="col-sm-12" style="text-align: center;">
                            <a href="javascript:void(0)" class="btn btn-success" id="md_submit_btn">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- end row -->

@section('js')
<script>
    $(document).ready(function () {
        $('#datatable2').DataTable();
        $('#datatable3').DataTable();

        $(document).on('click', '.account_settings', function(){
            console.log('comming here');
            var user_id = $(this).attr('id');
            var account_status = $(this).attr('title');
            $('#md_reason').hide();
            if(account_status == 'Approval'){
                $('#md_heading').text('Are you sure for approval of the Account');
                $('#md_account_status').val(1);
            }
            else if(account_status == 'Reject'){
                $('#md_heading').text('Are you sure for rejection of the Account');
                $('#md_account_status').val(2);
                $('#md_reason_text').text('');
                $('#md_reason').show();
            }
            else if(account_status == 'Suspend'){
                $('#md_heading').text('Are you sure for suspension of the Account');
                $('#md_account_status').val(3);
                $('#md_reason_text').text('');
                $('#md_reason').show();
            }
            else{
                $('#md_heading').text('Are you sure for renewal of the Account');
                $('#md_account_status').val(4);
            }
            $('#md_user').val(user_id);
            $('.md_account_settings').modal('show');
        });

        $(document).on('click','#md_submit_btn', function(){
            if($('#md_account_status').val() == 2){
                if($('#md_reason_text').val() == ''){
                    alert('Please Fill up the Reason Section');
                }
                else{
                    $('#md_account_setting_form').submit()[0];
                }
            }
            else{
                $('#md_account_setting_form').submit()[0];
            }
        });
    });


    var type = '{{Request::input("status")}}';
    if (type == 'OAM') {
        $('#oam-tab').addClass('active');
        $('#fmc-tab').removeClass('active');
    } else if (type == 'FMC') {
        $('#oam-tab').removeClass('active');
        $('#fmc-tab').addClass('active');
    }
</script> @endsection @endsection
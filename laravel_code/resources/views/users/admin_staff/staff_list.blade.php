@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
@section('css')
<style>
.user_action_btn{
    float: none !important;
}
</style>
@endsection
<!-- Page-Title -->
@php
    $counter = 1;
    if(Auth::user()->role_id == 1){
        $url = '/staff/form';
    }
    else if(Auth::user()->role_id == 2){
        $url = '/oam/staff/form';
    }
    else{
        $url = '/fmc/staff/form';
    }
    $base = URL::to('/');
@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{ $base.$url }}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add User</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    @include('flash_message')
    <div class="col-sm-9">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Role Description</th>
                        <th>Created At</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($staff) && count($staff)>0) 
                        @foreach($staff as $member)
                            <tr>
                                <td>{{$counter}}</td>
                                <td>
                                    {{ucwords($member['name'])}}
                                </td>
                                <td>
                                    {{ucwords($member['staff_members']['user_roles']['description'])}}
                                </td>
                               
                                <td>
                                    <?php echo date_format(new DateTime($member['created_at']), 'jS F Y g:ia');?>
                                </td>
                                <td> 
                                     {!! $member['status'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Deactive</span>' !!}
                                </td>
                                <td  style="text-align: center;">
                                    <div class="btn-group user_action_btn">
                                        <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                            <i class="zmdi zmdi-more"></i>
                                            <!-- <i class="fa fa-cog fa-fw"></i> -->
                                        </a>
                                        <div class="dropdown-menu">
                                             @php
                                                if(Auth::user()->role_id == 1){
                                                    $edit_url_path = '/staff/form?id='.$member['id'];
                                                    $delete_url_path = '/delete_role/'.$member['id'];
                                                }
                                                elseif(Auth::user()->role_id == 2){
                                                    $edit_url_path = '/oam/staff/form?id='.$member['id'];
                                                    $delete_url_path = '/oam/delete_role/'.$member['id'];
                                                }
                                                else{
                                                    $edit_url_path = '/fmc/staff/form?id='.$member['id'];
                                                    $delete_url_path = '/fmc/delete_role/'.$member['id'];
                                                }
                                            @endphp
                                            <a class="dropdown-item" href="{{url($edit_url_path)}}" title="Edit">
                                                    <i class="fa fa-edit text-info"></i>  Edit
                                            </a>
                                           <!--  <a class="dropdown-item" href="{{url($delete_url_path)}}" title="Delete">
                                                Delete
                                            </a> -->
                                            @if($member['status'] != 1)
                                                <a class="dropdown-item account_settings" href="javascript:void(0)" title="Approval" id="{{$member['id']}}">
                                                        <i class="fa fa-check text-success"></i> Active
                                                </a>
                                            @else
                                                <a class="dropdown-item account_settings" href="javascript:void(0)" title="Reject" id="{{$member['id']}}">
                                                        <i class="fa fa-ban text-danger"></i>   Deactive
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @php $counter++; @endphp
                        @endforeach
                    @endif  
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12">
            <div class="card page_box_shadow m-t-0">
                <h4 class="card-header sidebar_header m-b-0">
                    Job Stats
                </h4>
                <ul class="list-group list-group-flush sidbar_icon">
                    <li class="list-group-item">
                            <!-- <i class="fa fa-check-square"></i> -->
                            <span> Applied</span> 0
                    </li>
                    <li class="list-group-item">
                            <span> Shortlisted</span> 0
                    </li>
                    <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                </ul>
                
        
            </div>
            <div class="m-t-10">
                <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
            </div>
        </div>
</div>

<!-- Modal for Account Settings -->
<div class="modal fade md_account_settings in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header" style="text-align: center;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title lead" id="md_heading"></h4>
            </div>
            <div class="modal-body">
                <form action="{{url('update/staff/account/status')}}" method="POST" id="md_account_setting_form">
                    {{csrf_field()}}
                    <input type="hidden" name="account_status" id="md_account_status">
                    <input type="hidden" name="user_id" id="md_user">
                    <div class="form-group row" style="display: none" id="md_reason">
                        <label for="inputEmail3" class="col-sm-12 form-control-label">Reason
                        </label>
                        <div class="col-sm-12">
                            <textarea class="form-control" name="reason" id="md_reason_text"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-12 form-control-label">
                        </label>
                        <div class="col-sm-12" style="text-align: center;">
                            <a href="javascript:void(0)" class="btn btn-success-outline" id="md_submit_btn">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- end row -->
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.account_settings', function(){
            console.log('comming here');
            var user_id = $(this).attr('id');
            var account_status = $(this).attr('title');
            $('#md_reason').hide();
            if(account_status == 'Approval'){
                $('#md_heading').text('Are yo sure for Activation of the Account');
                $('#md_account_status').val(1);
            }
            if(account_status == 'Reject'){
                $('#md_heading').text('Are you sure for Deactivation of the Account');
                $('#md_account_status').val(2);
                $('#md_reason_text').text('');
                $('#md_reason').show();
            }
            $('#md_user').val(user_id);
            $('.md_account_settings').modal('show');
        });

        $(document).on('click','#md_submit_btn', function(){
            if($('#md_account_status').val() == 2){
                if($('#md_reason_text').val() == ''){
                    alert('Please Fill up the Reason Section');
                }
                else{
                    $('#md_account_setting_form').submit()[0];
                }
            }
            else{
                $('#md_account_setting_form').submit()[0];
            }
        });
    });
</script>
@endsection
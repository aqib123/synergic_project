@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
@php
    $name = '';
    $phone_number = '';
    $home_number = '';
    $email = '';
    $role_id = '';

    if(isset($record) && count($record) > 0){
        $name = $record['name'];
        $phone_number = $record['staff_members']['mobile_number'];
        $home_number = $record['staff_members']['home_number'];
        $email = $record['email'];
        $role_id = $record['staff_members']['role_id'];
    }
    if(Auth::user()->role_id == 1){
        $url = '/staff/member/list';
    }
    else if(Auth::user()->role_id == 2){
        $url = '/oam/staff/member/list';
    }
    else{
        $url = '/fmc/staff/member/list';
    }

@endphp
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url($url)}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To User List</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
    <div class="msg-box alert" style="display: none;">
        <ul style="text-decoration: none;" id="msg-list">
            
        </ul>
    </div>
    @include('flash_message')
    <div class="row ">
        @if(Auth::user()->role_id == 1)
        <form id="staff_form" action="{{url('staff/member/save')}}" method="POST">
        @elseif(Auth::user()->role_id == 2)
        <form id="staff_form" action="{{url('oam/staff/member/save')}}" method="POST">
        @else
        <form id="staff_form" action="{{url('fmc/staff/member/save')}}" method="POST">
        @endif    
            <div class="col-sm-9">
                <div class="card-box add_staff ">
                    <div class="row">
                    
                        <div class="col-sm-12">
                            <h4 class="m-t-0 m-b-0">Contact information</h4>
                            <hr class="m-t-0">
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="form_label" for="userName">Name (required)<span class="text-danger">*</span></label>
                                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                <input name="name" placeholder="Name" class="form-control" type="text" id="name" value="{{isset($name)?$name:''}}">
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="form_label" for="companyName">Phone</label>
                            <div class="phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-smartphone"></i></span>
                                <input class="form-control"  type="text" id="phone_number" name="phone_number" value="{{isset($phone_number)?$phone_number:''}}">
                            </div>
                            <div class=" phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                                <input class="form-control"  type="text" name="home_number" value="{{isset($home_number)?$home_number:''}}">
                            </div>
                           <!--  <div class=" phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-case"></i></span>
                                <input class="form-control"  type="text">
                            </div> -->
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>

                <div class="card-box add_staff ">
                        <div class="row">
                        
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">User information</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Email (required)<span class="text-danger">*</span></label>
                                <div class=" form-group phone_input input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                                    <input class="form-control"  type="text" name="email" value="{{isset($email)?$email:''}}">
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="userrole">Role (required)<span class="text-danger">*</span></label>
                                <div class=" form-group ">
                                    <select class="c-select" name="role">
                                        <option selected="" disabled>Select user role..</option>
                                        @foreach($roles as $key => $value)
                                            <option value="{{ $value['id'] }}" <?php echo ($role_id == $value['id']) ? 'selected' : '' ?>>{{ $value['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div> 
                </div>
                <div class="card-box add_staff m-b-10">
                        <div class="row">
                        
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Inspections</h4>
                                <hr class="m-t-0">
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <div class="checkbox checkbox-success m-b-0">
                                    <input id="invit_inspection" type="checkbox">
                                    <label for="invit_inspection">
                                        Send invite for Property Inspections
                                    </label>
                                </div> -->
                                <p class="lead font-13 m-b-0 m-t-10">When you click save, we will:</p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span> Use the listed email address as the username</span>
                                </p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span>Assign a temporary password</span>
                                </p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span>Email instructions to the user telling them how to sign in</span>
                                </p>
                            </div>
                        </div>
                </div>
                 
            <a href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</a>
            <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
        </div>
        <div class="col-sm-3 col-lg-3 col-xs-12">
                <div class="card page_box_shadow m-t-0">
                    <h4 class="card-header sidebar_header m-b-0">
                        Job Stats
                    </h4>
                    <ul class="list-group list-group-flush sidbar_icon">
                        <li class="list-group-item">
                                <!-- <i class="fa fa-check-square"></i> -->
                                <span> Applied</span> 0
                        </li>
                        <li class="list-group-item">
                                <span> Shortlisted</span> 0
                        </li>
                        <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                                    <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                    </ul>
                  
            
                </div>
                <div class="m-t-10">
                    <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
                </div>
            </div>
        </form>
    </div>

@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        @if(Auth::user()->role_id == 1)
            var url_path = "{{ generate_url('/check_staff_validations') }}";
        @elseif(Auth::user()->role_id == 2)
            var url_path = "{{ generate_url('/oam/check_staff_validations') }}";
        @else
            var url_path = "{{ generate_url('/fmc/check_staff_validations') }}";
        @endif

        $(document).on('click', '.save_button', function(){
            data = $('#staff_form').serialize();
            $.ajax({
                type: 'POST',
                url: url_path,
                data: data,
                success:function(data){
                    if(data.status == 'success'){
                        $('#staff_form').submit()[0];
                    }
                    else{
                        var errorArray = data.msg_data;
                        var list = '';
                        errorArray.forEach(function(e){
                            list = list +'<li>'+e+'</li>'; 
                        });

                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                    $('.loading_gif').hide();
                }
            });
        });
    })
</script>    
@endsection

<!-- end col-->


@endsection
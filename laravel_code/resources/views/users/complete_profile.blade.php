@extends('layouts.app') 
@section('content')
@section('css')
<!--venobox lightbox-->
<link rel="stylesheet" href="{{ URL::asset('assets/plugins/magnific-popup/css/magnific-popup.css') }}"/>
<style>
    .profile_img{
        border: 4px solid #822767;
        padding: 25px;
        font-size: 25px;
        font-weight: 700;
        color:#822767;
        border-radius: 50%;
        letter-spacing: 0px;
    }
</style>
@endsection
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('users?status=OAM')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Companies</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:'User Profile'}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div clss="col-sm-12">
            <div class="left company_image">
                @if(isset($profile->image) && !empty($profile->image))
                <img id="profile_image" class="img-circle" width="100" height="100" src="{{ asset('laravel_code/storage/app/public/user_image/'.$profile->image) }}"
                    alt="{{ $profile->image }}" /> 
                @else
                @php
                    $words = explode(" ", Auth::user()->name);
                    $acronym = "";
                    $i = 0;
                    foreach ($words as $w) {
                        if($i < 2){ 
                            $acronym .= $w[0]; 
                        }
                        $i++;
                    }
                @endphp
                <div class="profile_img">{{$acronym}}</div>
                @endif
            </div>
            <div class="col-sm-3 m-t-20">
                <p class="lead m-b-0">
                    <b>{{isset($profile->name)?$profile->name:''}}</b>
                </p>
                <p class="text-muted font-13 m-t-0">{{isset($role->role)?$role->role:''}}</p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card-box primary_border_top">
            <div class="col-sm-12">
                <div class="pull-left">
                    <h4 class="m-t-10 header-title">User Information</h4>
                </div>
                <div class="pull-right">
                    <div class="btn-group m-b-0">
                        <a href="user_form?id={{Request::input('id')}}" class="btn btn-info-outline waves-effect waves-light btn-sm">Edit</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <hr>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Date Of Joining</p>
                <p class="text-muted m-b-10 font-13">
                    <?php echo date_format(new DateTime($profile->created_at), 'jS F Y g:ia');?>
                </p>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Email</p>
                <p class="text-muted m-b-10 font-13">{{isset($profile->email)?$profile->email:'-----'}}</p>
            </div>
            <div class="col-sm-12">
                <h4 class="m-t-10 header-title">Company Information</h4>
                <hr>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Company Name</p>
                <p class="text-muted m-b-10 font-13">{{isset($company->name)?$company->name:'-----'}}</p>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Contact #</p>
                <p class="text-muted m-b-10 font-13">{{isset($company->contact_no)?$company->contact_no:'-----'}}</p>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Office Address</p>
                <p class="text-muted m-b-10 font-13">{{isset($company->office_address)?$company->office_address:'-----'}}</p>
            </div>
            <div class="col-sm-6">
                <p class="text-dark m-b-0 m-t-0">Billing Address</p>
                <p class="text-muted m-b-10 font-13">{{isset($company->billing_address)?$company->billing_address:'------'}}</p>
            </div>
            <div class="col-sm-12">
                <h4 class="m-t-10 header-title">Company License</h4>
                <hr> @if(isset($company->license) && !empty($company->license))
                <a href="{{ asset('laravel_code/storage/app/public/company_image/'.$company->license) }}" class="image-popup" title="{{ $company->license }}">
                    <img class="img-thumbnail thumb-img" width="80" height="80" src="{{ asset('laravel_code/storage/app/public/company_image/'.$company->license) }}"
                    alt="{{ $company->license }}" /> 
                </a>
                
                    @else
                    <span class="text-danger">None</span>
                 @endif
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end row -->
@section('js')
<!-- Magnific popup -->
<script type="text/javascript" src="{{ URL::asset('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>
<script>
$(document).ready(function() {
    $('.image-popup').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        mainClass: 'mfp-fade',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        }
    });
});
</script>
@endsection
@endsection
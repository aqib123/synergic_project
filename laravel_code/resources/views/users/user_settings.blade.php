@extends('layouts.app') @section('content') @section('css')
<style>
    .association_many_tab .nav-link {
        padding: 0.5em 2em;
    }
    #msg-list li{
        list-style-type: none;
    }
    .ajax-pre-loader{
        margin-top: 5px;
    }
</style>
@endsection
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
        @include('flash_message')
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs association_nav association_many_tab m-b-0" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active admin_settings" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-expanded="true">
                    <b>Settings</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms">
                    <b>Terms & Conditions</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link admin_job_tc" id="default-template-tab" data-toggle="tab" href="#default_template" role="tab" aria-controls="default_template">
                    <b>Job Terms & Conditions</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="userterms-tab" data-toggle="tab" href="#userterms" role="tab" aria-controls="userterms">
                    <b>Fair Usage Terms & Conditions</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="company-ad-tab" data-toggle="tab" href="#company_ad" role="tab" aria-controls="company_ad">
                    <b>Company Ads</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="company-tab" data-toggle="tab" href="#company" role="tab" aria-controls="company">
                    <b>Company Roles</b>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 m-t-0">
        <div class="card-box ">
            <div class="tab-content m-t-20" id="myTabContent">
                <div class="msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" id="msg-list">

                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane fade in active" id="settings" aria-labelledby="settings-tab">
                    <form role="form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('settings/update/') }}">
                        {{ csrf_field() }}
                        <div class="col-sm-12 col-xs-12 col-md-6">
                            <div class="p-10">
                                <h4 class="header-title m-t-0 m-b-15">Setting information</h4>
                                <input type="hidden" name="id" value="{{isset(Auth::user()->id)?Auth::user()->id:''}}">
                                <div class="form-group row">
                                    <div class="col-sm-6 padding-left-0 padding-right-0">
                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Select User Varification
                                            <span class="text-danger">*</span>
                                        </label>
                                        <div class="col-sm-12">
                                            <select class="form-control" name="varification_option" id="varification_option">
                                                <option value="1" <?php echo($user_setting[ 'status']==1 ) ? "Selected" : "" ; ?> >1 Factor Varification</option>
                                                <option value="2" <?php echo($user_setting[ 'status']==2 ) ? "Selected" : "" ; ?>>2 Factor Varification</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @if(GeneralFunctions::check_edit_permission('/settings'))
                        <div class="col-sm-12 col-xs-12 col-md-12">
                            <div class="form-group row">
                                <div class="col-sm-8">
                                    <button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                                        Update
                                    </button>
                                </div>
                            </div>
                        </div>
                        @endif
                    </form>
                    <div class="clearfix"></div>
                </div>

                <div class="tab-pane fade " id="terms" role="tabpanel" aria-labelledby="terms-tab">
                    <form class="synergic_term_condition_form" id="synergic_term_condition_form">
                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        <div class="col-sm-12 col-xs-12 col-md-6">
                            <textarea class="form-control synergic_term_condition" id="synergic_term_condition" rows="5" name="description"></textarea>
                            <div class="m-t-20">
                                <div class="form-group row">
                                    @if(GeneralFunctions::check_edit_permission('/settings'))
                                    <div class="col-sm-8">
                                        <button type="button" class="profie_update btn btn-success waves-effect waves-light Save_TermCondition">
                                            Save
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>

                <div class="tab-pane fade " id="userterms" role="tabpanel" aria-labelledby="userterms-tab">
                    <form id="user_termtc_form" role="form" data-parsley-validate novalidate>
                        {{ csrf_field() }}
                        <div class="col-sm-12 col-xs-12 col-md-6">
                            <textarea class="form-control" rows="5" name="description"></textarea>
                            <div class="m-t-20">
                                <div class="form-group row">
                                    @if(GeneralFunctions::check_edit_permission('/settings'))
                                    <div class="col-sm-8">
                                        <button type="button" class="profie_update btn btn-success waves-effect waves-light save_userterm_tc">
                                            Save
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <div class="tab-pane fade" id="default_template" role="tabpanel" aria-labelledby="default-template-tab">
                    <form id="default_tc_for_job" role="form" data-parsley-validate novalidate method="POST">
                        {{ csrf_field() }}
                        <div class="col-sm-12 col-xs-12 col-md-10">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 form-control-label">Title
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-10">
                                    <input required="" parsley-type="text" placeholder="Title" name="title" value="{{isset($job_tc->title)?$job_tc->title:''}}" class="form-control" type="text">
                                    <input type="hidden" id="job_tc_id" name="id" value="{{Request::input('id')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 form-control-label">Description
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="5" name="description">{{isset($job_tc->description)?$job_tc->description:''}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hori-pass1" class="col-sm-2 form-control-label">Status
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="radio radio-info radio-inline staff_area_radio">
                                        <input id="scope1_standard" class="scope1_radio" value="1" @if(isset($job_tc->status) && $job_tc->status == 1) checked='checked' @endif name="status" type="radio">
                                        <label for="scope1_standard">Active</label>
                                    </div>
                                    <div class="radio radio-info radio-inline staff_area_radio">
                                        <input id="scope1_customized" class="scope1_radio" value="0" @if(isset($job_tc->status) && $job_tc->status == 0) checked='checked' @endif name="status" type="radio">
                                        <label for="scope1_customized">Inactive</label>
                                    </div>
                                </div>
                            </div>
                            <div class="m-t-20">
                                <div class="form-group row">
                                    @if(GeneralFunctions::check_edit_permission('/settings'))
                                    <div class="col-sm-8">
                                        <button type="button" class="profie_update btn btn-success waves-effect waves-light save_job_tc">
                                            Save
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <div class="tab-pane fade " id="company_ad" role="tabpanel" aria-labelledby="company-ad-tab">
                        <div class="col-sm-12 col-xs-12 col-md-10">
                            <form name="company-ad-form" id="company-ad-form" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 form-control-label">Title
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-5">
                                    <input required="" id="title" parsley-type="text" placeholder="Ad Title" value="{{isset($ad->title)?$ad->title:''}}" name="title" class="form-control" type="text">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="csrf-token" />
                                    <input type="hidden" id="ad_id" name="id" value="{{Request::input('id')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 form-control-label">Image
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-10">
                                    <div style="position:relative;">
                                        <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                            Choose File..
                                            <input id="image" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                name="user_image" size="40" onchange='$("#upload-file-info").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span class='label label-info' id="upload-file-info"></span>
                                    </div>
                                    <div class="m-t-10">
                                        <p class="text-muted font-13 m-t-0"> Upload ad image in GIF, PNG or JPEG format max size 2MB</p>
                                        @if(isset($ad->image) && $ad->image != '')
                                            <img class="img img-circle" width="50" height="50" src="{{ asset('laravel_code/storage/app/public/company_ad/'.$ad->image) }}" alt="Ad" /> 
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hori-pass1" class="col-sm-2 form-control-label">Status
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-sm-9">
                                    <div class="radio radio-info radio-inline staff_area_radio">
                                        <input id="scope1_standard" class="scope1_radio" @if(isset($ad->status) && $ad->status == 1) checked='checked' @endif value="1" name="status" type="radio">
                                        <label for="scope1_standard">Active</label>
                                    </div>
                                    <div class="radio radio-info radio-inline staff_area_radio">
                                        <input id="scope1_customized" class="scope1_radio" @if(isset($ad->status) && $ad->status == 0) checked='checked' @endif value="0" name="status" type="radio">
                                        <label for="scope1_customized">Inactive</label>
                                    </div>
                                </div>
                            </div>
                            <div class="m-t-20">
                                <div class="form-group row">
                                    @if(GeneralFunctions::check_edit_permission('/settings'))
                                    <div class="col-sm-1">
                                        <button type="button" class="profie_update btn btn-success waves-effect waves-light save_company_ad">
                                            Save
                                        </button>
                                    </div>
                                    <div class="col-sm-1 ajax-pre-loader">
                                        <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important;">
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                </div>

                <div role="tabpanel" class="tab-pane fade in" id="company" aria-labelledby="company-tab">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Created At</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(isset($all_roles) && count($all_roles)>0) @foreach($all_roles as $role)
                                        <tr>
                                            <td>{{$role->id}}</td>

                                            <td>
                                                @if($role->id == 1) {{ucwords($role->name)}} @else
                                                <a class="custom_links" href="{{url('users?status='.$role->name)}}">{{ucwords($role->name)}}</a>

                                            </td>
                                            @endif
                                            <td>{{$role->description}}</td>
                                            <td>
                                                <?php echo date_format(new DateTime($role->created_at), 'jS F Y g:ia');?>
                                            </td>
                                        </tr>
                                        @endforeach @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('js')
<script src="assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        visibility: 'visible',
        plugins: "lists"

    });
</script>
<script>
    $(document).ready(function () {
        $.urlParam = function(name){
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            return results[1] || 0;
        }
        var status = $.urlParam('status');

        $(document).on('click', '.Save_TermCondition', function () {           
            $('.msg-box').removeClass("alert-success").removeClass("alert-danger");
            $('.msg-box').hide();
            tinyMCE.triggerSave();
            var description = $("#synergic_term_condition_form").serialize();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_termCondition")}}',
                data: description,
                success: function (result) {
                    console.log(result);
                    $("#msg-list").empty();
                    if (result.flag === 0) {
                        var errorArray = result.errors;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                    } else if (result.flag == 1) {
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                    }
                }
            });
        });

        if(status == 3){
            $('#settings-tab').removeClass('active');
            $('#settings').removeClass('in active');
            $('#default-template-tab').addClass('active');
            $('#default_template').addClass('in active');
        }
        $(document).on('click', '.save_job_tc', function () {           
            $('.msg-box').removeClass("alert-success").removeClass("alert-danger");
            $('.msg-box').hide();
            tinyMCE.triggerSave();
            var params = $("#default_tc_for_job").serialize();
            console.log(params);
            $.ajax({
                type: 'POST',
                url: '{{generate_url("DefaultTC_for_job")}}',
                data: params,
                success: function (result) {
                    console.log(result);
                    $("#msg-list").empty();
                    if (result.flag === 0) {
                        var errorArray = result.errors;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                    } else if (result.flag == 1) {
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                    }
                }
            });

        });


        $(document).on('click', '.save_userterm_tc', function () {           
            $('.msg-box').removeClass("alert-success").removeClass("alert-danger");
            $('.msg-box').hide();
            tinyMCE.triggerSave();
            var params = $("#user_termtc_form").serialize();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("user_tc_content")}}',
                data: params,
                success: function (result) {
                    console.log(result);
                    $("#msg-list").empty();
                    if (result.flag === 0) {
                        var errorArray = result.errors;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                    } else if (result.flag == 1) {
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                    }
                }
            });
        });

        if(status == 5){
            $('#settings-tab').removeClass('active');
            $('#settings').removeClass('in active');
            $('#company-ad-tab').addClass('active');
            $('#company_ad').addClass('in active');
        }
        $(document).on('click', '.save_company_ad', function () {           
            $('.msg-box').removeClass("alert-success").removeClass("alert-danger");
            $('.msg-box').hide();
            var form = document.forms.namedItem("company-ad-form"); 
            var formdata = new FormData(form);
            $('.loading_gif').show();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_ad_image")}}',
                contentType: false,
                processData: false,
                data: formdata,
                success: function (result) {
                    $("#msg-list").empty();
                    if (result.flag === 0) {
                        var errorArray = result.errors;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                    } else if (result.flag == 1) {
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                    }
                    $('.loading_gif').hide();
                }
            });
        });
    });
</script> @stop @endsection
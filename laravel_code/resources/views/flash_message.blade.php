@if(count($errors) > 0)
        <div class="col-sm-12 flash_message_div">
            <div class="alert alert-danger alert-list" style="background-color: #161616;
    border-color: #ffb6ac;
    color: #fefefe;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
@endif 
@if(\Session::has('status'))
    <div class="col-sm-12 flash_message_div">
        <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ \Session::get('status')}}</p>
        </div>
    </div>
@endif
@if(\Session::has('error'))
    <div class="col-sm-12 flash_message_div">
        <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <p>{{ \Session::get('error')}}</p>
        </div>
    </div>
@endif
@extends('oam.app') @section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet">
<style type="text/css">

.star-rating {
  line-height:32px;
  font-size:1.25em;
}

.star-rating .fa-star{color: yellow;}
</style>
 @stop @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <!-- <a href="{{url('/oam/job_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Job</a> -->
        </div>
        <h4 class="page-title"></h4>
    </div>
</div>
@if(\Session::has('status'))
<div class="alert alert-success">
    <p>{{ \Session::get('status')}}</p>
</div>
@endif

<div class="row">
    <div class="col-xl-9">
        <div class="card page_box_shadow">
            <div class="card-header sidebar_header">

                <div class="pull-left">
                    <h4 class="header-title job_listing_heading m-b-0"> {{isset($title)?$title:''}}</h4>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="card-block description-font page_box_shadow">
                <div class="col-lg-9">
                    <p class="job_detail_description lead m-b-0 font-16 ">{{isset($detail->job_title)?$detail->job_title:''}}
                        <span class="text-muted  font-13">{!! date_format(new DateTime($detail->created_at), 'jS F Y g:ia') !!}</span>
                    </p>
                    <p class=" m-b-0 text-justify">{!! strip_tags(isset($detail->description)?$detail->description:'' )!!}</p>
                    <div class="m-t-10">
                        <p class="lead m-b-0 font-13 ">
                            <strong>Category</strong>
                            <span class="label label-primary">@if(isset($job_category_name) && count($job_category_name) > 0)
                                      @foreach($job_category_name as $cat_name)
                                        {{$cat_name->name}}&nbsp;&nbsp; 
                                        @endforeach
                                        @endif</span>
                        </p>
                        <p class="lead m-b-0 font-13 ">
                            <strong>Sub Category</strong>
                            <span class="label label-primary">@if(isset($job_sub_category) && count($job_sub_category) > 0)
                                        @foreach($job_sub_category as $sub_cat_name)
                                        {{$sub_cat_name->sub_category}}&nbsp;&nbsp; 
                                        @endforeach
                                        @endif</span>
                        </p>
                        <p class="lead m-b-0 font-13 ">
                            <strong>Service Code</strong>
                            <span class="label label-primary">@if(isset($job_service_code) && count($job_service_code) > 0)
                                        @foreach($job_service_code as $service_code_name)
                                        {{$service_code_name->service_code_name}}&nbsp;&nbsp; 
                                        @endforeach
                                        @endif</span>
                        </p>
                        <p class="lead m-b-0 font-13 ">
                            <strong>Service Name</strong>
                            <span class="label label-primary"> @if(isset($job_category) && count($job_category) > 0)
                                        @foreach($job_category as $cat_name)
                                        {{$cat_name['job_category']->service_name}}&nbsp;&nbsp; 
                                        @endforeach
                                        @endif</span>
                        </p>
                    </div>
                    <div class="m-t-10">
                        <p class="lead m-b-0 font-13 ">
                            <strong>Job Type: </strong>
                            <span class="font-13 m-b-0 text-muted">{{isset($detail->job_type)?$detail->job_type:''}}</span>
                        </p>
                    </div>
                    <div class="pull-left m-t-10">
                        <button type="button" class="btn btn-sm btn-primary-outline waves-effect waves-light show_full_detail">
                            <i class="fa fa-arrow-circle-o-down m-r-5"></i>
                            <span>Show Detail</span>
                        </button>
                        <button type="button" class="btn btn-sm btn-primary-outline waves-effect waves-light show_less_detail" style="display:none">
                            <i class="fa fa-arrow-circle-o-up m-r-5"></i>
                            <span>Less Detail</span>
                        </button>

                    </div>

                    
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="card-block description-font page_box_shadow" id="full_details" style="display: none;">
                <div class="col-md-6">
                    <p class="lead  text-primary  m-b-0">Building Detail</p>
                    <div class="col-md-12">
                        <p class="lead font-13   m-b-0">
                            <b>Building:</b>
                            <span class="font-13 m-b-0 text-muted">{{isset($building->name)?$building->name:''}}</span>
                        </p>

                        <div class="m-t-10">
                            <p class="lead font-13  m-b-0">
                                <b>Building Assets:</b>
                                @if(isset($building->assets) && !empty($building->assets)) @foreach(explode(',',$building->assets) as $b_asset)
                                <span class="label  label-primary">{{$b_asset}}</span>
                                @endforeach @endif
                            </p>
                        </div>
                        <div class="m-t-10">
                            <p class="lead font-13  m-b-0">
                                <b>Building Type:</b>
                                <span class="font-13 m-b-0 text-muted">{{isset($building->type)?$building->type:''}} </span>
                            </p>
                        </div>

                    </div>
                </div>

                <div class="col-md-6">
                    <p class="lead  text-primary  m-b-0">Point of Contact</p>
                    <div class="col-md-12">
                        <p class="lead  font-13  m-b-0">
                            <b>Name:</b>
                            @if(isset($detail->contact_name) && !empty($detail->contact_name))
                            <span class="font-13 m-b-0 text-muted">
                                @foreach(json_decode($detail->contact_name) as $name) {{$name. ", "}} @endforeach
                            </span>
                            @endif
                        </p>
                        <!-- <p class="font-13 m-b-0 text-dark">Omer Hayat</p> -->

                        <div class="m-t-10">
                            <p class="lead font-13  m-b-0">
                                <b>Email:</b>
                                @if(isset($detail->contact_email) && !empty($detail->contact_email))
                                <span class="font-13 m-b-0 text-muted">
                                    @foreach(json_decode($detail->contact_email) as $email) {{$email. ", "}} @endforeach
                                </span>
                                @endif
                            </p>
                        </div>
                        <div class="m-t-10">
                            <p class="lead font-13  m-b-0">
                                <b>Contact Number:</b>
                                @if(isset($detail->contact_number) && !empty($detail->contact_number))
                                <span class="font-13 m-b-0 text-muted">
                                    @foreach(json_decode($detail->contact_number) as $contact) {{$contact}} @endforeach
                                </span>
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="lead m-t-10  text-primary  m-b-0">Payment Term</p>
                    <div class="col-md-12">
                        <p class="lead font-13  m-b-0">
                            <b>Payment Via: </b>
                            <span class="font-13 m-b-0 text-muted">{{isset($detail->payment_terms)?$detail->payment_terms:''}} </span>
                        </p>
                    </div>
                </div>
                    <div class="col-md-12">
                    <div class="m-t-10">
                        <p class="lead text-primary m-t-10  m-b-0">Job Requirement</p>
                        <div class="col-md-12">
                            <p class="lead font-13  m-b-0">
                                <b>Staffing:</b>
                                <!-- <aclass="font-13 m-b-0 text-primary">blsjbkujbkg</p> -->
                                <span class="font-13 m-b-0 text-muted">{!! isset($detail->staffing)?$detail->staffing:'' !!}</span>
                            </p>
                            <p class="lead font-13 m-b-0">
                                <b>Uniform:</b>
                                <span class="font-13 m-b-0 text-muted">{!! isset($detail->uniform)?$detail->uniform:'' !!}</span>
                            </p>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12">
        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
                Job Stats
            </h4>
            @if(Auth::user()->role_id == 2) @if(isset($jobApplicationCount))
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item">
                    <a href="#" class="card-link">
                        <i class="fa fa-check-square"></i>
                        <span> Applied</span> {{$jobApplicationCount }}</a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="card-link">
                        <i class="fa fa-list-alt"></i>
                        <span> Shortlisted</span> {{ $shortListCount }}</a>
                </li>
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                        <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
            </ul>
            @endif @endif
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item">
                    <a href="#" class="card-link">
                        <i class="fa fa-check-square"></i>
                        <span> Job Progress : </span> {!! ($detail->job_status == 0) ? '<span class="label label-primary">In Progress</span>' : '<span class="label label-success">Job Complete</span>' !!}</a>
                </li>
                @if($detail->job_status == 0)
                <li class="list-group-item">
                    <a href="javascript:void(0)" class="btn btn-success show_review_modal">
                        <i class="fa fa-list-alt"></i>
                        <span> Job Complete ? </span></a>
                </li>
                @endif
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                        <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-9">
        <div class="card">
            <h4 class="card-header sidebar_header m-b-0">
                Job Assigned To Company
            </h4>
        </div>
    </div>
</div>
@if(isset($jobApplicationRecord)) @foreach($jobApplicationRecord as $key => $value)
<div class="row">
    <div class="col-xs-9">
        <div class="card-box widget-user">
            <div>
                <div class="col-md-2">
                    <div class="fmc_company_image">
                        <img class="img-responsive img-circle" src="{{ asset('laravel_code/storage/app/public/user_image/'.$value['users']['image']) }}"
                        alt="{{ $value['users']['image'] }}">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="fmc_company_detail">
                        <div class="wid-u-info description-font  fmc_application_section">
                            <div class="pull-left">
                                <h5 class="m-t-10 m-b-5"><a href="{{url('oam/fmc/company/details/'.$value['user_id'])}}"> {{ $value['companies']['name'] }}</a> </h5>
                                <p class="m-b-0 font-13"><b>Applied Date :</b>
                                    <span class="text-muted  font-13">{!! date_format(new DateTime($value['created_at']), 'jS F Y g:ia') !!}</span>
                                </p>
                                <p class="text-muted m-b-0 font-13">{{$value['users']['email']}}</p>
                                
                                <!-- <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-primary-outline waves-effect waves-light short_list">Short List</button>
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-secondary waves-effect decline_for_job">Reject</a> -->
                                
                                @if($value['status'] == 1)
                                    <p class="text-muted m-b-0 font-13">Status : <span class="label label-pill label-primary">Job Completed</span></p>
                                @endif
                                @if($value['status'] == 3)
                                    <p class="text-muted m-b-0 font-13">Status : <span class="label label-pill label-primary">Short Listed</span></p>
                                @endif
                                @if($value['status'] == 4)
                                    <p class="text-muted m-b-0 font-13">Status : <span class="label label-pill label-success">Job Assigned</span></p>
                                @endif
                                @if($value['status'] == 5)
                                    <p class="text-muted m-b-0 font-13">Status : <span class="label label-pill label-danger">Rejected</span></p>
                                @endif
                            </div>
                            @if($value['status'] == 1)
                            <div class="pull-right">
                                <div class="m-t-15">
                                    <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-block btn-primary-outline waves-effect waves-light short_list">Short List</a>
                                    <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-block btn-danger-outline waves-effect decline_for_job">Reject</a>
                                 </div>
                            </div>
                            @endif
                            @if($value['status'] == 3) @if($value['report_status'] == 1)
                            <div class="pull-right">
                                <div class="m-t-15">
                                    <input type="hidden" name="assing_company_name" value="{{$value['companies']['name'] }}" id="assing_company_name">
                                    <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-block btn-primary-outline waves-effect waves-light assing_job_to_company">Assign Job</a>
                                </div>
                            </div>
                            @endif @endif
                        </div>
                    </div>
                </div>
            <div class="clearfix"></div>
            </div>
        </div>
    </div>
<!-- </div> -->

<div class="modal fade md_job_complete in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myLargeModalLabel">Please Give the Review and Rating</h4>
        </div>
        
        <div class="modal-body">
            <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('oam/job/fmc/company/rating') }}" name="reject_company">
                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="job_id" value="{{$detail->id}}">
                <input type="hidden" name="user_uuid" value="{{$jobApplicationRecord[0]['user_id']}}">
                <!-- <input type="hidden" name="id" value="{{Request::input('id')}}"> -->
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card job_review page_box_shadow">
                            <div class="card-header font-16 ">
                                <b>Leave Feedback</b>
                            </div>
                            <div class="card-block">


                                <div class="col-md-10">
                                    <p class="lead font-13 m-b-10">
                                        Please leave feedback rate
                                        <a href="#">fmc Company url </a> for the project
                                        <a href="#">Project name/url </a>
                                    </p>
                                    <div class=" m-b-0">
                                        <div class="col-sm-8">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th class="font-13">Company Rating</th>
                                                    <td colspan="2">
                                                        <div class="text-xs-left">
                                                            <div class="star-rating">
                                                                <span class="fa fa-star-o" data-rating="1"></span>
                                                                <span class="fa fa-star-o" data-rating="2"></span>
                                                                <span class="fa fa-star-o" data-rating="3"></span>
                                                                <span class="fa fa-star-o" data-rating="4"></span>
                                                                <span class="fa fa-star-o" data-rating="5"></span>
                                                                <input type="hidden" name="job_rating" class="rating-value" value="2">
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        <div class="col-sm-8"> 
                                            <p class=" font-13 m-b-0">
                                               <b>Comment:</b>
                                            </p>
                                            <textarea class="form-control" id="jobdescription" rows="3" name="job_description" placeholder="Job Description"></textarea>
                                        </div>
                                        
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-sm-8 m-t-0"> 
                        <button type="submit" class="btn btn-success waves-effect waves-light submit_job_post">
                            Rate company
                        </button>     
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
@endforeach @endif @section('js')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
<script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script type="text/javascript">
    var date = new Date();
    date.setDate(date.getDate() - 1);
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date
    });
    $(document).ready(function () {
        var get_uuid = $('#uuid').val();
        $(document).on('click', '.apply_for_job', function () {
            $('#staus').val(1);
            $('.loading_gif').show();
            $('#job_form').submit()[0];
        });
        $(document).on('click', '.decline_for_job', function () {
            $('#staus').val(2);
            $('.loading_gif').show();
            $('#job_form').submit()[0];
        });
        $(document).on('click', '.show_full_detail', function () {
            $('.show_full_detail').hide();
            $('.show_less_detail').show();
            $("#full_details").slideToggle("slow", function () {
                // Animation complete.
            });
        });
        $(document).on('click', '.show_less_detail', function () {
            $('.show_less_detail').hide();
            $('.show_full_detail').show();
            $("#full_details").slideToggle("slow", function () {
                // Animation complete.
            });
            $('.show_full_detail').show();
        });
        $(document).on('click', '.short_list', function () {
            var user_id = $(this).attr('id');
            $('.sl_company_name').text('');
            var company_name = $(this).parent('.fmc_application_section').find('h5').html();
            // console.log(company_name);
            $('#sl_company_name').text(company_name);
            $('#user_uuid').val(user_id);
            $('.add_shortlisted_company').modal('show');

        });

        $(document).on('click','.decline_for_job', function(){
            var user_id = $(this).attr('id');
            var company_name = $(this).parent('.fmc_application_section').find('h5').html();
            $('#rj_company_name').text('');
            $('#rj_company_name').text('Are You Sure you Want Reject Company '+company_name);
            $('.rj_company_name').text('');
            $('.rj_company_name').text(company_name);
            console.log(user_id);
            $('#rl_user_uuid').val(user_id);
            console.log($('#rl_user_uuid').val());
            $('.md_reject_company').modal('show');
        });

        $(document).on('click', '.assing_job_to_company', function(){
            var user_id = $(this).attr('id');
            var company_name = $(this).parent('.fmc_application_section').find('#assing_company_name').val();
            $('#assign_company_detail').text('');
            $('#assign_company_detail').html('Are you sure you want to Assign Job to Company <u>'+ company_name +'</u>');
            $('#assign_user_uuid').val(user_id);
            $('.assign_compnay_job_md').modal('show');
        });

        $(document).on('click', '.show_review_modal', function(){
            $('.md_job_complete').modal('show');
        });
        tinymce.init({
            selector: '#jobdescription',
            height: 150,
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            }
        });

        var $star_rating = $('.star-rating .fa');

        var SetRatingStar = function() {
          return $star_rating.each(function() {
            if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
              return $(this).removeClass('fa-star-o').addClass('fa-star');
            } else {
              return $(this).removeClass('fa-star').addClass('fa-star-o');
            }
          });
        };

        $star_rating.on('click', function() {
          $star_rating.siblings('input.rating-value').val($(this).data('rating'));
          return SetRatingStar();
        });

        SetRatingStar();
    })
</script> @endsection
<!-- end row -->
@endsection
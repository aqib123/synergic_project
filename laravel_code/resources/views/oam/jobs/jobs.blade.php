@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app')) @section('content')
<!-- Page-Title -->
@section('css')
<style>
.list_detail {
    border: none;
    padding: 7px 8px 2px 8px;
}

.list_desgin {
    margin-bottom: 5px;
    background-color: #F9F9F9;
    padding: 10px 5px !important;
    border: 1px solid #ccc !important;
    border-radius: 2px;
}

.job_description_title {
    font-size: 13px;
    font-weight: 600 !important;
    color: #000 !important;
}

.text_dark_blue {
    color: #0087e0;
}

.job_description_detail {
    color: #515151 !important;
}

.font-11 {
    font-size: 11px;
}

.job_stats {
    margin-right: 15px;
}
.custom_size{
    padding: 0.50rem 1rem;
    font-size: 0.675rem;
}
.inbox-widget .inbox-item
{
    box-shadow: 8px 7px #80808059;
}
</style>
@endsection
<div class="row">
    <div class="col-sm-12">
        @if(GeneralFunctions::check_view_permission('/oam/job_form'))
            @if(Auth::user()->role_id == 2)
                <div class="btn-group pull-right m-t-15">
                    <a href="{{url('/oam/job_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Post New Job Ad</a>
                </div>
            @endif
        @endif
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@php

@endphp
@include('flash_message')
<div class="row">
    <div class="col-xl-9">
        <ul class="nav association_nav  nav-tabs m-b-0" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pending-job-tab" data-toggle="tab" href="#pending_jobs" role="tab" aria-controls="pending-job-tab"   aria-expanded="true">Pending</a>
            </li>
 <li class="nav-item">
                <a class="nav-link " id="approved-job-tab" data-toggle="tab" href="#approved_jobs" role="tab" aria-controls="approved-job-tab"
                  >Approved</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="review-job-tab" data-toggle="tab" href="#review_jobs" role="tab" aria-controls="review-job-tab">Reviewed jobs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="review-job-tab" data-toggle="tab" href="#expired_jobs" role="tab" aria-controls="expired-job-tab">Expired jobs</a>
            </li>
        </ul>
        <div class="card page_box_shadow">
            <!-- <div class="card-header sidebar_header">
                <div class="clearfix"></div>
            </div> -->
            <div class="card-block list_detail  ">
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div>
                            @if(isset($jobs) && count($jobs)>0)
                            <div class="tab-content" id="myTabContent">
                                <!-- Approved Job Tab -->
                                <div role="tabpanel" class="tab-pane fade  " id="approved_jobs" aria-labelledby="approved-job-tab">
                                    <!-- Approved Job Content -->
                                    <div class="inbox-widget nicescroll" style="min-height: 478px;">
                                        @php $j = 0; @endphp @foreach($jobs as $row)
                                      <!--   @php
                                            $is_assigned = \App\JobApply::where('job_id', $row->id)->where('status', 4)->get();
                                            $is_assigned = count($is_assigned);
                                        @endphp
                                        if($row->admin_approval == 1 && $is_assigned < 1)
                                         -->
                                        @if($row->admin_approval == 1 )
                                        <div>
                                                <a class="custom_links" href="{{url('oam/oam_job_details/'.$row->id)}}">
                                                <div class="inbox-item list_desgin">
                                                    <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                                        <p class="inbox-item-author pull-left job_description_title">
                                                            @if(Auth::user()->role_id == 1)
                                                                <a class="custom_links" href="{{url('oam_job_details/'.$row->id)}}">{{substr($row->job_title,0,20)}}....</a>
                                                            @else
                                                    {{substr($row->job_title,0,20)}}....
                                                            @endif
                                                        </p>
                                                        <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                                            @if(GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date) == 'Job Post Expired')
                                                            {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                            $row->admin_approval_date) !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                            $row->admin_approval_date) !!} @endif
                                                        </p>
                                                    </div>
                                                    <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,100)) }}</p>

                                                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->admin_approval_date), 'jS F Y'); @endphp</p>

                                                    <div class="job_stats">
                                                        <ul class="list-inline m-b-0  text-xs-left">
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                                                  Applied {{ $row->jobApply->count()}}
                                                            </li>
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{ $row->count_views}}  </li>
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item"> {!! GeneralFunctions::short_list($row->id) !!}</li>
                                                        </ul>

                                                    </div>
                                                </div>

                                            </a>
                                            @if(Auth::user()->role_id == 1)
                                                <div class="col-lg-3 m-t-10">
                                                    <div class="pull-right ">
                                                        @php
                                                        $deleteUrl = '/delete_job/'.$row->id;
                                                        $editUrl = '/job_form?id='.$row->id;
                                                        if(Auth::user()->role_id == 2){
                                                            $deleteUrl = '/oam/delete_job/'.$row->id;
                                                            $editUrl = '/oam/job_form?id='.$row->id;
                                                        }
                                                        @endphp
                                                        <!-- <a href="{{url($deleteUrl)}}" type="button" class="btn btn-sm btn-danger-outline waves-effect waves-light right" style="margin-left:5px; ">Delete</a> -->
                                                        <a href="{{url($editUrl)}}" class="btn btn-sm btn-dark-outline waves-effect waves-light right">Edit</a>
                                                        </div>
                                                </div>
                                            @endif
                                            <div class="clearfix"></div>
                                        </div>
                                        @php $j++; @endphp
                                        @endif
                                        @endforeach
                                        @if($j < 1)
                                        <div class="col-md-12">
                                            <div class="text-xs-center">
                                                <div class="p-20">
                                                    <p class="card-title lead">Empty approved job listing</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>

                                    <!-- End Approved Job Content -->
                                </div>
                                <!-- End Approved Job Tab -->

                                <!-- Pending Job Tab -->
                                <div class="tab-pane fade in active" id="pending_jobs" role="tabpanel" aria-labelledby="pending-job-tab" aria-expanded="true">
                                    <!-- Pending Job Content -->
                                    <div class="inbox-widget nicescroll" style="min-height: 478px;">
                                        @php $i = 0; @endphp @foreach($jobs as $row) @if($row->admin_approval == 0)
                                        <div class=" ">
                                                <div class="inbox-item list_desgin">
                                                    <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                                        <p class="inbox-item-author pull-left job_description_title">
                                                            @if(Auth::user()->role_id == 1)
                                                                <a class="custom_links" href="{{url('oam_job_details/'.$row->id)}}">{{$row->job_title}}</a>
                                                             @else
                                                                <a class="custom_links" href="{{url('oam/oam_job_details/'.$row->id)}}" data-toggle="tooltip" title="Posteb By {{$row['user_tooltip']['name'] != null ? $row['user_tooltip']['name'] : $creator}}">{{$row->job_title}}</a>
                                                             @endif
                                                        </p>
                                                        <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                                            {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,$row->admin_approval_date) !!}
                                                        </p>
                                                    </div>
                                                    <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,100)) }}</p>

                                                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y'); @endphp</p>

                                                    <div class="job_stats">
                                                        <ul class="list-inline m-b-0  text-xs-left">
                                                            @php
                                                                $deleteUrl = '/delete_job/'.$row->id;
                                                                $editUrl = '/job_form?id='.$row->id;
                                                                if(Auth::user()->role_id == 2){
                                                                    $deleteUrl = '/oam/delete_job/'.$row->id;
                                                                    $editUrl = '/oam/job_form?id='.$row->id;
                                                                }
                                                            @endphp
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item"><a href="{{url($editUrl)}}" class="custom_size btn btn-sm btn-primary-outline waves-effect waves-light  ">Edit</a>
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                                                    <a href="{{url($deleteUrl)}}" type="button" class="btn custom_size btn-sm btn-danger-outline waves-effect waves-light  ">Delete</a>

                                                            </li>
                                                            </li>
                                                        </ul>

                                                    </div>
                                                </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    @php $i++; @endphp
                                    @endif
                                    @endforeach
                                    @if($i < 1)
                                    <div class="col-md-12">
                                        <div class="text-xs-center">
                                            <div class="p-20">
                                                <p class="card-title lead">Empty pending job listing</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                                <!-- End Pending Job Content -->

                                <!-- Review Job Tab -->
                                <div class="tab-pane fade" id="review_jobs" role="tabpanel" aria-labelledby="review-job-tab">
                                    <!-- Review Job Content -->
                                    <div class="inbox-widget nicescroll" style="min-height: 478px;">
                                        @php $i = 0; @endphp @foreach($jobs as $row) @if($row->admin_approval == 3)
                                        <a class="custom_links" href="{{url('oam/oam_job_details/'.$row->id)}}" data-toggle="tooltip" title="Posteb By {{$row['user_tooltip']['name'] != null ? $row['user_tooltip']['name'] : $creator}}">
                                        <div class="  ">
                                            <div class="inbox-item list_desgin">
                                                <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                                    <p class="inbox-item-author pull-left job_description_title">
                                                        @if(Auth::user()->role_id == 1)
                                                            <a class="custom_links" href="{{url('oam_job_details/'.$row->id)}}">{{$row->job_title}}</a>
                                                         @else
                                                           {{$row->job_title}}
                                                         @endif
                                                    </p>
                                                    <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                                        @if(GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date) == 'Job Post Expired')
                                                        {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                        $row->admin_approval_date) !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                        $row->admin_approval_date) !!} @endif
                                                    </p>
                                                </div>
                                                <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,100)) }}</p>

                                                <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y'); @endphp</p>

                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        </a>
                                    @php $i++; @endphp
                                    @endif
                                    @endforeach
                                    @if($i < 1)
                                    <div class="col-md-12">
                                        <div class="text-xs-center">
                                            <div class="p-20">
                                                <p class="card-title lead">Empty Review job listing</p>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <!-- End Review Job Content -->
                            </div>
                            <!-- End Review Job Tab -->

                            <!-- Expired Jobs -->
                                <div class="tab-pane fade" id="expired_jobs" role="tabpanel" aria-labelledby="expired-job-tab">
                                    <!-- Review Job Content -->
                                    <div class="inbox-widget nicescroll" style="min-height: 478px;">
                                        @php $j = 0; @endphp @foreach($jobs as $row)
                                        @php
                                            $is_assigned = \App\JobApply::where('job_id', $row->id)->where('status', 4)->get();
                                            $is_assigned = count($is_assigned);
                                        @endphp
                                        @if($row->job_expiry == 1)
                                        <div>
                                                <a class="custom_links" href="{{url('oam/oam_job_details/'.$row->id)}}" data-toggle="tooltip" title="Posteb By {{$row['user_tooltip']['name'] != null ? $row['user_tooltip']['name'] : $creator}}">
                                                <div class="inbox-item list_desgin">
                                                    <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                                        <p class="inbox-item-author pull-left job_description_title">
                                                            @if(Auth::user()->role_id == 1)
                                                                <a class="custom_links" href="{{url('oam_job_details/'.$row->id)}}">{{substr($row->job_title,0,20)}}....</a>
                                                            @else
                                                    {{substr($row->job_title,0,20)}}....
                                                            @endif
                                                        </p>
                                                        <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                                            @if(GeneralFunctions::days_remaining_in_expiration($row->purposal_validity, $row->admin_approval_date) == 'Job Post Expired')
                                                            {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                            $row->admin_approval_date) !!} @else {!! GeneralFunctions::days_remaining_in_expiration($row->purposal_validity,
                                                            $row->admin_approval_date) !!} @endif
                                                        </p>
                                                    </div>
                                                    <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,100)) }}</p>

                                                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->admin_approval_date), 'jS F Y'); @endphp</p>

                                                    <div class="job_stats">
                                                        <ul class="list-inline m-b-0  text-xs-left">
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                                                  Applied {{ $row->jobApply->count()}}
                                                            </li>
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{ $row->count_views}}  </li>
                                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item"> {!! GeneralFunctions::short_list($row->id) !!}</li>
                                                        </ul>

                                                    </div>
                                                </div>

                                            </a>
                                            <div class="clearfix"></div>
                                        </div>
                                        @php $j++; @endphp
                                        @endif
                                        @endforeach
                                        @if($j < 1)
                                        <div class="col-md-12">
                                            <div class="text-xs-center">
                                                <div class="p-20">
                                                    <p class="card-title lead">Empty Expired job listing</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                <!-- End Review Job Content -->
                            </div>
                        </div>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-sm-3 col-lg-3 col-xs-12">
    <div class="card page_box_shadow m-t-40">
        <h4 class="card-header sidebar_header m-b-0">
            Job Stats
        </h4>
        <ul class="list-group list-group-flush sidbar_icon">
            <li class="list-group-item">
                    <!-- <i class="fa fa-check-square"></i> -->
                    <span> Applied</span> 0
            </li>
            <li class="list-group-item">
                    <span> Shortlisted</span> 0
            </li>
            <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                        <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
        </ul>


    </div>
    <div class="m-t-10">
        <img class="card-img-top img-fluid" src="{{ asset('laravel_code/storage/app/public/company_ad/'.$ad_image) }}"  alt="add banner">
    </div>
</div>
</div>
<!-- end row -->
@section('js')
<script>
$(document).ready(function () {

    //approved-job-tab
    var tab = '{{Request::get("tab")}}';
    if(tab == 'active'){
        $("#pending-job-tab").removeClass('active');
        $("#approved-job-tab").addClass('active');
        $("#pending_jobs").removeClass('in active');
        $("#approved_jobs").addClass(' in active');
    }

    $('[data-toggle="tooltip"]').tooltip();
    var role_id = "{{Auth::user()->role_id}}";
    if(role_id == 1)
    {
        $("#pending-job-tab").addClass("active");
        $("#approved-job-tab").removeClass("active");

    }
   });
</script>
@endsection
@endsection

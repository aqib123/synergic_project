@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('css')
<!-- Include SmartWizard CSS -->
<link href="{{ URL::asset('assets/css/smart_wizard/smart_wizard.css') }} " rel="stylesheet">
<!-- Optional SmartWizard theme -->
<link href="{{ URL::asset('assets/css/smart_wizard/smart_wizard_theme_circles.css') }} " rel="stylesheet">
<link href="{{ URL::asset('assets/css/smart_wizard/smart_wizard_theme_arrows.css') }} " rel="stylesheet">
<link href="{{ URL::asset('assets/css/smart_wizard/smart_wizard_theme_dots.css') }} " rel="stylesheet">
@endsection
@section('content')
@php $payment_terms = []; $payment_terms_des = []; $contact_name
= []; $contact_email = []; $contact_number = []; if(isset($job->id))
{ $payment_terms = ($job->payment_term != null) ? json_decode($job->payment_term
, true) : array(); $payment_terms_des = ($job->payment_percentages != null) ? json_decode($job->payment_percentages, true)
: array(); $contact_name = ($job->contact_name != null) ? json_decode($job->contact_name , true) : array(); $contact_email
= ($job->contact_email != null) ? json_decode($job->contact_email , true) : array(); $contact_number = ($job->contact_number
!= null) ? json_decode($job->contact_number , true) : array(); }
@endphp
<style>
.error-message {
    list-style-type: none !important;
}

.staffing_switch {
    margin-top: 10px;
}

.staffing_button {
    margin-right: 22px;
}

.open_all_switch {
    margin-top: 17px;
}
.fixed_width{
    width:30px;
}
.fixed_width2{
    width: 20%;
}
.term_condition_area{
    /* height: 600px; */
}
.term_condition_content{
    height:450px;
    overflow:auto;
}

.sw-theme-dots > ul.step-anchor::before {
    right: 0px !important;
    top: 62px !important;
}
.sw-theme-dots > ul.step-anchor > li > a::before {
    bottom: 1px;
    left: 36%;
}
.sw-theme-dots .sw-toolbar {
    padding-left: 9px;
    padding-bottom: 15px;

}
.togl-height {
    height: 60px;
}
</style>

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url('/oam/jobs')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Jobs</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <div class="msg-box alert" style="display: none;">
        <ul style="text-decoration: none;" id="msg-list">

        </ul>
    </div>
    @include('flash_message')
    @if(Auth::user()->role_id == 2)
    <form role="form" id="job_form_data" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('oam/save_job') }}">
        @else
        <form role="form" id="job_form_data" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('save_job') }}">
            @endif

            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
            <div id="smartwizard" class="col-sm-12 col-xs-12 col-md-12">
                <ul>
                    <li><a href="#step-1">Step 1<br /><small>Job Detail & Category</small></a></li>
                    <li><a href="#step-2">Step 2<br /><small>Communities & Point of Contact</small></a></li>
                    <li><a href="#step-3">Step 3<br /><small>Payment Terms & Terms and Conditions</small></a></li>
                </ul>
                <div>
                    <div id="step-1" class="">
                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        <div class="p-20">
                            <div class="col-md-12">
                                <div class="p-10">
                                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                                    <input type="hidden" name="new_row" class="new_row" value="0">
                                    <input type="hidden" name="total_percentage" class="total_percentage" value="{{isset($job->id)? 100 : 0}}">
                                    <input type="hidden" name="upload_files" class="upload_files" value="0">
                                    <input type="hidden" name="page" class="page" value="1">
                                    {{ csrf_field() }}
                                    <div class="form-group row">
                                        <h5 class="m-t-10 header-title">Job Detail</h5>
                                        <hr>
                                        <div class="col-sm-4 padding-left-0 padding-right-0">
                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Job Title
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-sm-12">
                                                <input type="text" required parsley-type="text" placeholder="Job Title" name="job_title" value="{{old('job_title')}}{{isset($job->job_title)?$job->job_title:''}}"
                                                class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4 padding-left-0 padding-right-0">
                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Purposal Validity
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select class="form-control" name="purposal_validity">
                                                    <option value="15" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 15 ? 'selected' : '') : '' !!}>15 Days</option>
                                                    <option value="30" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 30 ? 'selected' : '') : '' !!}>30 Days</option>
                                                    <option value="45" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 45 ? 'selected' : '') : '' !!}>45 Days</option>
                                                    <option value="60" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 60 ? 'selected' : '') : '' !!}>60 Days</option>
                                                    <option value="75" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 75 ? 'selected' : '') : '' !!}>75 Days</option>
                                                    <option value="90" {!! isset($job->purposal_validity) ? ($job->purposal_validity == 90 ? 'selected' : '') : '' !!}>90 Days</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-sm-4 padding-left-0 padding-right-0">
                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Job Reporting
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-sm-12">
                                                <select class="form-control" name="job_reporting">
                                                    <option selected="selected">Weekly</option>
                                                    <option>Monthly</option>
                                                    <option>Yearly</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="job type" class="col-sm-12 form-control-label">Select Job Type</label>
                                        <div class="col-sm-12">

                                            <div class="radio radio-info radio-inline">
                                                <input class="select_job_type" id="annual_maintenance" value="Annual Maintenance" name="job_type" @if(isset($job->job_type) && !empty($job->job_type) && $job->job_type == "Annual Maintenance") checked='checked'
                                                @endif type="radio">
                                                <label for="annual_maintenance">Annual Maintenance</label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input class="select_job_type" id="breakdown_maintenance" value="Breakdown Maintenance" name="job_type" @if(isset($job->job_type) && !empty($job->job_type) && $job->job_type == "Breakdown Maintenance") checked='checked'
                                                @endif type="radio">
                                                <label for="breakdown_maintenance">Breakdown Maintenance</label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input class="select_job_type" id="enhancement_maintenance" value="Enhancement Maintenance" name="job_type" @if(isset($job->job_type) && !empty($job->job_type) && $job->job_type == "Enhancement Maintenance") checked='checked'
                                                @endif type="radio">
                                                <label for="enhancement_maintenance">Enhancement Maintenance</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Job Expiry
                                            </label>
                                            <select class="form-control job_expiry" name="job_expiry" id="job_expiry" >

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-12 padding-left-0 padding-right-0">
                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Job Description
                                                <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-sm-8">
                                                <!-- id="jobdescription" -->
                                                <textarea class="form-control" id="jobdescription" rows="3" name="job_description" placeholder="Job Description">{{isset($job->description)?$job->description:''}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                            <div class="p-20">
                                <div class="col-md-12">
                                    <div class="p-10">
                                        <input type="hidden" name="id" value="{{Request::input('id')}}">
                                        <div class="form-group row">
                                            <h5 class="m-t-10 header-title">Job Category</h5>
                                            <hr>
                                            @if(count($job_categories_details) > 0) @foreach($job_categories_details as $key => $value) @if($key == 0)
                                            <div class="job_category_div">
                                                <div class="row">
                                                    <div class="col-sm-3 padding-left-0 padding-right-0">
                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Category
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control jc_category" name="category[]">
                                                                <option value="">Select Value</option>
                                                                @foreach($new_categories as $key => $focal_value)
                                                                <option <?php echo ($value['job_category']['category_id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">
                                                                    {{$focal_value['name']}}
                                                                </option>
                                                                @endforeach
                                                            </select>
                                                            {{ csrf_field() }}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 padding-left-0 padding-right-0">
                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Sub Category
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control jc_sub_category" name="sub_category[]">
                                                                <option value="">Select Value</option>
                                                                @foreach($value['get_sub_categories'] as $key => $focal_value)
                                                                <option <?php echo ($value['job_category']['sub_category_id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">{{$focal_value['sub_category']}}</option>
                                                                @endforeach
                                                            </select>
                                                            <input type="hidden" name="page" class="page" value="1">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 padding-left-0 padding-right-0">
                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Service Name
                                                            <span class="text-danger">*</span>
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <select class="form-control jc_service_name" name="service_name[]">
                                                                <option value="">Select Value</option>
                                                                @foreach($value['get_service_name'] as $key => $focal_value)
                                                                <option <?php echo ($value['job_category']['id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">{{$focal_value['service_name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 padding-left-0 padding-right-0">
                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 add_job_cat_detaial">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-plus"></i>
                                                                </span>Add More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="job_category_div">
                                                    <div class="row">
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Category
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control jc_category" name="category[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($new_categories as $key => $focal_value)
                                                                    <option <?php echo ($value['job_category']['category_id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">{{$focal_value['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Sub Category
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control jc_sub_category" name="sub_category[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($value['get_sub_categories'] as $key => $focal_value)
                                                                    <option <?php echo ($value['job_category']['sub_category_id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">{{$focal_value['sub_category']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Service Name
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control jc_service_name" name="service_name[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($value['get_service_name'] as $key => $focal_value)
                                                                    <option <?php echo ($value['job_category']['id'] == $focal_value['id']) ? 'selected="selected"' : '' ?> value="{{$focal_value['id']}}">{{$focal_value['service_name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_job_cat_detaial">
                                                                    <span class="btn-label">
                                                                        <i class="fa fa-trash"></i>
                                                                    </span>Clear</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif @endforeach @else
                                                    <div class="job_category_div">
                                                        <div class="row">
                                                            <div class="col-sm-2 padding-left-0 padding-right-0">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">Category
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-sm-12">
                                                                    <select class="form-control jc_category" name="category[]">
                                                                        <option value="">Select Value</option>
                                                                        @foreach($new_categories as $key => $focal_value)
                                                                        <option value="{{$focal_value['id']}}">{{$focal_value['name']}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 padding-left-0 padding-right-0">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">Sub Category
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-sm-12">
                                                                    <select class="form-control jc_sub_category" name="sub_category[]">
                                                                        <option value="">Select Value</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 padding-left-0 padding-right-0">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">Service Name
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-sm-12">
                                                                    <select class="form-control jc_service_name" name="service_name[]">
                                                                        <option value="">Select Value</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2 padding-left-0 padding-right-0">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                                </label>
                                                                <div class="col-sm-12">
                                                                    <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 add_job_cat_detaial">
                                                                        <span class="btn-label">
                                                                            <i class="fa fa-plus"></i>
                                                                        </span>Add More</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-sm-6 padding-left-0 padding-right-0">
                                                            <div class="col-sm-12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div id="step-2" class="">
                                 <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                 <div class="p-20">
                                    <div class="col-md-12">
                                        <div class="p-10">
                                            <div class="form-group row">
                                                <h5 class="m-t-10 header-title">Building</h5>
                                                <hr>
                                                <div class="col-sm-4 padding-left-0 padding-right-0">
                                                    <label for="inputEmail3" class="col-sm-12 form-control-label">Select Building
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <div id="fmc_div" class="form-group row">
                                                            <div class="col-md-12 input-group">
                                                                <select class="form-control" name="building" id="all_buildings">
                                                                    <option value="">Select Building</option>
                                                                    @if(isset($buildings) && !empty($buildings)) @foreach($buildings as $building)
                                                                    <option value="{{$building->id}}">{{$building->name}}</option>
                                                                    @endforeach @endif
                                                                </select>
                                                                <input type="hidden" name="page" class="page" value="2">
                                                                {{ csrf_field() }}
                                                                @if(GeneralFunctions::check_add_permission('/oam/building_form'))
                                                                <span class="input-group-btn">
                                                                    <a href="javascript:void(0)" class="btn btn-default" id="addBuilding">
                                                                        <i class="fa fa-plus"></i>
                                                                    </a>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" id="hidden_building" value="{{isset($job->building)?$job->building:''}}" />
                                                </div>
                                                <div class="col-sm-4 padding-left-0 padding-right-0">
                                                    <label for="inputEmail3" class="col-sm-12 form-control-label">Assets
                                                        <span class="text-danger">*</span>
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <div id="fmc_div" class="row">
                                                            <div class="col-md-12">

                                                                <select class="form-control" style="width:100%" name="assets[]" placeholder="Assets" id="building_assets">
                                                                    @if(isset($assets) && !empty($assets)) @if(isset($building) && !empty($building)) @php $b_assets = explode(', ',$building->assets);
                                                                    @endphp @else @php $b_assets = array(); @endphp @endif @foreach($assets as
                                                                    $asset)
                                                                    <option>{{$asset->name}}</option>
                                                                    @endforeach @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-4 padding-left-0 padding-right-0">
                                                    <label for="building_type" class="col-sm-12 form-control-label">Building Type
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <input type="text" parsley-type="text" placeholder="Building Type" name="building_type" value="" class="form-control building_type" readonly
                                                        >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <input type="hidden" name="page" class="page" value="2">
                                <div class="p-20">
                                    <div class="col-md-12">
                                        <div class="p-10">
                                            <input type="hidden" name="id" value="{{Request::input('id')}}">
                                            <div class="form-group row">
                                                <h5 class="m-t-10 header-title">Point of Contact</h5>
                                                <hr> @if(count($contact_name) > 0) @foreach($contact_name as $key => $value) @if($key == 0)
                                                <div class="point_of_contact_div">
                                                    <div class="row">
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Name
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control pos_name" name="focal_name[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($staff_list as $focal_key => $focal_value)
                                                                    <option <?php echo ($value == $focal_value['name']) ? 'selected="selected"' : '' ?> value="{{$focal_value['name']}}">{{$focal_value['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                                {{ csrf_field() }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Email
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="email" required parsley-type="email" placeholder="Email" name="focal_email[]" value="{{$contact_email[$key]}}"
                                                                class="form-control pos_email" readonly>
                                                                <input type="hidden" name="page" class="page" value="2">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Contact Number
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="text" required placeholder="Contact Number" name="contact_number[]" value="{{$contact_number[$key]}}" class="form-control pos_contact_no" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 add_contact_detaial">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-plus"></i>
                                                                </span>Add More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="point_of_contact_div">
                                                    <div class="row">
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Name
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control pos_name" name="focal_name[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($staff_list as $focal_key => $focal_value)
                                                                    <option <?php echo ($value == $focal_value['name']) ? 'selected="selected"' : '' ?> value="{{$focal_value['name']}}">{{$focal_value['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Email
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="email" required parsley-type="email" placeholder="Email" name="focal_email[]" value="{{$contact_email[$key]}}"
                                                                class="form-control pos_email" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Contact Number
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="text" required placeholder="Contact Number" name="contact_number[]" value="{{$contact_number[$key]}}" class="form-control pos_contact_no" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_contact_detaial">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-trash"></i>
                                                                </span>Clear</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif @endforeach @else
                                                <div class="point_of_contact_div">
                                                    <div class="row">
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Name
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <select class="form-control pos_name" name="focal_name[]">
                                                                    <option value="">Select Value</option>
                                                                    @foreach($staff_list as $focal_key => $focal_value)
                                                                    <option value="{{$focal_value['name']}}">{{$focal_value['name']}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Email
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="email" required parsley-type="email" placeholder="Email" name="focal_email[]" class="form-control pos_email" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">Contact Number
                                                                <span class="text-danger">*</span>
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <input type="text" required placeholder="Contact Number" name="contact_number[]" class="form-control pos_contact_no" readonly>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3 padding-left-0 padding-right-0">
                                                            <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                            </label>
                                                            <div class="col-sm-12">
                                                                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 add_contact_detaial">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-plus"></i>
                                                                </span>Add More</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="row clearfix">
                                                <div class="col-sm-6 padding-left-0 padding-right-0">
                                                    <div class="col-sm-12">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                        </div>
                                        <div id="step-3" class="">
                                            <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="page" class="page" value="3">
                                            <div class="p-20">
                                                <div class="col-md-12">
                                                    <div class="p-10">
                                                        <div class="form-group row">
                                                            <h5 class="m-t-10 header-title">Payment Terms</h5>
                                                            <hr>
                                                            <div class="col-sm-4 padding-left-0 padding-right-0">
                                                                <label for="inputEmail3" class="padding-left-0 padding-right-0 form-control-label">Payment Method
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <select class="form-control" name="payment_terms">
                                                                    <option @if(isset($job->payment_terms) && $job->payment_terms == 'Cheque') selected='selected' @endif value="Cheque" selected="selected">Cheque</option>
                                                                </select>
                                                                {{ csrf_field() }}
                                                                <input type="hidden" name="page" class="page" value="5">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">

                                                            @if(count($payment_terms) > 0) @foreach($payment_terms as $key => $value) @if($key == 0)
                                                            <div class="payment_terms_div">
                                                                <div class="row">
                                                                    <div class="col-sm-6 ">
                                                                        <input type="hidden" name="get_old_percentage" class="get_old_percentage" value="{{ $payment_terms_des[$key] }}">
                                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Payment Description
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <div class="col-sm-12">
                                                                            <input type="text" class="form-control term_description" name="term_description[]" value="{{ $value }}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <label for="inputEmail3" class="col-sm-12 form-control-label">Select Percentage
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <div class="col-sm-12 input-group">
                                                                          <input type="number" class="form-control percentage" name="percentage[]" required value="{{ $payment_terms_des[$key] }}" min="0">
                                                                          <span class="input-group-addon">%</span>
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-sm-2">
                                                                    <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                                        <!-- <span class="text-danger"></span> -->
                                                                    </label>
                                                                   <!--  <div class="col-sm-12 m-t-20">
                                                                        <a href="javascript:void(0);" class="remove_payment_term btn btn-danger">Remove</a>
                                                                    </div> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @else
                                                        <div class="payment_terms_div">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <input type="hidden" name="get_old_percentage" class="get_old_percentage" value="{{ $payment_terms_des[$key] }}">
                                                                    <label for="inputEmail3" class="col-sm-12 form-control-label">Payment Description
                                                                        <span class="text-danger">*</span>
                                                                    </label>
                                                                    <div class="col-sm-12">
                                                                        <input type="text" class="form-control term_description" placeholder="Terms Description" name="term_description[]" value="{{ $value }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <label for="inputEmail3" class="col-sm-12 form-control-label">Select Percentage
                                                                        <span class="text-danger">*</span>
                                                                    </label>
                                                                    <div class="col-sm-12 input-group">
                                                                      <input type="number" class="form-control percentage" name="percentage[]" required value="{{ $payment_terms_des[$key] }}" min="0">
                                                                      <span class="input-group-addon">%</span>
                                                                  </div>
                                                              </div>
                                                              <div class="col-sm-2">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">
                                                                    <!-- <span class="text-danger"></span> -->
                                                                </label>
                                                                <div class="col-sm-12 m-t-20">
                                                                    <a href="javascript:void(0);" class="remove_payment_term btn btn-danger">Remove</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif @endforeach @else
                                                    <div class="payment_terms_div">
                                                        <div class="row">
                                                            <div class="col-sm-6 ">
                                                                <input type="hidden" name="get_old_percentage" class="get_old_percentage" value="0">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">Payment Description
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-sm-12">
                                                                    <input type="text" class="form-control term_description" name="term_description[]" required>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4 ">
                                                                <label for="inputEmail3" class="col-sm-12 form-control-label">Select Percentage
                                                                    <span class="text-danger">*</span>
                                                                </label>
                                                                <div class="col-sm-12 input-group">
                                                                  <input type="number" class="form-control percentage" name="percentage[]" required min="0">
                                                                  <span class="input-group-addon">%</span>
                                                    <!-- <select class="form-control percentage" name="percentage[]" required>
                                                        <option value="">Select Percentage</option>
                                                        <option value="20">20%</option>
                                                        <option value="30">30%</option>
                                                        <option value="40">40%</option>
                                                        <option value="50">50%</option>
                                                    </select> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-sm-6 m-t-20">
                                        <a href="javascript:void(0);" class="btn btn-success add_payment">
                                            <span class="btn-label">
                                                <i class="fa fa-plus"></i>
                                            </span>Add Payment Timeline
                                        </a>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-sm-6 ">
                                        <div class="col-sm-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <input type="hidden" name="page" class="page" value="3">
                    <div class="p-20 term_condition_area">
                        <div class="col-md-12">
                            <div class="p-10">
                                <input type="hidden" name="id" value="{{Request::input('id')}}">

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="btn-group m-t-15">
                                            <h5 class="m-t-10 header-title">Terms & Conditions</h5>
                                        </div>
                                        <hr>
                                    </div>
                                    <!-- <div class="col-sm-6">
                                        <div class="checkbox checkbox-primary pull-right open_all_switch">
                                            <input id="on_all_switch" type="checkbox">
                                            <label for="checkbox0">
                                                Check All
                                            </label>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row">
                                    @if(isset($t_c) && count($t_c)>0) @foreach($t_c as $tc)
                                    <div class="col-sm-6 togl-height padding-left-0 padding-right-0">
                                        <div class="col-sm-10">
                                            <b class="m-t-0 font-13">{{$tc['title']}}</b>
                                        </div>
                                        <div class="col-sm-2 m-t-0">
                                            <div class="switchery-demo">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="page" class="page" value="6">
                                                <input type="checkbox" data-size="small" class="terms_switch" name="{{$tc['title'].'_SWITCH'}}" data-plugin="switchery" data-color="#1bb99a"
                                                data-secondary-color="#ff5d48" <?php echo (in_array($tc['title'], $job_t_c)) ? 'checked' : ''; ?> />
                                            </div>
                                        </div>
                                        <div class="col-sm-12 parent_terms_div">
                                            @if(in_array($tc['title'], $job_t_c))
                                            <div class="standard_custom_div">
                                                @else
                                                <div class="standard_custom_div" style="display: none;">
                                                    @endif
                                                    <div class="radio radio-info radio-inline staff_area_radio">
                                                        <input id="rrfse_standard"  class="standard_check" value="1" name="job_terms_selected[{{$tc['title']}}]" type="radio"  title="{{$tc['title']}}" <?php echo (GeneralFunctions::get_terms_condition_status($tc['title'], isset($job->id) ? $job->id : 0) == 1) ? 'checked' : ''; ?>>
                                                        <label>Standard</label>
                                                    </div>
                                                    <div class="radio radio-info radio-inline staff_area_radio">
                                                        <input id="rrfse_customized" class="customized_check" value="2" name="job_terms_selected[{{$tc['title']}}]" type="radio" title="{{$tc['title']}}" <?php echo (GeneralFunctions::get_terms_condition_status($tc['title'], isset($job->id) ? $job->id : 0) == 2) ? 'checked' : ''; ?>>
                                                        <label>Customized</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach @endif
                                    </div>
                                    @if(!isset($job->id))
                                    <div class="form-group row m-t-20">
                                        <div class="col-xs-12">
                                            <div class="checkbox checkbox-primary">
                                                <input id="terms_and_conditions" type="checkbox" name="terms_and_conditions">
                                                <label for="terms_and_conditions">I accept
                                                    <a data-toggle="modal" data-target=".bs-example-modal-lg" href="#">Terms and Conditions</a>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <input type="hidden" name="terms_con_flag" class="terms_con_flag" value="0"> @endif
                                    <div class="row clearfix">
                                        <div class="col-sm-6 padding-left-0 padding-right-0">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="col-sm-1 padding-left-0 padding-right-0">
                    <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light submit_job_post" style="display: none;">
                        Save
                    </a>
                </div>
                <div class="col-sm-1">
                    <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                </div>
            </div>
            <!-- Block For Job Post (Job Category) with multiple Selections-->
        </div>
    </div>

</div>
<!-- Modal content for the above example  -->
<div class="modal fade bs-example-modal-lg in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Terms and Conditions</h4>
            </div>
            <div class="modal-body">
                <?php if (isset($t_and_c) && $t_and_c != '') {
                    echo $t_and_c->description;
                } else {
                    echo '';
                }?>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal  -->
<!-- Job Post Preview Modal  -->
<div class="modal fade job-post-modal-lg in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Review your Job Post</h4>
            </div>
            <div class="modal-body">
                <div class="row job_post_preview">

                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="alert alert-info lead m-t-0 m-b-0">Job Detail</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Job Title</th>
                                        <td>
                                            <p class="font-13 preview_job_title"></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Purposal Validity</th>
                                        <td>
                                            <p class="font-13 preview_proposal_validity">

                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Job Reporting</th>
                                        <td>
                                            <p class="font-13 preview_job_reporting"></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Job Type</th>
                                        <td>
                                            <p class="font-13 preview_job_type">

                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Job Description</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <p class="m-b-0 font-13 preview_job_description">

                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <p class="lead  alert alert-info m-t-0 m-b-0">Payment Terms</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Payment Via</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p class="font-13">
                                                cheque
                                            </p>
                                            <div class="preview_payment_terms">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="alert alert-info  lead m-t-10 m-b-0">Community Detail</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Community</th>
                                        <td><p class="font-13 preview_building">

                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Community Assets</th>
                                    <td>
                                        <p class="font-13 preview_building_assets">
                                        </p>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Community Type</th>
                                    <td>
                                        <p class="font-13 preview_building_type">
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <p class="lead alert alert-info m-t-10 m-b-0">Point of Contact</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td><p class="font-13 preview_contact_details">
                                    </p></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="alert alert-info  lead m-t-10 m-b-0">Job Category Detail</p>
                            <table class="table table-bordered">
                                <tr>
                                    <td><p class="font-13 final_job_category_details">
                                    </p></td>
                                </tr>

                            </table>
                        </div>
                    </div>
                </div>

                <!-- old desgin -->
                    <!-- <div class="col-sm-12">
                        <p class="lead m-t-10 m-b-0">Job Detail</p>
                        <hr class="m-t-0 m-b-10">
                    </div>
                    <div class="col-sm-12">
                        <p class="font-13 preview_job_title">
                            <strong>Job Title:</strong>
                        </p>
                        <p class="font-13 preview_proposal_validity">
                            <strong> Purposal Validity: </strong>
                            50
                        </p>
                        <p class="font-13 preview_job_reporting">
                            <strong>Job Reporting: </strong>

                        </p>
                        <p class="font-13 preview_job_type">
                            <strong>Job Type: </strong>
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <strong>Job Description: </strong>
                        <p class="m-b-0 font-13">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.
                        </p>
                    </div>

                    <div class="col-sm-12">
                        <p class="lead m-t-10 m-b-0">Building Detail</p>
                        <hr class="m-t-0 m-b-10">
                        <p class="font-13 preview_building">
                            <strong>Building: </strong>

                        </p>
                        <p class="font-13 preview_building_assets">
                            <strong>Building Assets: </strong>
                        </p>
                        <p class="font-13 preview_building_type">
                            <strong>Building Type: </strong>
                        </p>
                    </div>

                    <div class="col-sm-12 preview_contact_details">
                    </div>
                    <div class="col-sm-12">
                        <p class="lead m-t-10 m-b-0">Payment Terms</p>
                        <hr class="m-t-0 m-b-10">
                        <p class="font-13">
                            <strong>Payment Via: </strong>
                            cheque
                        </p>
                        <div class="preview_payment_terms">

                        </div>
                    </div> -->
                    <!-- <div class="col-sm-12">
                        <p class="lead m-t-10 m-b-0">job Requirement</p>
                        <hr class="m-t-0 m-b-10">
                        <p class="font-13 preview_staffing">
                        </p>
                        <p class="font-13 preview_uniform">
                        </p>
                    </div> -->
                </div>
            </div>
            @if(GeneralFunctions::check_add_permission('/oam/job_form'))
            <div class="modal-footer">
                <a type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    <a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light save_job_record">Save</a>
                </div>
                @endif
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal  -->
    <!-- Model for creating new Communitiess -->
    <div class="modal fade add_building in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Add Community</h4>
                </div>

                <form role="form" class="add_building_form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data"
                action="{{ url('oam/save_building') }}" name="add_building_form">
                <div class="modal-body">
                    <div class="error-msg-box alert" style="display: none;">
                        <ul style="text-decoration: none;" id="error-msg-list">

                        </ul>
                    </div>
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="ajax" value="1">
                    <!-- <input type="hidden" name="id" value="{{Request::input('id')}}"> -->
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="inputEmail3" class="col-sm-3 form-control-label">Name
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" required parsley-type="text" placeholder="Name" name="name" class="form-control" id="buil_name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="inputEmail3" class="col-sm-3 form-control-label">Type
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <fieldset class="">
                                    <select class="form-control" name="type" id="building_type">
                                        <option value="" selected="selected" disabled>Select Role</option>
                                        <option  value="Commercial">Commercial</option>
                                        <option value="Residential">Residential</option>
                                        <option value="villa">villa</option>
                                        <option value="Commercial And Residential">Commercial And Residential</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="inputEmail3" class="col-sm-3 form-control-label">Address
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" required parsley-type="text" name="address" class="form-control" placeholder="Address" id="buil_address">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Makani
                            </label>
                            <div class="col-sm-8">
                                <input placeholder="Makani Number" id="buil_makani" required parsley-type="text" data-mask="99999 99999" class="form-control" type="text" name="makani">

                            </div>
                        </div>
                    </div>

                    <div class="form-group row for-residentials">
                        <div class="col-sm-6 retail">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Retail
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" required parsley-type="text" name="retail" class="form-control" placeholder="Retail" id="buil_retail">
                            </div>
                        </div>
                        <div class="col-sm-6 office">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Number of Office
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" required parsley-type="text" name="office" class="form-control" placeholder="Number of Office" id="buil_office">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 floor">

                            <label for="hori-pass1" class="col-sm-3 form-control-label">Floor
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8  input-group">
                                <span class="input-group-addon"><b>G+</b></span>
                                <input type="text" required parsley-type="text" name="floor" class="form-control" placeholder="Floor" id="buil_floor">
                            </div>
                        </div>
                        <div class="col-sm-6 apartment">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Apartment
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" required parsley-type="text" name="apartment" class="form-control" placeholder="Apartment" id="buil_apartment">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6">
                            <label for="inputEmail3" class="col-sm-3 form-control-label">Assets
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-8">
                                <fieldset class="">
                                    <select class="select2 form-control select2-multiple" style="width:100%" name="building_assets[]" id="assets_list" multiple="multiple"
                                    data-placeholder="Community Assets">
                                    @if(isset($assets) && !empty($assets)) @if(isset($building) && !empty($building)) @php $b_assets = explode(', ',$building->assets);
                                    @endphp @else @php $b_assets = array(); @endphp @endif @foreach($assets as $asset)
                                    <option>{{$asset->name}}</option>
                                    @endforeach @endif
                                </select>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="t_and_c1" value="{{isset($settings->t_and_c1)?$settings->t_and_c1:''}}">
            <!-- Terms and condition model -->
            <div class="modal fade" id="myModal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Terms & Conditions</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">

                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End terms and condition model -->

            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light save_building">
                    Save
                </a>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- end of Community -->
</form>

<!-- Copy of Payment Terms Div -->
<div class="payment_terms_div_copy" style="display: none;">
    <div class="row">
        <div class="col-sm-6">
            <input type="hidden" name="get_old_percentage" class="get_old_percentage" value="0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Terms Description
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <input type="text" class="form-control term_description" name="term_description[]" required>
            </div>
        </div>
        <div class="col-sm-4">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Select Percentage
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12 input-group">
              <input type="number" class="form-control percentage" name="percentage[]" required>
              <span class="input-group-addon">%</span>
                <!-- <select class="form-control percentage" name="percentage[]" required>
                    <option value="">Select Percentage</option>
                    <option value="20">20%</option>
                    <option value="30">30%</option>
                    <option value="40">40%</option>
                    <option value="50">50%</option>
                </select> -->
            </div>
        </div>
        <div class="col-sm-2">
            <label for="inputEmail3" class="col-sm-12 form-control-label">
                <!-- <span class="text-danger"></span> -->
            </label>
            <div class="col-sm-12 m-t-20">
                <a href="javascript:void(0);" class="remove_payment_term btn btn-danger">Remove</a>
            </div>
        </div>
    </div>
</div>

<!-- Copy of point of control div -->
<div class="point_of_contact_div_copy" style="display: none;">
    <div class="row">
        <div class="col-sm-3 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Name
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <select class="form-control pos_name" name="focal_name[]">
                    <option value="">Select Value</option>
                    @foreach($staff_list as $key => $focal_value)
                    <option value="{{$focal_value['name']}}">{{$focal_value['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-3 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Email
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <input type="email" required parsley-type="email" placeholder="Email" name="focal_email[]" class="form-control pos_email" readonly>
            </div>
        </div>
        <div class="col-sm-3 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Contact Number
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <input type="text" required placeholder="Contact Number" name="contact_number[]" class="form-control pos_contact_no" readonly>
            </div>
        </div>
        <div class="col-sm-3 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">
            </label>
            <div class="col-sm-12">
                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_contact_detaial">
                    <span class="btn-label">
                        <i class="fa fa-trash"></i>
                    </span>Clear</a>
                </div>
            </div>
        </div>
    </div>
    @include('oam.jobs.job_category_copy')
    <!-- Modal for the Terms and Conditions to Show Dynamically -->
    <!-- Modal -->
    <div id="terms_and_conditions_md" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Terms And Conditions</h4>
                </div>
                <div class="modal-body">
                    <form id="md_terms_form">
                        <div class="form-group row">
                            <label class="control-label col-sm-2" for="email">Title</label>
                            <div class="col-sm-10">
                                <p class="md_title_terms_cond"></p>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-12" for="pwd">Description:</label>
                            <div class="col-sm-12">
                                <input type="hidden" name="md_title" id="md_title">
                                <input type="hidden" name="md_id" id="md_id">
                                <textarea name="md_description_terms_cond" class="md_description_terms_cond" id="md_description_terms_cond" rows="4"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect waves-light term_condition_update">Save</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <!-- End of the Terms and Condition Modal -->
    @section('js')
    <script src="{{ URL::asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }} " type="text/javascript"></script>
    <!-- Include jQuery Validator plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>

    <script src="{{ URL::asset('assets/js/smart_wizard/jquery.smartWizard.js') }} " type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){


        });
    </script>
    <script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script> @if(isset($job->uniform_value) && $job->uniform_value == 0)
<!-- <script>
    $(document).ready(function () {
        tinyMCE.get('uniform').setMode('code');
    });
</script> -->
@endif

<script>
    $(document).ready(function () {
     var job_id = 0;
     @if(isset($job->id))
     var job_id = '{{$job->id}}';
     @endif
     var files = null;
     var percentage = $('.total_percentage').val();
     var get_old_percentage = 0;
     $('#building_type').change(function(){
        $('.floor').show();
        $('.retail').show();
        $('.office').show();
        if($('#building_type').val() == 'Residential') {
            $('.for-residentials').hide();
        } else {
            $('.for-residentials').show();
        }

        if($('#building_type').val() == 'Commercial') {
            $('.apartment').hide();
        } else {
            $('.apartment').show();
        }
        if($('#building_type').val() == 'villa') {
            $('.floor').hide();
            $('.retail').hide();
            $('.office').hide();
        }
    });
     $('#all_terms').click(function(event) {
        if(this.checked) {
                // Iterate each checkbox
                $('.term_condition').each(function() {
                    this.checked = true;
                });
            }
            else {
                $('.term_condition').each(function() {
                    this.checked = false;
                });
            }
        });

     $('#on_all_switch').change(function () {
        if ($(this).prop('checked')) {
            var event = jQuery.Event("true");
            $('.check_all').trigger("click", "bindClick");
        } else {
            $('.check_all').trigger("click", "unbindClick");
        }
    });
        // prefilled_assets();
        // Add events
        $('input[type=file]').on('change', prepareUpload);

        // Grab the files and set them to our variable
        function prepareUpload(event) {
            $('.upload_files').val(1);
        }
        // $(document).on('change', '#all_buildings', function(){
        //     console.log('asdsadsad');
        // });
        $('#all_buildings').on('change', function () {
            var buildingID = $(this).val();

            if (buildingID) {
                var url = '';
                var building_assets = $('#building_assets');
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('get_building_assets') }}";
                @else
                url = "{{ generate_url('oam/get_building_assets') }}";
                @endif
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: { id: $(this).val() },
                    success: function (data) {
                        $('.building_type').val(data.building_type);
                        if (data != 0) {
                            var s2 = $('#building_assets').select2({
                                minimumInputLength: 0,
                                multiple: true,
                                tags: true
                            });
                            var selected_data = [];
                            var vals = data.selectedData;

                            var i = 1;
                            vals.forEach(function (e) {
                                if (!s2.find('option:contains(' + e + ')').length)
                                    s2.append($('<option>').text(e));
                            });

                            s2.val(vals).trigger("change");
                        } else {
                            $('#building_assets').html('');
                        }
                    }
                });
            }
        });

        $('.staff_radio').change(function () {
            if ($(this).val() == 0) {
                tinyMCE.get('staffing').setMode('code');
            }
            else {
                tinyMCE.get('staffing').setMode('readonly');
            }
        });
        $('.uniform_radio').change(function () {
            if ($(this).val() == 0) {
                tinyMCE.get('uniform').setMode('code');
            }
            else {
                tinyMCE.get('uniform').setMode('readonly');
            }
        });

        $('#all_buildings option').each(function () {
            var hidden_building = $('#hidden_building').val();
            //console.log($(this).attr('value'));
            if ($(this).attr('value') == hidden_building) {
                $(this).attr("selected", "selected");
                prefilled_assets();

            }
        });

        $(document).on('click', '.submit_job_post', function () {
            var data = $('#job_form_data').serialize();

            if (percentage > 100 || percentage < 100) {
                alert('Please Limit the Payment Terms Percentage within 100 %');
            }
            else {
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('job_form_validation') }}";
                @else
                url = "{{ generate_url('oam/job_form_validation') }}";
                @endif
                $('.loading_gif').show();
                // Clear the Preview Data Result First
                $('.preview_job_title').text('');
                $('.preview_proposal_validity').text('');
                $('.preview_job_reporting').text('');
                $('.preview_job_description').text('');
                $('.preview_building').text('');
                $('.preview_building_type').text('');
                $('.preview_job_type').text('');
                $('.preview_building_assets').html('');
                $('.preview_contact_details').html('');
                $('.preview_payment_terms').html('');

                $('.preview_staffing').html('');
                $('.preview_uniform').html('');
                // End Implementation

                $('.msg-box').hide();
                tinyMCE.triggerSave();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        $("#msg-list").empty();
                        if (data.status == 'success') {
                            console.log(data);
                            var assets_string = '';
                            var contact_string = '';
                            var payment_terms = '';
                            var job_category_details = '';

                            // Section For Previewing the Job Post
                            $('.preview_job_title').append(data.data.job_title);
                            $('.preview_proposal_validity').append( data.data.purposal_validity);
                            $('.preview_job_reporting').append(data.data.job_reporting);
                            $('.preview_job_description').html(data.data.job_description);
                            $('.preview_building').append(data.data.building);
                            $('.preview_building_type').append( data.data.building_type);
                            $('.preview_job_type').append(data.data.job_type);

                            for (var i = 0; i < data.data.assets.length; i++) {
                                assets_string = assets_string + '<span class="label label-primary">' + data.data.assets[i] + '</span>';
                            }
                            for (var i = 0; i < data.data.focal_name.length; i++) {
                                contact_string = contact_string +
                                '<p class="font-13"><strong>Name: </strong>' +
                                data.data.focal_name[i] +
                                '</p>' +
                                '<p class="font-13"><strong>Email: </strong>' +
                                data.data.focal_email[i] +
                                '</p>' +
                                '<p class="font-13"><strong>Contact Number: </strong>' +
                                data.data.contact_number[i] +
                                '</p>';
                            }
                            // preview_payment_terms
                            for (var i = 0; i < data.data.term_description.length; i++) {
                                payment_terms = payment_terms + '<p class="font-13"><strong>Terms Description: </strong>' +
                                data.data.term_description[i] +
                                '</p><p class="font-13"><strong>Percentage: </strong>' +
                                data.data.percentage[i] +
                                '</p>';
                            }
                            //job categories
                            for (var i = 0; i < data.job_categories_details.length; i++) {
                                job_category_details = job_category_details +
                                '<p class="font-13"><strong>Category: </strong>' +
                                data.job_categories_details[i].category_name +
                                '</p>' +
                                '<p class="font-13"><strong>Sub Category: </strong>' +
                                data.job_categories_details[i].sub_category_name +
                                '</p>' +
                                '<p class="font-13"><strong>Service Name: </strong>' +
                                data.job_categories_details[i].service_name +
                                '</p><hr>';
                            }
                            $('.preview_building_assets').append(assets_string);
                            $('.preview_contact_details').append(contact_string);
                            $('.preview_payment_terms').append(payment_terms);
                            $('.final_job_category_details').html(job_category_details);

                            $('.preview_staffing').append('<strong>Staffing: </strong> ' + data.data.staffing);
                            $('.preview_uniform').append('<strong>Unifrom: </strong> ' + data.data.uniform);

                            $('.job-post-modal-lg').modal('show');
                            // End of Section
                            // $('#job_form_data')[0].submit();
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('#msg-list').append(list);
                            $('.msg-box').addClass("alert-danger").show();
                            $("html, .container").animate({ scrollTop: 0 }, 600);
                        }
                        $('.loading_gif').hide();
                    }
                });
}
});
$(document).on('click', '#addBuilding', function () {
    $('.add_building').modal('show');
});

$('.add_building').on('shown.bs.modal', function (e) {
    @if(Auth::user()->role_id == 1)
    url = "{{ generate_url('save_building') }}";
    @else
    url = "{{ generate_url('oam/save_building') }}";
    @endif
    $(document).on('click', '.save_building', function () {
        var data = {};
        data.name = $('#buil_name').val();
        data.address = $('#buil_address').val();
        data.makani = $("#buil_makani").val();
        data._token = $('#csrf_token').val();
        data.floor = $('#buil_floor').val();
        data.apartment = $('#buil_apartment').val();
        data.retail = $('#buil_retail').val();
        data.office = $('#buil_office').val();
        data.assets = $('#assets_list').val();
        data.type = $('#building_type').val();
        data.ajax = 1;
        $('.error-msg-box').hide();
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            success: function (data) {
                $("#error-msg-list").empty();
                if (data.status == 'success') {
                    $('#msg-list').html('<li>Record has been saved successfully</li>')
                    $('.msg-box').addClass("alert-success").show();
                    var building_options = '<option value="">Select Building</option>';
                    data.all_buildings.forEach(function (e) {
                        building_options = building_options + '<option value="'+e.id+'">' + e.name + '</option>';
                    });
                    $('#all_buildings').html(building_options);
                    $('.add_building').modal('hide');
                }
                else {
                    var errorArray = data.msg_data;
                    var list = '';
                    errorArray.forEach(function (e) {
                        list = list + '<li>' + e + '</li>';
                    });

                    $('#error-msg-list').append(list);
                    $('.error-msg-box').addClass("alert-danger").show();
                }
            }
        });

    });
});

function prefilled_assets() {
    console.log('prefilled_assets');
    var buildingID = $('#all_buildings').val();
            //console.log(buildingID);

            if (buildingID) {
                var building_assets = $('#building_assets');
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('get_building_assets') }}";
                @else
                url = "{{ generate_url('oam/get_building_assets') }}";
                @endif
                console.log(building_assets);
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: { id: $('#all_buildings').val() },
                    success: function (data) {
                        $('.building_type').val(data.building_type);
                        if (data != 0) {
                            var s2 = $('#building_assets').select2({
                                minimumInputLength: 0,
                                multiple: true,
                                tags: true
                            });
                            var selected_data = [];
                            var vals = data.selectedData;

                            var i = 1;
                            vals.forEach(function (e) {
                                if (!s2.find('option:contains(' + e + ')').length)
                                    s2.append($('<option>').text(e));
                            });

                            s2.val(vals).trigger("change");
                        } else {
                            $('#building_assets').html('');
                        }
                    }
                });
            }
        }

        $(document).on('change', '#terms_and_conditions', function () {
            if (this.checked) {

                $('.terms_con_flag').val(1);
            }
            else {
                $('.terms_con_flag').val(0);
            }
            console.log($('.terms_con_flag').val());
        });


        //Section For Payment Terms
        var maxGroup = 10;

        //add more fields group in Assets
        $(".add_payment").click(function () {
            if (percentage >= 100) {
                alert('Please Limit the Payment Terms Percentage within 100 %');
            }
            else {
                var last_percentage = $('body').find('.payment_terms_div:last .percentage').val();
                var last_term_description = $('body').find('.payment_terms_div:last .term_description').val();
                if (last_percentage == '' || last_term_description == '') {

                }
                else {
                    if ($('body').find('.payment_terms_div').length < maxGroup) {
                        var fieldHTML = '<div class="payment_terms_div">' + $(".payment_terms_div_copy").html() + '</div>';
                        $('body').find('.payment_terms_div:last').after(fieldHTML);
                    } else {
                        alert('Maximum ' + maxGroup + ' Assets are allowed.');
                    }
                }
            }
        });
        $("body").on("click", ".remove_payment_term", function () {
            var get_perc = $(this).parents(".payment_terms_div").find('.percentage').val();
            $(this).parents(".payment_terms_div").remove();
            percentage = percentage - parseInt(get_perc);
            console.log('percentage after remove', percentage);

        });

        $(document).on('change', '.percentage', function () {
            get_old_percentage = $(this).parents(".payment_terms_div").find('.get_old_percentage').val();
            percentage = (percentage - parseFloat(get_old_percentage)) + parseFloat($(this).val());
            console.log('percentage = ', percentage);
            $(this).parents(".payment_terms_div").find('.get_old_percentage').val($(this).val());
            if (percentage > 100) {
                alert('Please Limit the Payment Terms Percentage within 100 %');
            }
        });

        /* Section For Point of Contact Information */
        $(".add_contact_detaial").click(function () {

            if ($('body').find('.point_of_contact_div').length < maxGroup) {
                var fieldHTML = '<div class="point_of_contact_div">' + $(".point_of_contact_div_copy").html() + '</div>';
                $('body').find('.point_of_contact_div:last').after(fieldHTML);
            } else {
                alert('Maximum ' + maxGroup + ' Contacts are allowed.');
            }
        });

        $("body").on("click", ".remove_contact_detaial", function () {
            $(this).parents(".point_of_contact_div").remove();
        });

        $(document).on('click', '.save_job_record', function () {
            $('#job_form_data')[0].submit();
        });


        $(document).on('change', '.standard_check', function () {
            standardCheckFunction();

        });

        $(document).on('change', '.customized_check', function () {
            $('.md_title_terms_cond').text('');
            $('.md_description_terms_cond').val('');
            @if(Auth::user()->role_id == 1)
            url = "{{ generate_url('get_terms_condition_content') }}";
            @else
            url = "{{ generate_url('oam/get_terms_condition_content') }}";
            @endif
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "title": $(this).attr('title'),
                    "job_id": job_id
                },
                success: function (data) {
                    $('.md_title_terms_cond').text(data.result.title);
                    $('#md_title').val(data.result.title);
                    $('#md_id').val(data.result.id);
                    tinyMCE.get('md_description_terms_cond').setContent(data.result.description);
                    tinyMCE.get('md_description_terms_cond').setMode('code');
                    $('.md_description_terms_cond').val(data.result.description);
                    $('#terms_and_conditions_md').modal('show');
                }
            });
        });

        $(document).on('click', '.term_condition_update', function () {
            var params = $('#md_terms_form').serialize();
            console.log(params);
            @if(Auth::user()->role_id == 1)
            url = "{{ generate_url('update_term_condition_session') }}";
            @else
            url = "{{ generate_url('oam/update_term_condition_session') }}";
            @endif
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "title": $("#md_title").val(),
                    "id": $("#md_id").val(),
                    "description": $("#md_description_terms_cond").val()
                },
                success: function (data) {
                    $('#terms_and_conditions_md').modal('hide');
                }
            });
        });

        function standardCheckFunction(title)
        {
            $('.md_title_terms_cond').text('');
            $('.md_description_terms_cond').val('');
            @if(Auth::user()->role_id == 1)
            url = "{{ generate_url('get_terms_condition_content') }}";
            @else
            url = "{{ generate_url('oam/get_terms_condition_content') }}";
            @endif
            $.ajax({
                type: 'POST',
                url: url,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "title": title,
                    "job_id": job_id
                },
                success: function (data) {
                    $('.md_title_terms_cond').text(data.result.title);
                    tinyMCE.get('md_description_terms_cond').setContent(data.result.description);
                    tinyMCE.get('md_description_terms_cond').setMode('readonly');
                    $('#md_title').val(data.result.title);
                    $('#md_id').val(data.result.id);
                    $('.md_description_terms_cond').val(data.result.description);
                    $('#terms_and_conditions_md').modal('show');
                }
            });
        }
        $(document).on('change', '.terms_switch', function(e){

            // console.log($('.terms_switch').trigger('click'));
            if(this.checked){
                $(this).parent().parent().siblings('.parent_terms_div').find('.standard_custom_div').show();
                $(this).parent().parent().siblings('.parent_terms_div').find('.standard_check').prop("checked", true);
                var title=$(this).parent().parent().siblings('.parent_terms_div').find('.standard_check').attr('title');
                standardCheckFunction(title);
            }
            else{
                $(this).parent().parent().siblings('.parent_terms_div').find('.standard_custom_div').hide();
                $(this).parent().parent().siblings('.parent_terms_div').find('.standard_check').prop('checked', false);
                $(this).parent().parent().siblings('.parent_terms_div').find('.customized_check').prop('checked', false);
            }
        });

        $(document).on('change', '.pos_name', function(){

            var selected_option = $(this).val();
            var pos_email = $(this).parent().parent().siblings('div').find('.pos_email');
            var pos_contact_no = $(this).parent().parent().siblings('div').find('.pos_contact_no');
            if(selected_option == ''){
                pos_email.val('');
                pos_contact_no.val('');
            }
            else{
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('/get_staff_record') }}";
                @else
                url = "{{ generate_url('/oam/get_staff_record') }}";
                @endif
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "name": selected_option,
                    },
                    success: function (response) {
                        if(response.status == 'success'){
                            pos_email.val(response.result.email);
                            pos_contact_no.val(response.result.staff_members.mobile_number);
                        }
                    }
                });
            }
        });

        // Toolbar extra buttons
        var btnFinish = $('<button></button>').text('Finish')
        .addClass('btn btn-info')
        .on('click', function(){
            if( !$(this).hasClass('disabled')){
                var elmForm = $("#job_form_data");
                if(elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.find('.has-error');
                    if(elmErr && elmErr.length > 0){
                        alert('Oops we still have error in the form');
                        return false;
                    }else{
                        alert('Great! we are ready to submit form');
                        elmForm.submit();
                        return false;
                    }
                }
            }
        });
        var btnCancel = $('<button></button>').text('Cancel')
        .addClass('btn btn-danger')
        .on('click', function(){
            $('#smartwizard').smartWizard("reset");
            $('#job_form_data').find("input, textarea").val("");
        });



        // Smart Wizard
        $('#smartwizard').smartWizard({
            selected: 0,
            theme: 'dots',
            transitionEffect:'fade',
            toolbarSettings: {toolbarPosition: 'bottom',
            toolbarExtraButtons: []
        },
        anchorSettings: {
                            markDoneStep: true, // add done css
                            markAllPreviousStepsAsDone: true, // When a step selected by url hash, all previous steps are marked done
                            removeDoneStepOnNavigateBack: true, // While navigate back done step after active step will be cleared
                            enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                        }
                    });

        $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
            var elmForm = $("#form-step-" + stepNumber);
            // stepDirection === 'forward' :- this condition allows to do the form validation
            // only on forward navigation, that makes easy navigation on backwards still do the validation when going next
            if(stepDirection === 'forward' && elmForm){
                elmForm.validator('validate');
                var elmErr = elmForm.children('.has-error');
                if(elmErr && elmErr.length > 0){
                    // Form validation failed
                    return false;
                }
            }
            /**
             *
             * Check Validations on Job Post with given Wizards
             *
             */
             if(stepDirection == 'forward'){
                var step_number = stepNumber+1;
                if (step_number == 3 && (percentage > 100 || percentage < 100)) {
                    alert('Please Limit the Payment Terms Percentage within 100 %');
                    var status = false;
                }
                else{
                    if(step_number == 2){
                        $('.submit_job_post').show();
                    }
                   
                    var  formData =  $('#step-'+step_number+' input, #step-'+step_number+' select, #step-'+step_number+' textarea').serialize();
                
                   
                    @if(Auth::user()->role_id == 1)
                    url = "{{ generate_url('job_form_validation') }}";
                    @else
                    url = "{{ generate_url('oam/job_form_validation') }}";
                    @endif
                    var status = false;
                    $('.loading_gif').show();
                    $('.msg-box').hide();
                    tinyMCE.triggerSave();
                    $.ajax({
                        async: false,
                        type: 'POST',
                        url: url,
                        data: formData,
                        success: function (data) {
                            console.log(data);
                            // return;
                            $("#msg-list").empty();
                            if (data.status == 'success') {
                                status = true;
                            }
                            else {

                                var errorArray = data.msg_data;
                                var list = '';
                                errorArray.forEach(function (e) {
                                    list = list + '<li>' + e + '</li>';
                                });

                                $('#msg-list').append(list);
                                $('.msg-box').addClass("alert-danger").show();
                                $("html, .container").animate({ scrollTop: 0 }, 600);
                                status = false;
                            }
                            $('.loading_gif').hide();
                        }
                    });
                }
                return status;
            }
        });

        $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            if(stepNumber == 3){
                $('.btn-finish').removeClass('disabled');
            }else{
                $('.btn-finish').addClass('disabled');
            }
        });
    });
</script>

<script>

    tinymce.init({
        selector: 'textarea',
        height: 150,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

</script> @if(isset($job->uniform_value) && $job->uniform_value == 0)
<script>
    tinymce.init({
        selector: '#uniform',
        height: 150,
        readonly: false,
        menubar: false,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
</script> @else
<script>
    tinymce.init({
        selector: '#uniform',
        height: 150,
        readonly: true,
        menubar: false,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
</script> @endif @if(isset($job->staffing_value) && $job->staffing_value == 0)
<script>
    tinymce.init({
        selector: '#staffing',
        height: 150,
        readonly: false,
        menubar: false,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });
</script> @else
<script>
    tinymce.init({
        selector: '#staffing',
        height: 150,
        readonly: true,
        menubar: false,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });
</script> @endif
<!-- MuliSelect Libraries -->

<!-- ======================================================================================================= -->

<!-- New Implementation For Job Category -->
<script type="text/javascript">
    $(document).ready(function(){
        // Populate Sub Categories on Basis of the Job Category
        var req_id = null;
        $(document).on('change', '.jc_category', function(){
            var selected_option = $(this).val();
            var jc_sub_category = $(this).parent().parent().siblings('div').find('.jc_sub_category');
            var jc_service_code = $(this).parent().parent().siblings('div').find('.jc_service_code');
            var jc_service_name = $(this).parent().parent().siblings('div').find('.jc_service_name');
            if(selected_option == ''){
                jc_sub_category.val('');
                // jc_service_name.val('');
            }
            else{
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('/sub/category') }}";
                @else
                url = "{{ generate_url('/oam/sub/category') }}";
                @endif
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        "category_uuid": selected_option,
                        "req_id" : req_id
                    },
                    success: function (response) {
                        if(response.status == 'success'){
                            console.log(response.result);
                            jc_sub_category.html(response.result);
                            jc_service_code.val('');
                            jc_service_name.val('');
                        }
                    }
                });
            }
        });

        //populate Service Code from selection
        $(document).on('change', '.jc_sub_category', function(){
            var selected_option_sub = $(this).val();
            var jc_category = $(this).parent().parent().siblings('div').find('.jc_category');
            var jc_service_code = $(this).parent().parent().siblings('div').find('.jc_service_code');
            var jc_service_name = $(this).parent().parent().siblings('div').find('.jc_service_name');
            if(selected_option_sub == ''){
                jc_sub_category.val('');
                // jc_service_name.val('');
            }
            else{
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('/service_code/category') }}";
                @else
                url = "{{ generate_url('/oam/service_code/category') }}";
                @endif
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        "sub_category_uuid": selected_option_sub,
                        "category_uuid" : jc_category.val(),
                        "req_id" : req_id
                    },
                    success: function (response) {
                        if(response.status == 'success'){
                            if(response.result){
                                jc_service_name.html(response.result);
                            }
                            else{
                                alert('There is no formation set for these combinations');
                            }
                        }
                    }
                });
            }
        });

        //populate Service Name from selection
        $(document).on('change', '.jc_service_code', function(){
            var selected_option_sub = $(this).val();
            var jc_category = $(this).parent().parent().siblings('div').find('.jc_category');
            var jc_sub_category = $(this).parent().parent().siblings('div').find('.jc_sub_category');
            var jc_service_name = $(this).parent().parent().siblings('div').find('.jc_service_name');
            if(selected_option_sub == ''){
                jc_sub_category.val('');
                // jc_service_name.val('');
            }
            else{
                @if(Auth::user()->role_id == 1)
                url = "{{ generate_url('/service_name/category') }}";
                @else
                url = "{{ generate_url('/oam/service_name/category') }}";
                @endif
                $.ajax({
                    type: 'GET',
                    url: url,
                    data: {
                        "service_code": selected_option_sub,
                        "category_uuid" : jc_category.val(),
                        "sub_category_uuid" : jc_sub_category.val(),
                        "req_id" : req_id
                    },
                    success: function (response) {
                        if(response.status == 'success'){
                            if(response.result){
                                jc_service_name.html(response.result);
                            }
                            else{
                                alert('There is no formation set for these combinations');
                            }
                        }
                    }
                });
            }
        });

        var maximum_limit = 10;
        $(".add_job_cat_detaial").click(function () {
            console.log('add_job_cat_detaial');
            if ($('body').find('.job_category_div').length < maximum_limit) {
                var fieldHTML = '<div class="job_category_div">' + $(".job_category_copy").html() + '</div>';
                $('body').find('.job_category_div:last').after(fieldHTML);
            } else {
                alert('Maximum ' + maximum_limit + ' Inputs are allowed.');
            }
        });

        $("body").on("click", ".remove_job_cat_detaial", function () {
            $(this).parents(".job_category_div").remove();
        });

        $(document).on('change', '.select_job_type', function(){

            if($(this).attr('id') == 'annual_maintenance'){

                $('.job_expiry').html('<option value="0">Select Default</option><option value="15">15 Days</option><option value="30">30 Days</option><option value="45">45 Days</option><option value="60">60 Days</option>');
            }
            else if($(this).attr('id') == 'breakdown_maintenance'){
                $('.job_expiry').html('<option value="0">Select Default</option><option value="7">7 Days</option><option value="15">15 Days</option><option value="30">30 Days</option>');
            }
            else{
                $('.job_expiry').html('<option value="30">Select Default</option><option value="15">15 Days</option><option value="0">30 Days</option><option value="45">45 Days</option><option value="60">60 Days</option>');
            }

        });

        @if(isset($job->job_expiry_days))
        var expiry_days = '{{$job->job_expiry_days}}';
        @if($job->job_type == 'Annual Maintenance')
        $('.job_expiry').html('<option value="0" '+(expiry_days == 0 ? 'selected' : '')+'>Select Default</option><option value="15" '+(expiry_days == 15 ? 'selected' : '')+'>15 Days</option><option value="30" '+(expiry_days == 30 ? 'selected' : '')+'>30 Days</option><option value="45" '+(expiry_days == 45 ? 'selected' : '')+'>45 Days</option><option value="60">60 Days</option>');

        @elseif($job->job_type == 'Breakdown Maintenance')
        console.log('breakdown_maintenance');
        $('.job_expiry').html('<option value="0" '+(expiry_days == 0 ? 'selected' : '')+'>Select Default</option><option value="7" '+(expiry_days == 7 ? 'selected' : '')+'>7 Days</option><option value="15" '+(expiry_days == 15 ? 'selected' : '')+'>15 Days</option><option value="30" '+(expiry_days == 30 ? 'selected' : '')+'>30 Days</option>');

        @else
        $('.job_expiry').html('<option value="0" '+(expiry_days == 0 ? 'selected' : '')+'>Select Default</option><option value="15" '+(expiry_days == 15 ? 'selected' : '')+'>15 Days</option><option value="30" '+(expiry_days == 30 ? 'selected' : '')+'>30 Days</option><option value="45" '+(expiry_days == 45 ? 'selected' : '')+'>45 Days</option><option value="60" '+(expiry_days == 60 ? 'selected' : '')+'>60 Days</option>');
        @endif
        @endif

        // end here
    });
</script>
<!--End of New Implementation For Job Category -->
<script type="text/javascript" src="{{ URL::asset('assets/plugins/multiselect/js/jquery.multi-select.js') }} "></script>
<script src="{{ URL::asset('assets/plugins/select2/js/select2.full.min.js') }} " type="text/javascript"></script>
<script type="text/javascript" src="{{ URL::asset('assets/pages/jquery.formadvanced.init.js') }}"></script> @stop @endsection
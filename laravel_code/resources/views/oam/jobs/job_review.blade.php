@extends('oam.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <!-- <div class="btn-group pull-right m-t-15">
            <a href="{{url('/oam/job_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Job</a>
        </div> -->
        <h4 class="page-title"></h4>
    </div>
</div>
@include('flash_message')


<div class="row">
    <div class="col-xl-12">
        <div class="card job_review page_box_shadow">
            <div class="card-header font-16 ">
                <b>Leave Feedback</b>
            </div>
            <div class="card-block">


                <div class="col-md-10">
                    <p class="lead font-13 m-b-10">
                        Please leave feedback rate
                        <a href="#">fmc Company url </a> for the project
                        <a href="#">Project name/url </a>
                    </p>
                    <div class=" m-b-0">
                        <div class="col-sm-8">
                            <table class="table table-bordered">
                                <tr>
                                    <th class="font-13">Professionalism</th>
                                    <td colspan="2">
                                        <div class="text-xs-left">
                                            <div class="rating-sm" style="cursor: pointer;">
                                                <i class="fa fa-star-o text-muted" title="bad" data-score="1"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="poor" data-score="2"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="regular" data-score="3"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="good" data-score="4"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="gorgeous" data-score="5"></i>
                                                <input name="score" type="hidden">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-13">Communication</th>
                                    <td colspan="2">
                                        <div class="text-xs-left">
                                            <div class="rating-sm" style="cursor: pointer;">
                                                <i class="fa fa-star-o text-muted" title="bad" data-score="1"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="poor" data-score="2"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="regular" data-score="3"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="good" data-score="4"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="gorgeous" data-score="5"></i>
                                                <input name="score" type="hidden">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="font-13">Would you work with this employer again?</th>
                                    <td colspan="2">
                                        <div class="text-xs-left">
                                            <div class="rating-sm" style="cursor: pointer;">
                                                <i class="fa fa-star-o text-muted" title="bad" data-score="1"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="poor" data-score="2"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="regular" data-score="3"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="good" data-score="4"></i>&nbsp;
                                                <i class="fa fa-star-o text-muted" title="gorgeous" data-score="5"></i>
                                                <input name="score" type="hidden">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>
                        <div class="col-sm-8"> 
                            <p class=" font-13 m-b-0">
                               <b>Comment:</b>
                            </p>
                            <textarea class="form-control" id="jobdescription" rows="3" name="job_description" placeholder="Job Description"></textarea>
                        </div>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            
        </div>
        
    </div>
    <div class="col-sm-8 m-t-0"> 
            <a href="javascript:void(0)" class="btn btn-success waves-effect waves-light submit_job_post">
                Rate company
            </a>     
    </div>
</div>
<!-- end row -->
@section('js')
<script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>

    tinymce.init({
        selector: '#jobdescription',
        height: 150,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });

</script>
@stop 

@endsection
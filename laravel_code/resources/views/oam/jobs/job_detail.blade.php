@extends( Auth::user()->role_id == 2  ?  'oam.app' : 'fmc.app' )
@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <!-- <a href="{{url('/oam/job_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Job</a> -->
        </div>
        <h4 class="page-title"></h4>
    </div>
</div>
@if(\Session::has('status'))
<div class="alert alert-success">
    <p>{{ \Session::get('status')}}</p>
</div>
@endif

<div class="row">
<div class="col-xl-9">
    <div class="card page_box_shadow">
        <div class="card-header sidebar_header">

            <div class="pull-left">
            <h4 class="header-title job_listing_heading m-b-0"> {{isset($title)?$title:''}}</h4>
            </div>
            <div class="pull-right">
                @if(Auth::user()->role_id == 2)
                <a href="" type="button" class="btn btn-sm btn-danger-outline waves-effect waves-light right" style="margin-left:5px; ">Delete</a>
                <a href="" class="btn btn-sm btn-dark-outline waves-effect waves-light right">Edit</a>
                @endif
            </div>
            <div class="clearfix"></div>
        </div>
    
        <div class="card-block page_box_shadow">
            <div class="col-lg-9">
                <p class="lead m-b-0 font-17 ">{{isset($detail->job_title)?$detail->job_title:''}} <span class="text-muted  font-13">{!! date_format(new DateTime($detail->created_at), 'jS F Y g:ia') !!}</span></p>
                <p class="m-b-0 text-justify">{!! isset($detail->description)?$detail->description:'' !!}</p>
                <div class="m-t-10">
                    <p class="lead m-b-0 font-17 ">Job Categories</p>
                    @php $job_cat = explode(',',$detail->category); @endphp
                    @foreach($category as $cat)
                        <span class="label label-pill label-primary">{{$cat->title}}</span>
                    @endforeach
                </div>
                <div class="m-t-10">
                    <p class="lead m-b-0 font-17 ">Job Type</p>
                    <p class="font-13 m-b-0 text-dark">{{isset($detail->job_type)?$detail->job_type:''}}</p>
                </div>
                <div class="pull-left m-t-10">
                    <a href="#" class="btn btn-sm btn-primary-outline waves-effect waves-light show_full_detail">Show Detail</a>
                </div>
                <div class="pull-right m-t-10">
                @if($job_applied_status == 0)
                    <form action="{{url('fmc/job/apply')}}" id="job_form" method="POST">
                        <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light apply_for_job">Apply For Job</button>
                         {{csrf_field()}}    
                        <input type="hidden" value="{{ $detail->id }}" name="uuid" id="uuid">
                        <input type="hidden" name="status" id="staus">
                        <a href="javascript:void(0)" class="btn btn-sm btn-secondary waves-effect decline_for_job">Decline</a>
                        <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                    </form>
                @endif
                </div>
            </div>
            <div class="col-lg-3">           
                <div class="pull-right ">     
                        <p class="font-13 text-muted  m-b-0"><strong> Purposal Validity: </strong>
                        <span class="text-danger">{{isset($detail->purposal_validity)?$detail->purposal_validity:''}} Days</span>  </p> 
                        <p class="font-13 text-muted  m-b-0"><strong> Status: </strong><span class="text-success">@if($job_applied_status == 1)
                            Applied for this Job.
                        @endif
                        @if($job_applied_status == 2)
                            Decline this Job Already.
                        @endif</span>  </p>     
                    <!-- <p class="lead text-xs-right font-13 text-dark m-b-0">67 views </p>
                    <p class="lead text-xs-right font-13 text-primary m-b-10">120 bids </p> -->
                   
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card-block page_box_shadow" id="full_details" style="display: none;">
            <div class="col-md-6">
                <p class="lead  text-primary  m-b-0">Building Detail</p>
                <p class="lead font-17  m-b-0">Building</p>
                <p class="font-13 m-b-0 text-dark">{{isset($building->name)?$building->name:''}}</p>
                
                <div class="m-t-10">
                    <p class="lead font-17  m-b-0">Building Assets</p>
                    @if(isset($building->assets) && !empty($building->assets))
                        @foreach(explode(',',$building->assets) as $b_asset)
                           <span class="label  label-primary">{{$b_asset}}</span>
                        @endforeach
                    @endif
                </div>
                <div class="m-t-10">
                    <p class="lead font-17  m-b-0">Building Type</p>
                    <p class="font-13 m-b-0 text-dark">{{isset($building->type)?$building->type:''}} </p>
                </div>
            </div>

            <div class="col-md-6">
                <p class="lead  text-primary  m-b-0">Point of Contact</p>
                <p class="lead  font-17  m-b-0">Name</p>
                @if(isset($detail->contact_name) && !empty($detail->contact_name))
                    <p class="font-13 m-b-0 text-dark">
                        @foreach(json_decode($detail->contact_name) as $name)
                            {{$name. ", "}}
                        @endforeach
                    </p>
                @endif
                <!-- <p class="font-13 m-b-0 text-dark">Omer Hayat</p> -->
                
                <div class="m-t-10">
                    <p class="lead font-17  m-b-0">Email</p>
                    @if(isset($detail->contact_email) && !empty($detail->contact_email))
                    <p class="font-13 m-b-0 text-dark">
                        @foreach(json_decode($detail->contact_email) as $email)
                            {{$email. ", "}}
                        @endforeach
                    </p>
                @endif
                </div>
                <div class="m-t-10">
                    <p class="lead font-17  m-b-0">Contact Number</p>
                    @if(isset($detail->contact_number) && !empty($detail->contact_number))
                    <p class="font-13 m-b-0 text-dark">
                        @foreach(json_decode($detail->contact_number) as $contact)
                            {{$contact}}
                        @endforeach
                    </p>    
                    @endif
                </div>
            </div>
            <div class="col-md-12">
                    <p class="lead m-t-10  text-primary  m-b-0">Payment Term</p>
                <p class="lead font-17   m-b-0">Payment Via</p>
                <p class="font-13 m-b-0 text-dark">{{isset($detail->payment_terms)?$detail->payment_terms:''}} </p>
                
                <div class="m-t-10">
                    <p class="lead font-17   text-primary   m-b-0">Job Requirement</p>
                    <aclass="font-13 m-b-0 text-primary">Staffing</p>
                    <!-- <aclass="font-13 m-b-0 text-primary">blsjbkujbkg</p> -->
                    {!! isset($detail->staffing)?$detail->staffing:'' !!}
                </div>
                <div class="m-t-10">
                    <p class="lead font-17  m-b-0">Uniform</p>
                    {!! isset($detail->uniform)?$detail->uniform:'' !!}
                </div>
                
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
</div>
<div class="col-sm-3 col-lg-3 col-xs-12">
    <div class="card page_box_shadow">
        <h4 class="card-header sidebar_header m-b-0">
        Job Stats
        </h4>
        <ul class="list-group list-group-flush sidbar_icon">
            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-check-square"></i> <span> Applied</span> </a></li>
            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-list-alt"></i> <span> Shortlisted</span> </a></li>
            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li>
        </ul>
    </div>
</div>


</div>
@section('fmc_js')
<script type="text/javascript">
    $(document).ready(function(){
        var get_uuid = $('#uuid').val();
        $(document).on('click', '.apply_for_job', function(){
            $('#staus').val(1); 
            $('.loading_gif').show();
             $('#job_form').submit()[0];
        });
        $(document).on('click', '.decline_for_job', function(){
            $('#staus').val(2);
            $('.loading_gif').show();
            $('#job_form').submit()[0];
        });
        $(document).on('click','.show_full_detail', function(){
            $('#full_details').toggle();    
        });
    })
</script>
@endsection
<!-- end row -->
@endsection
<!-- Copy of point of control div -->
<div class="job_category_copy" style="display: none;">
    <div class="row">
        <div class="col-sm-2 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Category
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <select class="form-control jc_category" name="category[]">
                    <option value="">Select Value</option>
                    @foreach($new_categories as $key => $focal_value)
                        <option value="{{$focal_value['id']}}">{{$focal_value['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-sm-2 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Sub Category
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <select class="form-control jc_sub_category" name="sub_category[]">
                    <option value="">Select Value</option>
                </select>
            </div>
        </div>
        <!-- <div class="col-sm-2 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Service Code
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <select class="form-control jc_service_code" name="service_code[]">
                    <option value="">Select Value</option>
                </select>
            </div>
        </div> -->
        <div class="col-sm-2 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">Service Name
                <span class="text-danger">*</span>
            </label>
            <div class="col-sm-12">
                <select class="form-control jc_service_name" name="service_name[]">
                    <option value="">Select Value</option>
                </select>
            </div>
        </div>
        <div class="col-sm-2 padding-left-0 padding-right-0">
            <label for="inputEmail3" class="col-sm-12 form-control-label">
            </label>
            <div class="col-sm-12">
                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect m-t-20 remove_job_cat_detaial">
                    <span class="btn-label">
                        <i class="fa fa-trash"></i>
                    </span>Clear</a>
            </div>
        </div>
    </div>
</div>
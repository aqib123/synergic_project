@extends('oam.app') @section('content') @section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet">
<!--venobox lightbox-->
<link rel="stylesheet" href="{{ URL::to('assets/plugins/magnific-popup/css/magnific-popup.css') }}" />
<style>
    .profile_img {
        border: 4px solid #822767;
        padding: 40px;
        font-size: 25px;
        font-weight: 700;
        color: #822767;
        border-radius: 50%;
        letter-spacing: 0px;
    }
</style>
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <!-- <a href="#" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Staff List</a> -->
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>

@include('flash_message')
<div class="row ">
    <form action="{{url('/oam/update_oam_profile/'.Auth::user()->id)}}" method="POST" enctype="multipart/form-data">
        <div class="card-box add_staff ">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="m-t-0 m-b-0">Creator information</h4>
                    <hr class="m-t-0">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="form_label" for="primaryemail">Creator Name (required)
                        <span class="text-danger">*</span>
                    </label>
                    <div class=" form-group phone_input input-group">
                        <input type="text" parsley-type="text" placeholder="Name" name="creator_name" value="{{old('creator_name')}}{{isset(Auth::user()->creator_name)?Auth::user()->creator_name:''}}"
                            class="form-control">
                        {{ csrf_field() }}
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form_label" for="primaryemail">Creator Email (required)
                        <span class="text-danger">*</span>
                    </label>
                    <div class=" form-group phone_input input-group">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                        </span>
                        <input type="email" parsley-type="email" name="creator_email" value="{{old('creator_email')}}{{isset(Auth::user()->creator_email)?Auth::user()->creator_email:''}}"
                            class="form-control" placeholder="Email">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label class="form_label" for="primaryemail">Creator Designation (required)
                        <span class="text-danger">*</span>
                    </label>
                    <div class=" form-group phone_input input-group">
                        <input type="text" parsley-type="text" placeholder="Designation" name="creator_designation" value="{{old('creator_designation')}}{{isset(Auth::user()->creator_designation)?Auth::user()->creator_designation:''}}"
                            class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <label class="form_label" for="primaryemail">User Name (required)
                        <span class="text-danger">*</span>
                    </label>
                    <div class=" form-group phone_input input-group">
                        <input type="text" parsley-type="text" placeholder="User Name" name="user_name" value="{{old('user_name')}}{{isset(Auth::user()->user_name)?Auth::user()->user_name:''}}"
                            class="form-control" readonly>
                        <input type="hidden" name="user_name_hidden" value="{{old('user_name')}}{{isset(Auth::user()->user_name)?Auth::user()->user_name:''}}"
                            class="form-control">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="card-box add_staff ">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="m-t-0 m-b-0">Reference Url (For Vendors to Register Using your Referance Link)</h4>
                    <hr class="m-t-0">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <label class="form_label" for="primaryemail">Url
                    </label>
                    <div class=" form-group phone_input input-group">
                        <input type="text" required parsley-type="text" name="company_url"
                            class="form-control" readonly value="{!! Auth::user()->company_url !!}">
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
</div>
<div class="col-sm-12">
    <button type="submit" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</button>
</div>
</form>
<!-- The Modal -->
<div class="modal  bs-example-modal-lg" id="myModal">
    <div class="modal-dialog  modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Password</h4>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form method="POST" id="oam_password">
                    <div class="row">

                        <div class="msg-box alert" style="display: none;">
                            <ul style="text-decoration: none;" id="msg-list">

                            </ul>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="form_label" for="userName">Old Password (required)
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                <input name="old_password" id="old_password" placeholder="Old Password" class="form-control" type="password">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="form_label" for="userName">New Password (required)
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="new_password" id="new_password" placeholder="New Password" class="form-control" type="password">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="form_label" for="userName">Confirm New Password (required)
                                    <span class="text-danger">*</span>
                                </label>
                                <input name="confirm_new_password" id="confirm_new_password" placeholder="Confirm New Password" class="form-control" type="password">
                            </div>
                        </div>

                    </div>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-success" id="save_oam_password">Save</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

@section('js')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
<!-- Magnific popup -->
<script type="text/javascript" src="{{ URL::to('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

<script>
    $(document).ready(function () {

        $("#save_oam_password").click(function () {
            var params = $("#oam_password").serialize();
            $.ajax({
                type: 'POST',
                url: '{{ generate_url("oam/update_user_password") }}',
                data: params,
                success: function (data) {
                    $("#msg-list").empty();
                    if (data.status == 'success') {
                        $('#msg-list').append('<li>Record has been saved successfully</li>')
                        $('.msg-box').addClass("alert-success").show();
                    }
                    else {
                        var errorArray = data.message;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#msg-list').append(list);
                        $('.msg-box').addClass("alert-danger").show();
                    }
                }
            });
        });

        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            }
        });
        $('#datepicker-autoclose').datepicker({
            autoclose: true,
        });
    });
</script> @endsection

<!-- end col-->


@endsection

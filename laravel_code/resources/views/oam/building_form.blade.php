@extends('oam.app') @section('content')

@php
$makani = '';
$floor = '';
if(Request::input('id')){
$makani = (int) $building->makani;
$floor = (int) $building->floor;
}

@endphp

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('/oam/home')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back to Dashbroad</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<form role="form" data-parsley-validate novalidate method="POST" action="{{ url('/oam/save_building') }}">
    <div class="row">
        @include('flash_message')
        <div class="col-sm-9 col-xs-12 col-md-9">
            <div class="card-box">
                <div class="p-20">
                    <h4 class="header-title m-t-0 m-b-15">Add New Communities</h4>
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Name
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" required parsley-type="text" placeholder="Name" name="name" value="{{old('name')}}{{isset($building->name)?$building->name:''}}"
                            class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Communities Type
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <fieldset class="">
                                <select class="form-control" name="type" id="building_type">
                                    <option value="">Select Type</option>
                                    <option value="Commercial">Commercial</option>
                                    <option value="Residential">Residential</option>
                                    <option value="villa">Villa/Town houses</option>
                                    <option value="Commercial And Residential">Commercial And Residential</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Address
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="text" required parsley-type="text" name="address" value="{{old('address')}}{{isset($building->address)?$building->address:''}}"
                            class="form-control" placeholder="Address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Makani Number
                        </label>
                        <div class="col-sm-6">
                            <input placeholder="Makani Number" required parsley-type="text" data-mask="99999 99999" class="form-control" type="text" name="makani" value="{{old('makani')}}{{$makani}}">
                        </div>
                    </div>
                    <div class="form-group row floor">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Floor Number
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6 input-group">
                            <span class="input-group-addon"><b>G+</b></span>
                            <input type="number" min="1" required parsley-type="number" name="floor" class="form-control numeric_values" placeholder="Floor" value="{{old('floor')}}{{$floor}}">
                        </div>
                    </div>
                    <div class="form-group row apartment" style="<?php echo (isset($building->type)) ? ($building->type == 'Commercial' ? 'display:none' : '') : ''; ?>">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Apartment of Number
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="number"  min="0"  required parsley-type="number" name="apartment" class="form-control numeric_values" placeholder="Apartment" value="{{old('apartment')}}{{isset($building->apartment)?$building->apartment:''}}">
                        </div>
                    </div>

                    <div class="for-residentials" style="<?php echo (isset($building->type)) ? ($building->type == 'Residential' ? 'display:none' : '') : ''; ?>">

                        <div class="form-group row retail">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Retail Number
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-6">
                                <input type="number"  min="0"   required parsley-type="number" name="retail" class="form-control numeric_values" placeholder="Retail" value="{{old('retail')}}{{isset($building->retail)?$building->retail:''}}">
                            </div>
                        </div>
                        <div class="form-group row office">
                            <label for="hori-pass1" class="col-sm-3 form-control-label">Office Number
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-6">
                                <input type="number" min="0" required parsley-type="number" name="office" class="form-control numeric_values" placeholder="Office" value="{{old('office')}}{{isset($building->office)?$building->office:''}}">
                            </div>
                        </div>

                    </div>
                    <div class="form-group row residential_unit" style="<?php echo (isset($building->type)) ? ($building->type == 'vila' ? 'display:none' : '') : ''; ?>">
                        <label for="hori-pass1" class="col-sm-3 form-control-label"> Residential Unit
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="number"  min="0"  required parsley-type="number" name="residential_unit" class="form-control numeric_values" placeholder="Residential Unit" value="{{old('apartment')}}{{isset($building->residential_unit)?$building->residential_unit:''}}">
                        </div>
                    </div>
                    <div class="form-group row commercial_unit" style="<?php echo (isset($building->type)) ? ($building->type == 'vila' ? 'display:none' : '') : ''; ?>">
                        <label for="hori-pass1" class="col-sm-3 form-control-label"> Commercial Unit
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <input type="number"  min="0"  required parsley-type="number" name="commercial_unit" class="form-control numeric_values" placeholder="Commercial Unit" value="{{old('apartment')}}{{isset($building->commercial_unit)?$building->commercial_unit:''}}">
                        </div>
                    </div>

                  <!--   <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Assets
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-6">
                            <select class="select2 form-control select2-multiple" style="width:100%" name="assets[]" multiple="multiple" data-placeholder="Building Assets">
                                @if(isset($assets) && !empty($assets))
                                @if(isset($building) && !empty($building))
                                @php $b_assets = explode(', ',$building->assets);
                                @endphp 
                                @else 
                                @php $b_assets = array();
                                @endphp 
                                @endif


                                @foreach($assets as $asset)
                                <option @if(in_array($asset->name, $b_assets)) selected='selected' @endif value="{{$asset->name}}">{{$asset->name}}</option>
                                @endforeach @endif
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group row assets_div assets_class0 ">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Assets
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-3">
                           <input type="text" required parsley-type="text" name="assets[]"
                           class="form-control assets_class" placeholder="Assets">
                       </div>
                       <div class="col-sm-3">
                           <input type="text" readonly required parsley-type="text" name="codes[]"
                           class="form-control code"  placeholder="Assets Code">
                       </div>
                        <div class="col-sm-2 padding-left-0 padding-right-0">
                            <div class="col-sm-12">
                                <a  class="custom_btn btn btn-secondary waves-effect  add_asset_div">
                                    <span class="btn-label">
                                        <i class="fa fa-plus"></i>
                                    </span>Add More</a>

                            </div>
                        </div>
                          <div class="col-sm-2 already_exist" style="display: none;">
                            <p class="assets_check" style="color:red">Asset exist </p>
                        </div>   
                </div>


               </div>
           </div>
       </div>
       <div class="col-sm-3 col-lg-3 col-xs-12">

        <div class="m-t-10">
            <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner">
        </div>
    </div>
    <!-- preview section -->

    <div class="col-sm-12 col-xs-12 col-md-12">
        <div class="card-box">
            <div class="p-20">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{Request::input('id')}}">
                <div class="row">
                    <div class=" col-sm-2">
                        <p class="m-b-0"><b>Name:</b></p>
                    </div>
                    <div class="col-sm-6">
                        <p class="text-muted">
                            {{isset($building->name)?$building->name:''}}
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class=" col-sm-2">
                        <p class="m-b-0"><b>Community Type:</b></p>
                    </div>
                    <div class="col-sm-6">
                        <p class="text-muted">
                           {{isset($building->type)?$building->type:''}}
                       </p>
                   </div>
               </div>
               <div class="row">
                <div class=" col-sm-2">
                    <p class="m-b-0"><b>Address:</b></p>
                </div>
                <div class="col-sm-6">
                    <p class="text-muted">
                       {{isset($building->address)?$building->address:''}}
                   </p>
               </div>
           </div>
           <div class="row">
            <div class=" col-sm-2">
                <p class="m-b-0"><b>Makani Number:</b></p>
            </div>
            <div class="col-sm-6">
                <p class="text-muted">
                   {{$makani}}
               </p>
           </div>
       </div>
       <div class="row">
        <div class=" col-sm-2">
            <p class="m-b-0"><b>Floor Number:</b></p>
        </div>
        <div class="col-sm-6">
            <p class="text-muted">
                G+ {{$floor}}
            </p>
        </div>
    </div>
    <div class="row">
        <div class=" col-sm-2">
            <p class="m-b-0"><b>Apartment Number:</b></p>
        </div>
        <div class="col-sm-6">
            <p class="text-muted">
               {{isset($building->apartment)?$building->apartment:'None'}}
           </p>
       </div>
   </div>
   <div class="row">
    <div class=" col-sm-2">
        <p class="m-b-0"><b>Retail Number:</b></p>
    </div>
    <div class="col-sm-6">
        <p class="text-muted">
            {{isset($building->retail)?$building->retail:'None'}}
        </p>
    </div>
</div>
<div class="row">
    <div class=" col-sm-2">
        <p class="m-b-0"><b>Office Number:</b></p>
    </div>
    <div class="col-sm-6">
        <p class="text-muted">
            {{isset($building->office)?$building->office:'None'}}
        </p>
    </div>
</div>
<div class="row">
    <div class=" col-sm-2">
        <p class="m-b-0"><b>Assets:</b></p>
    </div>
    <div class="col-sm-6">
        <p class="text-muted">
            @if(isset($assets) && !empty($assets)) @if(isset($building) && !empty($building)) @php $b_assets = explode(', ',$building->assets);
            @endphp @else @php $b_assets = array(); @endphp @endif
            @foreach($assets as $asset)
            @if(in_array($asset->name, $b_assets))
            {{$asset->name}}
            @endif
            @endforeach
            @endif
        </p>
    </div>
</div>


</div>
</div>
</div>

</div>
@if(GeneralFunctions::check_add_permission('/oam/building_form'))
<div class="row form-group">
    <div class="col-sm-12">
        <button type="submit" class="btn btn-success waves-effect waves-light btn_checkValue">
            Save
        </button>
            <!-- <button type="reset" class="btn btn-danger waves-effect m-l-5">
                Reset
            </button> -->
        </div>
    </div>
    @endif
</form>
 <div class="form-group row office assets_div_copy" style="display: none;">
                        <label for="hori-pass1" class="col-sm-3 form-control-label">Assets
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-3">
                           <input type="text" required parsley-type="text" name="assets[]"
                           class="form-control assets_class" placeholder="Assets">
                       </div>
                       <div class="col-sm-3">
                           <input type="text" readonly required parsley-type="text" name="codes[]"
                           class="form-control code" placeholder="Assets Code">
                       </div>
                        <div class="col-sm-2 padding-left-0 padding-right-0">
                            <div class="col-sm-12">
                                <a href="javascript:void(0);" class="custom_btn btn btn-secondary waves-effect remove_asset_div">
                                    <span class="btn-label">
                                        <i class="fa fa-trash"></i>
                                    </span>Clear</a>

                            </div>
                        </div>
                        <div class="col-sm-2 already_exist" style="display: none;">
                            <p class="assets_check" style="color:red">Asset exist </p>
                        </div>  
                   </div>


@section('js')
<script src="{{ URL::asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }} " type="text/javascript"></script>
<script>
    $(document).ready(function (){
        var url='{{ ('building_form') }}';
        $('.residential_unit').hide();
        $('.commercial_unit').hide();
         var counter=0;


     $(".add_asset_div").click(function () {
            var maxGroup = 10;
            if(counter==0)
            {
            if ($('body').find('.assets_div').length < maxGroup) {
                var fieldHTML = '<div class="form-group row assets_div assets_class'+counter+'">' + $(".assets_div_copy").html() + '</div>';
                $('body').find('.assets_div:last').after(fieldHTML);
                counter++;
            } else {
                alert('Maximum ' + maxGroup + ' Assets are allowed.');
            }
            }
            else
            {
                alert('Choose valid Assets First');
            }
        });

    $("body").on("click", ".remove_asset_div", function () {
            $(this).parents(".buisness_ins_div").remove();
    }); 
    var url_asset = "<?php echo url('oam/building_form_ajax'); ?>";
     $(document).on('keyup','.assets_class',function(){
        var assets=$(this).val();
        var code_assets=$(this).parent().next('.col-sm-3').find('.code');
        var already_tag=code_assets.parent().next('.col-sm-2').next('.col-sm-2');

        $.ajax({
                type: 'POST',
                url: url_asset,
                data: {
                    "_token": "{{ csrf_token() }}",
                     "assets":assets
                },
                success: function (data) {
                    console.log(data);
                    if(data.data== null)
                    { 
                        already_tag.hide();
                        code_assets.val(data.assets_code);
                        if(counter >0)
                        {
                            counter--;
                            $('.btn_checkValue').prop("disabled", false);
                        }
                        
                    }
                    else
                    {
                       already_tag.show();
                       counter++;
                       $('.btn_checkValue').prop('disabled',true);
                    }
                }
                });

    });
     // //  $(this).parent().next('.code_div').find('.code').val(ata.assets_code);


       //  $('select').select2({
       //      language: {
       //          noResults: function() {
       //              return "<div class=target ><a>Other</a></div>";
       //          }
       //      },
           
       //      escapeMarkup: function (markup) {
       //          return markup;
       //      },
       //      // insertTag: function (data, tag) {
       //      // data.push(tag);
       //      // console.log(data);
       //      // }
       //  });
       //  $('body').on('click', '.target', function(){

       //       //$('select').select2("open");
            
       //      // var newOption = new Option(data.text, data.id, true, true);

       //      // $('select').append(newOption).trigger('change');

       //    $('.others').show();
       // });
        

        $('.numeric_values').on('keypress', function(e){
            return e.metaKey || // cmd/ctrl
            e.which <= 0 || // arrow keys
            e.which == 8 || // delete key
            /[0-9]/.test(String.fromCharCode(e.which)); // numbers
        });

        $('#building_type').change(function(){
            $('.floor').show();
            $('.retail').show();
            $('.office').show();
            $('.residential_unit').hide();
            $('.commercial_unit').hide();
            if($('#building_type').val() == 'Residential') {
                $('.for-residentials').hide();
            } else {
                $('.for-residentials').show();
            }

            if($('#building_type').val() == 'Commercial') {
                $('.apartment').hide();
            } else {
                $('.apartment').show();
            }
            if($('#building_type').val() == 'villa') {
                $('.floor').hide();
                $('.retail').hide();
                $('.office').hide();
                $('.apartment').hide();
                $('.residential_unit ').show();
                $('.commercial_unit').show();
            }
        });


        var building = '{{isset($building->type)?$building->type:''}}';
        if(building == "Residential"){
            $('.for-residentials').hide();
        } else {
            $('.for-residentials').show();
        }

        if(building == 'villa') {
            $('.floor').hide();
            $('.retail').hide();
            $('.office').hide();
        }
        $("#building_type option").each(function()
        {
            if($(this).val() == building){
                $(this).attr('selected','selected');
            }
        });



    });
</script>
@endsection
@endsection
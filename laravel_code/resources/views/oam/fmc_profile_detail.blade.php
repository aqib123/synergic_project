@extends( Auth::user()->role_id == 2 ? 'oam.app' : 'fmc.app' ) @section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet">
<style type="text/css">
    .fa-star {
        color: yellow;
    }
</style>
@stop @section('content')

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<!-- Fmc profile details -->
<div class="row">
    <div class="card-box bg_grey">
        <div class="col-md-9">
            <div class="p-10">

                <div class="col-md-2">
                    <div class="card-box widget-user fmc_bg_shadow ">
                        <div class="fmc_company_image">
                            <img class="img-responsive " src="{{ asset( 'laravel_code/storage/app/public/user_image/' . $company_details->users->image) }}"
                                alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="card-box fmc_bg_shadow">
                        <div class="fmc_company_detail">
                            <div class="wid-u-info description-font fmc_profile fmc_application_section">

                                <div class="col-md-12">
                                    <h4 class="m-t-0">
                                        {{$company_details->name}}
                                    </h4>
                                    <hr class="m-t-0 m-b-10">
                                </div>
                                <div class="col-md-6">

                                    <p class="text-muted m-b-0 font-13">
                                        <b class="text-dark font-13">Email:</b> {{$company_details->users->email}}</p>
                                    <p class="text-muted  font-13 ">
                                        <b class="text-dark font-13">Contact:</b> {{$company_details->contact_no}}</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="m-t-0">
                                        <p class="text-muted  font-13 ">
                                            <b class="text-dark font-13">Trade Licence Number:</b> {{$company_details->licence_number}}</p>
                                        <p class="text-muted  font-13 ">
                                            <b class="text-dark font-13">Trade Licence Expiry:</b> {{$company_details->license_expiry}}</p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h4 class="font-16 m-t-10">
                                        Job Categories
                                    </h4>
                                    <hr class="m-t-0 m-b-10">@foreach($job_category as $key => $value)
                                    <span class="label label-primary">{{$value}}</span>
                                    @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card">
                        <div class="card-header font-16 ">
                            <b>Recent Reviews </b>
                        </div>
                        @if(isset($getAllReviews) && !empty($getAllReviews)) @foreach($getAllReviews as $key => $value)
                        <div class="card-block">
                            <div class="col-md-2">
                                <div class="fmc_company_image">
                                    <img class="img-responsive img-circle" src="{{ asset( 'laravel_code/storage/app/public/user_image/' . $value['oam_company']['users']['image']) }}"
                                        alt="">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="pull-left">
                                    <h5 class="m-t-10 m-b-5">
                                        <a href="#">{{$value['oam_company']['name']}}</a>
                                    </h5>
                                    <div class="text-xs-left">

                                        <div id="targetFormat" class="rating-md" style="cursor: pointer;">
                                            <span class="rating_box font-13">{{$value['ratings']}}</span>
                                            <i class="fa {!! ($value['ratings'] >= 1) ? 'fa-star' : 'fa-star-o'!!} text-muted" title="bad" data-score="1"></i>&nbsp;
                                            <i class="fa {!! ($value['ratings'] >= 2) ? 'fa-star' : 'fa-star-o'!!} text-muted"
                                                title="poor" data-score="2"></i>&nbsp;
                                            <i class="fa {!! ($value['ratings'] >= 3) ? 'fa-star' : 'fa-star-o'!!} text-muted"
                                                title="regular" data-score="3"></i>&nbsp;
                                            <i class="fa {!! ($value['ratings'] >= 4) ? 'fa-star' : 'fa-star-o'!!} text-muted"
                                                title="good" data-score="4"></i>&nbsp;
                                            <i class="fa {!! ($value['ratings'] >= 5) ? 'fa-star' : 'fa-star-o'!!} text-muted"
                                                title="gorgeous" data-score="5"></i>
                                            <input name="score" type="hidden">
                                        </div>
                                    </div>
                                    <p class="m-b-0 m-t-10 font-13">
                                        <b>{!! $value['review'] !!}</b>
                                        <span class="text-muted  font-13"> {!! date_format(new DateTime($value['created_at']), 'jS F Y g:ia') !!} </span>
                                    </p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endforeach @else 
                        <div class="card-block">
                            <div class="col-md-12">
                                <div class="text-xs-center">
                                    <div class="p-20">
                                        <p class="card-title lead">You have no reviews yet</p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endif
                    </div>


                </div>


            </div>
        </div>

        <div class="col-sm-3 col-lg-3 col-xs-12">
            <div class="card fmc_profile_sidebar fmc_bg_shadow page_box_shadow">
                <h4 class="card-header font-16 sidebar_header m-b-0">
                    <b>Details</b>
                </h4>

                <ul class="list-group list-group-flush  ">
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Office Address:
                        </p>
                        <span>{{ $company_details->office_address}}</span>
                    </li>
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Billing Address:
                        </p>
                        <span>{{ $company_details->billing_address}}</span>
                    </li>
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Alternative Contact:
                        </p>
                        <span>{{ $company_details->contact_alter}}</span>
                    </li>
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Alternative Email:
                        </p>
                        <span>{{ $company_details->email_alter}}</span>
                    </li>
                </ul>

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@section('js')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
<script>
    var date = new Date();
    date.setDate(date.getDate() - 1);
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date
    });
</script> @stop @endsection
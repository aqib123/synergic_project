@extends('oam.app') @section('content')
@section('css')
<style> 
.user_action_btn{
    float: none;
}
</style>

@endsection
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        @if(GeneralFunctions::check_view_permission('/oam/building_form'))
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('/oam/building_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add Community</a>
        </div>
        @endif
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-9">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Address</th>
                        <!-- <th>Makani</th>
                        <th>Floor</th>
                        <th>Apartment</th>
                        <th>Retail</th>
                        <th>Office</th> -->
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($building) && count($building)>0) @foreach($building as $row)
                    <tr>
                        <td>{{$row->id}}</td>
                        <td>{{ucwords($row->name)}}</td>
                        <td>{{$row->type}}</td>
                        <td>{{$row->address}}</td>
                        <!-- <td>{{$row->makani}}</td>
                        <td>{{$row->floor}}</td>
                        <td>{{$row->apartment}}</td>
                        <td>{{$row->retail}}</td>
                        <td>{{$row->office}}</td> -->
                        <td>
                            <?php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia');?>
                        </td>
                        <td style="text-align: center;"> 
                            <div class="btn-group user_action_btn">
                                <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <div class="dropdown-menu">
                                    @if(GeneralFunctions::check_edit_permission('/oam/buildings'))
                                    <a class="dropdown-item" href="{{url('oam/building_form?id='.$row->id)}}" title="Edit">
                                        <i class="fa fa-edit text-info"></i> Edit
                                    </a>
                                    @endif
                                    @if(GeneralFunctions::check_delete_permission('/oam/buildings'))
                                    <a class="dropdown-item delete_building_btn" href="javascript:void(0)" id="{{$row->id}}" title="Delete">
                                        <i class="fa fa-trash text-danger"></i> Delete
                                    </a>
                                    @endif
                                </div>
                            </div>

                        </td>
                    </tr>
                    @endforeach @endif
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12">
        <div class="card page_box_shadow m-t-0">
            <h4 class="card-header sidebar_header m-b-0">
                Job Stats
            </h4>
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item">
                        <!-- <i class="fa fa-check-square"></i> -->
                        <span> Applied</span> 0
                </li>
                <li class="list-group-item">
                        <span> Shortlisted</span> 0
                </li>
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
            </ul>
            
    
        </div>
        <div class="m-t-10">
            <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
        </div>
    </div>
</div>
<!-- end row -->

<!-- Modal Box for Deletion confirmation -->
<div class="modal fade md_delete_confirmation" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click', '.delete_building_btn', function(){
            
            var uuid = $(this).attr('id');
            $('#user_uuid').val(uuid);
            /* Show Confirmation alert box before deletion */
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click','.md_ok_delete_btn', function(){
            var user_uuid = $('#user_uuid').val();
            var url = "{{url('oam/delete_building/')}}/"+user_uuid;
            window.location.href = url;
        });
    });
</script>
@endsection
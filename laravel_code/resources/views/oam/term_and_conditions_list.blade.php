@extends('oam.app') @section('content') @php //dd($t_and_c); @endphp
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="  pull-right m-t-15">
            <a href="{{url('/oam/terms_and_conditions')}}" type="button" class="btn btn-dark-outline waves-effect waves-light">Add Term & Conditions</a>
            <a href="javascript:void(0)" type="button" class="btn btn-dark-outline waves-effect waves-light set_to_default_btn">Set To Default</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Posted By</th>
                        <th>Created At</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($t_and_c) && count($t_and_c)>0) @foreach($t_and_c as $tc)
                    <tr>
                        <td>{{$tc['id']}}</td>
                        <td>{{ucwords($tc['title'])}}</td>
                        <td>
                            <?= substr($tc['description'],0,200) ?>
                        </td>
                        <td>
                            @if($tc['status'] == 1)
                            <span class="label label-success">Active</span>
                            @else
                            <span class="label label-danger">InActive</span>
                            @endif
                        </td>
                        <td>{{$tc['posted_by']}}</td>
                        <td>
                            <?php echo date_format(new DateTime($tc['created_at']), 'jS F Y g:ia');?>
                        </td>
                        <td>

                            <div class="btn-group user_action_btn">
                                <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                    <i class="zmdi zmdi-more"></i>
                                </a>
                                <div class="dropdown-menu">
                                    @if($tc['posted_by'] == 'OAM')
                                    <a class="dropdown-item" href="{{url('/oam/terms_and_conditions?id='.$tc['id'])}}" title="Edit">
                                        <i class="fa fa-edit text-info"></i> Edit
                                    </a>
                                    <a class="dropdown-item delete_terms_cond_btn" href="javascript:void(0)" id="{{$tc['id']}}" title="Delete">
                                        <i class="fa fa-trash text-danger"></i> Delete
                                    </a>
                                    @if($tc['status'] == 0)
                                        <a class="dropdown-item" href="{{url('/oam/change_tc_status?status=1&id='.$tc['id'])}}" title="Delete">
                                            <i class="fa fa-check text-info"></i> Active
                                        </a>
                                    @else
                                        <a class="dropdown-item" href="{{url('/oam/change_tc_status?status=0&id='.$tc['id'])}}" title="Delete">
                                            <i class="fa fa-check text-info"></i> Inactive
                                        </a>
                                    @endif
                                    @else
                                    <a class="dropdown-item copy_tc" href="javascript:void(0)" id="{{$tc['id']}}" title="Delete">
                                        <i class="fa fa-copy text-info"></i> Copy
                                    </a>
                                    @endif
                                </div>
                            </div>

                        </td>
                    </tr>
                    @endforeach @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->

<!-- Modal Box for Deletion confirmation -->
<div class="modal fade md_delete_confirmation" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Box for copy terms and condition text -->
<div class="modal fade md_copy_tc" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Copy Term & Condition</h4>
            </div>
            <div class="col-sm-12"> 
                <div class="tccopy-message alert" style="display: none;">
                    <ul style="text-decoration: none;" id="tccopy-msg-list">
                    </ul>
                </div>
            </div>
            <form class="submit_tc_copy">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="modal-body">
                    <div class="col-sm-12 col-xs-12 col-md-10">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">Title
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-10">
                                <input required="" parsley-type="text" placeholder="Name" name="title" class="form-control copy_tc_title" type="text">
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">Description
                                <span class="text-danger">*</span>
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control copy_tc_description" id="copy_tc_description" rows="5" name="description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-12 col-xs-12 col-md-12">
                        <button type="button" class="btn btn-primary save_tc_new_copy">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Box for Set to Default confirmation -->
<div class="modal fade" id="default_tc_confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to set to default terms and conditions? </p>
                <input type="hidden" name="user_uuid" id="user_uuid">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm confirm_delete_tc">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@endsection @section('js')
<script src="{{ URL::asset('assets/tinymce/js/tinymce/tinymce.min.js') }} "></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.delete_terms_cond_btn', function () {

            var uuid = $(this).attr('id');
            $('#user_uuid').val(uuid);
            /* Show Confirmation alert box before deletion */
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click', '.set_to_default_btn', function () {

            /* Show Confirmation alert box before deletion */
            $('#default_tc_confirm').modal('show');
        });

        $(document).on('click', '.confirm_delete_tc', function () {
            var url = "{{url('/oam/set_to_default')}}";
            window.location.href = url;
        });

        $(document).on('click', '.md_ok_delete_btn', function () {
            var user_uuid = $('#user_uuid').val();
            var url = "{{url('/oam/delete_term_and_conditions/')}}/" + user_uuid;
            window.location.href = url;
        });

        $(document).on('click', '.copy_tc', function () {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{ generate_url("oam/get_term_condition_copy") }}',
                data: { 'id': id, '_token': '{{ csrf_token() }}' },
                success: function (result) {
                    if (result.flag == 1) {
                        var title = result.title + " - Copy"
                        $('.copy_tc_title').val(title);
                        tinyMCE.get('copy_tc_description').setContent(result.description);
                        $('.md_copy_tc').modal('show');
                    }
                }
            });
        });

        $(document).on('click', '.save_tc_new_copy', function () {
            tinyMCE.triggerSave();
            $('.tccopy-message').hide();
            var params = $('.submit_tc_copy').serialize(); 
            console.log(params);
            $.ajax({
                type: 'POST',
                url: '{{ generate_url("oam/submit_tc_copy") }}',
                data: params,
                success: function (result) {
                    $("#tccopy-msg-list").empty();
                    $(".tccopy-message").removeClass('alert-danger').removeClass('alert-success');
                    if(result.flag === 0){
                        var errorArray = result.errors;
                        var list = '';
                        errorArray.forEach(function (e) {
                            list = list + '<li>' + e + '</li>';
                        });
                        $('#tccopy-msg-list').append(list);
                        $('.tccopy-message').addClass("alert-danger").show();
                    }else if (result.flag == 1) {
                        $('#tccopy-msg-list').append("Record has been saved successfully");
                        $('.tccopy-message').addClass("alert-success").show();
                        setTimeout(location.reload.bind(location), 2000);
                    }
                }
            });
        });

    });
</script>
<script>
    tinymce.init({
        selector: 'textarea',
        visibility: 'visible',
        plugins: "lists"

    });
</script> @endsection

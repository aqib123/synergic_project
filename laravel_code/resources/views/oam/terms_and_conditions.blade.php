@extends('oam.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('/oam/term_conditions_list')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Listing</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('/oam/save_term_condition') }}">

        <div class="col-sm-9 col-xs-12 col-md-9">
            <div class="card-box">
                <div class="p-20">
                    @include('flash_message') {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Title
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <input type="text" required="required" parsley-type="text" name="title" value="{{old('title')}}{{isset($t_and_c->title)?$t_and_c->title:''}}"
                                class="form-control" placeholder="Title">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-2 form-control-label">Description
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <div>
                                <textarea required class="form-control" name="description" placeholder="Description">{{old('description')}}<?php if(isset($t_and_c->description)){echo $t_and_c->description;}else{echo '';}?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-2 form-control-label">Status
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-9">
                            <div class="radio radio-info radio-inline staff_area_radio">
                                <input id="scope1_standard" class="scope1_radio" value="1" name="status" type="radio" <?php if(isset($t_and_c->status)){ if($t_and_c->status == 1){ echo 'checked'; } }?>>
                                <label for="scope1_standard">Active</label>
                            </div>
                            <div class="radio radio-info radio-inline staff_area_radio">
                                <input id="scope1_customized" class="scope1_radio" value="0" name="status" type="radio" <?php if(isset($t_and_c->status)){ if($t_and_c->status == 0){ echo 'checked'; } }?>>
                                <label for="scope1_customized">Inactive</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-success waves-effect waves-light">
                            Save
                        </button>
                        <!-- <button type="reset" class="btn btn-danger waves-effect m-l-5">
                            Reset
                        </button> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3 col-lg-3 col-xs-12">
                <div class="card page_box_shadow m-t-0">
                    <h4 class="card-header sidebar_header m-b-0">
                        Job Stats
                    </h4>
                    <ul class="list-group list-group-flush sidbar_icon">
                        <li class="list-group-item">
                                <!-- <i class="fa fa-check-square"></i> -->
                                <span> Applied</span> 0
                        </li>
                        <li class="list-group-item">
                                <span> Shortlisted</span> 0
                        </li>
                        <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                                    <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                    </ul>
                  
            
                </div>
                <div class="m-t-10">
                    <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
                </div>
            </div>
    </form>
    @section('js')
    <script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 150,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
"textcolor",
                'insertdatetime media table contextmenu paste code help wordcount'
            ],  
            toolbar: 'insert | undo redo |  formatselect | bold italic backcolor forecolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
        });
    </script> @endsection @endsection
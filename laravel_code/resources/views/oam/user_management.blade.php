@extends('oam.app') @section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url('/oam/jobs')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Users</a>
        </div>
        <h4 class="page-title">Add staff member</h4>
    </div>
</div>
<div class="row ">
    <div class="col-sm-12">
        <div class="card-box add_staff ">
            <div class="row">
            
                <div class="col-sm-12">
                    <h4 class="m-t-0 m-b-0">Contact information</h4>
                    <hr class="m-t-0">
                </div>
            </div>
            <div class="row">
            
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form_label" for="userName">Name (required)<span class="text-danger">*</span></label>
                        <input name="name"  required="" placeholder="Name" class="form-control" type="text">
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form_label" for="companyName">Company name (required)<span class="text-danger">*</span></label>
                        <input name="companyName"  required="" placeholder="Company name" class="form-control" type="text">
                    </div>
                    
                </div>
                <div class="col-sm-4"> 
                    <div class="checkbox checkbox-success m-t-30">
                        <input id="company" type="checkbox">
                        <label for="company">
                            Company
                        </label>
                    </div> 
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-4">
                    <label class="form_label" for="companyName">Phone</label>
                    <div class="phone_input input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-smartphone"></i></span>
                        <input class="form-control"  type="text">
                    </div>
                    <div class=" phone_input input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                        <input class="form-control"  type="text">
                    </div>
                    <div class=" phone_input input-group">
                        <span class="input-group-addon"><i class="zmdi zmdi-case"></i></span>
                        <input class="form-control"  type="text">
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </div>

        <div class="card-box add_staff ">
                <div class="row">
                
                    <div class="col-sm-12">
                        <h4 class="m-t-0 m-b-0">User information</h4>
                        <hr class="m-t-0">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label class="form_label" for="primaryemail">Primary email (required)<span class="text-danger">*</span></label>
                        <div class=" form-group phone_input input-group">
                            <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                            <input class="form-control"  type="text">
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-4">
                        <label class="form_label" for="userrole">Role (required)<span class="text-danger">*</span></label>
                        <div class=" form-group ">
                            <select class="c-select">
                                <option selected="" disabled>Select user role..</option>
                                <option value="1">Assistant Community Specialist</option>
                                <option value="2">Asst. Community Accountant</option>
                                <option value="3">Caretaker</option>
                            </select>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-4">
                        <label class="form_label" for="access">user access </label> 
                        <div class="radio radio-success">
                            <input name="radio" id="radio1" value="option1" type="radio">
                            <label for="radio1">
                                This user can view all properties
                            </label>
                        </div>
                        <div class="radio radio-success">
                            <input name="radio" id="radio2" value="option2" checked=""  type="radio">
                            <label for="radio2">
                                This user can only view the following properties
                            </label>
                        </div>  
                    </div>
                    
                    
                </div> 

                <div class="row">
                    <div class="col-sm-4">
                        <div class="card card-block m-b-0">
                            <div class="col-sm-6"> 
                                <select class="form-control">
                                    <option>All</option>
                                    <option>Associations</option>
                                </select> 
                            </div>
                            <div class="col-sm-6"> 
                                <div class="phone_input input-group m-b-0">
                                    <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
                                    <input class="form-control"  type="text">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="card-footer text-muted">
                            <div class="checkbox checkbox-success m-b-0">
                                <input id="selectall" type="checkbox">
                                <label for="selectall">
                                    Select all
                                </label>
                            </div>
                        </div>
                        <ul class="list-group list-group-flush select_list_border sidbar_icon">
                            <li class="list-group-item">
                                <a href="#" class="card-link">
                                    <span >  Z-HOAM - Demo</span>
                                    <i class="pull-right fa fa-plus"></i>  
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="card  text-xs-left">
                            <div class="text-success card_custom_color card-header  ">
                                1 selected properties
                            </div>
                            <div class="card-block result_box">
                                <div class="col-sm-12">
                                    <a href="#" class=" text-dark card-link">
                                        <i class=" text-success fa fa-times"></i>  
                                        <span  class=" text-success">  Z-HOAM - Demo</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <div class="card-box add_staff m-b-10">
                <div class="row">
                
                    <div class="col-sm-12">
                        <h4 class="m-t-0 m-b-0">Inspections</h4>
                        <hr class="m-t-0">
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="checkbox checkbox-success m-b-0">
                            <input id="invit_inspection" type="checkbox">
                            <label for="invit_inspection">
                                Send invite for Property Inspections
                            </label>
                        </div>
                        <p class="lead font-13 m-b-0 m-t-10">When you click save, we will:</p>
                        <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                            <span> Use the listed email address as the username</span>
                        </p>
                        <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                            <span>Assign a temporary password</span>
                        </p>
                        <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                            <span>Email instructions to the user telling them how to sign in</span>
                        </p>
                    </div>
                </div>
        </div>
        
    </div>
    <div class="col-sm-12">
        <button type="button" class="btn btn-dark-outline waves-effect waves-light m-b-10">Save</button>
    </div>

</div>

<!-- end col-->



<!-- Add user section starts from here -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="{{url('/oam/jobs')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To User roles</a>
        </div>
        <h4 class="page-title">Add user role</h4>
    </div>
</div>
<div class="row ">
    <div class="col-sm-12">
        <div class="card-box add_staff add_user_role">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form_label" for="userName">Name </label>
                        <input name="name" placeholder="Name" class="form-control" type="text">
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="form_label" for="userName">Description </label>
                        <textarea name="name" placeholder="Name" class="form-control" type="text"></textarea>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-sm-12">
                    <div class="bottom_border">
                        <h6 class="m-t-0 m-b-10 font-13"><b>Select the features you want to enable for this type of user</b></h6>
                    </div> 
                </div> 
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="bottom_border add_user_role_detail">
                        <div class="checkbox checkbox-success m-b-0 m-t-10">
                            <input id="leasing" type="checkbox">
                            <label for="leasing">
                                Leasing menu
                            </label>
                        </div>
                        <p class="text-muted font-13">Listings, applicants, eLease, and renewals</p>
                        <div class="card-box add_user_role_box">
                            <dl class="row bottom_border m-b-10">
                                <dt class="col-sm-2">View</dt>
                                <dt class="col-sm-2">Edit</dt>
                                <dt class="col-sm-2">Delete</dt>
                            </dl>
                            <dl class="row bottom_border m-b-10">
                                <dt class="col-sm-2">
                                    <span class=" m-b-0 m-t-10">
                                        <input id="leasing" type="checkbox" checked>
                                        <label for="leasing"></label>
                                    </span>
                                </dt>
                                <dt class="col-sm-2">
                                    <span class=" m-b-0 m-t-10">
                                        <input id="leasing" type="checkbox">
                                        <label for="leasing"></label>
                                    </span>
                                </dt>
                                <dt class="col-sm-2">
                                    <span class=" m-b-0 m-t-10">
                                        <input id="leasing" type="checkbox">
                                        <label for="leasing"></label>
                                    </span>
                                </dt>
                                <dd class="col-sm-6">Listings</dd>
                            </dl>
                            <dl class="row bottom_border m-b-10">
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox" checked>
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox">
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox">
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dd class="col-sm-6">Applicants</dd>
                                </dl>
                                <dl class="row bottom_border m-b-10">
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox" checked>
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox">
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dt class="col-sm-2">
                                        <span class=" m-b-0 m-t-10">
                                            <input id="leasing" type="checkbox">
                                            <label for="leasing"></label>
                                        </span>
                                    </dt>
                                    <dd class="col-sm-6">eLease, draft leases, and renewals</dd>
                                </dl>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12">
                    <button type="button" class="btn btn-dark-outline waves-effect waves-light m-b-10">Save user role</button>
                </div>
            </div>

        </div>
    </div>
</div>



@endsection
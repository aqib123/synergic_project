@extends('oam.app') @section('content')
<!-- Page-Title -->
<style type="text/css">
.form-check-inline input[type="radio"]{
    margin-right: 10px !important;
}
</style>
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row"> 
    <div class="col-sm-12 col-xs-12 col-md-9"> 
         <div class="card-box">
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="1">Manual Data Entery
              </label>
            </div>
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="2">Bulk Import
              </label>
            </div>
    </div>
    <div class="col-sm-12 col-xs-12 col-md-9 manual_adding" style="display: none;"> 
        <form role="form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="{{ url('oam/save_assets/') }}">
            {{ csrf_field() }}

           
                <div class="card-box">
                    <div class="form-group row">
                        <div class="col-sm-6 padding-left-0 padding-right-0">
                            <div class="col-sm-12">
                                <h5 class="m-t-0 m-b-15">Asset Name</h5>
                            </div>
                            <div class="col-sm-12">
                                <input type="hidden" name="id" value="{{Request::input('id')}}">
                                <input type="text" class="form-control" placeholder="Asset Name" name="assets" value="{{old('name')}}{{isset($assets->name)?$assets->name:''}}">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
           
            <div class="col-sm-12 col-xs-12 col-md-12">
                <div class="form-group row">
                    <div class="col-sm-8">
                        <button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-sm-6 col-xs-12 col-md-6 row bulk_importing" style="display: none;">
        <div class="card-box">
            <form enctype="multipart/form-data" role="form" data-parsley-validate novalidate method="POST" action="{{ url('oam/bulk/import/assets/') }}">
                <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }} row">
                    <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                    <div class="col-md-6">
                        <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-4">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="header"> File contains header row?
                            {{ csrf_field() }}
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            Parse CSV
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-sm-3 col-lg-3 col-xs-12">
        <div class="card page_box_shadow m-t-0">
            <h4 class="card-header sidebar_header m-b-0">
                Job Stats
            </h4>
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item">
                        <!-- <i class="fa fa-check-square"></i> -->
                        <span> Applied</span> 0
                </li>
                <li class="list-group-item">
                        <span> Shortlisted</span> 0
                </li>
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                            <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
            </ul>
            
    
        </div>
        <div class="m-t-10">
            <img class="card-img-top img-fluid" src="{{ asset('assets/images/sidebanner.jpg') }}"  alt="add banner"> 
        </div>
    </div>
</div>

    


@endsection
@section('js')
    <script type="text/javascript">    

            $(document).on('click', '.data_entry_type', function(){
                if($(this).val() == 1){
                    $('.bulk_importing').hide();
                    $('.manual_adding').show();
                }else{
                    $('.bulk_importing').show();
                    $('.manual_adding').hide();
                }
            });
    </script>
@endsection
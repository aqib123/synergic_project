@extends('oam.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <form role="form" data-parsley-validate novalidate method="POST" enctype="multipart/form-data" action="">
        {{ csrf_field() }}
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="card-box">
                <!-- <div class="col-md-2"></div> -->
                <div class="col-md-12">
                        <div class="text-xs-center">
                            <div class="p-20">
                                <h3 class="card-title lead">Coming Soon...!</h3>
                            </div>
                        </div>
                </div>
                <!-- <div class="col-md-2"></div> -->
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group row">
                <div class="col-sm-8">
                    <!--<button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                        Update
                    </button>-->
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
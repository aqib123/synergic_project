<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content=""> 

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Favicon -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

    <!-- App title -->
    <title>{{ isset($title)?$title:'' }}</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/morris/morris.css') }}">

    <!-- Switchery css -->
    <link href="{{ URL::to('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />

    <!-- Jquery filer css -->
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/jquery.filer.css') }}" rel="stylesheet" />
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />
    <!-- DataTables -->
    <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Muliselect -->
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }} " rel="stylesheet" type="text/css"/>
    <!-- App CSS -->
    <link href="{{ URL::to('assets/css/style.css?ver=4.8.0') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/custom.css?ver=3.2.4') }}" rel="stylesheet" type="text/css" />
    <!-- Plugins css -->
    <link href="{{ URL::to('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/froala_editor.css') }}" rel="stylesheet">
    
    <!-- C3 charts css -->
    <link href="{{ URL::asset('assets/plugins/c3/c3.min.css') }}" rel="stylesheet" type="text/css"  />
    <style>
     .link_no_href{
      cursor: pointer;
     }

    </style>
    <!-- Sweet Alert css -->
    <link href="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css"/>
    <style>
        .notification_icon{
        float: left;
        height: 36px;
        width: 36px;
        line-height: 36px;
        text-align: center;
        margin-right: 10px;
        border-radius: 50%;
        }
            </style>
    <!--calendar css-->
    @yield('calendarCss')
    @yield('css')
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    <!-- Modernizr js -->
    <script src="{{ URL::to('assets/js/modernizr.min.js') }}"></script>

</head>


<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="{{url('/oam/home')}}" class="logo">
                        <span>
                             <img src="{{ URL::to('assets/images/logo_login.png') }}" alt="logo" width="60%">
                        </span>
                    </a>
                </div>
                <!-- End Logo container-->


                <div class="menu-extras">

                    <ul class="nav navbar-nav pull-right">

                        <li class="nav-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" id="oam_bell_icon" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <i class="zmdi zmdi-notifications-none noti-icon"></i>
                                <span id="notification_badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>
                                        <small class="notification_counter">
                                            <span class="label label-default pull-xs-right"></span>Notification
                                        </small>
                                    </h5>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="{{ URL::to('assets/images/loading.gif') }}" style="height: 40px !important; margin-left: 116px;" style="display: none;" class="loading_gif_class">
                                <div class="notification_data">
                                </div>

                                <!-- All-->
                                <a href="{{url('admin_all_notifications')}}" class="dropdown-item notify-item notify-all">
                                    View All
                                </a>

                            </div>
                        </li>

                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link imag_link   dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                @if(isset(Auth::user()->image))
                                <div class="user_image_padding"> 
                                    <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image) }}" alt="{{ Auth::user()->image }}"
                                    /> 
                                </div>
                                @else
                                <div class="user_image_padding"> 
                                    <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/default_user.png') }}" alt="{{ Auth::user()->image }}"
                                    /> 
                                </div> 
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow">
                                        <small>Welcome ! 
                                            @if(isset(Auth::user()->name))
                                                {{Auth::user()->name}} : {{'Admin'}}
                                            @endif
                                        </small>
                                    </h5>
                                </div>

                                <!-- item-->
                                <a href="{{url('oam/oam_profile')}}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-account-circle"></i>
                                    <span>Profile</span>
                                </a>

                                <!-- item-->
                                <!--<a href="{{url('oam/settings')}}" class="dropdown-item notify-item">-->
                                <!--    <i class="zmdi zmdi-settings"></i>-->
                                <!--    <span>Settings</span>-->
                                <!--</a> -->

                                <!-- item-->
                                <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i>
                                    <span class="logout">Logout</span>
                                </a>

                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end menu-extras -->
                <div class="clearfix"></div>
            </div>
            <!-- end container -->
        </div>
        <!-- end topbar-main -->
        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li>
                            <a href="{{ url('/oam/home') }}">
                                <span> Home </span>
                            </a>
                        </li>
                        @if(GeneralFunctions::check_view_permission('/oam/jobs'))
                        <li class="has-submenu">
                            <a href="{{url('/oam/jobs')}}">
                                <span> Jobs </span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/oam/job_form'))
                                        <li>
                                            <a href="{{ url('/oam/job_form') }}">Add New Job</a>
                                        </li>
                                        @endif
                                        @if(GeneralFunctions::check_view_permission('/oam/jobs'))
                                        <li>
                                            <a href="{{url('/oam/jobs')}}">Job List</a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        @if(GeneralFunctions::check_view_permission('/oam/buildings'))
                        <li class="has-submenu">
                            <a href="{{ url('/oam/buildings') }}">
                                <span>Communities</span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/oam/building_form'))
                                        <li>
                                            <a href="{{ url('/oam/building_form') }}">Add Building</a>
                                        </li>
                                        @endif
                                        @if(GeneralFunctions::check_view_permission('/oam/buildings'))
                                        <li>
                                            <a href="{{ url('/oam/buildings') }}">Buildings List</a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        @if(GeneralFunctions::check_view_permission('/oam/assets'))
                        <li class="has-submenu">
                            <a href="{{ url('/oam/assets') }}">
                                <span> Assets </span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/oam/add_assets'))
                                        <li>
                                            <a href="{{ url('/oam/add_assets') }}">Add Assets</a>
                                        </li>
                                        @endif
                                        @if(GeneralFunctions::check_view_permission('/oam/assets'))
                                        <li>
                                            <a href="{{ url('/oam/assets') }}">Assets Listing</a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        <!-- <li class="has-submenu">
                            <a href="{{ url('/oam/term_conditions_list') }}">
                                <span> Templates </span>
                            </a>
                             
                        </li> -->
                        <li class="has-submenu">
                            <a href="{{ url('/oam/show_assigned_jobs') }}">
                                <span> Assigned Jobs </span>
                            </a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{url('oam/company/account/details')}}">
                                <span> Company Details </span>
                            </a>
                        </li>
                        @if(Auth::user()->parent_id == 0)
                        <li class="has-submenu">
                            <a class="link_no_href">
                                <span> More... </span>
                            </a>
                            <!-- <button type="button" class=" link_no_href btn btn-sm btn-secondary btn-rounded waves-effect">More...</button> -->
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <!-- <li>
                                            <a href="{{ url('/oam/staff/form') }}">Add Staff Member</a>
                                        </li> -->
                                        <li>
                                            <a href="{{ url('/oam/staff/member/list') }}">Users</a>
                                        </li>
                                        <!-- <li>
                                            <a href="{{ url('/oam/staff/roles') }}">Add Roles</a>
                                        </li> -->
                                        <li>
                                            <a href="{{ url('/oam/roles/list') }}">User Roles</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/oam/term_conditions_list') }}">
                                                Templates  
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @endif

                    </ul>
                    <!-- End navigation menu  -->
                </div>
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="wrapper">
        <div class="container">

            @yield('content')

            <!-- Footer -->
            <footer class="footer text-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            2018 © Synergic.
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->
            
        </div>
        <!-- container -->



    </div>
    <!-- End wrapper -->

    <!-- jQuery  -->
    <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>
    
    <script src="{{ URL::to('assets/js/tether.min.js') }}"></script>
    <!-- Tether for Bootstrap -->
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/waves.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/switchery/switchery.min.js') }}"></script>
    <!-- Counter Up  -->
    <script src="{{ URL::to('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

    
    <!-- Page specific js -->
    <!-- <script src="{{ URL::to('assets/pages/jquery.dashboard.js') }}"></script> -->
    <!-- Jquery filer js -->
    <script src="{{ URL::to('assets/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>
    <!-- page specific js -->
    <!-- <script src="{{ URL::to('assets/pages/jquery.fileuploads.init.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/mjolnic-bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script> -->
    <!-- <script src="{{ URL::to('assets/pages/jquery.form-pickers.init.js') }}"></script> -->
    <!-- Required datatable js -->
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- MuliSelect Libraries -->
    <script type="text/javascript" src="{{ URL::asset('assets/plugins/multiselect/js/jquery.multi-select.js') }} "></script>
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.full.min.js') }} " type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/pages/jquery.formadvanced.init.js') }}"></script>
    <!-- App js -->
    <script src="{{ URL::to('assets/js/jquery.core.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.app.js') }}"></script>
    <!-- Sweet Alert js -->
    <script src="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <script src="{{ URL::asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/plugins/jquery-play-sound/jquery.playSound.js') }}"></script>
      <script src="{{ URL::asset('assets/js/jquery.repeater.min.js') }}"></script>
    
    @yield('js')
    <script>
       
        $(document).ready(function(){
            $(".navigation-menu a").each(function () {
                if (this.href == window.location.href) {
                    $(this).parent().addClass("active"); // add active to li of the current link
                    $(this).parent().parent().parent().addClass("active"); // add active class to an anchor
                    $(this).parent().parent().parent().parent().parent().addClass("active"); // add active class to an anchor
                }
            });
            $('#datatable').DataTable();
            var resizefunc = [];
            $(document).on('click', '.logout', function(){
                var url = "{{ URL::to('/logout') }}";
                location.replace(url);
            });

            // Setting Page Append Functionality
            //group add limit
            var maxGroup = 10;
            
            //add more fields group in Assets
            $(".addMore").click(function(){
                if($('body').find('.fieldGroup').length < maxGroup){
                    var fieldHTML = '<div class="form-group fieldGroup">'+$(".fieldGroupCopy").html()+'</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' Assets are allowed.');
                }
            });
            
            //add more fields group in Buildings
            $(".addMoreBuilding").click(function(){
                if($('body').find('.fieldGroupBuilding').length < maxGroup){
                    var fieldHTML = '<div class="form-group fieldGroupBuilding">'+$(".fieldGroupBuildingCopy").html()+'</div>';
                    $('body').find('.fieldGroupBuilding:last').after(fieldHTML);
                }else{
                    alert('Maximum '+maxGroup+' Buildings are allowed.');
                }
            });
            //remove fields group
            $("body").on("click",".removeBuilding",function(){ 
                $(this).parents(".fieldGroupBuilding").remove();
            });

            $("body").on("click",".remove",function(){ 
                $(this).parents(".fieldGroup").remove();
            });

            $('#oam_bell_icon').on('click', function(){
                $('.notification_data').html('');
                $('.notification_counter').html('');
                $('.loading_gif_class').show(); 
                $.ajax({
                    type: 'GET',
                    url: "{{ generate_url('get_oam_notification') }}",
                    success:function(data){
                        console.log(data);
                        setTimeout(function(){ 
                            $('.loading_gif_class').hide();
                            if(data.counter > 0){
                                $('.notification_counter').html('<span class="label label-danger pull-xs-right">'+data.counter+'</span>');
                            }
                            $('.notification_data').html(data.html);
                             $('#notification_badge').removeClass('noti-icon-badge');
                        }, 200);
                    }
                });
            });
            setInterval(function(){
                $.ajax({
                    type: 'GET',
                    url: '{{generate_url("get_oam_instant_notification")}}',
                    success: function(response) {
                        $('#sa-close').modal('show');
                        if(response.flag == 1){
                            swal({
                                title: response.title,
                                text: response.description,
                                timer: 4000,
                                showConfirmButton: false
                            });
                            $.playSound("{{asset('assets/plugins/jquery-play-sound/sound/notify')}}");
                            $('#notification_badge').addClass('noti-icon-badge');
                        }
                    }
                });
            }, 5000);
            
            @if(\Session::has('profile_not_complete'))
                setTimeout(function(){
                    $('.md_profile_completion').modal('show');
                }, 400);
            @endif
        });
        
    </script>

</body>
<!-- Modal Box for Profile Completion -->
<div class="modal fade md_profile_completion" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Alert Information</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Please complete the Profile first before submitting the Job.</p>
            </div>
        </div>
    </div>
</div>

</html>
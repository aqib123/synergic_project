@extends('layouts.app') @section('content') @section('css')
<style>
    .newsletter_area {
        cursor: pointer;
    }

    .editable-input input[type="text"] {
        width: 400px;
    }

    textarea {
        width: 500px !important;
    }
</style>
<!-- X-editable css -->
<link type="text/css" href="{{ url('assets/plugins/x-editable/css/bootstrap-editable.css')}}" rel="stylesheet">
<link href="{{ url('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" /> @endsection
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">
    </ul>
</div>
<ul class="nav nav-tabs association_nav" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="newsletter-tab" data-toggle="tab" href="#newsletter" role="tab" aria-controls="newsletter"
            aria-expanded="true">Newsletter</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="subscription-tab" data-toggle="tab" href="#subscription" role="tab" aria-controls="subscription">Subscription</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="signup-tab" data-toggle="tab" href="#signup" role="tab" aria-controls="signup">Sign Up</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="change-credentials-tab" data-toggle="tab" href="#change_credentials" role="tab" aria-controls="change_credentials">Change Credentials</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="app-disapp-account-tab" data-toggle="tab" href="#app_disapp_account" role="tab" aria-controls="app_disapp_account">Approve Disapprove Account</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <!-- FOR NEWSLETTER -->
    <div role="tabpanel" class="tab-pane fade in active" id="newsletter" aria-labelledby="newsletter-tab">
        <div class="row">
            <!-- <form role="form" data-parsley-validate novalidate method="POST" action="{{url('save_newsletter')}}">
                {{ csrf_field() }} -->
                <div class="col-sm-12 col-xs-12 col-md-12">
                    <div class="card-box">
                        <!-- <div class="col-md-2"></div> -->
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <h4 class="lead font-13 m-t-10">
                                    <b>Select OAM</b>
                                </h4>
                            </div>
                            <div class="col-md-4">
                                <select id="oam_multiselect" class="select2 form-control select2-multiple" name="oam_email[]" multiple="multiple" multiple
                                    data-placeholder="Choose ...">
                                    @if(isset($oam) && count($oam) > 0) @foreach($oam as $oam_email)
                                    <option value="{{$oam_email->email}}">{{$oam_email->name}}</option>
                                    @endforeach @endif
                                </select>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox checkbox-success checkbox-inline" style="margin-top: 5px;">
                                    <input id="oam_checkbox" value="1" name="all_oam" type="checkbox">
                                    <label for="oam_checkbox"> Select All </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="lead font-13 m-t-10">
                                    <b>Select FMC</b>
                                </h4>
                            </div>
                            <div class="col-md-4">
                                <select id="fmc_multiselect" class="select2 form-control select2-multiple" name="fmc_email[]" multiple="multiple" multiple
                                    data-placeholder="Choose ...">
                                    @if(isset($fmc) && count($fmc) > 0) @foreach($fmc as $fmc_email)
                                    <option value="{{$fmc_email->email}}">{{$fmc_email->name}}</option>
                                    @endforeach @endif
                                </select>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox checkbox-success checkbox-inline" style="margin-top: 5px;">
                                    <input id="fmc_checkbox" value="1" name="all_fmc" type="checkbox">
                                    <label for="fmc_checkbox"> Select All </label>
                                </div>
                            </div>
                        </div> -->
                        <div class="row  m-t-20">
                            <div class="col-md-6 ">
                                <h4 class="lead font-13 m-t-10">
                                    <b>Newsletter
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Newsletter Preview"></i>
                                    </b>
                                </h4>
                                <div class="card m-b-0 card-inverse card-info">
                                    <div class="card-block text-sm-center">
                                        <blockquote class="card-blockquote">
                                            <h3 id="main_heading" class="main_heading">{{isset($newsletter->heading)?$newsletter->heading:''}}</h3>
                                            <h5 id="sub_heading" class="sub_heading">{{isset($newsletter->sub_heading)?$newsletter->sub_heading:''}}</h5>
                                            <p class="short_description" id="short_description" data-type="textarea" data-pk="1" data-placeholder="Short description here..."
                                                data-title="Enter Description">{{isset($newsletter->short_description)?$newsletter->short_description:''}}</p>
                                        </blockquote>
                                    </div>
                                </div>
                                <div class="card ">
                                    <div class="card-block text-sm-left">
                                        <p class="full_description" id="full_description" class="card-text" data-type="textarea" data-pk="1" data-placeholder="Write content here..."
                                            data-cols="4" data-title="Enter Description">{{isset($newsletter->full_description)?$newsletter->full_description:''}}</p>
                                    </div>
                                    <div class="card-footer text-sm-center text-muted">
                                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                            <div>Copyright © 2018
                                                <a class="card-link" href="#" target="_blank"> Synergic </a>. All&nbsp;rights&nbsp;reserved.</div>
                                            <div>If you do not want to receive emails from us, you can
                                                <a class="card-link" href="#" target="_blank">
                                                    unsubscribe </a>.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-12 col-xs-12 col-md-12">
                    <div class="form-group">
                        <button type="button" id="send_newsletter" class="profie_update btn btn-success waves-effect waves-light">
                            Save
                        </button>
                    </div>
                </div>
            <!-- </form> -->
        </div>
    </div>
    <!-- END NEWSLETTER -->

    <!-- FOR SUBSCRIPTION -->
    <div role="tabpanel" class="tab-pane fade" id="subscription" aria-labelledby="subscription-tab">
        <div class="card-box row">
            <div class="col-md-6 ">
                <h4 class="lead font-13 m-t-10">
                    <b>Subscription
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Subscription Preview"></i>
                    </b>
                </h4>
                    <div class="card m-b-0 card-inverse card-info">
                        <div class="card-block text-sm-center">

                            <blockquote class="card-blockquote">
                                <h3 id="main_heading2" class="main_heading">{{isset($subscription->heading)?$subscription->heading:''}}</h3>
                                <h5 id="sub_heading2" class="sub_heading">{{isset($subscription->sub_heading)?$subscription->sub_heading:''}}</h5>
                                <p class="short_description" id="short_description2" data-type="textarea" data-pk="1" data-placeholder="Short description here..."
                                    data-title="Enter Description">{{isset($subscription->short_description)?$subscription->short_description:''}}.</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="card ">
                        <div class="card-block text-sm-left">
                            <p class="full_description" id="full_description2" class="card-text" data-type="textarea" data-pk="1" data-placeholder="Write content here..."
                                data-cols="4" data-title="Enter Description">{{isset($subscription->full_description)?$subscription->full_description:''}}</p>
                        </div>
                        <div class="card-footer text-sm-center text-muted">
                            <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                                <div>Copyright © 2018
                                    <a class="card-link" href="#" target="_blank"> Synergic </a>. All&nbsp;rights&nbsp;reserved.</div>
                                <div>If you do not want to receive emails from us, you can
                                    <a class="card-link" href="#" target="_blank">
                                        unsubscribe </a>.</div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group">
                <button type="button" id="save_subscription" class="profie_update btn btn-success waves-effect waves-light save_mailing_content">
                    Save
                </button>
            </div>
        </div>
    </div>

    <!-- FOR SIGNUP -->
    <div role="tabpanel" class="tab-pane fade" id="signup" aria-labelledby="signup-tab">
        <div class="card-box row">
            <div class="col-md-6 ">
                <h4 class="lead font-13 m-t-10">
                    <b>Signup
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Signup Preview"></i>
                    </b>
                </h4>
                <div class="card m-b-0 card-inverse card-info">
                    <div class="card-block text-sm-center">
                        <blockquote class="card-blockquote">
                            <h3 id="main_heading3" class="main_heading">{{isset($signup->heading)?$signup->heading:''}}</h3>
                            <h5 id="sub_heading3" class="sub_heading">{{isset($signup->sub_heading)?$signup->sub_heading:''}}</h5>
                            <p class="short_description" id="short_description3" data-type="textarea" data-pk="1" data-placeholder="Short description here..."
                                data-title="Enter Description">{{isset($signup->short_description)?$signup->short_description:''}}</p>
                        </blockquote>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-block text-sm-left">
                        <p class="full_description" id="full_description3" class="card-text" data-type="textarea" data-pk="1" data-placeholder="Write content here..."
                            data-cols="4" data-title="Enter Description">{{isset($signup->full_description)?$signup->full_description:''}}</p>
                    </div>
                    <div class="card-footer text-sm-center text-muted">
                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                            <div>Copyright © 2018
                                <a class="card-link" href="#" target="_blank"> Synergic </a>. All&nbsp;rights&nbsp;reserved.</div>
                            <div>If you do not want to receive emails from us, you can
                                <a class="card-link" href="#" target="_blank">
                                    unsubscribe </a>.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group">
                <button type="button" id="save_signup" class="profie_update btn btn-success waves-effect waves-light save_mailing_content">
                    Save
                </button>
            </div>
        </div>
    </div>
    <!-- END SIGNUP -->

    <!-- FOR CHANGE CREDENTIALS -->
    <div role="tabpanel" class="tab-pane fade" id="change_credentials" aria-labelledby="change-credentials-tab">
        <div class="card-box row">
            <div class="col-md-6 ">
                <h4 class="lead font-13 m-t-10">
                    <b>Change Credentials
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Change Credentials Preview"></i>
                    </b>
                </h4>
                <div class="card m-b-0 card-inverse card-info">
                    <div class="card-block text-sm-center">
                        <blockquote class="card-blockquote">
                            <h3 id="main_heading4" class="main_heading">{{isset($change_credentials->heading)?$change_credentials->heading:''}}</h3>
                            <h5 id="sub_heading4" class="sub_heading">{{isset($change_credentials->sub_heading)?$change_credentials->sub_heading:''}}</h5>
                            <p class="short_description" id="short_description4" data-type="textarea" data-pk="1" data-placeholder="Short description here..."
                                data-title="Enter Description">{{isset($change_credentials->short_description)?$change_credentials->short_description:''}}</p>
                        </blockquote>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-block text-sm-left">
                        <p class="full_description" id="full_description4" class="card-text" data-type="textarea" data-pk="1" data-placeholder="Write content here..."
                            data-cols="4" data-title="Enter Description">{{isset($change_credentials->full_description)?$change_credentials->full_description:''}}</p>
                    </div>
                    <div class="card-footer text-sm-center text-muted">
                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                            <div>Copyright © 2018
                                <a class="card-link" href="#" target="_blank"> Synergic </a>. All&nbsp;rights&nbsp;reserved.</div>
                            <div>If you do not want to receive emails from us, you can
                                <a class="card-link" href="#" target="_blank">
                                    unsubscribe </a>.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group">
                <button type="button" id="save_credentials" class="profie_update btn btn-success waves-effect waves-light save_mailing_content">
                    Save
                </button>
            </div>
        </div>
    </div>
    <!-- END CREDENTIALS -->

    <!-- FOR APPROVE DISAPPROVE ACCOUNT -->
    <div role="tabpanel" class="tab-pane fade" id="app_disapp_account" aria-labelledby="app-disapp-account-tab">
        <div class="card-box row">
            <div class="col-md-6 ">
                <h4 class="lead font-13 m-t-10">
                    <b>Approve or Disapprove Account
                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Approve or Disapprove Account Preview"></i>
                    </b>
                </h4>
                <div class="card m-b-0 card-inverse card-info">
                    <div class="card-block text-sm-center">
                        <blockquote class="card-blockquote">
                            <h3 id="main_heading5" class="main_heading">{{isset($app_disapp->heading)?$app_disapp->heading:''}}</h3>
                            <h5 id="sub_heading5" class="sub_heading">{{isset($app_disapp->sub_heading)?$app_disapp->sub_heading:''}}</h5>
                            <p class="short_description" id="short_description5" data-type="textarea" data-pk="1" data-placeholder="Short description here..."
                                data-title="Enter Description">{{isset($app_disapp->short_description)?$app_disapp->short_description:''}}</p>
                        </blockquote>
                    </div>
                </div>
                <div class="card ">
                    <div class="card-block text-sm-left">
                        <p class="full_description" id="full_description5" class="card-text" data-type="textarea" data-pk="1" data-placeholder="Write content here..."
                            data-cols="4" data-title="Enter Description">{{isset($app_disapp->full_description)?$app_disapp->full_description:''}}</p>
                    </div>
                    <div class="card-footer text-sm-center text-muted">
                        <div style="font-family:Helvetica,Arial,sans-serif;font-size:13px;color:#828282;text-align:center;line-height:120%;">
                            <div>Copyright © 2018
                                <a class="card-link" href="#" target="_blank"> Synergic </a>. All&nbsp;rights&nbsp;reserved.</div>
                            <div>If you do not want to receive emails from us, you can
                                <a class="card-link" href="#" target="_blank">
                                    unsubscribe </a>.</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group">
                <button type="button" id="save_account_status" class="profie_update btn btn-success waves-effect waves-light save_mailing_content">
                    Save
                </button>
            </div>
        </div>
    </div>
    <!-- END APPROVE DISAPPROVE ACCOUNT -->
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="modal-title" id="exampleModalLabel">Important Note</h5>
            </div>
            <div class="modal-body">
                To customizes newsletter do following steps:-
                <br /> 1. Click on each text having dotted underline
                <br /> 2. Edit content
                <br /> 3. Click on
                <b>Tick</b> icon
                <br />
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@section('js')
<!-- XEditable Plugin -->
<script src="{{ url('assets/plugins/moment/moment.js')}}"></script>
<script type="text/javascript" src="{{ url('assets/plugins/x-editable/js/bootstrap-editable.min.js')}}"></script>
<script type="text/javascript" src="{{ url('assets/pages/jquery.xeditable.js')}}"></script>

<script type="text/javascript" src="{{ url('assets/pages/jquery.formadvanced.init.js')}}"></script>
<script src="{{ url('assets/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
<script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
<script>

    $(function () {

        $('.main_heading').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Heading',
            mode: 'inline'
        });
        $('.sub_heading').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Sub Heading',
            mode: 'inline'
        });
        $('.short_description').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
        $('.full_description').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });


        $('#main_heading2').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Heading',
            mode: 'inline'
        });
        $('#sub_heading2').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Sub Heading',
            mode: 'inline'
        });
        $('#short_description2').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
        $('#full_description2').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });

        $('#main_heading3').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Heading',
            mode: 'inline'
        });
        $('#sub_heading3').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Sub Heading',
            mode: 'inline'
        });
        $('#short_description3').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
        $('#full_description3').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });

        $('#main_heading4').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Heading',
            mode: 'inline'
        });
        $('#sub_heading4').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Sub Heading',
            mode: 'inline'
        });
        $('#short_description4').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
        $('#full_description4').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });


        $('#main_heading5').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Heading',
            mode: 'inline'
        });
        $('#sub_heading5').editable({
            type: 'text',
            pk: 1,
            name: 'username',
            title: 'Enter Sub Heading',
            mode: 'inline'
        });
        $('#short_description5').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
        $('#full_description5').editable({
            showbuttons: 'bottom',
            mode: 'inline'
        });
    });
    $(document).ready(function () {
        var e_success = '{{\Session::has("alert-danger")}}';
        var e_success2 = '{{\Session::has("alert-success")}}';
        //alert(e_success);
        if (e_success == '' && e_success2 == '') {
            $(window).on('load', function () {
                $('#myModal').modal('show');
            });
        }

        $(document).on('click', '#send_newsletter', function () {
            var data = {};
            data.heading = $("#main_heading").text();
            data.sub_heading = $("#sub_heading").text();
            data.short_description = $("#short_description").text();
            data.full_description = $("#full_description").text();
            data.status = 4;
            data.title = "Newsletters";
            data._token = "{{ csrf_token() }}";
            $('.msg-box').hide();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_mailing_content")}}',
                data: data,
                success: function(result){
                    $("#msg-list").empty();
                    if(result.flag == 1){
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        });


        $(document).on('click', '#save_subscription', function () {
            var data = {};
            data.heading = $("#main_heading2").text();
            data.sub_heading = $("#sub_heading2").text();
            data.short_description = $("#short_description2").text();
            data.full_description = $("#full_description2").text();
            data.status = 5;
            data.title = "Subscription";
            data._token = "{{ csrf_token() }}";
            $('.msg-box').hide();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_mailing_content")}}',
                data: data,
                success: function(result){
                    $("#msg-list").empty();
                    if(result.flag == 1){
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });

        });

         $(document).on('click', '#save_signup', function () {
            var data = {};
            data.heading = $("#main_heading3").text();
            data.sub_heading = $("#sub_heading3").text();
            data.short_description = $("#short_description3").text();
            data.full_description = $("#full_description3").text();
            data.status = 1;
            data.title = "Signup";
            data._token = "{{ csrf_token() }}";
            $('.msg-box').hide();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_mailing_content")}}',
                data: data,
                success: function(result){
                    $("#msg-list").empty();
                    if(result.flag == 1){
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        });

        $(document).on('click', '#save_credentials', function () {
            var data = {};
            data.heading = $("#main_heading4").text();
            data.sub_heading = $("#sub_heading4").text();
            data.short_description = $("#short_description4").text();
            data.full_description = $("#full_description4").text();
            data.status = 3;
            data.title = "Changing Credentials";
            data._token = "{{ csrf_token() }}";
            $('.msg-box').hide();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_mailing_content")}}',
                data: data,
                success: function(result){
                    $("#msg-list").empty();
                    if(result.flag == 1){
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        });

        $(document).on('click', '#save_account_status', function () {
            var data = {};
            data.heading = $("#main_heading5").text();
            data.sub_heading = $("#sub_heading5").text();
            data.short_description = $("#short_description5").text();
            data.full_description = $("#full_description5").text();
            data.status = 2;
            data.title = "Approve or disapprove account";
            data._token = "{{ csrf_token() }}";
            console.log(data);
            $('.msg-box').hide();
            $.ajax({
                type: 'POST',
                url: '{{generate_url("save_mailing_content")}}',
                data: data,
                success: function(result){
                    $("#msg-list").empty();
                    if(result.flag == 1){
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        });

        $('#oam_checkbox').change(function () {
            if ($(this).prop('checked')) {
                $("#oam_multiselect").prop("disabled", true);
            }
            else {
                $("#oam_multiselect").prop("disabled", false);
            }
        });
        $('#fmc_checkbox').change(function () {
            if ($(this).prop('checked')) {
                $("#fmc_multiselect").prop("disabled", true);
            }
            else {
                $("#fmc_multiselect").prop("disabled", false);
            }
        });
    });
    tinymce.init({
        selector: 'textarea',
        height: 150,
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
        setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
        }
    });
</script> @endsection @endsection
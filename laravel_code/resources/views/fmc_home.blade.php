@extends('fmc.app') @section('content') @section('calendarCss')

<!--calendar css-->
<link href="{{ URL::to('assets/plugins/fullcalendar/dist/fullcalendar.css') }}" rel="stylesheet" />
@endsection
@section('css')
<style>
.fill_prof {
    font-size: 12px;
    font-weight: 800;
    margin-top: 13px;
}
.stat_boxes h6 {
    font-size: 11px;
}
.dashboard_grid_header {
    padding: 0.7rem 0.6rem;
    background-color: #fff;
    border-bottom: 1px solid #ccc;
    /* font-weight: 200; */
}

.dashboard_grid_header h4 {
    font-weight: 400;
    font-size: 17px;
    font-family: ProximaNova, arial, sans-serif !important;
    color: #000;
}

.list_detail {
    border: none;
    padding: 7px 6px 30px 6px;
}

.list_desgin {
    margin-bottom: 5px;
    background-color: #F9F9F9;
    padding: 10px 5px !important;
    border: 1px solid #ccc !important;
    border-radius: 2px;
}

.job_description_title {
    font-size: 13px;
    font-weight: 600 !important;
    color: #000 !important;
}

.text_dark_blue {
    color: #0087e0;
}

.job_description_detail {
    color: #515151 !important;
}

.font-11 {
    font-size: 11px;
}

.job_stats {
    margin-right: 15px;
}

.task_nav .active {
    font-weight: 700;
    font-family: ProximaNova, arial, sans-serif !important;
    color: #191919;
    background: #fff;
    border-left: 1px solid #BBB;
    border-right: 1px solid #BBB;
    border-top: 1px solid #BBB;
    z-index: 2;
}

.task_nav .nav-link {
    padding: 5px 6px;
    font-family: ProximaNova, arial, sans-serif !important;
    margin: 0 2px;
    color: #767676;
    text-decoration: none;
    border: 1px solid #DADADA;
    border-bottom: 0;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    background: linear-gradient(-180deg, #fff 75%, #E8EAED 100%);
    cursor: pointer;
    font-size: 12px;
}

.dashboard_balance {
    font-size: 24px;
    margin-left: 5px;
    border-right: 1px solid #DADADA;
    padding: 5px 5px 5px 0px;
    vertical-align: middle;
}

.outstanding_balances {
    padding: 8px;
    vertical-align: middle;
}

.main_balances {
    margin-bottom: 5px;
}

.balance_detail {

    position: relative;
    display: inline-block;
    width: 100%;
    padding: 12px 10px;
    background-color: #F4F4F4;
    background: linear-gradient(180deg, rgba(240, 240, 240, .2) 0, rgba(209, 209, 209, .2) 100%);
    background-image: linear-gradient(rgba(240, 240, 240, 0.2) 0px, rgba(209, 209, 209, 0.2) 100%);
    border: 1px solid #BBB;
    border-radius: 3px;
    margin-bottom: 0px;
}

.balance_detail:hover {

    background-image: linear-gradient(-180deg, #e8eaed 0, #fbfcfd 99%);

}

.custom_calendar .fc-button {
    padding: 2px 5px !important;
}

.custom_calendar .fc-toolbar h2 {
    font-size: 14px;
}

.custom_calendar .fc-view {
    margin-top: 10px;
}

.custom_calendar .fc-toolbar {
    margin-top: 5px;
}

.custom_calendar table {
    width: 99% !important;
}

.top_boxes {
    min-height: 0px;
}

.bg_cards .bg-icon {
    height: 60px;
    width: 60px;
    text-align: center;
    -webkit-border-radius: 50%;
    border-radius: 50%;
    -moz-border-radius: 50%;
    background-clip: padding-box;
    border: 1px dashed #818a91;
    background-color: #f7f7f9;
    margin-right: 0px;
}

.bg_cards .bg-icon i {
    line-height: 60px;
    font-size: 26px;
    color: #818a91;
}

.custom_padding {
    padding: 19px 2px;
}

.dashboard_icons .bg-icon i {
    font-size: 16px !important;
    line-height: 50px !important;
}

.dashboard_icons .bg-icon {
    height: 50px;
    width: 50px;
    margin-top: 10px;
}
.alert_border{
    border-left: 4px solid  !important;
}
</style>
<!--Date Picker CSS-->
<link href="{{ URL::to('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />
@endsection
<div class=" m-t-10">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="header-title m-b-10">Welcome,
                <strong>Synergic360 - {{Auth::user()->name}}</strong>
            </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-lg-9 col-xl-9 dashboard_icons padding-left-0 stat_boxes top_boxes padding-right-0 ">

            <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
                <a href="{{url('fmc/job_list?tab=all')}}" class="text-dark">
                    <div class="card-box  m-b-10 bg_cards tilebox-three">
                        <div class="bg-icon pull-xs-left">
                            <i class="zmdi zmdi-notifications-none noti-icon"></i>
                        </div>
                        <div class="text-xs-right">
                            <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Filter New Jobs</h6>
                            <h3 class="m-b-10">
                                <span data-plugin="counterup ">{{isset($total_jobs)?$total_jobs:0}}</span>
                            </h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
                <a href="#" class="text-dark">
                    <div class="card-box  m-b-10 bg_cards tilebox-three">
                        <div class="bg-icon pull-xs-left">
                            <i class="zmdi zmdi-notifications-none noti-icon"></i>
                        </div>
                        <div class="text-xs-right">
                            <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Short Listed</h6>
                            <h3 class="m-b-10">
                                <span data-plugin="counterup ">{{isset($short_list)?$short_list:0}}</span>
                            </h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
                <a href="{{url('fmc/job_list')}}" class="text-dark">
                    <div class="card-box  m-b-10 bg_cards tilebox-three">
                        <div class="bg-icon pull-xs-left">
                            <i class="zmdi zmdi-notifications-none noti-icon"></i>
                        </div>
                        <div class="text-xs-right">
                            <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Applies Jobs</h6>
                            <h3 class="m-b-10">
                                <span data-plugin="counterup ">{{isset($job_apply)?$job_apply:0}}</span>
                            </h3>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-3 col-lg-3 col-xl-3">
               <a href="{{url('fmc/show_assigned_jobs')}}" class="text-dark">
                <div class="card-box  m-b-10 bg_cards tilebox-three">
                    <div class="bg-icon pull-xs-left">
                        <i class="zmdi zmdi-notifications-none noti-icon"></i>
                    </div>
                    <div class="text-xs-right">
                        <h6 class="text-success font-13 text-uppercase m-b-15 m-t-10">Assigned Jobs</h6>
                        <h3 class="m-b-10">
                            <span data-plugin="counterup ">{{isset($assigned_jobs)?$assigned_jobs:0}}</span>
                        </h3>
                    </div>
                </div>
            </a>
        </div>

    </div>

    <div class="col-xs-12 col-lg-3 col-xl-3">
        <div class="card-box custom_padding m-b-10">

            <div class="  col-lg-4 col-md-4 col-xl-4">
                <div class="  widget-user m-b-0 ">
                    <div style="text-align: center;">
                        <img src="{{asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image)}}" class="img-responsive img-circle" alt="user">
                        <div class="wid-u-info">
                            <h6 class="text-muted  m-b-0 font-11" style="margin-top: 5px;">{{Auth::user()->name}}</h6>

                        </div>
                    </div>
                </div>
            </div>

            <div class="  col-lg-8 col-md-8 col-xl-8 ">
                @if(Auth::user()->profile_strength == "100%")
                <span class="text-success pull-xs-right font-11 m-t-10  fill_prof" data-toggle="tooltip" title="Congratulations! Your profile is now completed"><i class="ion-checkmark-round"></i>&nbsp;Completed</span>

                @else
                <a href="{{url('fmc/fmc_profile')}}" class="btn btn-sm btn-primary-outline waves-effect waves-light pull-xs-right">Complete Profile</a>
                @endif
                <h6 class="text-dark m-t-10 m-b-10">Profile Status</h6>
                <p class="font-600 text-muted m-b-10">Set up your account
                    <span class="text-primary pull-right">
                        <b>{{Auth::user()->profile_strength}}</b>
                    </span>
                </p>
                <progress class="progress progress-striped progress-sm progress-primary m-b-0" value="{{str_replace('%','',Auth::user()->profile_strength)}}"
                    max="100"></progress>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12 col-lg-8 col-xl-8 dashboard_icons stat_boxes top_boxes padding-right-0 ">
            <div class=" dashboard_grid_header  ">
                <h4 class="header-title m-t-0 m-b-0">Listings</h4>
            </div>
            <div class="card-box list_detail m-b-10">
                <ul class="nav task_nav nav-tabs m-b-10" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="applied-jobs-tab" data-toggle="tab" href="#applied_jobs" role="tab" aria-controls="applied-jobs-tab">Applied Jobs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="assigned-jobs-tab" data-toggle="tab" href="#assigned_jobs" role="tab" aria-controls="assigned-jobs-tab">Assigned Jobs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="my-job-tab" data-toggle="tab" href="#my_jobs" role="tab" aria-controls="my-job-tab"   aria-expanded="true">My Jobs</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="all-job-tab" data-toggle="tab" href="#all_jobs" role="tab" aria-controls="all-job-tab"
                        >All Jobs</a>
                    </li>
                </ul>
                <div class="inbox-widget nicescroll" style="height: 678px;">

                    <div class="tab-content" id="myTabContent">
                        <!-- applied jobs Tab -->
                        <div role="tabpanel" class="tab-pane fade in active" id="applied_jobs" aria-labelledby="applied-jobs-tab">
                            @if(isset($myjobs) && count($myjobs)>0) @foreach($myjobs as $job)

                            <a href="{{url('/fmc/payment_screen/'.$job->jobs->id)}}" data-toggle="tooltip" title="Posted By  ">
                                <div class="inbox-item list_desgin">
                                    <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                        <p class="inbox-item-author pull-left job_description_title">
                                            {{substr($job->jobs->job_title,0,20)}}....
                                        </p>
                                        <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                            {!! GeneralFunctions::days_remaining_in_expiration($job->jobs->purposal_validity, $job->jobs->admin_approval_date) !!}
                                        </p>
                                    </div>

                                    <p class="inbox-item-text job_description_detail">dummy description added</p>

                                    <p class="inbox-item-date text-success">@php echo date_format(new DateTime($job->jobs->admin_approval_date), 'jS F Y'); @endphp</p>
                                    <div class="job_stats">
                                        <ul class="list-inline m-b-0  text-xs-left">
                                            <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                              Applied {!! $job->jobs->jobApply->count() !!}
                                          </li>
                                          <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed 2 </li>
                                      </ul>
                                  </div>
                              </div>
                          </a>
                          @endforeach @else
                          <div class="card-block  ">
                            <div class="col-lg-12">

                                <div class="text-xs-center">
                                    <p class="text-muted font-13">
                                        <!-- You have not listed any job please post a job. -->
                                        There are no applied job listing.
                                    </p>
                                    <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                        <i class="zmdi zmdi-mood"></i>
                                    </h1>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        @endif
                    </div>

                    <!-- Assigned jobs Tab -->

                    <div role="tabpanel" class="tab-pane fade" id="assigned_jobs" aria-labelledby="assigned-jobs-tab">
                        @if(isset($assigned_jobs_list) && count($assigned_jobs_list)>0) @foreach($assigned_jobs_list as $job)

                        <a href="{{url('/fmc/payment_screen/'.$job->jobs->id)}}" data-toggle="tooltip" title="Posted By  ">
                            <div class="inbox-item list_desgin">
                                <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                    <p class="inbox-item-author pull-left job_description_title">
                                        {{substr($job->jobs->job_title,0,20)}}....
                                    </p>
                                    <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                        {!! GeneralFunctions::days_remaining_in_expiration($job->jobs->purposal_validity, $job->jobs->admin_approval_date) !!}
                                    </p>
                                </div>

                                <p class="inbox-item-text job_description_detail">dummy description added</p>

                                <p class="inbox-item-date text-success">@php echo date_format(new DateTime($job->jobs->admin_approval_date), 'jS F Y'); @endphp</p>
                                <div class="job_stats">
                                    <ul class="list-inline m-b-0  text-xs-left">
                                        <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                          Applied {!! $job->jobs->jobApply->count() !!}
                                      </li>
                                      <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed 2 </li>
                                  </ul>

                              </div>
                          </div>
                      </a>
                      @endforeach @else
                      <div class="card-block  ">
                        <div class="col-lg-12">

                            <div class="text-xs-center">
                                <p class="text-muted font-13">
                                    <!-- You have not listed any job please post a job. -->
                                    There are no applied job listing.
                                </p>
                                <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                    <i class="zmdi zmdi-mood"></i>
                                </h1>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade " id="my_jobs" aria-labelledby="my-job-tab">
                    @if(isset($data) && count($data)>0) @foreach($data as $row)
                    @php 
                    $is_assigned = \App\JobApply::where('job_id', $row['id'])->where('status', 4)->get(); 
                    $is_assigned = count($is_assigned);
                    @endphp
                    <a href="{{url('/fmc/payment_screen/'. $row['id'])}}" data-toggle="tooltip" title="Posted By  ">
                        <div class="inbox-item list_desgin">
                            <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                <p class="inbox-item-author pull-left job_description_title">
                                   {{$row['job_title']}}
                                </p>
                                <!-- <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                    {!! GeneralFunctions::days_remaining_in_expiration($job->jobs->purposal_validity, $job->jobs->admin_approval_date) !!}
                                </p> -->

                            </div> 
                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row['job_description'],0,180)) }}</p>

                            <p class="inbox-item-date text-success"> <?php echo date_format(new DateTime($row['date']), 'jS F Y g:ia');?></p>
                            <div class="job_stats">
                                <ul class="list-inline m-b-0  text-xs-left">
                                       <!--  <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                          Applied {!! $job->jobs->jobApply->count() !!}
                                      </li> -->
                                      <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{isset($row['count_views'])?$row['count_views']:0}} </li>
                                  </ul>

                              </div>
                          </div>
                      </a>



                      @endforeach @endif
                  </div>
                  <div role="tabpanel" class="tab-pane fade  " id="all_jobs" aria-labelledby="all-job-tab">
                    @if(isset($allJobs) && count($allJobs)>0) @foreach($allJobs as $row)

                    <a href="{{url('/fmc/payment_screen/'. $row['id'])}}" data-toggle="tooltip" title="Posted By  ">
                        <div class="inbox-item list_desgin">
                            <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                <p class="inbox-item-author pull-left job_description_title">
                                 {{$row['job_title']}}
                              </p>
                          </div>

                            <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row['description'],0,180)) }}</p>
                          <p class="inbox-item-date text-success"> <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?></p>
                          <div class="job_stats">
                            <ul class="list-inline m-b-0  text-xs-left">
                                       <!--  <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                          Applied {!! $job->jobs->jobApply->count() !!}
                                      </li> -->
                                      <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item">Viewed {{isset($row['count_views'])?$row['count_views']:0}} </li>
                                  </ul>

                              </div>
                          </div>
                      </a>



                      @endforeach @endif
                  </div>
              </div>

          </div>

                        <!-- <div class="pull-right" style="margin-top: 5px;">
                                <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  "><strong>View all</strong></a>
                            </div>  -->
                        </div>
                        <div class=" dashboard_grid_header  ">
                            <h4 class="header-title m-t-0 m-b-0">Assigned Jobs</h4>
                        </div>
                        <div class="card-box list_detail m-b-10" style="height: 356px;">
                            <ul class="nav task_nav nav-tabs m-b-10" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="timeline-tab" data-toggle="tab" href="#timeline" role="tab" aria-controls="timeline-tab"
                                    aria-expanded="true">Timeline</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="list-tab" data-toggle="tab" href="#list" role="tab" aria-controls="list-tab">List</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <!-- timeline Tab -->
                                <div role="tabpanel" class="tab-pane fade in active" id="timeline" aria-labelledby="timeline-tab">
                                    <div class="pull-right">
                                        <div class="btn-group m-b-0 m-b-0">
                                            <button type="button" id="this-year-chart-button" class="btn btn-sm btn-dark-outline waves-effect waves-light active">Yearly</button>
                                            <button type="button" id="previous-year-chart-button" class="btn btn-sm btn-dark-outline waves-effect waves-light">Monthly</button>
                                        </div>
                                    </div>
                                    <!-- Line Chart -->
                                    <div class="p-20">
                                        <canvas id="monthly_line_chart" height="200"></canvas>
                                        <canvas id="yearly_lien_chart" height="200" style="display:none"></canvas>
                                    </div>

                                </div>
                                <!-- timeline Tab -->
                                <div role="tabpanel" class="tab-pane fade" id="list" aria-labelledby="list-tab">

                                    <div class="p-20">
                                        <div class="text-xs-center">
                                            <p class="text-muted font-13">
                                                <!-- You have not listed any job please post a job. -->
                                                There are no Lists.
                                            </p>
                                            <h1 class=" display-2 m-t-30" style="color: #D9D9D8;">
                                                <i class="zmdi zmdi-mood"></i>
                                            </h1>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col-xs-12 col-lg-4 col-xl-4">
                        <div class=" dashboard_grid_header  ">
                            <h4 class="header-title m-t-0 m-b-0">Submissions</h4>
                        </div>
                        <div class="card-box list_detail m-b-10">
                            <ul class="nav task_nav nav-tabs m-b-10" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active " id="site-visit-requests-tab" data-toggle="tab" href="#site_visit_requests" role="tab" aria-controls="site-visit-requests-tab"
                                    aria-expanded="true">Site visit requests</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="quote-submitted-tab" data-toggle="tab" href="#quote_submitted" role="tab" aria-controls="quote-submitted-tab">Quote Submitted</a>
                                </li>
                            </ul>
                            <div class="inbox-widget nicescroll" style="height: 270px;">

                                <div class="tab-content" id="myTabContent">

                                    <!-- Incoming requests Tab -->
                                    <div role="tabpanel" class="tab-pane fade in active" id="site_visit_requests" aria-labelledby="site-visit-requests-tab">
                                        @if(isset($site_visit) && count($site_visit) > 0) @foreach($site_visit as $row)
                                        <a href="{{url('fmc/payment_screen/'.$row->job_id)}}">
                                            <div class="inbox-item list_desgin">
                                                <p class="inbox-item-author job_description_title">
                                                    {{substr($row->job_title,0,20)}}....
                                                </p>
                                                <p class="inbox-item-author text-info font-11">
                                                    <b>{{$row->name}}</b>
                                                </p>
                                                <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                                                <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->inspection_date), 'jS F Y g:ia'); @endphp</p>
                                            </div>
                                        </a>
                                        @endforeach @else
                                        <div class="card-block  ">
                                            <div class="col-lg-12">

                                                <div class="text-xs-center">
                                                    <p class="text-muted font-13">
                                                        <!-- You have not listed any job please post a job. -->
                                                        There are no site visit requests.
                                                    </p>
                                                    <h1 class=" display-2 m-t-30" style="color: #D9D9D8;">
                                                        <i class="zmdi zmdi-mood"></i>
                                                    </h1>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        @endif

                                    </div>

                                    <!-- Assigned to me Tab -->
                                    <div role="tabpanel" class="tab-pane fade  " id="quote_submitted" aria-labelledby="assigned-to-me-tab">
                                        @if(isset($qoute_submitted) && count($qoute_submitted) > 0) @foreach($qoute_submitted as $row)
                                        <a href="{{url('fmc/payment_screen/'.$row->job_id)}}">
                                            <div class="inbox-item list_desgin">
                                                <p class="inbox-item-author job_description_title">
                                                    {{substr($row->job_title,0,20)}}....
                                                </p>
                                                <p class="inbox-item-author text-info font-11">
                                                    <b>AED {{$row->quotation_amount}}</b>
                                                </p>
                                                <p class="inbox-item-text job_description_detail">{{ strip_tags(substr($row->description,0,120)) }}</p>
                                                <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp</p>
                                            </div>
                                        </a>
                                        @endforeach @else
                                        <div class="card-block  ">
                                            <div class="col-lg-12">

                                                <div class="text-xs-center">
                                                    <p class="text-muted font-13">
                                                        <!-- You have not listed any job please post a job. -->
                                                        There are no quote submitted.
                                                    </p>
                                                    <h1 class="display-2 m-t-30" style="color: #D9D9D8;">
                                                        <i class="zmdi zmdi-mood"></i>
                                                    </h1>

                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                        @endif
                                    </div>

                                </div>

                            </div>

                    <!-- <div class="pull-right" style="margin-top: 5px;">
                                    <a href="{{url('oam/jobs')}}" class="  font-11" style="margin-left:5px;  "><strong>View all</strong></a>
                                </div>  -->
                            </div>
                            <div class=" dashboard_grid_header  ">
                                <h4 class="header-title m-t-0 m-b-0">Appointments</h4>
                            </div>
                            <div class="card-box list_detail m-b-10">

                                <div class="inbox-widget nicescroll" style="height:320px;">
                                    <div class="custom_calendar" id="calender_data"></div>
                                </div>
                            </div>

                            @if(GeneralFunctions::check_view_permission('/oam/jobs'))
                            <div class=" dashboard_grid_header  ">

                                <h4 class="header-title m-t-0 m-b-0">Staff </h4>
                            </div>
                            <div class="card-box list_detail m-b-10">
                                <div class="inbox-widget nicescroll" style="height: 320px;">
                                    @if(isset($staff) && !empty($staff)  && count($staff)>0)  @foreach($staff as $row)

                                    <a>
                                        <div class="inbox-item list_desgin">
                                            <!-- <div class="inbox-item-img"><img src="assets/images/users/avatar-1.jpg" class="img-circle" alt=""></div> -->
                                            <p class="inbox-item-author job_description_title">
                                                {{ucwords($row->name)}}
                                                @if($row['status'] == 2)
                                                <span >
                                                    <i class="text-danger fa fa-circle" data-toggle="tooltip" title="Inactive" style="font-size: 10px;"></i>
                                                </span>
                                                @endif
                                                @if($row['status'] == 1)
                                                <span>
                                                    <i class="text-success fa fa-circle" data-toggle="tooltip" title="Active" style="font-size: 10px;"></i>
                                                </span>
                                                @endif
                                            </p>
                                            <p class="inbox-item-text job_description_detail">{{ $row->staff_members->user_roles->name}}</p>
                                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia'); @endphp</p>

                                        </div>
                                    </a>
                                    @endforeach @else
                                    <div class="card-block page_box_shadow">
                                        <div class="col-lg-12">

                                            <div class="text-xs-center">
                                                <p class="text-muted">
                                                    You have not any staff member.
                                                </p>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>
                                    @endif

                                    <div class="clearfix"></div>
                                </div>
                                @if(isset($staffData) && count($staffData)>0)
                                <div class="pull-right" style="margin-top: 5px;">
                                    <a href="{{url('oam/staff/member/list')}}" class="  font-11" style="margin-left:5px;  ">
                                        <strong>View all</strong>
                                    </a>
                                </div>
                                @endif

                            </div>

                            @endif


                        </div>


    <!-- <div class="col-xs-12 col-md-12 col-lg-12 col-xl-4">
        <div class="fmc_card_box  fixed_height">
            <div class="row">
                <div class="col-xs-12 col-xl-12">
                    <div class="card-box image_box widget-user m-b-10 text-xs-center">
                        <div>
                            <img src="{{asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image)}}" class="img-responsive img-circle" alt="user">
                            <div class="wid-u-info">
                                <h6 class="text-muted m-t-10 m-b-0 font-13">{{Auth::user()->name}}</h6>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-xl-12">
                    <div class="card-box m-b-10">
                        @if(Auth::user()->profile_strength == "100%")
                        <span class="text-success pull-xs-right fill_prof" data-toggle="tooltip" title="Congratulations! Your profile is now completed"><i class="ion-checkmark-round"></i>&nbsp;Completed</span>
                        @else
                            <a href="{{url('fmc/fmc_profile')}}" class="btn btn-sm btn-primary-outline waves-effect waves-light pull-xs-right">Complete Profile</a>
                        @endif
                        <h6 class="text-dark m-t-10 m-b-10">Profile Status</h6>
                        <p class="font-600 text-muted m-b-5">Set up your account <span class="text-primary pull-right"><b>{{Auth::user()->profile_strength}}</b></span></p>
                        <progress class="progress progress-striped progress-sm progress-primary m-b-0" value="{{str_replace('%','',Auth::user()->profile_strength)}}" max="100"></progress>
                    </div>
                </div>
                <div class="col-xs-12 col-xl-12">
                    <a href="{{url('fmc/job_list')}}">
                        <div class="card-box m-b-10  bg_cards tilebox-two">
                            <i class="icon-layers pull-xs-right m-t-0"></i>
                            <h6 class="text-success text-uppercase m-b-10 m-t-0">Jobs Posted by OAM</h6>
                            <h2 class="m-b-0">
                                <span data-plugin="counterup">{{isset($total_jobs)?$total_jobs:0}}</span>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-xl-12">
                    <a href="{{url('fmc/job_list')}}">
                        <div class="card-box m-b-10 bg_cards tilebox-two">
                            <i class="icon-docs pull-xs-right m-t-0"></i>
                            <h6 class="text-pink text-uppercase m-b-10 m-t-0">Applied Jobs</h6>
                            <h2 class="m-b-0" data-plugin="counterup">{{isset($job_apply)?$job_apply:0}}</h2>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-xl-12">
                    <a href="{{url('fmc/staff/member/list')}}">
                        <div class="card-box bg_cards m-b-0 tilebox-two">
                            <i class="icon-people pull-xs-right m-t-0"></i>
                            <h6 class="text-info text-uppercase m-b-10 m-t-0">Total Staff</h6>
                            <h2 class="m-b-0">
                                <span data-plugin="counterup">{{isset($staff_count)?$staff_count:0}}</span>
                            </h2>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div> -->
</div>

<div class="row">
    <!-- end col-->

    <div class="col-lg-6">

        <!-- BEGIN MODAL -->
        <div class="modal fade none-border" id="event-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h5 class="modal-title">Add New Event</h5>
                    </div>
                    <div class="modal-body p-20"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                        <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Add Category -->
        <div class="modal fade none-border" id="add-category">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h5 class="modal-title">Add a category</h5>
                    </div>
                    <div class="modal-body p-20">
                        <form role="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Category Name</label>
                                    <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">Choose Category Color</label>
                                    <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                        <option value="success">Success</option>
                                        <option value="danger">Danger</option>
                                        <option value="info">Info</option>
                                        <option value="pink">Pink</option>
                                        <option value="primary">Primary</option>
                                        <option value="warning">Warning</option>
                                        <option value="inverse">Inverse</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL -->
    </div>
    <!-- end col-12 -->
</div>
</div>
<!-- <div class="notification_messege">
        <div class="alert alert-success " role="alert">
            <strong>Well done!</strong> You successfully read this important alert
            message.
        </div>
    </div> -->

    <div class="modal fade" id="subscription_alert" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Alert Message</h4>
          </div>
          <div class="modal-body">
            <p id="subscription_text"></p>
            <form action="{{ url('account/subscription/renewal') }}" method="GET">
                <input type="hidden" name="email" value="{{Auth::user()->email}}">
                <button type="submit" class="btn btn-primary">Renew Subscription</button>
            </form>
        </div>
    </div>
</div>
</div>

<!-- After login terms and condition popup-->
<!-- The Modal -->
<div class="modal fade" id="login_term_condition">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Synergic360 Fair Usage Terms and conditions</h4>
      </div>
      <div class="col-sm-12">
        <div class="tc_msg_box alert" style="display: none;">
            <ul style="text-decoration: none;" id="tc_msg_list">
            </ul>
        </div>
    </div>
    <!-- Modal body -->
    <form class="user_term_condition_form">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="modal-body">
            @php if(isset($user_tc->description)){ echo $user_tc->description; }
            else{ echo "";}
            @endphp
            <div class="checkbox checkbox-primary">
               <input id="agree_term_condition" type="checkbox" name="agree_term_condition" value="1">
               <label for="agree_term_condition">Agree with terms & conditions</label>
           </div>
       </div>

       <!-- Modal footer -->
       <div class="modal-footer">
          <button type="button" class="btn btn-primary user_term_condition">Save</button>
      </div>
  </form>
</div>
</div>
</div>

@section('js')
<script src="{{ URL::to('assets/plugins/chart.js/chart.min.js') }}"></script>
<!-- Date Picker -->
<script src="{ URL::to('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Jquery-Ui -->
<script src="{{ URL::to('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- BEGIN PAGE SCRIPTS -->
<script src="{{ URL::to('assets/plugins/moment/moment.js') }}"></script>
<script src="{{ URL::to('assets/plugins/fullcalendar/dist/fullcalendar.min.js') }}"></script>
<script src="{{ URL::to('assets/pages/jquery.fullcalendar.js') }}"></script>

<script type="text/javascript" src="{{ URL::to('assets/plugins/d3/d3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('assets/plugins/c3/c3.min.js') }}"></script>
<script src="{{ URL::to('assets/plugins/chart.js/chart.min.js') }}"></script>
<script>

    $(document).ready(function () {
        var synergic_rules = '{{Auth::user()->synergic_term_condition}}';

        if (synergic_rules === '0') {
            $('#login_term_condition').modal({
                backdrop: 'static',
                keyboard: false,
            });
            $('#login_term_condition').modal('show');
        }

        $(document).on('click', '.user_term_condition', function () {
            var params = "";
            if ($("#agree_term_condition").prop('checked') == true) {
                params = 1;
            } else {
                params = 0;
            }
            $('.tc_msg_box').hide();
            $('.tc_msg_box').removeClass("alert-success").removeClass("alert-danger");
            $.ajax({
                type: 'POST',
                url: "{{ generate_url('/submit_user_term_condition') }}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "agree_term_condition": params
                },
                success: function (data) {
                    $("#tc_msg_list").empty();

                    if (data.flag === 0) {
                        $('#tc_msg_list').append(data.response);
                        $('.tc_msg_box').addClass("alert-danger").show();
                    } else if (data.flag == 1) {
                        $('#tc_msg_list').append(data.response);
                        $('.tc_msg_box').addClass("alert-success").show();
                        setTimeout(function () {
                            $('#login_term_condition').modal('hide');
                            window.location.href = "{{ url('fmc/company/account/details') }}";
                        }, 2000);
                    }

                }
            });
        });

        // job listing tabs
        $("#jobs").click(function () {
            $(".all_jobs").show();
            $(".my_jobs").hide();
            $("#my_jobs").removeClass("active");
            $("#jobs").addClass("active");
        });
        $("#my_jobs").click(function () {
            $(".my_jobs").show();
            $(".all_jobs").hide();
            $("#jobs").removeClass("active");
            $("#my_jobs").addClass("active");
        });

        // job Chart tabs

        $("#weekly_chart").click(function () {
            $("#pie_chart_weekly").show();
            $("#pie_chart_monthly").hide();
            $("#pie_chart_yearly").hide();
            $("#monthly_chart").removeClass("active");
            $("#yearly_chart").removeClass("active");
            $("#weekly_chart").addClass("active");
        });

        $("#monthly_chart").click(function () {
            $("#pie_chart_weekly").hide();
            $("#pie_chart_monthly").show();
            $("#pie_chart_yearly").hide();
            $("#weekly_chart ").removeClass("active");
            $("#yearly_chart").removeClass("active");
            $("#monthly_chart").addClass("active");
        });
        $("#yearly_chart").click(function () {
            $("#pie_chart_weekly").hide();
            $("#pie_chart_monthly").hide();
            $("#pie_chart_yearly").show();
            $("#weekly_chart").removeClass("active");
            $("#monthly_chart").removeClass("active");
            $("#yearly_chart").addClass("active");
        });

        $("#previous_year_chart").hide();
        $("#this-year-chart-button").click(function () {
            $("#monthly_line_chart").show();
            $("#yearly_lien_chart").hide();
            $("#previous-year-chart-button").removeClass("active");
            $("#this-year-chart-button").addClass("active");
        });
        $("#previous-year-chart-button").click(function () {
            $("#yearly_lien_chart").show();
            $("#monthly_line_chart").hide();
            $("#this-year-chart-button").removeClass("active");
            $("#previous-year-chart-button").addClass("active");
        });

        var dataSource = 12; //Issue
        var data = JSON.parse(dataSource);
        $('#calender_data').fullCalendar({
            height: 443,
            events: data,
            eventClick: function (calEvent, jsEvent, view) {
                window.location.replace('{{url("fmc/payment_screen")}}/' + calEvent.id);
            }
        });
    });


!function ($) {
    "use strict";
        //Pie Chart Weekly
        var ChartC3 = function () { };
        ChartC3.prototype.init = function () {
            c3.generate({
                bindto: '#pie_chart_weekly',
                data: {
                    columns: [
                    @if($apply_week_jobs != 0)
                    ['Assigned Jobs', '{{isset($apply_week_jobs)?$apply_week_jobs:0}}'],
                    ['Rejected Jobs', '{{isset($reject_apply_week_jobs)?$reject_apply_week_jobs:0}}'],
                    @endif
                    ],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" }   },//this is correct
                },
                color: {
                    pattern: ["#ff5d48", "#3db9dc"]
                },
                pie: {
                    label: {
                        show: false
                    }
                }
            });

        },
        $.ChartC3 = new ChartC3, $.ChartC3.Constructor = ChartC3

        //Pie Chart Monthly
        var ChartC1 = function () { };
        ChartC1.prototype.init = function () {
            c3.generate({
                bindto: '#pie_chart_monthly',
                data: {
                    columns: [
                    @if($apply_month_jobs != 0)
                    ['Assigned Jobs', '{{isset($apply_month_jobs)?$apply_month_jobs:0}}'],
                    ['Rejected Jobs', '{{isset($reject_apply_month_jobs)?$reject_apply_month_jobs:0}}'],
                    @endif
                    ],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" }   },//this is correct
                },
                color: {
                    pattern: ["#9261C6", "#F1B53D"]
                },
                pie: {
                    label: {
                        show: false
                    }
                }
            });

        },
        $.ChartC1 = new ChartC1, $.ChartC1.Constructor = ChartC1

        //Pie Chart Yearly
        var ChartC2 = function () { };
        ChartC2.prototype.init = function () {
            c3.generate({
                bindto: '#pie_chart_yearly',
                data: {
                    columns: [
                    @if($apply_year_jobs != 0)
                    ['Assigned Jobs', '{{isset($apply_year_jobs)?$apply_year_jobs:0}}'],
                    ['Rejected Jobs', '{{isset($reject_apply_year_jobs)?$reject_apply_year_jobs:0}}'],
                    @endif
                    ],
                    type: 'pie',
                    empty: { label: { text: "No Data Available" }   },//this is correct
                },
                color: {
                    pattern: ["#1bb99a", "#ff7aa3"]
                },
                pie: {
                    label: {
                        show: false
                    }
                }
            });

        },
        $.ChartC2 = new ChartC2, $.ChartC2.Constructor = ChartC2

    }(window.jQuery),

        //initializing
        function ($) {
            "use strict";
            $.ChartC1.init()
            $.ChartC2.init()
            $.ChartC3.init()
        }(window.jQuery);

        $(document).ready(function () {
            var get_subscription_msg = '{{ $sub_msg }}';
            console.log(get_subscription_msg);
            if (get_subscription_msg != '') {
                setTimeout(function () {
                    $('#subscription_text').text(get_subscription_msg);
                    $('#subscription_alert').modal('show');
                }, 1000);
                console.log('comming here');
            }
        });

        !function($) {
            "use strict";

            var ChartJs = function() {};

            ChartJs.prototype.respChart = function(selector,type,data, options) {
                var ctx = selector.get(0).getContext("2d");
                var container = $(selector).parent();
                $(window).resize( generateChart );
                function generateChart(){
                    var ww = selector.attr('width', $(container).width() );
                    switch(type){
                        case 'Bar':
                        new Chart(ctx, {type: 'bar', data: data, options: options});
                        break;
                    }
                };
                generateChart();
            },
            ChartJs.prototype.init = function() {
                var barChart = {
                    labels: [
                    <?php
                    $yearly = array(
                        date('m', strtotime(' -1 month')),
                        date('m', strtotime(' -2 month')),
                        date('m', strtotime(' -3 month')),
                        date('m', strtotime(' -4 month')),
                        date('m', strtotime(' -5 month')),
                        date('m', strtotime(' -6 month')),
                        date('m', strtotime(' -7 month')),
                        date('m', strtotime(' -8 month')),
                        date('m', strtotime(' -9 month')),
                        date('m', strtotime(' -10 month')),
                        date('m', strtotime(' -11 month')),
                    );

                    echo '"' . date('F') . '", ';
                    echo '"' . date('F', strtotime(' -1 month')) . '", ';
                    echo '"' . date('F', strtotime(' -2 month')) . '", ';
                    echo '"' . date('F', strtotime(' -3 month')) . '", ';
                    echo '"' . date('F', strtotime(' -4 month')) . '", ';
                    echo '"' . date('F', strtotime(' -5 month')) . '", ';
                    echo '"' . date('F', strtotime(' -6 month')) . '", ';
                    echo '"' . date('F', strtotime(' -7 month')) . '", ';
                    echo '"' . date('F', strtotime(' -8 month')) . '", ';
                    echo '"' . date('F', strtotime(' -9 month')) . '", ';
                    echo '"' . date('F', strtotime(' -10 month')) . '", ';
                    echo '"' . date('F', strtotime(' -11 month')) . '", ';
                    ?>
                    ],
                    datasets: [
                    {
                        label: "Assign Jobs",
                        backgroundColor: "#F1B53D",
                        borderColor: "#F1B53D",
                        borderWidth: 1,
                        hoverBackgroundColor: "#F1B53D",
                        hoverBorderColor: "#F1B53D",
                        data: [
                        <?php
                        $fmc_yearly_array = [];
                        foreach ($last_year as $key => $value) {
                            $fmc_yearly_array[$value->month] = $value->count;
                        }
                        foreach ($yearly as $key => $value) {
                            if (array_key_exists($value, $fmc_yearly_array)) {
                                echo $fmc_yearly_array[$value] . ', ';
                            } else {
                                echo '0 ,';
                            }
                        }
                        ?>
                        ]
                    }
                    ]
                };
                this.respChart($("#monthly_line_chart"),'Bar',barChart);
            },
            $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
        }(window.jQuery),

        function($) {
            "use strict";
            $.ChartJs.init()
        }(window.jQuery);


        !function($) {
            "use strict";

            var ChartJs = function() {};

            ChartJs.prototype.respChart = function(selector,type,data, options) {
                var ctx = selector.get(0).getContext("2d");
                var container = $(selector).parent();
                $(window).resize( generateChart );
                function generateChart(){
                    var ww = selector.attr('width', $(container).width() );
                    switch(type){
                        case 'Bar':
                        new Chart(ctx, {type: 'bar', data: data, options: options});
                        break;
                    }
                };
                generateChart();
            },
            ChartJs.prototype.init = function() {
                var barChart = {
                    labels: [
                    <?php
                    $monthly = [];
                    for ($i = 0; $i <= 30; $i++) {
                        echo "'" . date('d', strtotime('- ' . $i . ' day')) . "', ";
                    }
                    $monthly = array(
                        date('d', strtotime('- ' . 0 . ' day')),
                        date('d', strtotime('- ' . 1 . ' day')),
                        date('d', strtotime('- ' . 2 . ' day')),
                        date('d', strtotime('- ' . 3 . ' day')),
                        date('d', strtotime('- ' . 4 . ' day')),
                        date('d', strtotime('- ' . 5 . ' day')),
                        date('d', strtotime('- ' . 6 . ' day')),
                        date('d', strtotime('- ' . 7 . ' day')),
                        date('d', strtotime('- ' . 8 . ' day')),
                        date('d', strtotime('- ' . 9 . ' day')),
                        date('d', strtotime('- ' . 10 . ' day')),
                        date('d', strtotime('- ' . 11 . ' day')),
                        date('d', strtotime('- ' . 12 . ' day')),
                        date('d', strtotime('- ' . 13 . ' day')),
                        date('d', strtotime('- ' . 14 . ' day')),
                        date('d', strtotime('- ' . 15 . ' day')),
                        date('d', strtotime('- ' . 16 . ' day')),
                        date('d', strtotime('- ' . 17 . ' day')),
                        date('d', strtotime('- ' . 18 . ' day')),
                        date('d', strtotime('- ' . 19 . ' day')),
                        date('d', strtotime('- ' . 20 . ' day')),
                        date('d', strtotime('- ' . 21 . ' day')),
                        date('d', strtotime('- ' . 22 . ' day')),
                        date('d', strtotime('- ' . 23 . ' day')),
                        date('d', strtotime('- ' . 24 . ' day')),
                        date('d', strtotime('- ' . 25 . ' day')),
                        date('d', strtotime('- ' . 26 . ' day')),
                        date('d', strtotime('- ' . 27 . ' day')),
                        date('d', strtotime('- ' . 28 . ' day')),
                        date('d', strtotime('- ' . 29 . ' day')),
                    );
                    ?>
                    ],
                    datasets: [
                    {
                        label: "Assign Jobs",
                        backgroundColor: "#FF5D48",
                        borderColor: "#FF5D48",
                        borderWidth: 1,
                        hoverBackgroundColor: "#FF5D48",
                        hoverBorderColor: "#FF5D48",
                        data: [
                        <?php
                        $fmc_monthly_array = [];
                        foreach ($last_month as $key => $value) {
                            $fmc_monthly_array[$value->day] = $value->count;
                        }
//print_r($fmc_monthly_array); exit;
                        foreach ($monthly as $key => $value) {
                            if (array_key_exists($value, $fmc_monthly_array)) {
                                echo $fmc_monthly_array[$value] . ', ';
                            } else {
                                echo '0 ,';
                            }
                        }
                        ?>
                        ]
                    }
                    ]
                };
                this.respChart($("#yearly_lien_chart"),'Bar',barChart);
            },
            $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
        }(window.jQuery),

        function($) {
            "use strict";
            $.ChartJs.init()
        }(window.jQuery);



    </script> @endsection @endsection
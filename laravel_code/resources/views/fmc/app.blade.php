<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Favicon -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

    <!-- App title -->
    <title>{{isset($title)?$title:''}} - FMC</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/morris/morris.css') }}">

    <!-- Switchery css -->
    <link href="{{ URL::to('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
    <!-- Muliselect -->
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }} " rel="stylesheet" type="text/css"/>

    <!-- Jquery filer css -->
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/jquery.filer.css') }}" rel="stylesheet" />
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />

    <!-- App CSS -->
    <link href="{{ URL::to('assets/css/style.css?ver=4.8.0') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/custom.css?ver=2.5.0') }}" rel="stylesheet" type="text/css" />


    
    <style>
    .link_no_href{
      cursor: pointer;
  }

</style>
<!-- Sweet Alert css -->
<link href="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css"/>
<!--calendar css-->
@yield('calendarCss')
@yield('css')
<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <!-- Modernizr js -->
    <script src="{{ URL::to('assets/js/modernizr.min.js') }}"></script>

</head>


<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="{{url('/fmc/home')}}" class="logo">
                        <span>
                            <img src="{{ URL::to('assets/images/logo_login.png') }}" alt="logo" width="60%">
                        </span>
                    </a>
                </div>
                <!-- End Logo container-->


                <div class="menu-extras">

                    <ul class="nav navbar-nav pull-right">

                        <li class="nav-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>

                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" id="fmc_bell_icon" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                            aria-expanded="false">
                            <i class="zmdi zmdi-notifications-none noti-icon"></i>
                            <span id="notification_badge"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5>
                                    <small class="notification_counter">
                                        <span class="label label-default pull-xs-right"></span>Notification
                                    </small>
                                </h5>
                                <div class="clearfix"></div>
                            </div>
                            <img src="{{ URL::to('assets/images/loading.gif') }}" style="height: 40px !important; margin-left: 116px;" style="display: none;" class="loading_gif_class">
                            <div class="notification_data">
                            </div>

                            <!-- All-->
                            <a href="{{url('admin_all_notifications')}}" class="dropdown-item notify-item notify-all">
                                View All
                            </a>

                        </div>
                    </li>


                    <li class="nav-item dropdown notification-list">
                        <a class="nav-link imag_link user_image_detail dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                        aria-haspopup="false" aria-expanded="false">
                        @if(isset(Auth::user()->image))
                        <div class="user_image_padding"> 
                            <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image) }}" alt="{{ Auth::user()->image }}"
                            /> 
                        </div>
                        @else
                        <div class="user_image_padding"> 
                            <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/default_user.png') }}" alt="{{ Auth::user()->image }}"
                            /> 
                        </div> 
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow">
                                <small>Welcome ! 
                                    @if(isset(Auth::user()->name))
                                    {{Auth::user()->name}}
                                    @endif
                                </small>
                            </h5>
                        </div>

                        <!-- item-->
                        <a href="{{url('fmc/fmc_profile')}}" class="dropdown-item notify-item">
                            <i class="zmdi zmdi-account-circle"></i>
                            <span>Profile</span>
                        </a>

                                <!-- item
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-settings"></i>
                                    <span>Settings</span>
                                </a>-->

                                <!-- item-->
                                <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i>
                                    <span class="logout">Logout</span>
                                </a>

                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end menu-extras -->
                <div class="clearfix"></div>
            </div>
            <!-- end container -->
        </div>
        <!-- end topbar-main -->
        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li>
                            <a href="{{ url('/fmc/home') }}">
                                <span> Home </span>
                            </a>
                        </li>
                        @if(GeneralFunctions::check_view_permission('/fmc/job_list'))
                        <li class="has-submenu">
                            <a href="{{ url('fmc/job_list') }}">
                                <span>Listings</span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li>
                                            <a href="{{ url('fmc/job_list') }}">Jobs List</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        <li class="has-submenu">
                            <a href="{{url('fmc/show_assigned_jobs')}}">
                                <span> Assigned Jobs </span>
                            </a>
                        </li> 
                        <li class="has-submenu">
                            <a href="{{url('fmc/company/account/details')}}">
                                <span> Company Details </span>
                            </a>
                        </li>
                        <li class="has-submenu">
                            <a class="link_no_href">
                                <span> More... </span>
                            </a>
                            <ul class="submenu megamenu">

                                <li>
                                    <ul>
                                        @if(Auth::user()->parent_id == 0)
                                        <li>
                                            <a href="{{ url('/fmc/staff/member/list') }}">Users</a>
                                        </li>
                                        <!-- <li>
                                            <a href="{{ url('/fmc/staff/roles') }}">Add Roles</a>
                                        </li> -->
                                        <li>
                                            <a href="{{ url('/fmc/roles/list') }}">Users Roles</a>
                                        </li>
                                        @endif
                                        @if(GeneralFunctions::check_view_permission('/fmc/job_list'))

                                        <li>
                                            @php
                                            $user_id = Auth::user()->parent_id;
                                            if(Auth::user()->parent_id == 0){
                                            $user_id = Auth::user()->id;
                                        }
                                        @endphp
                                        <a href="{{ url('/fmc/review/ratings/list/'.$user_id) }}">
                                            Ratings 
                                        </a>
                                    </li>
                                    @endif 
                                </ul>
                            </li>


                        </ul>
                    </li>
                </ul>
                <!-- End navigation menu  -->
            </div>
        </div>
    </div>
</header>
<!-- End Navigation Bar-->



<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="wrapper">
    <div class="container">

        @yield('content')

        <!-- Footer -->
        <footer class="footer text-right">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        2018 © Synergic.
                    </div>
                </div>
            </div>
        </footer>
        <!-- End Footer -->
    </div>
    <!-- container -->

        <!-- <div class="notification_messege">
                <div class="alert alert-success " role="alert">
                    <strong>Well done!</strong> You successfully read this important alert
                    message.
                </div>
            </div> -->

        </div>

        <!-- End wrapper -->
        <!-- jQuery  -->


        <script src="{{ URL::to('assets/MDB-Free/js//jquery-3.3.1.min.js') }}"></script>
        <script src="{{ URL::to('assets/MDB-Free/js/popper.min.js') }}"></script>
        <script src="{{ URL::to('asset/MDB-Frees/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::to('assets/MDB-Free/js/mdb.min.js') }}"></script>


        <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/tether.min.js') }}"></script>
        <!-- Tether for Bootstrap -->
        <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::to('assets/js/waves.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/switchery/switchery.min.js') }}"></script>

        <!--Morris Chart-->
        <script src="{{ URL::to('assets/plugins/morris/morris.min.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/raphael/raphael-min.js') }}"></script>

        <!-- Counter Up  -->
        <script src="{{ URL::to('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
        <script src="{{ URL::to('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ URL::to('assets/js/jquery.core.js') }}"></script>
        <script src="{{ URL::to('assets/js/jquery.app.js') }}"></script>
        <!-- Multiselect js -->    
        <script type="text/javascript" src="{{ URL::asset('assets/plugins/multiselect/js/jquery.multi-select.js') }} "></script>
        <script src="{{ URL::asset('assets/plugins/select2/js/select2.full.min.js') }} " type="text/javascript"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/pages/jquery.formadvanced.init.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/plugins/jquery-play-sound/jquery.playSound.js') }}"></script>

        <!-- Page specific js -->
        <script src="{{ URL::to('assets/pages/jquery.dashboard.js') }}"></script>
        <!-- Jquery filer js -->
        <script src="{{ URL::to('assets/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>

        <!-- page specific js -->
        <script src="{{ URL::to('assets/pages/jquery.fileuploads.init.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/plugins/jquery-play-sound/jquery.playSound.js') }}"></script>
        <!-- Sweet Alert js -->
        <script src="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
        <script src="{{ URL::asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('assets/plugins/jquery-play-sound/jquery.playSound.js') }}"></script>


        <script src="{{ URL::asset('assets/js/jquery.repeater.min.js') }}"></script>
    
        


        @yield('js')
        <script>
            $(document).ready(function(){
                $(".navigation-menu a").each(function () {
                    if (this.href == window.location.href) {
                    $(this).parent().addClass("active"); // add active to li of the current link
                    $(this).parent().parent().parent().addClass("active"); // add active class to an anchor
                    $(this).parent().parent().parent().parent().parent().addClass("active"); // add active class to an anchor
                }
            });
                var resizefunc = [];
                $(document).on('click', '.logout', function(){
                    var url = "{{ URL::to('/logout') }}";
                    location.replace(url);
                });

                $(document).on('click', '.show_detail_btn', function(){
                    $.ajax({url: "{{url('/fmc/get/job/details')}}",
                        data : {'data' : $(".job_id").val() },
                        type : "GET",    
                        success: function(result){
                            console.log(result.data);
                            $('#job_type').text(result.data.job_type);
                            $('#email_id').text(result.data.focal_email);
                            $('#contact_number').text(result.data.companies.contact_no);
                            $('#office_address').text(result.data.companies.office_address);
                            $('#show_details_result').show();
                        }});
                });

                $('#fmc_bell_icon').on('click', function(){ 
                    $('.notification_data').html('');
                    $('.notification_counter').html('');
                    $('.loading_gif_class').show(); 
                    $.ajax({
                        type: 'GET',
                        url: "{{ generate_url('get_fmc_notification') }}",
                        success:function(data){
                            console.log(data);
                            setTimeout(function(){ 
                                $('.loading_gif_class').hide();
                                if(data.counter > 0){
                                    $('.notification_counter').html('<span class="label label-danger pull-xs-right">'+data.counter+'</span>');
                                }
                                $('.notification_data').html(data.html);
                                $('#notification_badge').removeClass('noti-icon-badge');
                            }, 200);
                        }
                    });
                });
                setInterval(function(){
                    $.ajax({
                        type: 'GET',
                        url: '{{generate_url("get_fmc_instant_notification")}}',
                        success: function(response) {
                            if(response.flag == 1){
                                swal({
                                    title: response.title,
                                    text: response.description,
                                    timer: 4000,
                                    showConfirmButton: false
                                });
                                $.playSound("{{asset('assets/plugins/jquery-play-sound/sound/notify')}}");
                                $('#notification_badge').addClass('noti-icon-badge');
                            }
                        }
                    });
                }, 5000);
            });
        </script>
        @yield('fmc_js')

    </body>

    </html>
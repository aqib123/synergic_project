@php
if(session()->has('popup') && session()->has('job_id')){
session()->forget('popup');
session()->forget('job_id');
}
@endphp
@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('css')
<link rel='stylesheet' href="{{ URL::asset('assets/glyphicons-only-bootstrap/css/bootstrap.min.css') }}" />
<link href="{{ URL::asset('assets/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css') }} " rel="stylesheet">
<style>
.dashboard_grid_header {
    padding: 0.7rem 0.6rem;
    background-color: #fff;
    border-bottom: 1px solid #ccc;
    /* font-weight: 200; */
}
.checkbox label::before {
    left: 3px !important ;
}
.table th, .table td {

    padding: 6px 10px !important ;
}
.dashboard_grid_header h4 {
    font-weight: 400;
    font-size: 17px;
    font-family: ProximaNova, arial, sans-serif !important;
    color: #000;
}
.list_detail {
    border: none;
    padding: 7px 8px 2px 8px;
}

.list_desgin {
    margin-bottom: 5px;
    background-color: #F9F9F9;
    padding: 10px 5px !important;
    border: 1px solid #ccc !important;
    border-radius: 2px;
}

.job_description_title {
    font-size: 13px;
    font-weight: 600 !important;
    color: #000 !important;
}

.text_dark_blue {
    color: #0087e0;
}

.job_description_detail {
    color: #515151 !important;
}

.font-11 {
    font-size: 11px;
}

.job_stats {
    margin-right: 15px;
}
.custom_size{
    padding: 0.10rem 0.75rem;
    font-size: 0.675rem;

}
.inbox-widget .inbox-item img {
    width: 50px !important;
}
.modal-header{
  background-color: #632972 !important;
}
.modal-header h4{
  color: #ffffff !important;
}
.modal-content{
  border-radius: unset !important;
}
</style>
@stop @section('content')
<!-- Page-Title -->

<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <!-- <a href="{{url('/oam/job_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Job</a> -->
        </div>
        <h4 class="page-title"></h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-xl-9">
        <div class="card page_box_shadow">
            <div class="card-header sidebar_header">

                <div class="pull-left">
                    <h4 class="header-title job_listing_heading m-b-0"> {{isset($title)?$title:''}}</h4>
                </div>
                <div class="pull-right">
                    @if(Auth::user()->role_id == 2) @if($detail->admin_approval == 0 || $detail->admin_approval == 3)
                    <!-- <a href="" type="button" class="btn btn-sm btn-danger-outline waves-effect waves-light right" style="margin-left:5px; ">Delete</a> -->
                    <a href="{{ url('/oam/job_form?id='.$detail->id) }}" class="btn btn-sm btn-dark-outline waves-effect waves-light right">Edit</a>
                    @endif @endif
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="card-block description-font page_box_shadow">
                <div class="col-lg-12">
                    <p class="job_detail_description lead m-b-0 font-16 "><b>{{isset($detail->job_title)?$detail->job_title:''}}</b>
                    </p>
                    <p class=" m-b-0 font-13">
                        <strong>Posted Date: </strong>
                        <span class="font-13 m-b-0 text-muted"> {!! date_format(new DateTime($detail->created_at), 'jS F Y g:ia') !!}</span>
                    </p>
                    <div class="m-t-0">
                        <p class="lead m-b-0 font-13 ">
                            <strong>Job Type: </strong>
                            <span class="font-13 m-b-0 text-muted">{{isset($detail->job_type)?$detail->job_type:''}}</span>
                        </p>
                    </div>
                    <p class=" m-b-0 text-justify">{!! strip_tags(isset($detail->description)?$detail->description:'' )!!}</p>
                    <div class="m-t-10">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                <th scope="col">Categories</th>
                                <th scope="col">Sub Categories</th>
                                <th scope="col">Service Name</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                                <p class="lead m-b-0 font-13 ">
                                    <span class="label label-success">
                                        @if(isset($job_category_name) && count($job_category_name) > 0)
                                        @foreach($job_category_name as $cat_name)
                                        {{$cat_name->name}}&nbsp;&nbsp;
                                        @endforeach
                                        @endif
                                    </span>
                                </p>
                            </td>
                            <td>
                             <p class="lead m-b-0 font-13 ">
                                <span class="label label-success">
                                    @if(isset($job_sub_category) && count($job_sub_category) > 0)
                                    @foreach($job_sub_category as $sub_cat_name)
                                    {{$sub_cat_name->sub_category}}&nbsp;&nbsp;
                                    @endforeach
                                    @endif
                                </span>
                            </p>
                        </td>
                        <td>
                            <p class="lead m-b-0 font-13 ">
                                <span class="label label-success">@if(isset($job_category) && count($job_category) > 0)
                                    @foreach($job_category as $cat_name)
                                    {{$cat_name['job_category']->service_name}}&nbsp;&nbsp;
                                    @endforeach
                                    @endif
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                    </tbody>
                </table>
            </div>


            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
            <div class="pull-left m-t-10">
                <button type="button" class="btn btn-sm btn-primary-outline waves-effect waves-light show_full_detail">
                    <i class="fa fa-arrow-circle-o-down m-r-5"></i>
                    <span>Show Detail</span>
                </button>
                <button type="button" class="btn btn-sm btn-primary-outline waves-effect waves-light show_less_detail" style="display:none">
                    <i class="fa fa-arrow-circle-o-up m-r-5"></i>
                    <span>Less Detail</span>
                </button>

            </div>
            @endif


        </div>
        <div class="col-lg-3">
            <div class="pull-right ">
                <div class="m-b-10">
                    @if(isset($job_applied_status)) @if($job_applied_status == 0)
                    @if(GeneralFunctions::check_view_permission('/fmc/job/apply'))

                    @if($license_expiry_left > 0)
                    <form action="{{url('fmc/job/apply')}}" id="job_form" method="POST">
                        <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light apply_for_job">Apply For Job</a>
                        {{csrf_field()}}
                        <input type="hidden" value="{{ $detail->id }}" name="uuid" id="uuid">
                        <input type="hidden" name="status" id="staus">
                        <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light decline_for_job">Decline</a>
                        <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                    </form>
                    @else
                    <div class="col-sm-12 flash_message_div">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p>Please Renew your trade license first for applying Jobs. Thanks</p>
                        </div>
                    </div>
                    @endif
                    @endif
                    @endif @endif
                </div>

                @if(isset($job_applied_status))
                <p class="font-13 text-muted  m-b-0">
                    <strong> Status: </strong>
                    <span class="text-success">
                        @if($job_applied_status == 1) Applied for this Job. @endif @if($job_applied_status == 2) Decline this Job Already. @endif  @if($job_applied_status == 3) Short Listed for this Job. @endif @if($job_applied_status == 4) Job has been assigned to You. @endif @if($job_applied_status == 5) Company has Rejected Your Application. @endif
                        @if($job_applied_status == 7) Applied for Workorder. @endif
                        @if($job_applied_status == 8) Job Awarded. @endif
                    </span>
                </p>

                <div class="col-md-12">
                    @if(isset($job_applied_status)) @if($job_applied_status == 4)
                    @if(GeneralFunctions::check_view_permission('/fmc/job/apply'))
                    <form action="{{url('fmc/job/apply')}}" id="job_form" method="POST">
                        <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light demand_for_workorder">Accept</a>
                        {{csrf_field()}}
                        <input type="hidden" value="{{ $detail->id }}" name="uuid" id="uuid">
                        <input type="hidden" name="status" id="staus">
                        <a href="javascript:void(0)" class="btn btn-sm btn-danger-outline waves-effect waves-light decline_for_job">Decline</a>
                        <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
                    </form>
                    @endif
                    @endif @endif
                </div>
                @endif

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="card-block description-font page_box_shadow" id="full_details" style="display: none;">
        @php
        $job_status_hide_info = false;
        if(Auth::user()->role_id == 3){
        if($job_applied_status == 3 || $job_applied_status == 4){
        $job_status_hide_info = true;
    }
}
else{
$job_status_hide_info = true;
}
@endphp

@if($job_status_hide_info)
<div class="col-md-6">
    <p class="lead  text-primary  m-b-0">Building Detail</p>
    <div class="col-md-12">
        <p class="lead font-13   m-b-0">
            <b>Building:</b>
            <span class="font-13 m-b-0 text-muted">{{isset($building->name)?$building->name:''}}</span>
        </p>

        <div class="m-t-10">
            <p class="lead font-13  m-b-0">
                <b>Building Assets:</b>
                @if(isset($building->assets) && !empty($building->assets)) @foreach(explode(',',$building->assets) as $b_asset)
                <span class="label  label-primary">{{$b_asset}}</span>
                @endforeach @endif
            </p>
        </div>
        <div class="m-t-10">
            <p class="lead font-13  m-b-0">
                <b>Building Type:</b>
                <span class="font-13 m-b-0 text-muted">{{isset($building->type)?$building->type:''}} </span>
            </p>
        </div>

    </div>
</div>

<div class="col-md-6">
    <p class="lead  text-primary  m-b-0">Point of Contact</p>
    <div class="col-md-12">
        <p class="lead  font-13  m-b-0">
            <b>Name:</b>
            @if(isset($detail->contact_name) && !empty($detail->contact_name))
            <span class="font-13 m-b-0 text-muted">
                @foreach(json_decode($detail->contact_name) as $name) {{$name. ", "}} @endforeach
            </span>
            @endif
        </p>
        <!-- <p class="font-13 m-b-0 text-dark">Omer Hayat</p> -->

        <div class="m-t-10">
            <p class="lead font-13  m-b-0">
                <b>Email:</b>
                @if(isset($detail->contact_email) && !empty($detail->contact_email))
                <span class="font-13 m-b-0 text-muted">
                    @foreach(json_decode($detail->contact_email) as $email) {{$email. ", "}} @endforeach
                </span>
                @endif
            </p>
        </div>
        <div class="m-t-10">
            <p class="lead font-13  m-b-0">
                <b>Contact Number:</b>
                @if(isset($detail->contact_number) && !empty($detail->contact_number))
                <span class="font-13 m-b-0 text-muted">
                    @foreach(json_decode($detail->contact_number) as $contact) {{$contact}} @endforeach
                </span>
                @endif
            </p>
        </div>
    </div>
</div>
@endif
<div class="col-md-12">
    <p class="lead m-t-10  text-primary  m-b-0">Payment Term</p>
    <div class="col-md-12">
        <p class="lead font-13  m-b-0">
            <b>Payment Via: </b>
            <span class="font-13 m-b-0 text-muted">{{isset($detail->payment_terms)?$detail->payment_terms:''}} </span>
        </p>
    </div>
    <table class="table table-bordered">
        <tr>
            <th>Payment Discription</th>
            <th>Payment Percentage</th>
        </tr>
        @php
        $payment_info = [];
        $percentage_info = [];
        if($detail->payment_term){
        $payment_info = json_decode($detail->payment_term, true);
        $percentage_info = json_decode($detail->payment_percentages, true);
    }
    @endphp
    @foreach($payment_info as $key => $value)
    <tr>
        <td>{{$value}}</td>
        <td>{{$percentage_info[$key]}} %</td>
    </tr>
    @endforeach
</table>
</div>
<div class="col-md-12">
    <div class="m-t-10">
        <p class="lead text-primary m-t-10  m-b-0">Job Requirement</p>
        <div class="col-md-12">
            @foreach($getTermsAndConditions as $key => $value)
            <p class="lead font-13  m-b-0">
                <b>{{$value['title']}}</b>
                <!-- <aclass="font-13 m-b-0 text-primary">blsjbkujbkg</p> -->
                <span class="font-13 m-b-0 text-muted">{!! isset($value['description'])?$value['description']:'' !!}</span>
            </p>
            @endforeach
        </div>
        <div class="clearfix"></div>
    </div>

</div>
<div class="clearfix"></div>
</div>

</div>
</div>
<div class="col-sm-3 col-lg-3 col-xs-12">
    <div class="card page_box_shadow">
        <h4 class="card-header sidebar_header m-b-0">
            Job Stats
        </h4>
        @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1) @if(isset($jobApplicationCount))
        <ul class="list-group list-group-flush sidbar_icon">
            <li class="list-group-item">
                <a href="#" class="card-link">
                    <i class="fa fa-check-square"></i>
                    <span> Applied</span> {{$jobApplicationCount }}</a>
                </li>
                <li class="list-group-item">
                    <a href="#" class="card-link">
                        <i class="fa fa-list-alt"></i>
                        <span> Shortlisted</span> {{ $shortListCount }}</a>
                    </li>
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                    <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                </ul>
                @endif @endif

                @if(Auth::user()->role_id == 2)
                @if(isset($renew_job_for_further))
                <form style="padding:12px;" action="#" method="post" enctype="multipart/form-data">
                    <fieldset class="form-group">
                        <label class="text">
                            <input type="hidden" name="job_id" value="{{$detail['id']}}">
                            {{ csrf_field()}}
                            <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light renew_job_btn">Renew Job</a>
                        </label>
                    </fieldset>
                </form>
                @endif
                @endif

                @if(Auth::user()->role_id == 3)
                <ul class="list-group list-group-flush sidbar_icon">
                    <li class="list-group-item">
                        <a href="#" class="card-link">
                            <i class="fa fa-check-square"></i>
                            <span> Total Application </span> {!! count($detail['jobApply']) !!}</a>
                        </li>
                        <li class="list-group-item">
                            <a href="#" class="card-link">
                                <i class="fa fa-list-alt"></i>
                                <span> Post Expiration </span>{!! GeneralFunctions::days_remaining_in_expiration($detail['job_expiry_days'], $detail['admin_approval_date']) !!}</a>
                            </li>
                <!-- <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span> </a></li>
                    <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li> -->
                </ul>
                @endif
            </div>
            @if(Auth::user()->role_id == 3) @if($job_applied_status != 0)
            <div class="card page_box_shadow">
                <h4 class="card-header sidebar_header m-b-0">
                    Engineering Report & Quotation
                </h4>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12">
                        @if(isset($report_status)) @if($report_status == 0) @if($site_visit_request == 2)
                        <form style="padding:12px;" action="{{url('fmc/upload/engineering/report')}}" method="post" enctype="multipart/form-data">
                            <fieldset class="form-group">
                                <label >Your Request for Site Visit has been approved for this Job Please Uploade the Engineering Report within a week after Inspection Date</label>
                                <p class="text-muted m-b-0 font-13">Inspection Date : <span class="text-muted  font-13">{!! (isset($inspection_date)) ? date_format(new DateTime($inspection_date), 'jS F Y g:ia') : '' !!}</span></p>
                                <label class="file">
                                    <input type="hidden" name="job_id" value="{{ $detail->id }}">
                                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                    <input type="file" id="engineeering_report" name="engineeering_report">
                                    <span class="file-custom"></span>
                                </label>
                            </fieldset>
                            <fieldset class="form-group">
                             <p class="text-muted m-b-0 font-13">Quotation Amount</p>
                             <label class="text">
                                <input type="number" class="form-control" id="quotation_amount" name="quotation_amount" required step=".01">
                            </label>
                        </fieldset>

                        <button type="submit" class="btn btn-sm btn-primary-outline waves-effect waves-light">Submit</button>
                    </form>
                    @elseif($site_visit_request == 1)
                    <div style="padding:12px;">
                        <p class="text-muted m-b-0 font-13"><span class="label label-pill label-success">Requested for Site Visit</span></p>
                    </div>
                    @endif
                    @else
                    <div style="padding:12px;">
                        <h6>Report and Quotation Uploaded</h6>
                        <p class="text-muted m-b-0 font-13">On : <span class="text-muted  font-13">{!! date_format(new DateTime($uploaded_date), 'jS F Y g:ia') !!}</span></p>
                        @if($uploaded_path != '')
                        <a href="{{ asset('laravel_code/storage/app/public/engineeering_report/'.$uploaded_path)}}" download>Engineering Report</a>
                        <p><b>Quotation Amount : </b>{!! $quotation_amount !!}</p>
                        @else
                        <p><b>Quotation Amount : </b>{!! $quotation_amount !!}</p>
                        @endif
                        @if($revise_quotation_status == 1)
                        <hr>
                        <a href="javascript:void(0)" class="revise_quotation_status">Revise Quotation</a>
                        @endif
                    </div>
                    @endif @endif
                </div>
            </div>
        </div>
        @endif
        @php
        $is_assigned = \App\JobApply::where('job_id', $detail->id)->where('status', 4)->get();
        $is_assigned = count($is_assigned);
        @endphp
        @if($job_applied_status == 7)
        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
                Workorder Invoice
            </h4>
            <div class="row">
                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12" style="padding: 20px;">
                    <div style="padding:12px;">
                        <a href="{{ asset('laravel_code/storage/app/public/invoice_report/'.$invoice_reporting_path)}}" download>Invoice Reporting</a>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endif
        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
               Refer Job
           </h4>
           <div class="row">
            <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12 col-xl-12" style="padding: 20px;">
               <a class="btn btn-sm btn-primary-outline waves-effect waves-light refer_job_btn" href="javascript:void(0)">Refer</a>
           </div>
       </div>
   </div>
</div>
</div>
@if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
@if(isset($jobApplicationRecord)) @if(count($jobApplicationRecord) > 0)
<div class="row">
    <div class="col-xs-9">
        <div class=" dashboard_grid_header list_detail   ">
            <div class="col-md-8">
                <h4 class="header-title m-t-10 m-b-0" style="margin-top: 8px;">Applications Request </h4>
            </div>
            <div class="col-md-4 padding-right-0 text-md-center">
              <p class="lead font-16  m-b-10" style="display: inline-block;margin-top: 6px;">Search:</p>
              <label class="pull-right"><input class="form-control input-sm" placeholder="" aria-controls="datatable" type="search"></label>
          </div>
          <div class="clearfix"></div>
      </div>

  </div>
</div>
@endif @endif

<!-- Model for Short Listing the FMC Company -->
<div class="modal fade add_shortlisted_company in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Short List Companies</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="md_short_list_company" data-parsley-validate novalidate method="POST" action="{{ url('oam/shortlist/fmc/company') }}" name="short_list_company">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-8">
                            <p>Are you sure, you want to shortlist this Company ?</p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-8">
                            <input type="hidden" class="form-control" name="sl_user_uuid" id="sl_user_uuid">
                            <input type="hidden" name="user_uuid" id="user_uuid">
                            <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light save_short_list">
                                Agree
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- Assign Company Job -->
<div class="modal fade assign_compnay_job_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Assingning Job Confirmation</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="short_list_company" data-parsley-validate novalidate method="POST" action="{{ url('oam/assing/fmc/company/job') }}" name="short_list_company">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">
                    <!-- <input type="hidden" name="id" value="{{Request::input('id')}}"> -->
                    <div class="row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-12 text-md-center">
                            <p id ="assign_company_detail"></p>
                            <input type="hidden" class="form-control" name="assign_user_uuid" id="assign_user_uuid">
                        </div>
                    </div>
                    <div class="row">
                     <label for="inputEmail3" class="col-sm-2 form-control-label">
                     </label>
                     <div class="col-sm-12 text-md-center">
                        <button class="btn btn-sm btn-primary-outline waves-effect waves-light short_list_btn">
                            Assign
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- end of Assign Company Modal -->


<!-- Assign Company Job -->
<div class="modal fade award_job_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Awarding Job Confirmation</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="short_list_company" data-parsley-validate novalidate method="POST" action="{{ url('oam/awarding/fmc/company/job') }}" name="short_list_company">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">
                    <!-- <input type="hidden" name="id" value="{{Request::input('id')}}"> -->
                    <div class="row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-12 text-md-center">
                            <p id ="awarding_company_detail"></p>
                            <input type="hidden" class="form-control" name="awarding_user_uuid" id="awarding_user_uuid">
                        </div>
                    </div>
                    <div class="row">
                     <label for="inputEmail3" class="col-sm-2 form-control-label">
                     </label>
                     <div class="col-sm-12 text-md-center">
                        <button class="btn btn-sm btn-primary-outline waves-effect waves-light short_list_btn">
                            Confirm Job
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- end of Assign Company Modal -->

<!-- Reject the Application Modal -->
<div class="modal fade md_reject_company in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Reject Job Confirmation</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="reject_company" data-parsley-validate novalidate method="POST" action="{{ url('oam/rejected/fmc/company') }}" name="reject_company">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">
                    <!-- <input type="hidden" name="id" value="{{Request::input('id')}}"> -->
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-8">
                            <p id ="rj_company_name"></p>
                            <input type="hidden" class="form-control" name="rl_user_uuid" id="rl_user_uuid">
                        </div>
                    </div>
                    <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 form-control-label">
                     </label>
                     <div class="col-sm-8">
                        <button class="btn btn-sm btn-primary-outline waves-effect waves-light short_list_btn">
                            Reject Job
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<!-- End of Reject the Application Modal -->

<!-- Model for Site Visit Date -->
<div class="modal fade add_site_visit_date in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Assign Site Visit Date</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="md_site_visit_date" data-parsley-validate novalidate method="POST" action="{{ url('oam/site_visit_date/fmc/company') }}" name="site_visit_date">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Date of Site Inspection
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <input type="text" class="form-control sl_inspection_date" placeholder="mm/dd/yyyy" name="sv_inspection_date" id="datepicker-autoclose" required>
                                <input type="hidden" name="sv_user_uuid" id="sv_user_uuid">
                                <span class="input-group-addon bg-custom b-0"><i class="icon-calender"></i></span>
                            </div><!-- input-group -->
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-8">
                            <a href="javascript:void(0)" class="btn btn-sm btn-primary-outline waves-effect waves-light save_site_visit_date">
                                Submit
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endif @if(isset($jobApplicationRecord))
<div class="row">
    <div class="col-md-9">
        <div class="card-box list_detail  ">
            @foreach($jobApplicationRecord as $key => $value)
            <div class="inbox-widget nicescroll" style = "background-color: #986ba1;">
                <div class="inbox-item list_desgin">
                    <div class="pull-left  padding-left-0 padding-right-0" style="margin-top: 7px;text-align: center; margin-right: 10px;">
                                <!-- <div class="checkbox checkbox-primary checkbox-single" style="display:inline-block;">
                                <input type="checkbox" name="checkbox" checked onclick="return false;">
                                <label></label>
                            </div> -->
                            <img  class="img-responsive img-circle" src="{{ asset('laravel_code/storage/app/public/user_image/'.$value['users']['image']) }}"
                            alt="{{ $value['users']['image'] }}">
                        </div>
                        <div class=" padding-left-0 padding-right-0" style="margin-top: 7px;">
                            <a href="javascript:void(0)"  id="jobDescription" data-id= "{{$value['user_id']}}">


                                <p class="inbox-item-author pull-left job_description_title">
                                    {{ $value['companies']['name'] }}
                                    <span class="text-success" data-toggle="tooltip" title="" data-original-title="Business Insured">
                                        <i class="fa fa-check-circle" aria-hidden="true"></i>

                                    </span>
                                </p>
                            </a>
                            <br>
                            <div id="targetFormat" class="rating-sm inbox-item-author  job_description_title" style="cursor: pointer; display: inline-block;">
                                <span class="rating_box font-13">5</span>
                                <i class="fa fa-star text-muted" title="bad" data-score="1"></i>&nbsp;
                                <i class="fa fa-star text-muted" title="poor" data-score="2"></i>&nbsp;
                                <i class="fa fa-star text-muted" title="regular" data-score="3"></i>&nbsp;
                                <i class="fa fa-star text-muted" title="good" data-score="4"></i>&nbsp;
                                <i class="fa fa-star text-muted" title="gorgeous" data-score="5"></i>
                                <input name="score" type="hidden">
                            </div>
                            <p class="pull-right lead font-11 text_dark_blue m-b-0 m-t-0 text-md-right ">
                                <!-- <a href="javascript:void(0)" id="{{$value['id']}}" class="show_cover_letter_details" style="display: block;">Cover Letter Details</a> -->
                                @if($value['engineering_report_path'] != null)
                                <a class=" lead font-11 text_dark_blue m-b-0 m-t-0" href="{{ asset('laravel_code/storage/app/public/engineeering_report/'.$value['engineering_report_path'])}}" download><i class="fa fa-download" aria-hidden="true"></i><u> Engineering Report</u></a>
                                @endif
                                @if($value['invoice_report_path'] != null)
                                <br>
                                <a class=" lead font-11 text_dark_blue m-b-0 m-t-0" href="{{ asset('laravel_code/storage/app/public/invoice_report/'.$value['invoice_report_path'])}}" download><i class="fa fa-download" aria-hidden="true"></i><u> Invoice Report</u></a>
                                @endif
                                <br>
                                @if($value['status'] == 1 || $value['status'] == 6)
                                @if($value['site_visit_request'] == 0 || $value['report_status'] == 1)
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm  btn-primary-outline waves-effect waves-light short_list">Short List</a>
                                @endif
                                @if($value['site_visit_request'] == 1)
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm  btn-primary-outline waves-effect waves-light site_visit_date">Site Visit Date</a>
                                @endif
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-danger-outline waves-effect decline_for_job">Reject</a>
                                @endif
                                @if($value['status'] == 3) @if($value['report_status'] == 1)
                                <br>
                                <input type="hidden" name="assing_company_name" value="{{$value['companies']['name'] }}" id="assing_company_name">
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" data-id="{{$value['companies']['name'] }}" class="btn btn-sm custom_size btn-primary-outline waves-effect waves-light assing_job_to_company" style="margin-top: 3px;">Assign Job</a>
                                @endif @endif
                                @if($value['status'] == 7)
                                <br>
                                <input type="hidden" name="assing_company_name" value="{{$value['companies']['name'] }}" id="assing_company_name">
                                <a href="javascript:void(0)" id="{{ $value['user_id'] }}" data-id="{{$value['companies']['name'] }}" class="btn btn-sm  btn-primary-outline waves-effect waves-light award_job">Award Job</a>
                                @endif

                            </p>
                            <br>
                            <div class="inbox-item-text ">
                                <ul class="list-inline m-b-0  text-xs-left" >
                                    <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats" data-toggle="tooltip" title="" data-original-title="Status">
                                        @if($value['status'] == 3)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size label-primary">Short Listed</span></p>
                                        @endif
                                        @if($value['status'] == 4)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size label-success">Job Assigned</span></p>
                                        @endif
                                        @if($value['status'] == 5)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-danger">Rejected</span></p>
                                        @endif
                                        @if($value['status'] == 8)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-warning">Job Awarded</span></p>
                                        @endif
                                    </li>
                                    <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item"  data-toggle="tooltip" title="" data-original-title="Site Visit Date">
                                        @if($value['inspection_date'] != null)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-primary">{!! date_format(new DateTime($value['inspection_date']), 'jS F Y g:ia') !!}</span></p>
                                        @elseif($value['site_visit_request'] == 0)
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-primary">Report Already Exist</span></p>
                                        @else
                                        <p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-primary">Requested for Site Visit</span></p>
                                        @endif
                                    </li>
                                    <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item"  data-toggle="tooltip" title="" data-original-title="Qoute"><p class="text-muted m-b-0 font-13"><span class="label label-pill custom_size  label-primary">{{ $value['quotation_amount'] }}</span></p></li>
                                </ul>
                            </div>

                        </div>
                        <p class="inbox-item-date text-success">{!! date_format(new DateTime($value['created_at']), 'jS F Y g:ia') !!}</p>
                        <div class="job_stats">

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                @endforeach


                <!-- <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr class="text-xs-center">
                        <th>Company</th>
                        <th>T&C</th>
                        <th>Site Visit Date</th>
                        <th>Qoute</th>
                        <th>Submited At</th>
                        <th>Cover Letter</th>
                        <th>Download Report</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($jobApplicationRecord as $key => $value)
                    <tr  class="text-xs-center">
                        <td style="width:155px; vertical-align: middle;" >
                            <img class="img-responsive img-circle" src="{{ asset('laravel_code/storage/app/public/user_image/'.$value['users']['image']) }}"
                        alt="{{ $value['users']['image'] }}">
                        <br>
                            <a href="{{url('oam/fmc/company/details/'.$value['user_id'])}}">{{ $value['companies']['name'] }}</a>
                        </td>

                        <td style="vertical-align: middle;">
                            <input type="checkbox" name="checkbox" checked onclick="return false;">
                        </td>
                        <td style="vertical-align: middle;">
                             @if($value['inspection_date'] != null)
                                {!! date_format(new DateTime($value['inspection_date']), 'jS F Y g:ia') !!}
                            @elseif($value['site_visit_request'] == 0)
                                  <p class="text-muted m-b-0 font-13"><span class="label label-pill label-primary">Report Already Exist</span></p>
                            @else
                                   <p class="text-muted m-b-0 font-13"><span class="label label-pill label-primary">Requested for Site Visit</span></p>
                            @endif
                        </td>
                        <td style="vertical-align: middle;">
                            {{ $value['quotation_amount'] }}
                        </td>
                        <td style="vertical-align: middle;">
                            {!! date_format(new DateTime($value['created_at']), 'jS F Y g:ia') !!}
                        </td>
                        <td style="vertical-align: middle;">
                            <a href="javascript:void(0)" id="{{$value['id']}}" class="show_cover_letter_details">Show Details</a>
                        </td>
                         <td style="vertical-align: middle;">
                            @if($value['engineering_report_path'] != null)
                                <a href="{{ asset('laravel_code/storage/app/public/engineeering_report/'.$value['engineering_report_path'])}}" download>Engineering Report</a>
                            @endif
                        </td>
                        <td style="vertical-align: middle;">
                            @if($value['status'] == 3)
                                <p class="text-muted m-b-0 font-13"><span class="label label-pill label-primary">Short Listed</span></p>
                            @endif
                            @if($value['status'] == 4)
                                <p class="text-muted m-b-0 font-13"><span class="label label-pill label-success">Job Assigned</span></p>
                            @endif
                            @if($value['status'] == 5)
                                <p class="text-muted m-b-0 font-13"><span class="label label-pill label-danger">Rejected</span></p>
                            @endif
                        </td>
                        <td style="vertical-align: middle;">
                            @if($value['status'] == 1 || $value['status'] == 6)
                            <div class="pull-right">
                                <div class="m-t-15  text-xs-left">
                                    @if($value['site_visit_request'] == 0 || $value['report_status'] == 1)
                                        <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm  btn-primary-outline waves-effect waves-light short_list">Short List</a>
                                    @endif
                                    @if($value['site_visit_request'] == 1)
                                        <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm  btn-primary-outline waves-effect waves-light site_visit_date">Site Visit Date</a>
                                    @endif
                                    <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-danger-outline waves-effect decline_for_job">Reject</a>
                                 </div>
                            </div>
                            @endif
                            @if($value['status'] == 3) @if($value['report_status'] == 1)
                            <div class="pull-right">
                                <div class="m-t-15  text-xs-left">
                                    <input type="hidden" name="assing_company_name" value="{{$value['companies']['name'] }}" id="assing_company_name">
                                    <a href="javascript:void(0)" id="{{ $value['user_id'] }}" class="btn btn-sm btn-primary-outline waves-effect waves-light assing_job_to_company">Assign Job</a>
                                </div>
                            </div>
                            @endif @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            <div class="clearfix"></div>
        </table>
            <div class="clearfix"></div>
        </div> -->
    </div>
</div>
</div>
@endif

<!-- Job Reviews if Job is Send for review to client -->
@if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
@if($detail->admin_approval == 3)
@if($detail->jobReview)
@if(count($detail->jobReview) > 0)
<div class="row">
    <div class="col-xs-9">
        <div class="card m-b-10">
            <h4 class="card-header sidebar_header m-b-0">
                Job Review Points
            </h4>
        </div>
    </div>
</div>
@foreach($detail->jobReview as $key => $value)
<div class="col-xs-9">
    <div class="card-box widget-user">
        <div>
            <div class="col-md-10">
                <div class="fmc_company_detail">
                    <div class="wid-u-info description-font  fmc_application_section">
                        {!! $value->review_text!!}
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endforeach
@endif
@endif
@endif
@endif
<!-- End of Job Review System -->

<!-- Refer Company Job -->
<div class="modal fade refer_compnay_job_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Refer Company Job Application</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="short_list_company" data-parsley-validate novalidate method="POST" action="{{ url(($user==3) ? 'fmc/company/job/reference' : 'oam/company/job/reference') }}" name="short_list_company">

                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">

                    <!-- individual Company -->
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                            Email Address
                        </label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" name="ref_email" id="ref_email">
                        </div>
                    </div>
                    <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 form-control-label">
                     </label>
                     <div class="col-sm-8">
                        <button class="btn btn-sm btn-primary-outline waves-effect waves-light short_list_btn">
                            Send Request
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<!-- Cover Letter Before Applying for the Job -->
<div class="modal fade apply_compnay_job_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Apply for the Job</h4>
            </div>

            <div class="modal-body">
                <div class="container">
                    <form role="form" class="apply_for_job_form" name="apply_for_job_form" action="{{url('fmc/job/apply')}}" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="job_id" value="{{$detail->id}}">
                        <input type="hidden" value="{{ $detail->id }}" name="uuid" id="uuid">
                        <input type="hidden" name="status" id="staus" value="1">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">
                            </label>
                            <div class="col-sm-8">
                                <p class="cover_letter_md_error" style="color: red;"></p>
                                {{ csrf_field() }}
                            </div>
                        </div>
                        <!-- Section for New Flow of Work -->
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 form-control-label">
                            </label>
                            <div class="col-sm-8">
                                <label class="radio-inline">
                                  <input class="site_visit_options" name="site_visit_options" type="radio" value="1"> Already have Engineering Report
                              </label>
                              <label class="radio-inline">
                                  <input class="site_visit_options" name="site_visit_options" type="radio" value="2" checked> Request for Site Visit
                              </label>
                          </div>
                      </div>
                      <div class="apply_md_engineering_report" style="display: none;">
                       <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                            Upload Report
                        </label>
                        <div class="col-sm-8">
                            <label class="file">
                                <input type="file" id="engineeering_report" name="apply_engineeering_report" class="apply_engineeering_report">
                                <span class="file-custom"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                            Quotation Amount
                        </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control apply_quotation_amount" id="apply_quotation_amount" name="apply_quotation_amount" step=".01">
                        </div>
                    </div>
                </div>
                <!-- End of Section of New Work -->
                <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-2 form-control-label">
                 </label>
                 <div class="col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary-outline waves-effect waves-light">Apply</button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<!-- Renew Company Job -->
<div class="modal fade renew_compnay_job_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Renew Job</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="short_list_company" data-parsley-validate novalidate method="POST" action="{{url('oam/renew/job/expiry')}}" name="short_list_company">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">

                    <!-- individual Company -->
                    <div class="form-group row">
                     <label for="inputEmail3" class="col-sm-2 form-control-label">
                     </label>
                     <div class="col-sm-8">
                        <button type="submit" class="btn btn-sm btn-primary-outline waves-effect waves-light short_list_btn">
                            Send Request
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<!-- Cover Letter Display -->
<div class="modal fade cover_letter_display in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Cover Letter Detail</h4>
            </div>

            <div class="modal-body">
                <div class="cover_letter_content">

                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Revise Quotation -->
<div class="modal fade revise_quotation_md in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Revise Quoation</h4>
            </div>

            <div class="modal-body">
                <form role="form" class="revise_quotation_form" method="POST" action="{{ url('fmc/company/job/revise/quotation') }}" name="revise_quotation_form">
                    <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="job_id" value="{{$detail->id}}">

                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                        </label>
                        <div class="col-sm-8">
                            <label class="radio-inline">
                              <input class="update_quotation_options" name="update_quotation_options" type="radio" value="1"> Update Quotation
                          </label>
                          <label class="radio-inline">
                              <input class="update_quotation_options" name="update_quotation_options" type="radio" value="2" checked> Stick with previous quotation
                          </label>
                      </div>
                  </div>
                  <div class="revise_quotation_form_quote" style="display: none;">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">
                            New Quotation Amount
                        </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control revise_quotation_amount" id="revise_quotation_amount" name="revise_quotation_amount" step=".01">
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                 <label for="inputEmail3" class="col-sm-2 form-control-label">
                 </label>
                 <div class="col-sm-8">
                    <button type="submit" class="btn btn-sm btn-primary-outline waves-effect waves-light revise_quotation_btn">
                        Submit
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>


<!-- Demand for Work Order -->
<div class="modal fade md_demand_for_work_order in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">Request For Work Order and Upload Invoice</h4>
            </div>

            <div class="modal-body">
                <div class="container">
                    <form role="form" class="md_demand_for_work_order_form" method="POST" action="{{ url('fmc/company/job/workorder/invoide') }}" name="workorder_invoice" enctype="multipart/form-data">
                        <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="job_id" value="{{$detail->id}}">
                        <div class="form-group row">
                            <label for="upload_invoice" class="col-sm-2 form-control-label">
                                Please upload Invoice
                            </label>
                            <div class="col-sm-8">
                                <label class="file">
                                    <input type="file" id="invoice_report" name="invoice_report" class="invoice_report" required>
                                    <span class="file-custom"></span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                         <label for="inputEmail3" class="col-sm-2 form-control-label">
                         </label>
                         <div class="col-sm-8">
                            <button type="submit" class="btn btn-sm btn-primary-outline waves-effect waves-light">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>



<!-- /.modal-dialog -->  <!-- /.modal-dialog -->  <!-- /.modal-dialog -->
<div class="modal fade show_information in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myLargeModalLabel">More Information</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" id="error-msg-list">

                    </ul>
                </div>

                <!-- Terms and condition model -->
                <div class="card-box add_staff ">
                   <div class="modal fade" id="myModal1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                        <div class="card-box bg_grey">
                            <div class="col-md-9">
                                <div class="p-10">
                                    <div class="col-md-2">
                                        <div class="card-box widget-user fmc_bg_shadow ">
                                            <div class="fmc_company_image">
                                                <img class="img-responsive company_image" src="{{ asset( 'laravel_code/storage/app/public/user_image/') }}" alt="">
                                            </div>
                                        </div>
                                    </div>
                                <div class="col-md-10">
                                    <div class="card-box fmc_bg_shadow">
                                        <div class="fmc_company_detail">
                                            <div class="wid-u-info description-font fmc_profile fmc_application_section">

                                                <div class="col-md-12">
                                                    <h4 class="m-t-0">

                                                    </h4>
                                                    <hr class="m-t-0 m-b-10">
                                                </div>
                                                <div class="col-md-6">

                                                    <p class="text-muted m-b-0 font-13">
                                                        <b class="text-dark font-13">Email:</b>
                                                         <span class="company_email"> </span>
                                                    </p>
                                                    <p class="text-muted  font-13 ">
                                                        <b class="text-dark font-13">Contact:</b>
                                                        <span class="company_contact"> </span>
                                                    </p>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="m-t-0">
                                                    <p class="text-muted  font-13 ">
                                                        <b class="text-dark font-13">Trade Licence Number:</b>
                                                        <span class="company_licence_number"> </span> </p>
                                                    </p>
                                                    <p class="text-muted  font-13 ">
                                                        <b class="text-dark font-13">Trade Licence Expiry:</b>
                                                        <span class="company_licence_expiry"> </span> </p>
                                                    </p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <h4 class="font-16 m-t-10">
                                                    Job Categories
                                                    </h4>
                                                    <hr class="m-t-0 m-b-10">
                                                        <div class="company_category_loop">
                                                            <!-- loop for category -->
                                                        </div>
                                                </div>
                                            </div>

                                        </div>
                                         <div class="clearfix"></div>
                                    </div>
                                        <div class="card">
                                            <div class="card-header font-16 ">
                                                <b>Recent Reviews </b>
                                            </div>
                                            <div class="company_review">
                                            </div>

                                        </div>


                                </div>


                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-3 col-xs-12">
                            <div class="card fmc_profile_sidebar fmc_bg_shadow page_box_shadow">
                                <h4 class="card-header font-16 sidebar_header m-b-0">
                                    <b>Details</b>
                                </h4>
                                <ul class="list-group list-group-flush  ">
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Office Address:
                                        </p>
                                        <span class="company_office_address"> </span>
                                    </li>
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Billing Address:
                                        </p>
                                        <span class="company_billing_address" > </span>
                                    </li>
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Alternative Contact:
                                        </p>
                                        <span class="company_contact_alter"> </span>
                                    </li>
                                    <li class="list-group-item">
                                        <p class="headings font-13 m-b-0">
                                            Alternative Email:
                                        </p>
                                        <span class="company_email_alter"></span>
                                    </li>
                                </ul>
                              

                            </div>
                        </div>
                        <div class="clearfix"></div>
                </div>
                        <!-- End terms and condition model -->
            </div>
        </div>

    </div>
            <!-- /.modal-content -->
</div>
        <!-- /.modal-dialog -->


<!--   <div class="card">
    
   
    </div> -->
            @section('js')
            <script src="{{ URL::asset('assets/moment/min/moment.min.js') }} "></script>
            <script src="{{ URL::asset('assets/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js') }} "></script>
            <script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
            <script type="text/javascript">
                var date = new Date();
                date.setDate(date.getDate() - 1);
                jQuery('#datepicker-autoclose').datetimepicker({
        // time: "fa fa-clock-o",
        date: "fa fa-calendar",
        // up: "fa fa-arrow-up",
        // down: "fa fa-arrow-down"
    });
                $(document).ready(function () {
                    $(document).on('click', '#jobDescription', function () {

                        var user_id=$(this).attr('data-id');
                        url="{{url('oam/company/details/')}}";
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {'user_id':user_id ,'_token': '{{csrf_token()}}' },
                            success: function (data) {
                                console.log(data.result);   
                                var company_image=data.result.users.image;
                                var company_email=data.result.users['email'];
                                var company_contact=data.result.company_details['contact_no'];
                                var company_licence_number=data.result.company_details['licence_number'];
                                var company_licence_expiry=data.result.company_details['license_expiry'];
                                var company_office_address=data.result.company_details['office_address'];
                                var company_billing_address=data.result.company_details['billing_address'];
                                var company_contact_alter=data.result.company_details['contact_alter'];
                                var company_email_alter=data.result.company_details['email_alter'];


                                var job_category='';
                                $.each(data.result.job_category, function(index,value)
                                {
                                    job_category=job_category+'<span class="label label-primary">'+value+'</span>'
                                }); 
                                console.log(data.result.getAllReviews);

                                var reviews_array='';
                                if( data.result.getAllReviews.length != 0)
                                {
                                    $.each(data.result.getAllReviews, function(index,value)
                                    {
                                        var review_image='{{ asset( "laravel_code/storage/app/public/user_image/") }}' + value.oam_company.users['image'];
                                        var review_name=value.oam_company['name'];
                                        var review=value['review'];
                                       
                                         
                                        reviews_array=reviews_array+' <div class="card-block"><div class="col-md-2"><div class="fmc_company_image"><img class="img-responsive img-circle" src="'+review_image+'"alt=""></div></div><div class="col-md-10"><div class="pull-left"><h5 class="m-t-10 m-b-5"><a href="#">'+ review_name+'</a> </h5><div class="text-xs-left"></div> <p class="m-b-0 m-t-10 font-13"><b>'+ review +'</b><span class="text-muted  font-13"> </span></p> </div></div><div class="clearfix"></div></div>';
                                    }); 
                                }
                                else
                                {     reviews_array='<div class="card-block"><div class="col-md-12"><div class="text-xs-center"><div class="p-20"><p class="card-title lead">You have no reviews yet</p></div></div></div><div class="clearfix"></div></div>';
                                }

 
        


                                $('.company_image').html(company_image);     
                                $('.company_email').html(company_email); 
                                $('.company_contact').html(company_contact); 
                                $('.company_licence_number').html(company_licence_number);     
                                $('.company_licence_expiry').html(company_licence_expiry);   
                                $('.company_category_loop').html(job_category);   
                                $('.company_office_address').html(company_office_address);   
                                $('.company_billing_address').html(company_billing_address);   
                                $('.company_contact_alter').html(company_contact_alter);   
                                $('.company_email_alter').html(company_email_alter);   
                                $('.company_review').html(reviews_array);                
                            }
                        });

                        $('.show_information').modal('show');

                    });
                    @if(Auth::user()->role_id == 3)
                    $("#full_details").show();
                    @endif
                    var get_uuid = $('#uuid').val();
                    $(document).on('click', '.apply_for_job', function () {
            //$('#staus').val(1);
            //$('.loading_gif').show();
            //$('#job_form').submit()[0];
            $('.apply_compnay_job_md').modal('show');
        });
                    $(document).on('click','.apply_for_job_md', function(){
                        if($('.cover_letter_md').val() == ''){
                            console.log('comming here');
                            $(".cover_letter_md_error").html('Please fill up the Cover Letter before Submittion');
                        }
                        else{
                            $('.apply_for_job_form').submit()[0];
                        }
                    });
                    $(document).on('click', '.decline_for_job', function () {
                        $('#staus').val(2);
                        $('.loading_gif').show();
                        $('#job_form').submit()[0];
                    });
                    $(document).on('click', '.show_full_detail', function () {
                        $('.show_full_detail').hide();
                        $('.show_less_detail').show();
                        $("#full_details").slideToggle("slow", function () {
                // Animation complete.
            });
                    });
                    $(document).on('click', '.show_less_detail', function () {
                        $('.show_less_detail').hide();
                        $('.show_full_detail').show();
                        $("#full_details").slideToggle("slow", function () {
                // Animation complete.
            });
                        $('.show_full_detail').show();
                    });
                    $(document).on('click', '.short_list', function () {
                        var user_id = $(this).attr('id');
                        $('.sl_company_name').text('');
                        var company_name = $(this).parent('.fmc_application_section').find('h5').html();
            // console.log(company_name);
            $('#sl_company_name').text(company_name);
            $('#user_uuid').val(user_id);
            $('.add_shortlisted_company').modal('show');

        });

                    $(document).on('click','.decline_for_job', function(){
                        var user_id = $(this).attr('id');
                        var company_name = $(this).parent('.fmc_application_section').find('h5').html();
                        $('#rj_company_name').text('');
                        $('#rj_company_name').text('Are You Sure you Want Reject Company '+company_name);
                        $('.rj_company_name').text('');
                        $('.rj_company_name').text(company_name);
                        console.log(user_id);
                        $('#rl_user_uuid').val(user_id);
                        console.log($('#rl_user_uuid').val());
                        $('.md_reject_company').modal('show');
                    });

                    $(document).on('click', '.assing_job_to_company', function(){
                        var user_id = $(this).attr('id');
                        var company_name = $(this).attr('data-id');
                        console.log(company_name);
                        $('#assign_company_detail').text('');
                        $('#assign_company_detail').html('Are you sure you want to Assign Job to Company <u>'+ company_name +'</u>');
                        $('#assign_user_uuid').val(user_id);
                        $('.assign_compnay_job_md').modal('show');
                    });

                    $(document).on('click', '.save_short_list', function(){
                        $('.md_short_list_company').submit()[0];
                    });
                    $(document).on('click', '.refer_job_btn', function(){
                        console.log('asdasdasd');
                        $('.refer_compnay_job_md').modal('show');
                    });

                    tinymce.init({
                        selector: 'textarea',
                        height: 150,
                        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                        setup: function (editor) {
                            editor.on('change', function () {
                                editor.save();
                            });
                        }
                    });

                    $(document).on('click','.show_cover_letter_details', function(){
                        var job_apply_id = $(this).attr('id');
                        @if(Auth::user()->role_id == 1)
                        url = "{{ generate_url('/get/cover_letter') }}";
                        @else
                        url = "{{ generate_url('/oam/get/cover_letter') }}";
                        @endif
                        $.ajax({
                            type: 'GET',
                            url: url,
                            data: {
                                "job_apply_id": job_apply_id
                            },
                            success: function (response) {
                                if(response.status == 'success'){
                                    $('.cover_letter_content').html(response.result.cover_letter);
                                    $('.cover_letter_display').modal('show');
                                }
                            }
                        });
                    });

                    $(document).on('click', '.renew_job_btn', function(){
                        $('.renew_compnay_job_md').modal('show');
                    });

                    $(document).on('click', '.site_visit_options', function(){
                        if($(this).val() == 1){
                            $('.apply_md_engineering_report').show();
                // $('.apply_engineeering_report').prop('required', true);
                $('.apply_quotation_amount').prop('required', true);
            }
            else{
                $('.apply_md_engineering_report').hide();
                $('.apply_engineeering_report').prop('required', false);
                $('.apply_quotation_amount').prop('required', false);
            }
        });

                    $(document).on('click', '.site_visit_date', function () {
                        var user_id = $(this).attr('id');
                        $('#sv_user_uuid').val(user_id);
                        $('.add_site_visit_date').modal('show');

                    });

                    $(document).on('click', '.save_site_visit_date', function(){
                        if($('.sl_inspection_date').val() != ''){
                            $('.md_site_visit_date').submit()[0];
                        }
                    });

                    $(document).on('click', '.revise_quotation_status', function(){
                        $('.revise_quotation_md').modal('show');
                    });

                    $(document).on('click', '.update_quotation_options', function(){
                        if($(this).val() == 1){
                            $('.revise_quotation_form_quote').show();
                            $('.revise_quotation_amount').prop('required',true);
                        }
                        else{
                            $('.revise_quotation_amount').prop('required',false);
                            $('.revise_quotation_form_quote').hide();
                        }

                    });

                    $(document).on('click', '.demand_for_workorder', function(){
                        $('.md_demand_for_work_order').modal('show');
                    });

                    $(document).on('click', '.award_job', function(){
                        var user_id = $(this).attr('id');
                        var company_name = $(this).attr('data-id');
                        console.log(company_name);
                        $('#awarding_company_detail').text('');
                        $('#awarding_company_detail').html('Are you sure you want to Award this Job to Company <u>'+ company_name +'</u>');
                        $('#awarding_user_uuid').val(user_id);
                        $('.award_job_md').modal('show');
                    });
                })
            </script> @endsection
            <!-- end row -->
            @endsection

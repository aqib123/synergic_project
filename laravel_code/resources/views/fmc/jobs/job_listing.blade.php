@extends('fmc.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <!-- <a href="{{url('jobcat_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add Category</a> -->
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-9 col-lg-9 col-xs-12">
        <ul class="nav association_nav  nav-tabs m-b-0" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="my-job-tab" data-toggle="tab" href="#my_jobs" role="tab" aria-controls="my-job-tab"   aria-expanded="true">My Jobs</a>
            </li>
        <li class="nav-item">
                <a class="nav-link " id="all-job-tab" data-toggle="tab" href="#all_jobs" role="tab" aria-controls="all-job-tab"
                  >All Jobs</a>
            </li>
        </ul>
        <!-- Job listing card end-->
        <div class="card">
            <div class="tab-content" id="myTabContent">
                <div role="tabpanel" class="tab-pane fade in active" id="my_jobs" aria-labelledby="my-job-tab">
                    @if(isset($data) && count($data)>0) @foreach($data as $row)
                    @php 
                        $is_assigned = \App\JobApply::where('job_id', $row['id'])->where('status', 4)->get(); 
                        $is_assigned = count($is_assigned);
                    @endphp
                    <div class="card-block listing_cards">
                        <div class="col-lg-8">
                            <p class="lead m-b-10">
                                <a href="#">{{$row['job_title']}}</a>
                            </p>
                            <!-- <hr class="m-t-0"> -->
                            <!-- <p class="m-b-0 font-13">Al-Amir, Lahore, Pakistan</p>                             -->
                            <p class="m-b-0">{{ strip_tags(substr($row['job_description'],0,180)) }}</p>
                        </div>
                        <div class="col-lg-4 ">
                            <div class="pull-right job_listing">
                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Posted On" class="lead font-13 text-muted m-b-10">
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date_format(new DateTime($row['date']), 'jS F Y g:ia');?>
                                </span>
                                <br />
                                <span class="lead font-13 text-muted m-b-10">Viewed {{isset($row['count_views'])?$row['count_views']:0}}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-footer footer_sidebar_header">
                        @if(GeneralFunctions::check_view_permission('/fmc/job_description'))
                        <span class="pull-right">
                            <button type="button" class="btn btn-sm btn-dark-outline waves-effect waves-light right show_datails" id="{{$row['id']}}"
                                style="margin-left:5px; ">View</button>
                        </span>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                    @endforeach @endif
                </div>
                <div role="tabpanel" class="tab-pane fade  " id="all_jobs" aria-labelledby="all-job-tab">
                    @if(isset($allJobs) && count($allJobs)>0) @foreach($allJobs as $row)
                    <div class="card-block listing_cards">
                        <div class="col-lg-8">
                            <p class="lead m-b-10">
                                <a href="#">{{$row['job_title']}}</a>
                            </p>
                            <!-- <hr class="m-t-0"> -->
                            <!-- <p class="m-b-0 font-13">Al-Amir, Lahore, Pakistan</p>                             -->
                            <p class="m-b-0">{{ strip_tags(substr($row['description'],0,180)) }}</p>
                        </div>
                        <div class="col-lg-4 ">
                            <div class="pull-right job_listing">
                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Posted On" class="lead font-13 text-muted m-b-10">
                                    <i class="fa fa-calendar"></i>
                                    <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                                </span>
                                <br />
                                <span class="lead font-13 text-muted m-b-10">Total Views {{isset($row['count_views'])?$row['count_views']:0}}</span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card-footer footer_sidebar_header">
                        @if(GeneralFunctions::check_view_permission('/fmc/job_description'))
                        <span class="pull-right">
                            <button type="button" class="btn btn-sm btn-dark-outline waves-effect waves-light right show_datails" id="{{$row['id']}}"
                                style="margin-left:5px; ">View</button>
                        </span>
                        @endif
                        <div class="clearfix"></div>
                    </div>
                @endforeach @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12 ">

        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
                Jobs in related categories
            </h4>
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item">
                    <a href="{{url('/fmc/job_list')}}" class="card-link">
                        <span> Show All related Categories </a>
                </li>
            </ul>

        </div>

    </div>
</div>
@section('js')
<script type="text/javascript">
    $(document).ready(function () {
        $("#my-job-tab").addClass("active");
        $(document).on('click', '.show_datails', function () {
            var path = "{{ url('fmc/payment_screen/') }}" + '/' + $(this).attr('id');
            window.location.href = path;
        });

    var tab = '{{Request::get("tab")}}';
    if(tab == 'all'){
        $("#my-job-tab").removeClass('active');
        $("#all-job-tab").addClass('active');
        $("#all_jobs").addClass('in active');
        $("#my_jobs").removeClass('in active');
        
    }

    });
</script> @endsection
<!-- end row -->
@endsection
@extends('fmc.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Date</th>
                        <th>Job Title</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($data) && count($data)>0) @foreach($data as $row)
                    <tr>
                        <td>{{$row['id'] }}</td>
                        <td><?php echo date_format(new DateTime($row['date']), 'jS F Y g:ia');?></td>
                        <td>{{ $row['description']}}</td>
                        <td>
                            <a href="{{ url('fmc/payment_screen/'.$row['id']) }}" class="btn btn-success apply" id="{{ $row['id'] }}">Show</button>
                        </td>
                    </tr>
                    @endforeach @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
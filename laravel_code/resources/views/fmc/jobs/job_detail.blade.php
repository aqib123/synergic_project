@extends('fmc.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('jobcat_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add Category</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-9 col-lg-9 col-xs-12 m-t-15">
        
                
        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
                Arfa Tower
            </h4>
            <div class="card-block">
                <div class="col-lg-8">
                    <p class="lead m-b-0">Job Description </p>
                    <p class="m-b-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere
                        erat a ante dolor sit amet, consectetur adipiscing elit. Integer posuere
                        erat a ante.
                    </p>
                </div>
                <div class="col-lg-4 ">           
                    <div class="pull-right ">           
                        <p class="lead text-xs-right font-13 text-danger m-b-0">Posted 1 hour ago</p>
                        <p class="lead text-xs-right font-13 text-primary m-b-0">67 views </p>
                        <p class="lead text-xs-right font-13 text-primary m-b-10">Report Job </p>
                        <button type="button" class="btn btn-sm btn-dark-outline waves-effect waves-light right" style="margin-left:5px; ">Save</button>
                        <button type="button" class="btn btn-sm btn-dark-outline waves-effect waves-light right">Apply Now</button>
                    </div>
            </div>
            <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-lg-3 col-xs-12 m-t-15">

        <div class="card page_box_shadow">
            <h4 class="card-header sidebar_header m-b-0">
                Job Stats
            </h4>
            <ul class="list-group list-group-flush sidbar_icon">
                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-check-square"></i> <span> Applied</span> </a></li>
                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-list-alt"></i> <span> Shortlisted</span> </a></li>
                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-calendar-check-o"></i> <span> Interview</span>  </a></li>
                <li class="list-group-item"><a href="#" class="card-link"><i class="fa fa-hand-pointer-o"></i> <span> Selected</span> </a></li>
            </ul>
            
        </div>

    </div>
</div>
<!-- end row -->
@endsection
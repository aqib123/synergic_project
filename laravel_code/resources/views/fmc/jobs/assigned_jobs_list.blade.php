@extends('fmc.app') @section('content')
@section('css')
<style>
.list_detail {
    border: none;
    padding: 6px 10px 0px 10px;
}

.list_desgin {
    margin-bottom: 5px;
    background-color: #F9F9F9;
    padding: 10px 5px !important;
    border: 1px solid #ccc !important;
    border-radius: 2px;
}

.job_description_title {
    font-size: 13px;
    font-weight: 600 !important;
    color: #000 !important;
}

.text_dark_blue {
    color: #0087e0;
}

.job_description_detail {
    color: #515151 !important;
}

.font-11 {
    font-size: 11px;
}

.job_stats {
    margin-right: 15px;
}

.custom_size{
    padding: 0.10rem 0.75rem;
font-size: 0.675rem;
}
</style>
@endsection
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-xl-9">
        <div class="card page_box_shadow">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="card-block list_detail">
                        @if(isset($assigned_jobs) && count($assigned_jobs)>0)
                          
                                    @foreach($assigned_jobs as $row) 
                                    <div class="inbox-widget nicescroll">
                                        <div class="inbox-item list_desgin">
                                            <div class="col-lg-12 padding-left-0 padding-right-0" style="margin-top: 7px;">
                                                <p class="inbox-item-author pull-left job_description_title"> 
                                                    <a class="custom_links" href="{{url('oam/oam_job_details_status/'.$row['id'])}}">{{$row->jobs->job_title}}</a>
                                                       
                                                </p>
                                                <p class="  pull-right lead font-11 text_dark_blue m-b-0 m-t-0 ">
                                                    {!! ($row->jobs->job_status) == 0 ? 'In Progress' : 'Completed' !!}
                                                </p>
                                            </div>
                                            <p class="inbox-item-text job_description_detail">
                                                 {{ strip_tags(substr($row->jobs->description,0,100)) }}</p>
                
                                            <p class="inbox-item-date text-success">@php echo date_format(new DateTime($row->jobs->created_at), 'jS F Y'); @endphp</p>
                                            <div class="job_stats">
                                                <ul class="list-inline m-b-0  text-xs-left">   
                                                     <li class="lead  font-11 text_dark_blue m-b-0 list-inline-item manage_stats">
                                                            <a href="{{url('fmc/payment_screen/'.$row->jobs->id)}}" class="btn custom_size btn-sm btn-dark-outline waves-effect waves-light show_datails"
        id="14"  >View</a> 
                                                    </li> 
                                                </ul>
                    
                                            </div>
                                        </div> 
                                           
                                        
                                        <div class="clearfix"></div>
                                    </div>
                                    @endforeach 
                        @else
                        <div class="col-md-12">
                                        <div class="text-xs-center">
                                            <div class="p-20">
                                                <p class="card-title lead">You have not assigned any job yet.</p>
                                                <a href="{{url('fmc/job_list')}}" class="btn btn-dark-outline waves-effect waves-light">Job list</a>
                                            </div>
                                        </div>
                                    </div>
                        @endif

                    </div>
                </div>
                
            </div>
        </div>
    </div>
  
</div>
<!-- end row -->
@endsection
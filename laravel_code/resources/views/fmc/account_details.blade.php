@extends( Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app' ) @section('css')
<link href="{{ URL::asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }} " rel="stylesheet">
<style type="text/css">
    .fa-star {
        color: yellow;
    }
</style>
@stop @section('content')
@php
    $getSubscriptionAccountType = $company_details->subscription_account;
    $getNoOfDays = GeneralFunctions::difference_bwt_two_dated($company_details->account_expiration_date,time());
    $previous_sub_details = [];
    if($company_details->previous_subscription_dates){
       $previous_sub_details = json_decode($company_details->previous_subscription_dates, true);
    }
    $count = 1;
@endphp

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<!-- Fmc profile details -->
<div class="row">
    <div class="card-box bg_grey">
        <div class="col-md-9">
            <div class="p-10">

                <div class="col-md-2">
                    <div class="card-box widget-user fmc_bg_shadow ">
                        <div class="fmc_company_image">
                            <img class="img-responsive " src="{{ asset( 'laravel_code/storage/app/public/user_image/' . $company_details->users->image) }}"
                                alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="card-box fmc_bg_shadow">
                        <div class="fmc_company_detail">
                            <div class="wid-u-info description-font fmc_profile fmc_application_section">

                                <div class="col-md-12">
                                    <h4 class="m-t-0">
                                        {{$company_details->name}}
                                    </h4>
                                    <hr class="m-t-0 m-b-10">
                                </div>
                                <div class="col-md-6">

                                    <p class="text-muted m-b-0 font-13">
                                        <b class="text-dark font-13">Email:</b> {{$company_details->users->email}}</p>
                                    <p class="text-muted  font-13 ">
                                        <b class="text-dark font-13">Contact:</b> {{$company_details->contact_no}}</p>
                                </div>
                                <div class="col-md-6">
                                    <div class="m-t-0">
                                        <p class="text-muted  font-13 ">
                                            <b class="text-dark font-13">Licence Number:</b> {{$company_details->licence_number}}</p>
                                        <p class="text-muted  font-13 ">
                                            <b class="text-dark font-13">Licence Expiry:</b> {{$company_details->license_expiry}}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="card">
                        <div class="card-header font-16 ">
                            <b>Previous Subscription Dates Details</b>
                        </div>
                        @if(isset($previous_sub_details) && !empty($previous_sub_details)) 
                            <table class="table table responsive">
                                <tr>
                                    <th>Subscription Dates</th>
                                </tr>
                                @foreach($previous_sub_details as $key => $value)
                                    <tr>
                                        <td>{!! $count !!}) {!! date_format(new DateTime($value), 'jS F Y g:ia') !!}</td>
                                    </tr>
                                @php
                                    $count++;
                                @endphp    
                                @endforeach
                            </table>
                        @else 
                            <div class="card-block">
                                <div class="col-md-12">
                                    <div class="text-xs-center">
                                        <div class="p-20">
                                            <p class="card-title lead">You have no reviews yet</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endif
                    </div>


                </div>


            </div>
        </div>

        <div class="col-sm-3 col-lg-3 col-xs-12">
            <div class="card fmc_profile_sidebar fmc_bg_shadow page_box_shadow">
                <h4 class="card-header font-16 sidebar_header m-b-0">
                    <b>Details</b>
                </h4>

                <ul class="list-group list-group-flush  ">
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Subscription Account Type
                        </p>
                        <span>{!! $getSubscriptionAccountType == 1 ? '3 Months Subscription' : '1 Month Subscription' !!}</span>
                    </li>
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Days Left For Account Expiration
                        </p>
                        <span><b>{!! $getNoOfDays !!}</b> No of Days Left</span>
                    </li>
                    <li class="list-group-item">
                        <p class="headings font-13 m-b-0">
                            Expiration Date
                        </p>
                        <span><b><u>{!! date_format(new DateTime($company_details->account_expiration_date), 'jS F Y g:ia') !!}</u></b></span>
                    </li>
                </ul>

            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

@section('js')
<script src="{{ URL::asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }} "></script>
<script>
    var date = new Date();
    date.setDate(date.getDate() - 1);
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        startDate: date
    });
</script> @stop @endsection
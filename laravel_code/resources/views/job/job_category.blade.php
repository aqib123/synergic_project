@extends('layouts.app') @section('content')
<style type="text/css">
.form-check-inline input[type="radio"]{
    margin-right: 10px !important;
}
</style>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('categories')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back to Categories</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@if(count($errors) > 0)
<div class="alert alert-danger">
    <ul style="text-decoration: none;">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif @if(\Session::has('status'))
<div class="alert alert-success">
    <p>{{ \Session::get('status')}}</p>
</div>
@endif
<div class="row">
    <div class="col-sm-6 col-xs-12 col-md-6">
        <div class="card-box">
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="1">Manual Data Entery
              </label>
            </div>
            <div class="form-check-inline">
              <label class="form-check-label">
                <input type="radio" class="form-check-input data_entry_type" name="optradio" value="2">Bulk Import
              </label>
            </div>
        </div>
    </div>
</div>
<div class="row bulk_importing" style="display: none;">
    <div class="col-sm-6 col-xs-12 col-md-6">
        <div class="card-box">
            <form enctype="multipart/form-data" role="form" data-parsley-validate novalidate method="POST" action="{{ url('bulk/import/job_category') }}">
                <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }} row">
                    <label for="csv_file" class="col-md-4 control-label">CSV file to import</label>

                    <div class="col-md-6">
                        <input id="csv_file" type="file" class="form-control" name="csv_file" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6 col-md-offset-4">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="header"> File contains header row?
                            {{ csrf_field() }}
                        </label>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-8 col-md-offset-4">
                        <button type="submit" class="btn btn-success">
                            Parse CSV
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row manual_adding" style="display: none;">
    <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('jobcat_save') }}" enctype="multipart/form-data">
        <div class="col-sm-6 col-xs-12 col-md-6">
            <div class="card-box">
                <div class="p-10">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Category
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-7">
                            <select class="form-control" name="category" id="parent_category">
                                <option value="">Select Option</option>
                                @foreach($categories as $key => $value)
                                    <option value="{{$value['id']}}" {!! isset($job->category_id) ? (($job->category_id == $value['id']) ? 'selected' : '') : ''  !!}>{{$value['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <a type="button" class="btn btn-primary add_category_detail" href="javascript:void(0)">
                                <i class="fa fa-plus-circle"></i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Sub Category
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-7">
                            <select class="form-control sub_category_class" name="sub_category">
                                <option value="">Select Option</option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <a type="button" class="btn btn-primary add_sub_category_detail" href="javascript:void(0)">
                                <i class="fa fa-plus-circle"></i>
                            </a>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Service Code
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-7">
                            <input type="text" required parsley-type="text" name="service_code" value="{{old('service_code')}}{{isset($job->service_code)?$job->service_code:''}}" class="form-control"
                                placeholder="Service Code">
                        </div>
                        <!-- <div class="col-sm-2">
                            <a type="button" class="btn btn-primary add_service_code_detail" href="javascript:void(0)">
                                <i class="fa fa-plus-circle"></i>
                            </a>
                        </div> -->
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 form-control-label">Service Name
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-7">
                            <input type="text" required parsley-type="text" name="service_name" value="{{old('service_name')}}{{isset($job->service_name)?$job->service_name:''}}" class="form-control"
                                placeholder="Service Name">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(GeneralFunctions::check_add_permission('/jobcat_form'))
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group row">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-success waves-effect waves-light">
                        Save
                    </button>
                    <!-- <button type="reset" class="btn btn-danger waves-effect m-l-5">
                        Reset
                    </button> -->
                </div>
            </div>
        </div>
        @endif
    </form>
</div>
@if(isset($csv_data))
    <div class="col-sm-12 col-xs-12 col-md-12">
        <div class="card-box">
            <form class="form-horizontal" method="POST" action="{{ url('import_process') }}">
                {{ csrf_field() }}
                    <table class="table">
                        @foreach ($csv_data as $row)
                            <tr>
                            @foreach ($row as $key => $value)
                                <td>{{ $value }}</td>
                            @endforeach
                            </tr>
                        @endforeach
                        <tr>
                            @foreach ($csv_data[0] as $key => $value)
                                <td>
                                    <select name="fields[{{ $key }}]">
                                        @foreach (config('app.db_fields') as $db_field)
                                            <option value="{{ $loop->index }}">{{ $db_field }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            @endforeach
                        </tr>
                    </table>
                <button type="submit" class="btn btn-primary">
                    Import Data
                </button>
            </form>
        </div>
    </div> 
@endif           
<!-- Modal Box for Adding Job Category -->
<div class="modal fade md_category_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Category</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_category_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="md_category_name">Category Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control md_category_name" id="md_category_name" name="md_category_name" placeholder="Enter Catgory Name">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_category_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Box for Adding Job Sub Category -->
<div class="modal fade s_category_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Sub Category</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_sub_category_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name"> Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_category_name"></p>
                          <input type="hidden" name="category_uuid" id="category_uuid" class="category_uuid">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Sub Category Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control s_sub_category_name" id="s_sub_category_name" name="s_sub_category_name" placeholder="Enter Sub Catgory Name">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="email"></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_sub_category_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Modal Box for Adding Service Code -->
<div class="modal fade s_service_code_details" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Add Service Code</h4>
            </div>
            <div class="modal-body">
                <div class="error-msg-box alert" style="display: none;">
                    <ul style="text-decoration: none;" class="error-msg-list">

                    </ul>
                </div>
                <form class="form-horizontal" id="add_service_code_form">
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_category_name"></p>
                          <input type="hidden" name="service_category_uuid" id="service_category_uuid" class="service_category_uuid">
                          {{ csrf_field() }}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Sub Category Name</label>
                        <div class="col-sm-10">
                          <p class="selected_sub_category_name"></p>
                          <input type="hidden" name="service_ub_category_uuid" id="service_ub_category_uuid" class="service_ub_category_uuid">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for="s_sub_category_name">Service Code</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control s_service_code" id="s_service_code" name="s_service_code" placeholder="Enter Service Code">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-sm-2" for=""></label>
                        <div class="col-sm-10">
                           <a href="javascript:void(0)" class="btn btn-success add_service_code_btn">Save</a>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $(document).ready(function(){
            @if(Request::input('id'))
                var req_id = "{{Request::input('id')}}";
            @else
                var req_id = null;
            @endif
            function getSubcategories(){
                var category_option = $('#parent_category').val();
                $.ajax({
                    type: 'GET',
                    url: "{{ generate_url('/sub/category') }}",
                    data: {'category_uuid' :category_option,
                            'req_id' : req_id},
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.sub_category_class').html(data.result);
                        }
                    }
                });
            }
            function getServiceCodes(){
                var category_option = $('#parent_category').val();
                var sub_category_option = $('.sub_category_class').val();
                console.log('comming here');
                $.ajax({
                    type: 'GET',
                    url: "{{ generate_url('/service_code/category') }}",
                    data: {'category_uuid' :category_option,
                            'sub_category_uuid' :sub_category_option,
                            'req_id' : req_id},
                    success: function (data) {
                        if (data.status == 'success') {
                            $('.service_code_class').html(data.result);
                        }
                    }
                });
            }
            $(document).on('change','#parent_category', function(){
                getSubcategories();
            });

            $(document).on('change','.sub_category_class', function(){
                getServiceCodes();
            });

            $(document).on('click','.add_category_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                $('.md_category_details').modal('show');
            });

            $(document).on('click','.add_category_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_category_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/category/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('#parent_category').html(data.result);
                            $('.md_category_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });

            $(document).on('click','.add_sub_category_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var category_option = $('#parent_category').val();
                if(category_option == ''){
                    alert('Please Select the Category first.');
                }
                else{
                    $('.selected_category_name').text($("#parent_category option:selected").text());
                    $('#category_uuid').val(category_option);
                    $('.s_category_details').modal('show');
                }
            });

            $(document).on('click','.add_sub_category_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_sub_category_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/sub/category/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('.sub_category_class').html(data.result);
                            $('.s_category_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });
            getSubcategories();
            setTimeout(function() {
                getServiceCodes();
            }, 1000);
            // New Implementation for Job Category
            $(document).on('click','.add_service_code_detail', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var category_option = $('#parent_category').val();
                var sub_category_option = $('.sub_category_class').val();

                if(category_option == '' || sub_category_option == ''){
                    alert('Please Select the Category first And Sub Category First');
                }
                else{
                    $('.selected_category_name').text($("#parent_category option:selected").text());
                    $('.selected_sub_category_name').text($(".sub_category_class option:selected").text());
                    $('.service_category_uuid').val(category_option);
                    $('.service_ub_category_uuid').val(sub_category_option);
                    $('.s_service_code_details').modal('show');
                }
            });
            $(document).on('click','.add_service_code_btn', function(){
                $(".error-msg-list").empty();
                $('.error-msg-box').hide();
                var form_data = $('#add_service_code_form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ generate_url('create/service_code/name') }}",
                    data: form_data,
                    success: function (data) {
                        $(".error-msg-list").empty();
                        if (data.status == 'success') {
                            $('.service_code_class').html(data.result);
                            $('.s_service_code_details').modal('hide');
                        }
                        else {
                            var errorArray = data.msg_data;
                            var list = '';
                            errorArray.forEach(function (e) {
                                list = list + '<li>' + e + '</li>';
                            });

                            $('.error-msg-list').html(list);
                            $('.error-msg-box').addClass("alert-danger").show();
                        }
                    }
                });
            });

            $(document).on('click', '.data_entry_type', function(){
                if($(this).val() == 1){
                    $('.bulk_importing').hide();
                    $('.manual_adding').show();
                }else{
                    $('.bulk_importing').show();
                    $('.manual_adding').hide();
                }
            });
        });
    </script>
@endsection

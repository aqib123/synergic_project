@extends('layouts.app') @section('content') @section('css')
<link href="{{ URL::to('assets/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::to('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    .image-box {
        padding: 10px 20px;
    }

    .ads-data {
        display: inline-block;
        height: 69px;
        vertical-align: middle;
        margin-left: 30px;
    }

    .list_detail {
        border: none;
        padding: 7px 6px 30px 6px;
    }

    /* .ads_block{display: block;} */
</style>
@endsection
<!-- Page-Title -->
<div class="row m-t-20 m-b-10">
    <div class="m-b-0">
        @include('flash_message')
        <div class="col-sm-3">
            <h4 class="lead" style="margin-top: 2px;">Assign Ads To Companies</h4>
        </div>
        <form action="{{url('assign_ads')}}" method="POST">
            {{csrf_field()}}
        <div class="col-sm-9">
            <div class="dashboard_grid_header">
                <div class="col-md-6 pull-left">
                    <div class="col-md-8">
                        <select class="select2 form-control select2-multiple oam_multiselect" name="oam[]" multiple="multiple" multiple data-placeholder="Choose OAM...">
                            @if(isset($oam) && count($oam) > 0) @foreach($oam as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox-success checkbox-inline" style="margin-top: 5px;">
                            <input id="oam_checkbox" class="select_all_oam" value="1" name="all_oam" type="checkbox">
                            <label for="oam_checkbox"> Select All </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 pull-right">
                    <div class="col-md-8">
                        <select class="select2 form-control select2-multiple fmc_multiselect" name="fmc[]" multiple="multiple" multiple data-placeholder="Choose FMC...">
                            @if(isset($fmc) && count($fmc) > 0) @foreach($fmc as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach @endif
                        </select>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox checkbox-success checkbox-inline" style="margin-top: 5px;">
                            <input id="fmc_checkbox" class="select_all_fmc" value="1" name="all_fmc" type="checkbox">
                            <label for="fmc_checkbox"> Select All </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="col-sm-12 col-xs-12 col-md-12">
    <div class="row">
        <div class="card-box list_detail m-b-10 image-box">
            
                @php 
                $i = 0; 
                $image = []; 
                @endphp
                @if(isset($ads) && count($ads) > 0) @foreach($ads as $ad)
                <div class="col-md-6 padding-left-0 padding-right-0 ">
                    <div class="card-box m-b-10" style="padding: 10px 17px 0px 0px;">
                        <div class="ads-data">
                            <div class="ads_block">
                                <div class="checkbox checkbox-success checkbox-inline">
                                    <input id="oam_checkbox{{$ad->id}}" value="{{$ad->id}}" name="image[]" type="checkbox">
                                    <label for="oam_checkbox{{$ad->id}}"></label>
                                </div>
                            </div>
                        </div>
                        <div class="ads-data">
                            <div class="user_image_padding m-t-15">
                                <img class="img_size" src="{{ asset('laravel_code/storage/app/public/company_ad/' . $ad->image) }}" alt="">
                            </div>
                        </div>
                        <div class="ads-data">
                            <h4 class="lead font-13 m-t-20">
                                <b>{{$ad->title}}</b>
                            </h4>
                        </div>
                        <div class="btn-group user_action_btn">
                            <div class="  ads-data ">
                                <div class="   m-t-20">
                                    <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                        <i class="zmdi zmdi-more"></i>
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ url('settings?status=5&id='.$ad->id) }}" title="Edit">
                                            <i class="fa fa-edit text-info"></i> Edit
                                        </a>
                                        <a class="dropdown-item delete_ad_btn" href="javascript:void(0)" id="{{$ad->id}}" title="Delete">
                                            <i class="fa fa-trash text-danger"></i> Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach @endif
                <div class="col-sm-12 padding-left-0">
                    <button type="submit" class="profie_update btn btn-success waves-effect waves-light">
                        Save
                    </button>
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- end row -->

<!-- Modal Box for Deletion confirmation -->
<div class="modal fade md_delete_confirmation" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header alert alert-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="success-message">Are you sure you wish to delete this record ? </p>
                <input type="hidden" name="user_uuid" id="ad_id">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
                <button class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@endsection @section('js')
<script type="text/javascript" src="{{ URL::to('assets/pages/jquery.formadvanced.init.js') }}"></script>
<script src="{{ URL::to('assets/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select_all_oam').change(function () {
            if ($(this).prop('checked')) {
                $(".oam_multiselect").prop("disabled", true);
            }
            else {
                $(".oam_multiselect").prop("disabled", false);
            }
        });
        $('.select_all_fmc').change(function () {
            if ($(this).prop('checked')) {
                $(".fmc_multiselect").prop("disabled", true);
            }
            else {
                $(".fmc_multiselect").prop("disabled", false);
            }
        });

        $(document).on('click', '.delete_ad_btn', function(){
            var id = $(this).attr('id');
            $('#ad_id').val(id);
            /* Show Confirmation alert box before deletion */
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click','.md_ok_delete_btn', function(){
            var ad_id = $('#ad_id').val();
            var url = "{{url('delete_ad/')}}/"+ad_id;
            window.location.href = url;
        });
    });
</script> @endsection
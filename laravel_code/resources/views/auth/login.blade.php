@extends('layouts.index') @section('content')
@section('css')
<style type="text/css">
body {
    /* overflow:hidden; */
    padding-bottom: 10px;
}
.account-pages{
    background-position: center top;
    background-size: cover;
}

</style>

@endsection
  
    <div class=" login_form">
        @include('flash_message')
        <div class="row">
            <div class="col-xs-12 text-xs-center">
                <h5 class="text-muted text-uppercase m-b-0 m-t-0"><b>Sign In</b>  </h5>
            </div>
        </div>
        <form class=" m-t-20" method="POST" action="{{URL::to('/login/user') }}">
            {{ csrf_field() }}
            <!-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                <div class="col-xs-12">
                    <input id="email" type="email" Placeholder ="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div> -->
            <div class="form-group{{ $errors->has('user_name') ? ' has-error' : '' }} row">
                <div class="col-xs-12">
                    <input id="user_name" type="text" Placeholder ="User Name" class="form-control" name="user_name" value="{{ old('user_name') }}" required autofocus> @if ($errors->has('user_name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('user_name') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                <div class="col-xs-12">
                    <input id="password" Placeholder ="Password"  type="password" class="form-control" name="password" required> @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-12">
                    <div class="checkbox checkbox-custom">
                        <input  id="checkbox-signup" type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}>
                        <label for="checkbox-signup">
                            Remember me 
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group text-center row m-t-10">
                <div class="col-xs-12">
                    <button class="btn btn-success-outline btn-block waves-effect waves-light" type="submit">Log In</button>
                </div>
            </div>
            <div class="form-group row m-t-30 m-b-0">
                <div class="col-sm-12">
                    <a href="{{ url('/forget_password') }}" class="text-muted">
                        <i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                </div>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
@endsection

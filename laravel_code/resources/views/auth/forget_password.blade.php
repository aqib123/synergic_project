@extends('layouts.index') @section('content')
    <div class="text-xs-center ">
        <div class="logo">
        <span> <img src="assets/images/logo_login.png" alt="logo" width="60%"></span>
        </div>
    </div>
    <div class=" login_form">
        <div class="row">
            <div class="col-xs-12 text-xs-center"> 
<h5 class="text-muted text-uppercase m-b-0 m-t-0"><b>Forget Password</b>  </h5>
            </div>
        </div>
        @if(\Session::has('error_msg'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('error_msg')}}</p>
            </div>
        @endif
        @if(\Session::has('status'))
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <p>{{ \Session::get('status')}}</p>
            </div>
        @endif
        <form class=" m-t-20" method="POST" action="{{URL::to('/forget/password') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} row">
                <div class="col-xs-12">
                    <input id="email" type="email" Placeholder ="Email" class="form-control" name="email" value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group text-center row m-t-10">
                <div class="col-xs-12">
                    <button class="btn btn-success-outline btn-block waves-effect waves-light" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
@endsection
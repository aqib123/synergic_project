@extends('layouts.index') @section('content')
    <div class="text-xs-center m-t-20">
        <a href="#" class="logo">
        <span> <img src="{{ url('assets/images/logo_login.png') }}" alt="logo" width="60%"></span>
        </a>
    </div>
        @include('flash_message')
        @if(\Session::has('error_msg'))
            <div class="alert alert-danger">
                <p>{{ \Session::get('error_msg')}}</p>
            </div>
        @endif
    <div class=" login_form">
        <div class="row">
            <div class="col-xs-12 text-xs-center">
                <h6 class="text-muted text-uppercase m-b-0 m-t-0">Enter New Password</h6>
            </div>
        </div>
        <form class=" m-t-20" method="POST" action="{{URL::to('/new/password/implementation') }}">
            {{ csrf_field() }}
            <input type="hidden" name="email" value="{{$email}}">
            <div class="form-group row">
                <div class="col-xs-12">
                    <input id="password" type="password" Placeholder ="Password" class="form-control" name="password" value="{{ old('email') }}" required autofocus> 
                </div>
            </div>
            <div class="form-group row">
                <div class="col-xs-12">
                    <input type="password" class="form-control" placeholder="Repeat password" name="password_confirmation">
                </div>
            </div>
            <div class="form-group text-center row m-t-10">
                <div class="col-xs-12">
                    <button class="btn btn-success-outline btn-block waves-effect waves-light" type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
    <div class="clearfix"></div>
@endsection
@extends('layouts.index')
@section('content')
@section('css')
 <!-- Sweet Alert css -->
 <link href="{{ URL::to('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
body {
    /* overflow:hidden; */
    padding-bottom: 10px;
}
.account-pages{
    background-position: center top;
    background-size: cover;
}
.image-window {
  cursor: pointer;
  height: 70px;
width: 180px;
  margin: 0 auto;
  overflow: hidden;
  /* border-radius: 10px; */
  transition: height 0.3s ease-in-out;
}

.image-window img {
  position: relative;
  top: 50%;
  transform: translateY(-50%);
}

.image-window.opened {
  height: 600px;
}

.refbox{
    padding: 7px 0px;
    border: 1px solid #CCCCCC;
    border-radius:5px;

}
</style>
@endsection
<div class="text-xs-center m-t-0">
    <div class="logo">
        <span> <img src="{{ URL::to('assets/images/logo_login.png') }}" alt="logo" width="45%"></span>
    </div>
</div>
<div class="logup_form">
    @include('flash_message')
    @if(isset($result))
        <div class="row">
            <div class="refrl_oam refbox">
            <div class="col-md-6">

                <div class="col-md-12">
                     <!-- <p class="text-white font-13 text-uppercase m-b-0 m-t-0"><b>Referral By</b>  </p> -->
                    <p class="text-muted font-13 m-t-0 m-b-0"> <strong class="text-white" >Company:</strong>
                  {!! $result['companies']['name'] !!}</p>
              </div>
              <div class="col-md-12">
                   <p class="text-muted font-13  m-t-0 m-b-0">
                       <strong class="text-white">Address:</strong>
                       {!! $result['companies']['office_address'] !!} </p>
              </div>
            </div>
            <div class="col-md-6">
                <div class="image-window">
                <a href="#">
                    <span> <img class="pull-md-right  " src="{{asset('laravel_code/storage/app/public/user_image/'.$result['image'])}}" alt="logo" width="40%"></span>
                </a>
            </div>
        </div>

            <div class="clearfix">
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-xs-12 text-xs-center">
<h5 class="text-muted text-uppercase m-b-0 m-t-10"><b>Register Now</b>  </h5>
        </div>
    </div>
    <form class="form-horizontal m-t-10" method="POST" action="{{ url('user_registration') }}" id="user_registration">
        {{ csrf_field() }}
        <div class="row">
            <label class="col-sm-4">Company Type</label>
            <div class="col-sm-8">
                @if(isset($result))
                <div  class="radio c-input radio-info">
                    <input name="role_id" id="role_id_fmc" value="3" checked type="radio" class="company_type">
                    <label for="role_id_fmc">
                        FMC
                    </label>
                </div>
                @else
                <div class="radio c-input radio-info ">
                    <input name="role_id" id="role_id_oam" checked value="2" type="radio" class="company_type">
                    <label for="role_id_oam">
                        OAM
                    </label>
                </div>
                <div  class="radio c-input radio-info">
                    <input name="role_id" id="role_id_fmc" value="3" checked type="radio" class="company_type">
                    <label for="role_id_fmc">
                        FMC
                    </label>
                </div>
                @endif
            </div>
        </div>
        <!--<div id="fmc_div" class="form-group row"  style="display:none">
            <div class="col-md-12">
                <select class="select2 form-control select2-multiple" style="width:100%" name="job_categories[]" multiple="multiple" data-placeholder="Job Categories">
                    @foreach(\App\JobCategory::all() as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach
                </select>
            </div>
        </div>-->
        <div class="form-group row {{ $errors->has('user_name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input type="text" placeholder="User Name" class="form-control" name="user_name" value="{{ old('user_name') }}" required> @if ($errors->has('user_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('user_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('email') ? ' has-error' : '' }}">
            <div class="col-md-12">
                 @if(isset($result))
                    <input type="hidden" name="reference_url" value="{{$url}}">
                 @else
                    <input type="hidden" name="reference_url">
                 @endif
                <input type="email" placeholder="Email" class="form-control" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
            <div class="col-md-12 input-group">
                <span class="input-group-addon">971</span>
                <input type="text" placeholder="Contact Number" class="form-control" name="contact_no" value="{{ old('contact_no') }}" required> @if ($errors->has('number'))
                <span class="help-block">
                    <strong>{{ $errors->first('number') }}</strong>
                </span>
                @endif
            </div>
        </div>
         <div class="form-group row {{ $errors->has('first_name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input type="text" placeholder="First Name" class="form-control" name="first_name" value="{{ old('first_name') }}" required> @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
         <div class="form-group row {{ $errors->has('last_name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{ old('last_name') }}" required> @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('designation') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input type="text" placeholder="Designation" class="form-control" name="designation" value="{{ old('designation') }}" required> @if ($errors->has('designation'))
                <span class="help-block">
                    <strong>{{ $errors->first('designation') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('name') ? ' has-error' : '' }}">
            <div class="col-md-12">
                <input type="text" placeholder="Company Name" class="form-control" name="name" value="{{ old('name') }}" required> @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row {{ $errors->has('password') ? ' has-error' : '' }}">
            <div class="col-md-12">
            <div class="effects">
                <div id="myPassword"></div>
            </div>
                <!-- <input placeholder="Password" type="password" class="form-control" name="password" required>  -->
                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row text-center m-t-10">
            <div class="col-xs-12">
                <button class="btn btn-success-outline btn-block waves-effect waves-light" id="" type="submit">Register</button>
            </div>
        </div>
    </form>
</div>
@section('js')
    <!-- Sweet Alert js -->
    <script src="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <!-- <script src="{{ URL::asset('assets/pages/jquery.sweet-alert.init.js') }}"></script> -->
<script>

</script>
@endsection
@endsection

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- App Favicon -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

    <!-- App title -->
    <title>Short Description</title>
     <!-- App CSS -->
    <link href="{{ URL::to('assets/css/style.css?ver=4.8.0') }}" rel="stylesheet" type="text/css" />
     <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
     <link href="https://www.synergic360.com/dev/synergic360_api/remodal.css" rel="stylesheet" type="text/css">

<link href="https://www.synergic360.com/dev/synergic360_api/remodal-default-theme.css" rel="stylesheet" type="text/css">
</head>
<body>    
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="https://www.synergic360.com/" type="button" class="btn btn-dark-outline  waves-effect waves-light">Go Back</a>
        </div>
        <h4 class="page-title">Short Description</h4>
    </div>
</div>
<div class="row">
    <div class="col-xl-9">
        <div class="card page_box_shadow">
            <div class="card-header sidebar_header">

                <div class="pull-left">
                    <h4 class="header-title job_listing_heading m-b-0">Job Detail</h4>
                </div>
                <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-dark-outline waves-effect waves-light right apply_now_button">Apply</button>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="card-block description-font page_box_shadow">
                <div class="col-lg-9">
                    <p class="job_detail_description lead m-b-0 font-16 ">{{isset($detail->job_title)?$detail->job_title:''}}
                        <span class="text-muted  font-13">{!! date_format(new DateTime($detail->created_at), 'jS F Y g:ia') !!}</span>
                    </p>
                    <p class=" m-b-0 text-justify">{!! strip_tags(isset($detail->description)?$detail->description:'' )!!}</p>
                    <div class="m-t-10">

                        <p class="lead m-b-0 font-13 ">
                            <strong>Categories:</strong>
                            <span class="label label-success">
                                @if(isset($job_category_name) && count($job_category_name) > 0) @foreach($job_category_name as $cat_name) {{$cat_name->name}}&nbsp;&nbsp;
                                @endforeach @endif
                            </span>
                        </p>
                        <p class="lead m-b-0 font-13 ">
                            <strong>Sub Categories:</strong>
                            <span class="label label-success">
                                @if(isset($job_sub_category) && count($job_sub_category) > 0) @foreach($job_sub_category as $sub_cat_name) {{$sub_cat_name->sub_category}}&nbsp;&nbsp;
                                @endforeach @endif
                            </span>
                        </p>
                        <p class="lead m-b-0 font-13 ">
                            <strong>Service Name:</strong>
                            <span class="label label-success">@if(isset($job_category) && count($job_category) > 0) @foreach($job_category as $cat_name) {{$cat_name['job_category']->service_name}}&nbsp;&nbsp;
                                @endforeach @endif
                            </span>
                        </p>
                    </div>
                    <div class="m-t-10">
                        <p class="lead m-b-0 font-13 ">
                            <strong>Job Type: </strong>
                            <span class="font-13 m-b-0 text-muted">{{isset($detail->job_type)?$detail->job_type:''}}</span>
                        </p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!--Popup Modal-->

<div class="remodal" data-remodal-id="delete_modal" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h4>Alert Information</h4>
    <p class="remodal-content">
    </p>
    <div class="login_button_status" style="display:none;">
        <br>
        <a href="https://www.synergic360.com/dev/login?popup_login=1&job_id= <?= $detail->id ?>" class="remodal-cancel">Login</a>
    </div>
</div>

<script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
<script src="https://www.synergic360.com/dev/synergic360_api/remodal.js"></script>
<script>
$(document).ready(function(){
$(".apply_now_button").click(function(){

    var job_id = '{{$detail->id}}';
        $.ajax({
            type: 'GET',
            url: "{{ generate_url('/check_user') }}",
            data: {'id' : job_id},
            success: function(data){
                console.log(data);
                if(data.status == 2){
                    console.log(data.url);
                    $('.remodal-content').html('Specific Job is not Linked with your account');
                    var inst = $('[data-remodal-id=delete_modal]').remodal();
                    inst.open();
                }else if(data.status == 1){
                    window.location.replace(data.url);
                    //window.location.href = data.url;
                }else if(data.status === 0){
                    var inst = $('[data-remodal-id=delete_modal]').remodal();
                    $('.login_button_status').show();
                    inst.open();
                    $('.remodal-content').html('Please Login before checking job.');
                }
            }
        });
    }); 
});
</script>


</body>
</html>
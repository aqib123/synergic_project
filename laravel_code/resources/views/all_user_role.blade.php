@extends('layouts.app') @section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <!-- <div class="btn-group pull-right m-t-15">
            <a href="{{url('user_role')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add User Role</a>
        </div> -->
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Created At</th>
                        <!-- <th>Action</th> -->
                    </tr>
                </thead>
                <tbody>
                    @if(isset($all_roles) && count($all_roles)>0) 
                        @foreach($all_roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td>
@if($role->id == 1 )
{{ucwords($role->name)}}
@else
<a class="custom_links" href="{{url('users?status='.$role->name)}}">{{ucwords($role->name)}}</a>
@endif
</td>
                                <td>{{$role->description}}</td>
                                <td><?php echo date_format(new DateTime($role->created_at), 'jS F Y g:ia');?></td>                                
                                <!-- <td>
                                <div class="btn-group user_action_btn">
                                    <a class="custm_btn" data-toggle="dropdown" aria-expanded="true"><i class="zmdi zmdi-more"></i></a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item"  href="{{url('user_role?id='.$role->id)}}" title="Edit">
                                            <i class="fa fa-edit text-info"></i> Edit
                                        </a>
                                        <a class="dropdown-item"  href="{{url('delete_role/'.$role->id)}}" title="Delete">
                                            <i class="fa fa-trash text-danger"></i> Delete
                                        </a>
                                    </div>
                                </div>

                                    
                                </td> -->
                            </tr>
                        @endforeach 
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end row -->
@endsection
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Favicon -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

    <!-- App title -->
    <title>{{isset($title)?$title:''}}</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/plugins/morris/morris.css') }}">

    <!-- Switchery css -->
    <link href="{{ URL::asset('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
    <!-- Muliselect -->
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') }}"> -->
    <!-- Jquery filer css -->
    <link href="{{ URL::asset('assets/plugins/jquery.filer/css/jquery.filer.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />
    <!-- DataTables -->
    <link href="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('assets/plugins/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{ URL::asset('assets/plugins/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Custom box css -->
    <link href="{{ URL::asset('assets/plugins/custombox/css/custombox.min.css') }}" rel="stylesheet">
    <!-- App CSS -->
    <link href="{{ URL::to('assets/css/style.css?ver=4.8.0') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/custom.css?ver=5.8.0') }}" rel="stylesheet" type="text/css" />
    <style>
     .link_no_href{
      cursor: pointer;
     }

    </style>
    <!-- Sweet Alert css -->
    <link href="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css"/>
    @yield('css')
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    <!-- Modernizr js -->
    <script src="{{ URL::asset('assets/js/modernizr.min.js') }}"></script>

</head>


<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="{{url('/home')}}" class="logo">
                        <span>
                            <img src="{{ url('assets/images/logo_login.png') }}" alt="logo" width="60%">
                        </span>
                    </a>
                </div>
                <!-- End Logo container-->


                <div class="menu-extras">

                    <ul class="nav navbar-nav pull-right">

                        <li class="nav-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>


                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" id="admin_bell_icon" data-toggle="dropdown" href="#" role="button" aria-haspopup="false"
                                aria-expanded="false">
                                <i class="zmdi zmdi-notifications-none noti-icon"></i>
                                <span id="notification_badge"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5>

                                        <small class="notification_counter">

                                            <span class="label label-default pull-xs-right"></span>
                                            Notification
                                        </small>
                                        <div class="clearfix"></div>
                                    </h5>
                                    <div class="clearfix"></div>
                                </div>
                                <img src="{{ URL::to('assets/images/loading.gif') }}" style="height: 40px !important; margin-left: 116px;" style="display: none;" class="loading_gif_class">
                                <div class="notification_data">
                                </div>

                                <!-- All-->
                                <a href="{{url('admin_all_notifications')}}" class="dropdown-item notify-item notify-all">
                                    View All
                                </a>

                            </div>
                        </li>

                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link imag_link user_image_detail dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                @if(isset(Auth::user()->image))
                                <div class="user_image_padding">
                                    <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/'.Auth::user()->image) }}" alt="{{ Auth::user()->image }}"
                                    />
                                </div>
                                @else
                                <div class="user_image_padding">
                                    <img class="img_size" src="{{ asset('laravel_code/storage/app/public/user_image/default_user.png') }}" alt="{{ Auth::user()->image }}"
                                    />
                                </div>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow">
                                        <small>Welcome !
                                            @if(isset(Auth::user()->name))
                                                {{Auth::user()->name}}
                                            @endif
                                        </small>
                                    </h5>
<div class="clearfix"></div>
                                </div>

                                <!-- item-->
                                <a href="{{url('user_profile')}}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-account-circle"></i>
                                    <span>Profile</span>
                                </a>

                                <!-- item-->
                                <a href="{{ url('/settings') }}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-settings"></i>
                                    <span>Settings</span>
                                </a>
 <!-- item-->
                                <a href="{{ url('/newsletter') }}" class="dropdown-item notify-item">
                                    <i class="fa fa-bullhorn"></i>
                                    <span>E-Marketing</span>
                                </a>
                                <!-- item-->
                                <a href="{{url('logout')}}" class="dropdown-item notify-item">
                                    <i class="zmdi zmdi-power"></i>
                                    <span class="logout">Logout</span>
                                </a>

                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end menu-extras -->
                <div class="clearfix"></div>
            </div>
            <!-- end container -->
        </div>
        <!-- end topbar-main -->
        <div class="navbar-custom">
            <div class="container">
                <div id="navigation">
                    <!-- Navigation Menu-->
                    <ul class="navigation-menu">
                        <li>
                            <a href="{{ url('/home') }}">
                                <span> Home </span>
                            </a>
                        </li>
                        <li class="has-submenu">
                            <a href="{{url('/jobs')}}">
                                <span> Jobs </span>
                            </a>
                        </li>
                        @if(GeneralFunctions::check_view_permission('/users'))
                        <li class="has-submenu">
                            <a href="{{ url('/users?status=OAM') }}">
                                <span> Companies</span>
                            </a>
                          <!--  <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/all_user_role')) @endif -->
                                        @if(GeneralFunctions::check_view_permission('/users'))
                                            <!-- <li>
                                                <a href="{{ url('/user_form') }}">Add New Company</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/users?status=OAM') }}">Companies</a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>-->
                        </li>
                        @endif
                      <!--  @if(GeneralFunctions::check_view_permission('/all_user_role'))
                        <li class="has-submenu">
                            <a href="{{ url('/all_user_role') }}">
                                <span> Company Roles</span>
                            </a>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/all_user_role'))
                                             <li>
                                                <a href="{{ url('/user_role') }}">Add Company Role</a>
                                            </li>
                                            <li>
                                                <a href="{{ url('/all_user_role') }}">Company Roles</a>
                                            </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @endif-->
                        @if(GeneralFunctions::check_view_permission('/categories'))
                        <li class="has-submenu">
                            <a href="{{ url('/categories') }}">
                                <span> Job Categories </span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/jobcat_form'))
                                       <li>
                                           <a href="{{ url('/jobcat_form') }}">Add Category</a>
                                       </li>
                                       @endif
                                        @if(GeneralFunctions::check_view_permission('/categories'))
                                        <li>
                                            <a href="{{ url('/categories') }}">Categories</a>
                                        </li>
                                         @endif
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        @if(GeneralFunctions::check_view_permission('/all_assets'))
                         <li class="has-submenu">
                            <a href="{{ url('/all_assets') }}">
                                <span> Assets </span>
                            </a>
                            <!-- <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        @if(GeneralFunctions::check_view_permission('/assets_form'))
                                        <li>
                                            <a href="{{ url('/assets_form') }}">Add Assets</a>
                                        </li>
                                        @endif
                                        @if(GeneralFunctions::check_view_permission('/all_assets'))
                                        <li>
                                            <a href="{{ url('/all_assets') }}">Assets</a>
                                        </li>
                                        @endif
                                    </ul>
                                </li>
                            </ul> -->
                        </li>
                        @endif
                        @if(GeneralFunctions::check_view_permission('/admin_job_list'))
                        <li class="has-submenu">
                            <a href="{{url('/admin_job_list')}}">
                                <span> Job Listing </span>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->parent_id == 0)
                        <li class="has-submenu">
                            <a class="link_no_href">
                                <span> Staff Management </span>
                            </a>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <!-- <li>
                                            <a href="{{ url('staff/form') }}">Add Staff Member</a>
                                        </li> -->
                                        <li>
                                            <a href="{{ url('/staff/member/list') }}">Staff List</a>
                                        </li>
                                        <!-- <li>
                                            <a href="{{ url('staff/roles') }}">Add Roles</a>
                                        </li> -->
                                        <li>
                                            <a href="{{ url('roles/list') }}">Roles List</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @endif
                        @if(Auth::user()->parent_id == 0)
                        <li class="has-submenu">
                            <a class="link_no_href">
                                <span> Notification Settings </span>
                            </a>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li>
                                            <a href="{{ url('/sms/service/settings') }}">Sms Service Settings</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/emails/list') }}">Email Service Settings</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        @endif
                        <li class="has-submenu">
                            <a class="link_no_href">
                                <span> More... </span>
                            </a>
                            <ul class="submenu megamenu">
                                <li>
                                    <ul>
                                        <li>
                                            <a href="{{ url('ads') }}">Ads</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('subscription/packages') }}">Subscription Packages</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/type/insurance/list') }}">Type of Insurance</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                </div>
            </div>
        </div>
    </header>
    <!-- End Navigation Bar-->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="wrapper">
        <div class="container">

            @yield('content')
            <!-- Footer -->
            <footer class="footer text-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            2018 © Synergic.
                        </div>
                    </div>
                </div>
            </footer>

            <!-- End Footer -->
        </div>
        <!-- container -->
    </div>
    <!-- End wrapper -->

    <!-- jQuery  -->
    <script src="{{ URL::asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/tether.min.js') }}"></script>

    <!-- Tether for Bootstrap -->
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/waves.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/switchery/switchery.min.js') }}"></script>
    <!-- Counter Up  -->
    <script src="{{ URL::asset('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ URL::asset('assets/js/jquery.core.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.app.js') }}"></script>

    <!-- Page specific js -->
    <script src="{{ URL::asset('assets/pages/jquery.dashboard.js') }}"></script>
    <!-- Jquery filer js -->
    <script src="{{ URL::asset('assets/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>

    <!-- page specific js -->
    <script src="{{ URL::asset('assets/pages/jquery.fileuploads.init.js') }}"></script>
    <!-- Required datatable js -->
    <script src="{{ URL::asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ URL::asset('assets/plugins/multiselect/js/jquery.multi-select.js') }} "></script>
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.full.min.js') }} " type="text/javascript"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('assets/pages/jquery.formadvanced.init.js') }}"></script> -->
   <!-- Modal-Effect -->
   <script src="{{ URL::asset('assets/plugins/custombox/js/custombox.min.js') }}"></script>
    <script src="{{ URL::asset('assets/plugins/custombox/js/legacy.min.js') }}"></script>
    <!-- Sweet Alert js -->
    <script src="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>
    <script src="{{ URL::asset('assets/pages/jquery.sweet-alert.init.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/plugins/jquery-play-sound/jquery.playSound.js') }}"></script>
    <script>
        $(document).ready(function(){
 $(".navigation-menu a").each(function () {
                if (this.href == window.location.href) {
                    $(this).parent().addClass("active"); // add active to li of the current link
                    $(this).parent().parent().parent().addClass("active"); // add active class to an anchor
                    $(this).parent().parent().parent().parent().parent().addClass("active"); // add active class to an anchor
                }
            });
            $('#datatable').DataTable();
            var resizefunc = [];
            $(document).on('click', '.logout', function(){
                var url = "{{ URL::to('/logout') }}";
                location.replace(url);
            });

            $('#admin_bell_icon').on('click', function(){
                $('.notification_data').html('');
                $('.notification_counter').html('');
                $('.loading_gif_class').show();
            $.ajax({
                type: 'GET',
                url: "{{ generate_url('get_admin_notification') }}",
                success:function(data){
                    console.log(data);
                    setTimeout(function(){
                        $('.loading_gif_class').hide();
                        if(data.counter > 0){
                            $('.notification_counter').html('<span class="label label-danger pull-xs-right">'+data.counter+'</span>');
                        }
                        $('.notification_data').html(data.html);
                         $('#notification_badge').removeClass('noti-icon-badge');
                    }, 200);
                }
            });
        });

        setInterval(function(){
            $.ajax({
                type: 'GET',
                url: '{{generate_url("get_admin_instant_notification")}}',
                success: function(response) {
                    $('#sa-close').modal('show');
                    if(response.flag == 1){
                        swal({
                            title: response.title,
                            text: response.description,
                            timer: 4000,
                            showConfirmButton: false
                        });
                        $.playSound("{{asset('assets/plugins/jquery-play-sound/sound/notify')}}");
                        $('#notification_badge').addClass('noti-icon-badge');
                    }
                }
            });
        }, 5000);
    });
    </script>
    @yield('js')
</body>

</html>

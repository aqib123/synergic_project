@extends('oam.app') @section('content')

<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('buildings')}}" type="button" class="btn btn-custom waves-effect waves-light">Back to Buildings</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12">
        <div class="card-box">
            <div class="p-20">
                @if(count($errors) > 0)
                <div class="alert alert-danger alert-list">
                    <div class="alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                    </div>
                    <ul style="text-decoration: none;">
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif @if(\Session::has('status'))
                <div class="alert alert-success">
                    <p>{{ \Session::get('status')}}</p>
                </div>
                @endif
                <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('save_building') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{Request::input('id')}}">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Name
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" required parsley-type="text" placeholder="Name" name="name" value="{{old('name')}}{{isset($building->name)?$building->name:''}}"
                                class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Location
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input type="location" required parsley-type="location" name="location" value="{{old('location')}}{{isset($building->location)?$building->location:''}}"
                                class="form-control" placeholder="Location">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="hori-pass1" class="col-sm-2 form-control-label">Area
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <input type="text" required parsley-type="area" name="area" class="form-control" placeholder="Area" value="{{old('area')}}{{isset($building->area)?$building->area:''}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Assets
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-8">
                            <fieldset class="">
                                <select class="select2 form-control select2-multiple" style="width:100%" name="building_assets[]" multiple="multiple" data-placeholder="Building Assets">
                                    <option value="Cleaning of common/shared areas">Cleaning of common/shared areas</option>
                                    <option value="Lift lobbies">Lift lobbies</option>
                                    <option value="Plant rooms">Plant rooms</option>
                                    <option value="Reception & Entrance area">Reception & Entrance area</option>
                                    <option value="Service Areas & Plant rooms">Service Areas & Plant rooms</option>
                                    <option value="Children’s Play Area & Owner’s lounge">Children’s Play Area & Owner’s lounge</option>
                                    <option value="Swimming Pool & Shower area">Swimming Pool & Shower area</option>
                                    <option value="Fitness center">Fitness center</option>
                                    <option value="Common area toilet & rooms">Common area toilet & rooms</option>
                                    <option value="Elevators">Elevators</option>
                                    <option value="Car Parking in the basements">Car Parking in the basements</option>
                                    <option value="Garbage Chute">Garbage Chute</option>
                                    <option value="External glass & Façade cleaning">External glass & Façade cleaning</option>
                                </select>
                            </fieldset>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-sm-8 col-sm-offset-2">
            <button type="submit" class="btn btn-success waves-effect waves-light">
                Save
            </button>
            <button type="reset" class="btn btn-danger waves-effect m-l-5">
                Cancel
            </button>
        </div>
        </form>
    </div>
</div>
@endsection
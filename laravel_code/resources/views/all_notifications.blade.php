@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('home')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Home</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>

<div class="row">
        <div class="col-sm-8 col-lg-8 col-xs-12 m-t-15">
            @include('flash_message')
            <div class="card">
                <div class="card-header font-16 ">
                    <b>Notification List</b>
                </div>
                @if(isset($notification) && count($notification)>0)
                @foreach($notification as $noti)
                <div class="card-block notifications_icon tilebox-three">
                    <div class="col-md-10">
                        <a href="{{url($noti->url)}}">
                            <h6 class="text-dark text-uppercase m-b-0 m-t-0">{{$noti->title}}</h6>
                        </a>
                        <p class="m-b-0 card-text">
                            <small class="text-muted"><span class="label label-primary"><?= date_format(new DateTime($noti->created_at), 'jS F Y g:ia');?></span></small>
                        </p>
                    </div>
                    <div class="col-md-2" >
                        <!-- <div class="pull-right notification_status">
                            <p class="m-b-10 card-text text-xs-right">
                                <small class="text-muted"><a href="{{url('admin_delete_notification/'.$noti->id)}}" type="button" class=" delete_btn_custom waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete this notification"><i class="fa fa-trash"></i></a></small>
                            </p>
                        </div> -->
                    </div>
                    <div class="col-md-12" >
                        <div class="text-xs-left" style="display: inline-block;">
                            <p class="m-b-10 card-text">
                                <small class="text-muted">{{$noti->description}}</small>
                            </p>
                        </div>
                        
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                @endforeach
                @endif
            </div>
            
        </div>
    </div>
<!-- end row -->
    
@endsection
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Favicon -->
    <!-- <link rel="shortcut icon" href="assets/images/favicon.ico"> -->

    <!-- App title -->
    <title>{{isset($title)?$title:''}} - FMC</title>

    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="{{ URL::to('assets/plugins/morris/morris.css') }}">

    <!-- Switchery css -->
    <link href="{{ URL::to('assets/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />
    <!-- Muliselect -->
    <link href="{{ URL::asset('assets/plugins/select2/css/select2.min.css') }} " rel="stylesheet" type="text/css"/>

    <!-- Jquery filer css -->
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/jquery.filer.css') }}" rel="stylesheet" />
    <link href="{{ URL::to('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css') }}" rel="stylesheet" />

    <!-- App CSS -->
    <link href="{{ URL::to('assets/css/style.css?ver=4.8.0') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('assets/css/custom.css?ver=4.70') }}" rel="stylesheet" type="text/css" />
    
    <!-- Sweet Alert css -->
    <link href="{{ URL::asset('assets/plugins/bootstrap-sweetalert/sweet-alert.css') }}" rel="stylesheet" type="text/css"/>

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    <!-- Modernizr js -->
    <script src="{{ URL::to('assets/js/modernizr.min.js') }}"></script>

</head>


<body>

    <!-- Navigation Bar-->
    <header id="topnav">
        <div class="topbar-main">
            <div class="container">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="{{url('/fmc/home')}}" class="logo">
                        <span>
                            <img src="{{ URL::to('assets/images/logo_login.png') }}" alt="logo" width="60%">
                        </span>
                    </a>
                </div>
                <!-- End Logo container-->


                <div class="menu-extras">

                    <ul class="nav navbar-nav pull-right">

                        <li class="nav-item">
                            <!-- Mobile menu toggle-->
                            <a class="navbar-toggle">
                                <div class="lines">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </a>
                            <!-- End mobile menu toggle-->
                        </li>
                        <li class="nav-item dropdown notification-list">
                            <a class="nav-link imag_link user_image_detail dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                @if(isset($image))
                                <div class="user_image_padding"> 
                                    <img class=" img_size" src="{{ asset('laravel_code/storage/app/public/user_image/'.$image) }}" alt="{{ $image }}"
                                    /> 
                                </div>
                                @else
                                @php
                                $words = explode(" ", $name);
                                $acronym = "";
                                $i = 0;
                                foreach ($words as $w) {
                                    if($i < 2){ 
                                        $acronym .= $w[0]; 
                                    }
                                    $i++;
                                }
                                @endphp
                                    <span class="profile_img_top">{{$acronym}}</span>
                                @endif
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow">
                                        <small>Welcome ! 
                                            @if(isset($name))
                                                {{$name}}
                                            @endif
                                        </small>
                                    </h5>
                                </div>

                                <!-- item-->
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- end menu-extras -->
                <div class="clearfix"></div>
            </div>
            <!-- end container -->
        </div>
    </header>
    <!-- End Navigation Bar-->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="wrapper">
        <div class="container">

            @yield('content')

            <!-- Footer -->
            <footer class="footer text-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            2018 © Synergic.
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->
        </div>
        <!-- container -->
        <!-- Right Sidebar -->
        <div class="side-bar right-bar">
            <div class="nicescroll">
                <ul class="nav nav-tabs text-xs-center">
                    <li class="nav-item">
                        <a href="#home-2" class="nav-link active" data-toggle="tab" aria-expanded="false">
                            Activity
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#messages-2" class="nav-link" data-toggle="tab" aria-expanded="true">
                            Settings
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="home-2">
                        <div class="timeline-2">
                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">5 minutes ago</small>
                                    <p>
                                        <strong>
                                            <a href="#" class="text-info">John Doe</a>
                                        </strong> Uploaded a photo
                                        <strong>"DSC000586.jpg"</strong>
                                    </p>
                                </div>
                            </div>

                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">30 minutes ago</small>
                                    <p>
                                        <a href="" class="text-info">Lorem</a> commented your post.</p>
                                    <p>
                                        <em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus
                                            ut tincidunt euismod. "</em>
                                    </p>
                                </div>
                            </div>

                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">59 minutes ago</small>
                                    <p>
                                        <a href="" class="text-info">Jessi</a> attended a meeting with
                                        <a href="#" class="text-success">John Doe</a>.</p>
                                    <p>
                                        <em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus
                                            ut tincidunt euismod. "</em>
                                    </p>
                                </div>
                            </div>

                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">1 hour ago</small>
                                    <p>
                                        <strong>
                                            <a href="#" class="text-info">John Doe</a>
                                        </strong>Uploaded 2 new photos</p>
                                </div>
                            </div>

                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">3 hours ago</small>
                                    <p>
                                        <a href="" class="text-info">Lorem</a> commented your post.</p>
                                    <p>
                                        <em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus
                                            ut tincidunt euismod. "</em>
                                    </p>
                                </div>
                            </div>

                            <div class="time-item">
                                <div class="item-info">
                                    <small class="text-muted">5 hours ago</small>
                                    <p>
                                        <a href="" class="text-info">Jessi</a> attended a meeting with
                                        <a href="#" class="text-success">John Doe</a>.</p>
                                    <p>
                                        <em>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus
                                            ut tincidunt euismod. "</em>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="messages-2">

                        <div class="row m-t-20">
                            <div class="col-xs-8">
                                <h5 class="m-0">Notifications</h5>
                                <p class="text-muted m-b-0">
                                    <small>Do you need them?</small>
                                </p>
                            </div>
                            <div class="col-xs-4 text-right">
                                <input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                            </div>
                        </div>

                        <div class="row m-t-20">
                            <div class="col-xs-8">
                                <h5 class="m-0">API Access</h5>
                                <p class="m-b-0 text-muted">
                                    <small>Enable/Disable access</small>
                                </p>
                            </div>
                            <div class="col-xs-4 text-right">
                                <input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                            </div>
                        </div>

                        <div class="row m-t-20">
                            <div class="col-xs-8">
                                <h5 class="m-0">Auto Updates</h5>
                                <p class="m-b-0 text-muted">
                                    <small>Keep up to date</small>
                                </p>
                            </div>
                            <div class="col-xs-4 text-right">
                                <input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                            </div>
                        </div>

                        <div class="row m-t-20">
                            <div class="col-xs-8">
                                <h5 class="m-0">Online Status</h5>
                                <p class="m-b-0 text-muted">
                                    <small>Show your status to all</small>
                                </p>
                            </div>
                            <div class="col-xs-4 text-right">
                                <input type="checkbox" checked data-plugin="switchery" data-color="#64b0f2" data-size="small" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end nicescroll -->
        </div>
        <!-- /Right-bar -->



    </div>
    <!-- End wrapper -->
    <!-- jQuery  -->
    <script src="{{ URL::to('assets/js/jquery.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/tether.min.js') }}"></script>
    <!-- Tether for Bootstrap -->
    <script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::to('assets/js/waves.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.nicescroll.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/switchery/switchery.min.js') }}"></script>

    <!--Morris Chart-->
    <script src="{{ URL::to('assets/plugins/morris/morris.min.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/raphael/raphael-min.js') }}"></script>

    <!-- Counter Up  -->
    <script src="{{ URL::to('assets/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ URL::to('assets/plugins/counterup/jquery.counterup.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ URL::to('assets/js/jquery.core.js') }}"></script>
    <script src="{{ URL::to('assets/js/jquery.app.js') }}"></script>
    <!-- Multiselect js -->    
    <script type="text/javascript" src="{{ URL::asset('assets/plugins/multiselect/js/jquery.multi-select.js') }} "></script>
    <script src="{{ URL::asset('assets/plugins/select2/js/select2.full.min.js') }} " type="text/javascript"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/pages/jquery.formadvanced.init.js') }}"></script>

    <!-- Page specific js -->
    <script src="{{ URL::to('assets/pages/jquery.dashboard.js') }}"></script>
    <!-- Jquery filer js -->
    <script src="{{ URL::to('assets/plugins/jquery.filer/js/jquery.filer.min.js') }}"></script>

    <!-- page specific js -->
    <script src="{{ URL::to('assets/pages/jquery.fileuploads.init.js') }}"></script>
    <script>
        $(document).ready(function(){
            var resizefunc = [];
            $(document).on('click', '.logout', function(){
                var url = "{{ URL::to('/logout') }}";
                location.replace(url);
            });

            $(document).on('click', '.show_detail_btn', function(){
                $.ajax({url: "{{url('/fmc/get/job/details')}}",
                data : {'data' : $(".job_id").val() },
                type : "GET",    
                success: function(result){
                    console.log(result.data);
                    $('#job_type').text(result.data.job_type);
                    $('#email_id').text(result.data.focal_email);
                    $('#contact_number').text(result.data.companies.contact_no);
                    $('#office_address').text(result.data.companies.office_address);
                    $('#show_details_result').show();
                }});
            });
        });

    </script>
    @yield('fmc_js')

</body>

</html>
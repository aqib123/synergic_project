@extends('general.app') @section('content')
<style type="text/css">
	.form-control-label {
	    font-size: 19px !important;
	}
</style>
<div class="row">
    <div class="col-sm-12">
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
<div class="row">
    <form role="form" data-parsley-validate novalidate method="GET" enctype="multipart/form-data" action="#">
        {{ csrf_field() }}
        @if(count($errors) > 0)
        <div class="alert alert-danger">
            <ul style="text-decoration: none;">
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif @if(\Session::has('status'))
        <div class="col-sm-12">
            <div class="alert alert-success">
                <p>{{ \Session::get('status')}}</p>
            </div>
        </div>
        @endif @if(\Session::has('error_msg'))
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <p>{{ \Session::get('error_msg')}}</p>
            </div>
        </div>
        @endif
        <div class="col-sm-12 col-xs-12 col-md-6">
            <div class="card-box">
                <!-- <div class="col-md-2"></div> -->
                <div class="col-md-12">
                    <div class="p-10">
                        <h4 class="header-title m-t-0 m-b-15">Renew Your Account for Further Activation</h4>
                        <input type="hidden" name="id" value="{{isset(Auth::user()->id)?Auth::user()->id:''}}">
                        <div class="form-group row">
                            <div class="col-sm-6 padding-left-0 padding-right-0">
                                <label for="inputEmail3" class="col-sm-12 form-control-label">Name
                                </label>
                                <div class="col-sm-12">
                                    <p>{{$name}}</p>
                                </div>
                            </div>
                            <div class="col-sm-6 padding-left-0 padding-right-0">
                                <label for="inputEmail3" class="col-sm-12 form-control-label">Email
                                </label>
                                <div class="col-sm-12">
                                	<p>{{$email}}</p>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
                <!-- <div class="col-md-2"></div> -->
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-12 col-xs-12 col-md-12">
            <div class="form-group row">
                <div class="col-sm-8">
                	@if(\Session::has('status'))
                		<a href="{{ url('/') }}" class="btn btn-dark-outline waves-effect waves-light">Login</a>
                	@else
                		<a href="{{ url('account/subscription/payment/charges/'.$uuid) }}" class="btn btn-success  waves-effect waves-light">Payment</a>
                	@endif
                	
                </div>
            </div>
        </div>
</div>
</form>
@endsection
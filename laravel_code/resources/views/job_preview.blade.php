@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15">
            <a href="{{url('admin_job_list')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Listing</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')

<style>
.table td, th{
    padding: 5px !important;
}
</style>

<div class="">
    <div class="">
        <div class="">
            <div class="card-header aprovel_buttons">
            	<a href="#review-modal" class="btn btn-sm btn-warning waves-effect waves-light" data-animation="swell" data-plugin="custommodal"
                    data-overlaySpeed="100" data-overlayColor="#36404a">Review Job</a>
                <div class="pull-right">  
                    <a href="#custom-modal" class="btn btn-sm btn-danger waves-effect waves-light" data-animation="swell" data-plugin="custommodal"
                    data-overlaySpeed="100" data-overlayColor="#36404a">Reject</a>
                    <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('update_job_status/'.$detail->id.'/'.$detail->user_id) }}" style="display: inline-block;">
                        {{csrf_field()}}
                        <input type="hidden" name="admin_approval" value="1">
                        <button type="submit" class="btn btn-sm btn-success waves-effect waves-light">Approve</button>
                    </form>
                    
                    
                    <!--@if($detail->admin_approval==1)
                    <span class="label label-success">Job Approved</span>
                    @elseif($detail->admin_approval==2)
                    <span class="label label-danger">Job Rejected</span>
                    @endif-->
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="m-b-0 card-box">
                <div class="row job_post_preview">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                            <p class="alert alert-info lead m-t-0 m-b-0">Job Detail</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Job Title</th>
                                        <td>{{isset($detail->job_title)?$detail->job_title:''}}</td>
                                    </tr>
                                    <tr>
                                        <th>Purposal Validity</th>
                                        <td>{{isset($detail->purposal_validity)?$detail->purposal_validity:''}} Days</td>
                                    </tr>
                                    <tr>
                                        <th>Categories</th>
                                        @if(isset($job_category_name) && count($job_category_name) > 0)
                                      <td>@foreach($job_category_name as $cat_name)
                                        {{$cat_name->name}}&nbsp;&nbsp; 
                                        @endforeach</td>
                                        @endif
                                    </tr>

                                    <tr>
                                        <th>Sub Categories</th>
                                        @if(isset($job_sub_category) && count($job_sub_category) > 0)
                                        <td>@foreach($job_sub_category as $sub_cat_name)
                                        {{$sub_cat_name->sub_category}}&nbsp;&nbsp; 
                                        @endforeach</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Service Code</th>
                                        @if(isset($job_service_code) && count($job_service_code) > 0)
                                        <td>@foreach($job_service_code as $service_code_name)
                                        {{$service_code_name->service_code_name}}&nbsp;&nbsp; 
                                        @endforeach</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Service Name</th>
                                        @php //dd($job_category); @endphp
                                        @if(isset($job_category) && count($job_category) > 0)
                                        <td>@foreach($job_category as $cat_name)
                                        {{$cat_name['job_category']->service_name}}&nbsp;&nbsp; 
                                        @endforeach</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Job Type</th>
                                        <td>{{isset($detail->job_type)?$detail->job_type:''}}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="2">Job Description</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">@php if(isset($detail->description)){echo $detail->description;} else{ echo '' ; } @endphp</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-sm-6">
                                <p class="lead  alert alert-info m-t-0 m-b-0">Payment Terms</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Payment Via</th>
                                    </tr>
                                    <tr>
                                        <td>{{isset($detail->payment_terms)?$detail->payment_terms:''}}</td>
                                    </tr>
                                </table>
                                <p class="lead  alert alert-info m-t-0 m-b-0"></p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Payment Discription</th>
                                        <th>Payment Percentage</th>
                                    </tr>
                                    @php
                                        $payment_info = [];
                                        $percentage_info = []; 
                                        if($detail->payment_term){
                                            $payment_info = json_decode($detail->payment_term, true);
                                            $percentage_info = json_decode($detail->payment_percentages, true);
                                        } 
                                    @endphp
                                    @foreach($payment_info as $key => $value)
                                    <tr>
                                        <td>{{$value}}</td>
                                        <td>{{$percentage_info[$key]}} %</td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="alert alert-info  lead m-t-10 m-b-0">Building Detail</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Building</th>
                                        <td>{{isset($building->name)?$building->name:''}}</td>
                                    </tr>
                                    <tr>
                                        <th>Building Assets</th>
                                        <td> @if(isset($building->assets) && !empty($building->assets))
                                            @foreach(explode(',',$building->assets) as $b_asset)
                                                <span class="label  label-info">{{$b_asset}}</span>
                                            @endforeach
                                        @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Building Type</th>
                                        <td>{{isset($building->type)?$building->type:''}}</td>
                                    </tr>
                                </table>
                            </div>
                             <div class="col-sm-6">
                                <p class="lead alert alert-info m-t-10 m-b-0">Point of Contact</p>
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Name</th>
                                        <td>@if(isset($detail->contact_name) && !empty($detail->contact_name))
                                            @foreach(json_decode($detail->contact_name) as $name)
                                                {{$name. ", "}}

                                            @endforeach
                                        @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>@if(isset($detail->contact_email) && !empty($detail->contact_email))
                                                @foreach(json_decode($detail->contact_email) as $email)
                                                    {{$email}}
                                                @endforeach
                                            @endif</td>
                                    </tr>
                                    <tr>
                                        <th>Contact Number</th>
                                        <td>@if(isset($detail->contact_number) && !empty($detail->contact_number))
                                            @foreach(json_decode($detail->contact_number) as $contact)
                                                {{$contact}}
                                            @endforeach
                                        @endif</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="alert alert-info lead m-t-10 m-b-0">Job Requirement</p>
                                <table class="table table-bordered">
                                    @foreach($getTermsAndConditions as $key => $value)
                                    <tr>
                                        <th>{{$value['title']}}</th>
                                        <td colspan="2">
                                            @if(isset($value['description']) && !empty($value['description']))
                                                {!! $value['description'] !!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal --> 
<div id="custom-modal" class="modal-demo">

    <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('update_job_status/'.$detail->id.'/'.$detail->user_id) }}">
        {{csrf_field()}}
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        
        <h4 class="custom-modal-title">Admin Rejection</h4>
        <div class="custom-modal-text">
            <p class="font-13 preview_staffing  m-b-10">
                <strong>Type Reason: </strong><span class="text-danger">*</span>
            </p>
            <input type="hidden" name="admin_approval" value="2">
            <textarea class="form-control" placeholder="Rejected Reason" name="disapproved_reason" placeholder="Description"></textarea>
            <div class=" m-t-10">
                <button type="submit" class="btn btn-primary-outline waves-effect waves-light save_job_record">Save</button>
            </div>
        </div>
    </form>    
</div>

<!-- Review Job Button Modal --> 
<div id="review-modal" class="modal-demo">

    <form role="form" data-parsley-validate novalidate method="POST" action="{{ url('send_job_review/'.$detail->id.'/'.$detail->user_id) }}">
        {{csrf_field()}}
        <button type="button" class="close" onclick="Custombox.close();">
            <span>&times;</span><span class="sr-only">Close</span>
        </button>
        
        <h4 class="custom-modal-title">Send</h4>
        <div class="custom-modal-text">
            <p class="font-13 preview_staffing  m-b-10">
                <strong>Type Reason: </strong><span class="text-danger">*</span>
            </p>
            <input type="hidden" name="admin_approval" value="3">
            <textarea class="form-control" id="job_review" rows="3" name="job_review" placeholder="Job Review"></textarea>
            <div class=" m-t-10">
                <button type="submit" class="btn btn-primary-outline waves-effect waves-light save_job_record">Save</button>
            </div>
        </div>
    </form>    
</div>
@section('js')
    <script src="{{ URL::asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js') }} " type="text/javascript"></script>
    <script src="{{ url('assets/tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            
        });
    </script>
@stop
@endsection
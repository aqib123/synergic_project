@extends('layouts.app') @section('content')
<div class="row">
    <div class="col-sm-12 m-t-20">
        <h4 class="header-title m-b-10">Welcome,
            <strong>{{isset($title)?$title:''}}</strong>
        </h4>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-6">
        <div class="card-box m-b-0">
            <div class="pull-left">
                <h4 class="header-title m-t-0">Job List</h4>
            </div>
            <div class="pull-right">
                <div class="btn-group m-b-0 m-b-0">
                    <button type="button" id="weekly_chart" class="btn active btn-sm btn-dark-outline waves-effect waves-light active">Weekly</button>
                    <button type="button" id="monthly_chart" class="btn btn-sm btn-dark-outline waves-effect waves-light">Monthly</button>
                    <button type="button" id="yearly_chart" class="btn btn-sm btn-dark-outline waves-effect waves-light">Yearly</button>
                </div>
            </div>
            <div class="m-t-30 p-20 p-b-0">
                    <canvas id="line_weekly" height="200"></canvas>
                    <canvas id="line_monthly" height="200" style="display:none"></canvas>
                    <canvas id="line_yearly" height="200" style="display:none"></canvas>

                <!-- <canvas id="bar_weekly" height="200"></canvas>
                <canvas id="bar_monthly" height="200" style="display:none"></canvas>
                <canvas id="bar_yearly" height="200" style="display:none"></canvas> -->
            </div>
        </div>
    </div>
    <div class="col-md-6 m-t-0">
        <div class="card-box stat_boxes m-b-0">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                <a class="blocks" href="{{url('users?status=OAM')}}">
                    <div class="card-box hvr-grow-shadow  tilebox-one">
                        <i class="icon-layers pull-xs-right text-muted"></i>
                        <h6 class="text-muted text-uppercase m-b-20">OAM Companies</h6>
                        <h2 data-plugin="counterup">{{isset($oam)?$oam:0}}</h2>

                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6">
                <a class="blocks" href="{{url('users?status=FMC')}}">
                    <div class="card-box hvr-grow-shadow  tilebox-one">
                        <i class="icon-paypal pull-xs-right text-muted"></i>
                        <h6 class="text-muted text-uppercase m-b-20">FM Companies</h6>
                        <h2>
                            <span data-plugin="counterup">{{isset($fmc)?$fmc:0}}</span>
                        </h2>

                    </div>
                </a>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6"> 
                <a class="blocks" href="{{url('jobs')}}"><div class="card-box hvr-grow-shadow  tilebox-one">
                    <i class="icon-chart pull-xs-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-0">Pending Job Approvals</h6>
                    <span class="text-muted  text-uppercase  font-13">(Previous one week)</span>
                    <h2>
                        <span data-plugin="counterup">{{isset($weekly_approval)?$weekly_approval:0}}</span>
                    </h2> 
                </div></a>  
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6"> 
                <div class="card-box hvr-grow-shadow tilebox-one">
                    <i class="icon-rocket pull-xs-right text-muted"></i>
                    <h6 class="text-muted text-uppercase m-b-0">Pending Quries</h6>
                    <span class="text-muted  text-uppercase  font-13">(Jobs + Companies)</span>
                    <h2 data-plugin="counterup">{{isset($query_count)?$query_count:0}}</h2> 
                </div> 
            </div>
            <div class="clearfix"></div>
    </div>

    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-10">
            <a href="{{url('admin_job_list')}}" type="button" class="btn btn-dark-outline btn-sm  waves-effect waves-light">View All</a>
        </div>
        <div class="pull-left m-t-20 m-b-10">
            <h4 class="header-title m-b-0">Latest Pending Jobs</h4>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <table id="datatable" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Job Title</th>
                        <th>Company Name</th>
                        <th>Job Type</th>
                        <th>Focal Name</th>
                        <th>Post Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($pending_jobs) && count($pending_jobs)>0) @foreach($pending_jobs as $row)
                    <tr>
                       
                        <td><a class="custom_links" href="{{url('job_preview?id='.$row['id'])}}">{{ ucwords($row['job_title']) }}</a></td>
                        <td><a class="custom_links" href="{{url('users')}}"> {{ucwords($row['companies']['name']) }}</a></td>
                        <td>{{ $row['job_type'] }}</td>
                        <td>
                            @foreach(json_decode($row['contact_name']) as $name) {{$name}}
                            <br /> @endforeach
                        </td>
                        <td>
                            <?php echo date_format(new DateTime($row['created_at']), 'jS F Y g:ia');?>
                        </td>
                        <td>
                            <a class="btn btn-sm btn-dark-outline  waves-effect waves-light" href="{{url('job_preview?id='.$row['id'])}}">Show Details</a>
                        </td>

                    </tr>
                    @endforeach @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('js')
<script src="{{ URL::to('assets/plugins/chart.js/chart.min.js') }}"></script>
<script src="{{ URL::to('assets/pages/chartjs.init.js') }}"></script>

<script>
    $(document).ready(function () {
        $("#line_monthly").hide();
        $("#line_yearly").hide();
        $("#weekly_chart").click(function () {
            $("#line_weekly").show();
            $("#line_monthly").hide();
            $("#line_yearly").hide();
            $("#monthly_chart").removeClass("active");
            $("#yearly_chart").removeClass("active");
            $("#weekly_chart").addClass("active");
        });
        $("#monthly_chart").click(function () {
            $("#line_weekly").hide();
            $("#line_monthly").show();
            $("#line_yearly").hide();
            $("#monthly_chart").addClass("active");
            $("#yearly_chart").removeClass("active");
            $("#weekly_chart").removeClass("active");
        });
        $("#yearly_chart").click(function () {
            $("#line_weekly").hide();
            $("#line_monthly").hide();
            $("#line_yearly").show();
            $("#monthly_chart").removeClass("active");
            $("#yearly_chart").addClass("active");
            $("#weekly_chart").removeClass("active");
        });
    });

    !function($) {
    "use strict";
    var ChartJs = function() {};
    ChartJs.prototype.respChart = function(selector,type,data, options) {
        var ctx = selector.get(0).getContext("2d");
        var container = $(selector).parent();
        $(window).resize( generateChart );
        function generateChart(){
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Line':
                new Chart(ctx, {type: 'line', data: data, options: options});
                break;
            }
        };
        generateChart();
    },
    ChartJs.prototype.init = function() {
        var lineChart = {
            labels: [
            <?php
            $weekly = array(date('l', strtotime(' 0 day')), date('l', strtotime(' -1 day')), date('l', strtotime(' -2 day')), date('l', strtotime(' -3 day')), date('l', strtotime(' -4 day')), date('l', strtotime(' -5 day')), date('l', strtotime(' -6 day')));
            echo '"' . date('l', strtotime(' 0 day')) . '", ';
            echo '"' . date('l', strtotime(' -1 day')) . '", ';
            echo '"' . date('l', strtotime(' -2 day')) . '", ';
            echo '"' . date('l', strtotime(' -3 day')) . '", ';
            echo '"' . date('l', strtotime(' -4 day')) . '", ';
            echo '"' . date('l', strtotime(' -5 day')) . '", ';
            echo '"' . date('l', strtotime(' -6 day')) . '", ';
            ?>
            ],
            datasets: [
                {
                    label: "Total Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#039cfd",
                    borderColor: "#039cfd",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#039cfd",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#039cfd",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                    $weekly_total_job_array = [];
                    $h1 = array();
                    foreach($weekly_total_jobs as $key => $value){ 
                        $day = date('l', strtotime($value->day));
                        $weekly_total_job_array[$day] = $value->count;
                        array_push($h1,$value->count);
                    }
                    $max1 = 0;
                    if(isset($h1) && count($h1) > 0){
                        $max1 = max($h1);
                    }
                    foreach($weekly as $key => $value){
                        if(array_key_exists($value, $weekly_total_job_array)){
                            echo $weekly_total_job_array[$value].', ';
                        }else{
                            echo '0 ,';
                        }
                    }
                    ?>
                    ]
                },{
                    label: "Pending Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#FF5D48",
                    borderColor: "#FF5D48",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#FF5D48",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#FF5D48",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                    $weekly_pending_job_array = [];
                    $h2= array();
                    foreach($weekly_pending_jobs as $key => $value){ 
                        $day = date('l', strtotime($value->day));
                        $weekly_pending_job_array[$day] = $value->count;
                        array_push($h2,$value->count);
                    }
                    $max2 = 0;
                    if(isset($h2) && count($h2) > 0){
                        $max2 = max($h2);
                    } 
                    foreach($weekly as $key => $value){
                        if(array_key_exists($value, $weekly_pending_job_array)){
                            echo $weekly_pending_job_array[$value].', ';
                        }else{
                            echo '0 ,';
                        }
                    }
                    ?>
                    ]
                },{
                    label: "Applied Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#F1B53D",
                    borderColor: "#F1B53D",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#F1B53D",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#F1B53D",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                    $weekly_applied_job_array = [];
                    $h3 = array();
                    foreach($weekly_applied_jobs as $key => $value){ 
                        $day = date('l', strtotime($value->day));
                        $weekly_applied_job_array[$day] = $value->count;
                        array_push($h3,$value->count);
                    }
                    $max3 = 0;
                    if(isset($h3) && count($h3) > 0){
                        $max3 = max($h3);
                    } 
                    foreach($weekly as $key => $value){
                        if(array_key_exists($value, $weekly_applied_job_array)){
                            echo $weekly_applied_job_array[$value].', ';
                        }else{
                            echo '0 ,';
                        }
                    }
                    $maximum = max($max1,$max2,$max3);
                    if($maximum < 1){
                        $maximum = 10;
                    }                    
                    ?>
                    ]
                }
            ]
        };
        var lineOpts = {
            scales: {
                yAxes: [{
                    ticks: {
                        max: <?php echo $maximum; ?>,
                        min: 0,
                        stepSize: 2
                    }
                }]
            }
        };
        this.respChart($("#line_weekly"),'Line',lineChart, lineOpts);
    },
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
}(window.jQuery),

function($) {
    "use strict";
    $.ChartJs.init()
}(window.jQuery);

!function($) {
    "use strict";
    var ChartJs2 = function() {};
    ChartJs2.prototype.respChart = function(selector,type,data, options) {
        var ctx = selector.get(0).getContext("2d");
        var container = $(selector).parent();
        $(window).resize( generateChart );
        function generateChart(){
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Line':
                new Chart(ctx, {type: 'line', data: data, options: options});
                break;
            }
        };
        generateChart();
    },
    ChartJs2.prototype.init = function() {
        var lineChart = {
            labels: [
            <?php 
                    $monthly = array(date('F', strtotime(' 0 month')), 
                    date('F', strtotime(' -1 month')), 
                    date('F', strtotime(' -2 month')),
                    date('F', strtotime(' -3 month')), 
                    date('F', strtotime(' -4 month')), 
                    date('F', strtotime(' -5 month')), 
                    date('F', strtotime(' -6 month')), 
                    date('F', strtotime(' -7 month')), 
                    date('F', strtotime(' -8 month')), 
                    date('F', strtotime(' -9 month')), 
                    date('F', strtotime(' -10 month')), 
                    date('F', strtotime(' -11 month')));

                    echo '"' . date('F') . '", ';
                    echo '"' . date('F', strtotime(' -1 month')) . '", ';
                    echo '"' . date('F', strtotime(' -2 month')) . '", ';
                    echo '"' . date('F', strtotime(' -3 month')) . '", ';
                    echo '"' . date('F', strtotime(' -4 month')) . '", ';
                    echo '"' . date('F', strtotime(' -5 month')) . '", ';
                    echo '"' . date('F', strtotime(' -6 month')) . '", ';
                    echo '"' . date('F', strtotime(' -7 month')) . '", ';
                    echo '"' . date('F', strtotime(' -8 month')) . '", ';
                    echo '"' . date('F', strtotime(' -9 month')) . '", ';
                    echo '"' . date('F', strtotime(' -10 month')) . '", ';
                    echo '"' . date('F', strtotime(' -11 month')) . '", ';    
                ?>
            ],
            datasets: [
                {
                    label: "Total Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#039cfd",
                    borderColor: "#039cfd",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#039cfd",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#039cfd",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $monthly_total_job_array = [];
                        $h4 = array();
                        foreach($monthly_total_jobs as $key => $value){
                            $dateObj = DateTime::createFromFormat('!m', $value->month);
                            $month = $dateObj->format('F'); 
                            $monthly_total_job_array[$month] = $value->count;
                            array_push($h4, $value->count);
                        } 
                        //dd($monthly_total_jobs);
                        $max4 = 0;
                        if(isset($h4) && count($h4) > 0){
                            $max4 = max($h4);
                        }
                        foreach($monthly as $key => $value){
                            if(array_key_exists($value, $monthly_total_job_array)){
                                echo $monthly_total_job_array[$value].', ';
                            }else{
                                echo '0 ,';
                            }
                        }
                    ?>
                    ]
                },{
                    label: "Pending Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#FF5D48",
                    borderColor: "#FF5D48",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#FF5D48",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#FF5D48",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $monthly_pending_job_array = [];
                        $h5 = array();
                        foreach($monthly_pending_jobs as $key => $value){ 
                            $dateObj = DateTime::createFromFormat('!m', $value->month);
                            $month = $dateObj->format('F'); 
                            $monthly_pending_job_array[$month] = $value->count;
                            array_push($h5, $value->count);
                        } 
                        $max5 = 0;
                        if(isset($h5) && count($h5) > 0){
                            $max5 = max($h5);
                        }
                        foreach($monthly as $key => $value){
                            if(array_key_exists($value, $monthly_pending_job_array)){
                                echo $monthly_pending_job_array[$value].', ';
                            }else{
                                echo '0 ,';
                            }
                        }
                    ?>
                    ]
                },{
                    label: "Applied Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#F1B53D",
                    borderColor: "#F1B53D",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#F1B53D",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#F1B53D",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $monthly_applied_job_array = [];
                        $h6 = array();
                        foreach($monthly_applied_jobs as $key => $value){ 
                            $dateObj = DateTime::createFromFormat('!m', $value->month);
                            $month = $dateObj->format('F'); 
                            $monthly_applied_job_array[$month] = $value->count;
                            array_push($h6, $value->count);
                        } 
                        $max6 = 0;
                        if(isset($h6) && count($h6) > 0){
                            $max6 = max($h6);
                        }
                        foreach($monthly as $key => $value){
                            if(array_key_exists($value, $monthly_applied_job_array)){
                                echo $monthly_applied_job_array[$value].', ';
                            }else{
                                echo '0 ,';
                            }
                        }
                        $maximun2 = max($max4, $max5, $max6);
                        if($maximun2 < 1){
                            $maximun2 = 10;
                        }
                    ?>
                    ]
                }
            ]
        };
        var lineOpts = {
            scales: {
                yAxes: [{
                    ticks: {
                        max: <?php echo $maximun2 ; ?>,
                        min: 0,
                        stepSize: 2
                    }
                }]
            }
        };
        this.respChart($("#line_monthly"),'Line',lineChart, lineOpts);
    },
    $.ChartJs2 = new ChartJs2, $.ChartJs2.Constructor = ChartJs2
}(window.jQuery),

function($) {
    "use strict";
    $.ChartJs2.init()
}(window.jQuery);
   

!function($) {
    "use strict";
    var ChartJs = function() {};
    ChartJs.prototype.respChart = function(selector,type,data, options) {
        var ctx = selector.get(0).getContext("2d");
        var container = $(selector).parent();
        $(window).resize( generateChart );
        function generateChart(){
            var ww = selector.attr('width', $(container).width() );
            switch(type){
                case 'Line':
                new Chart(ctx, {type: 'line', data: data, options: options});
                break;
            }
        };
        generateChart();
    },
    ChartJs.prototype.init = function() {
        var lineChart = {
            labels: [
            <?php 
                    $year = array(date('Y', strtotime(' 0 year')), 
                    date('Y', strtotime(' -1 year')), 
                    date('Y', strtotime(' -2 year')), 
                    date('Y', strtotime(' -3 year')), 
                    date('Y', strtotime(' -4 year')), 
                    date('Y', strtotime(' -5 year')), 
                    date('Y', strtotime(' -6 year')));

                    echo '"' . date('Y') . '", ';
                    echo '"' . date('Y', strtotime(' -1 year')) . '", ';
                    echo '"' . date('Y', strtotime(' -2 year')) . '", ';
                    echo '"' . date('Y', strtotime(' -3 year')) . '", ';
                    echo '"' . date('Y', strtotime(' -4 year')) . '", ';
                    echo '"' . date('Y', strtotime(' -5 year')) . '", ';
                    echo '"' . date('Y', strtotime(' -6 year')) . '", ';
                ?>
            ],
            datasets: [
                {
                    label: "Total Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#039cfd",
                    borderColor: "#039cfd",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#039cfd",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#039cfd",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $yearly_total_job_array = [];
                        $h7 = array();
                        foreach($yearly_total_jobs as $key => $value){ 
                            $yearly_total_job_array[$value->year] = $value->count;
                            array_push($h7, $value->count);
                        }
                        $max7 = 0;
                        if(isset($h7) && count($h7) > 0){
                            $max7 = max($h7);
                        }
                        foreach($year as $key => $value){
                            if(array_key_exists($value, $yearly_total_job_array)){
                                echo $yearly_total_job_array[$value].', ';
                            }else{
                                echo '0, ';
                            }
                        }
                        ?>
                    ]
                },
                {
                    label: "Pending Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#FF5D48",
                    borderColor: "#FF5D48",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#FF5D48",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#FF5D48",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $yearly_pending_job_array = [];
                        $h8 = array();
                        foreach($yearly_pending_jobs as $key => $value){ 
                            $yearly_pending_job_array[$value->year] = $value->count;
                            array_push($h8, $value->count);
                        }
                        $max8 = 0;
                        if(isset($h8) && count($h8) > 0){
                            $max8 = max($h8);
                        }
                        foreach($year as $key => $value){
                            if(array_key_exists($value, $yearly_pending_job_array)){
                                echo $yearly_pending_job_array[$value].', ';
                            }else{
                                echo '0 ,';
                            }
                        }
                        ?>
                    ]
                },{
                    label: "Applied Jobs",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "#F1B53D",
                    borderColor: "#F1B53D",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "#F1B53D",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "#F1B53D",
                    pointHoverBorderColor: "#eef0f2",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: [
                    <?php
                        $yearly_applied_job_array = [];
                        $h9 = array();
                        foreach($yearly_applied_jobs as $key => $value){ 
                            $yearly_applied_job_array[$value->year] = $value->count;
                            array_push($h9, $value->count);
                        }
                        $max9 = 0;
                        if(isset($h9) && count($h9) > 0){
                            $max9 = max($h9);
                        }
                        foreach($year as $key => $value){
                            if(array_key_exists($value, $yearly_applied_job_array)){
                                echo $yearly_applied_job_array[$value].', ';
                            }else{
                                echo '0 ,';
                            }
                        }
                        $maximun3 = max($max7, $max8, $max9);
                        if($maximun3 < 1){
                            $maximum = 10;
                        }
                        ?>
                    ]
                }
            ]
        };
        var lineOpts = {
            scales: {
                yAxes: [{
                    ticks: {
                        max: <?php echo $maximun3; ?>,
                        min: 0,
                        stepSize: 2
                    }
                }]
            }
        };
        this.respChart($("#line_yearly"),'Line',lineChart, lineOpts);
    },
    $.ChartJs = new ChartJs, $.ChartJs.Constructor = ChartJs
}(window.jQuery),

function($) {
    "use strict";
    $.ChartJs.init()
}(window.jQuery);
</script>

@stop @endsection
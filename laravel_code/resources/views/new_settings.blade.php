@extends( Auth::user()->role_id == 2 ? 'oam.app' :  (Auth::user()->role_id == 1 ? 'layouts.app' : 'fmc.app'))
@section('content')
@section('css')
<!--venobox lightbox-->
<link rel="stylesheet" href="{{ URL::to('assets/plugins/magnific-popup/css/magnific-popup.css') }}"/>
<style>
    .profile_img{
        border: 4px solid #822767;
        padding: 23px 26px;
        font-size: 25px;
        font-weight: 700;
        color:#822767;
        border-radius: 50%;
        letter-spacing: 0px;
    }
</style>
@endsection
<div class="row">
    <div class="col-sm-12">
        <div class="btn-group pull-right m-t-15 m-b-15 ">
            <a href="#" type="button" class="btn btn-dark-outline  waves-effect waves-light">Back To Staff List</a>
        </div>
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
    <div class="msg-box alert" style="display: none;">
        <ul style="text-decoration: none;" id="msg-list">
            
        </ul>
    </div>
    @include('flash_message')
    <div class="row ">
        @if(Auth::user()->role_id == 1)
        <form id="staff_form" action="{{url('staff/member/save')}}" method="POST">
        @elseif(Auth::user()->role_id == 2)
        <form id="staff_form" action="{{url('oam/staff/member/save')}}" method="POST">
        @else
        <form id="staff_form" action="{{url('fmc/staff/member/save')}}" method="POST">
        @endif    
            <div class="col-sm-12">
                <div class="card-box add_staff ">
                        <div class="col-sm-6">

                    <div class="row"> 
                        <div class="col-sm-12">
                            <h4 class="m-t-0 m-b-10">Personal information</h4> 
                        </div>
                    </div>
                    <div class="row">
                    
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="form_label" for="userName">Name (required)<span class="text-danger">*</span></label>
                                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                <input name="name" placeholder="Name" class="form-control" type="text" id="name" value="">
                            </div>
                        </div>
                    </div> 
                    <div class="row">
                    
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="form_label" for="userName">Company Name </label>
                                <input type="hidden" id="csrf_token" name="_token" value="{{ csrf_token() }}">
                                <input name="name" placeholder="Company Name" class="form-control" type="text"  value="">
                            </div> 
                        </div>
                        <div class="col-sm-2  m-t-20">
                            <div class="checkbox m-t-10 checkbox-success checkbox-inline">
                                <input id="Company" value="Company" type="checkbox">
                                <label for="Company"> Company</label>
                            </div>
                            
                    </div>
                    </div> 
                    <div class="row">
                        <div class="col-sm-8">
                            <label class="form_label" for="companyName">Phone</label>
                            <div class="phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-smartphone"></i></span>
                                <input class="form-control" placeholder="+971 2 0000000 " type="text" id="phone_number" name="phone_number" value="">
                            </div>
                            <div class=" phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-home"></i></span>
                                <input class="form-control" placeholder="+971 2 0000000 " type="text" name="home_number" value="">
                            </div>
                           <!--  <div class=" phone_input input-group">
                                <span class="input-group-addon"><i class="zmdi zmdi-case"></i></span>
                                <input class="form-control"  type="text">
                            </div> -->
                        </div>
                    </div>

                    
                </div>
                <div class="col-sm-6">

                    <div class="row"> 
                        <div class="col-sm-12">
                            <h4 class="m-t-0 m-b-10">Profile photo</h4> 
                        </div>
                    </div>
                    <div class="row">
                        <div clss="col-sm-12">
                            <div class="left company_image" style="margin-right: 20px;">
                                <div class="profile_img">FS</div>
                            </div>
                            <div class="col-sm-3">
                                <div class="thumb m-t-0">
                                    <a href="{{ URL::to('assets/images/gallery/11.jpg') }}" class="image-popup" title="Screenshot-11">
                                        <img src="{{ URL::to('assets/images/gallery/11.jpg') }}" class="thumb-img" alt="work-thumbnail">
                                    </a> 
                                </div>
                            </div>
                            <div style="position:relative;">
                                <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                    Choose File..
                                    <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                                </a>
                                &nbsp;
                                <span class='label label-info' id="upload-file-info"></span>
                            </div>
                            <div class="m-t-10"> 
                                <p class="text-muted font-13 m-t-0"> Upload a PNG or JPEG file to add a personal touch to your profile.</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>


                <div class="clearfix"></div> 

                </div>

                <div class="card-box add_staff ">
                        <div class="row">
                        
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">User information</h4>
                                <hr class="m-t-0">
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-sm-4">
                                <label class="form_label" for="primaryemail">Email (required)<span class="text-danger">*</span></label>
                                <div class=" form-group phone_input input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                                    <input type="hidden" name="id" value="">
                                    <input class="form-control" placeholder="Email"  type="text" name="email" value="">
                                </div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="form_label" for="userrole">Role (required)<span class="text-danger">*</span></label>
                                <div class=" form-group ">
                                    <select class="c-select" name="role">
                                        <option selected="" disabled>Select user role..</option>
                                       
                                            <option value="" >name</option>
                                            <option value="" >name</option>
                                            <option value="" >name</option>
                                        
                                    </select>
                                </div>
                            </div>
                        </div> 
                </div>
                <div class="card-box add_staff m-b-10">
                        <div class="row">
                        
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Task notifications</h4>
                                <hr class="m-t-0  m-b-10">
                            </div> 
                        </div>
                        <div class="row">
                            
                            <div class="col-md-3">
                                    <div class="col-md-12">
                                <p class="lead font-13 m-b-0 text-muted"> 
                                    <span>Incoming requests</span>
                                </p>
                                    
                                <div class="checkbox font-13 checkbox-success m-b-10">
                                    <input id="contact_requests" checked type="checkbox">
                                    <label for="contact_requests">
                                        Contact requests
                                    </label>
                                </div> 
                                <div class="checkbox font-13 checkbox-success m-b-10">
                                    <input id="resident_requests" checked type="checkbox">
                                    <label for="resident_requests">
                                        Resident requests
                                    </label>
                                </div>
                            </div>
                        </div>

                            <div class="col-md-7">

                                <div class="col-md-5">
                                    <p class="lead font-13 m-b-0 text-muted"> 
                                        <span>My tasks</span>
                                    </p>
                                        
                                    <div class="checkbox font-13 checkbox-success m-b-10">
                                        <input id="tasks_update" checked type="checkbox">
                                        <label for="tasks_update">
                                            My tasks update
                                        </label>
                                    </div> 
                                    <div class="checkbox font-13 checkbox-success m-b-10">
                                        <input id="daily_summary" checked type="checkbox">
                                        <label for="daily_summary">
                                            My tasks daily summary
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <p class="lead font-13 m-b-0 text-muted"> 
                                        <span>All tasks</span>
                                    </p>
                                        
                                    <div class="checkbox font-13 checkbox-success m-b-10">
                                        <input id="all_tasks_update" type="checkbox">
                                        <label for="all_tasks_update">
                                            All tasks update
                                        </label>
                                    </div> 
                                    <div class="checkbox font-13 checkbox-success m-b-10">
                                        <input id="all_daily_tasks_update" type="checkbox">
                                        <label for="all_daily_tasks_update">
                                            All tasks daily summary
                                        </label>
                                    </div>
                                </div>

                                <div class="col-md-9">
                                    <hr class="m-t-10 m-b-10"> 
                                        
                                        
                                    <div class="checkbox font-13 checkbox-success m-b-10">
                                        <input id="all_tasks_update" type="checkbox">
                                        <label for="all_tasks_update">
                                            Include a full history in every email
                                        </label>
                                    </div>  
                                </div>

                            </div>

                        </div>
                </div>
                <div class="card-box add_staff m-b-10">
                        <div class="row">
                        
                            <div class="col-sm-12">
                                <h4 class="m-t-0 m-b-0">Inspections</h4>
                                <hr class="m-t-0">
                            </div> 
                        </div>
                        <div class="row">
                            
                            <div class="col-md-12">
                                
                                <p class="lead font-13 m-b-0 m-t-0">When you click save, we will:</p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span> Use the listed email address as the username</span>
                                </p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span>Assign a temporary password</span>
                                </p>
                                <p class="lead font-13 m-b-0 "><i class=" text-success fa fa-check"></i>  
                                    <span>Email instructions to the user telling them how to sign in</span>
                                </p>
                            </div>
                        </div>
                </div>
                
            </div>
        <div class="col-sm-12">
            <a href="javascript:void(0)" class="btn btn-dark-outline waves-effect waves-light m-b-10 save_button">Save</a>
            <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important; display: none;">
        </div>
        </form>
    </div>

@section('js') 
<!-- Magnific popup -->
<script type="text/javascript" src="{{ URL::to('assets/plugins/magnific-popup/js/jquery.magnific-popup.min.js') }}"></script>

<script>
    $(document).ready(function() {
        $('.image-popup').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-fade',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            }
        });
    });
</script>
@endsection

<!-- end col-->


@endsection
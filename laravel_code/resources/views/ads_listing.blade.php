@extends('layouts.app') @section('content')
<style>
</style>
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <!-- <div class="btn-group pull-right m-t-15">
            <a href="{{url('user_form')}}" type="button" class="btn btn-dark-outline  waves-effect waves-light">Add New Company</a>
        </div> -->
        <h4 class="page-title">{{isset($title)?$title:''}}</h4>
    </div>
</div>
@include('flash_message')
<div class="msg-box alert" style="display: none;">
    <ul style="text-decoration: none;" id="msg-list">
    </ul>
</div>
<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs association_nav m-b-0" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="inactive-ad--tab" data-toggle="tab" href="#inactive" role="tab" aria-controls="inactive" aria-expanded="true">
                    <b>Inactive</b>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="assigned-ad-tab" data-toggle="tab" href="#assigned" role="tab" aria-controls="assigned">
                    <b>Assigned</b>
                </a>
            </li>
        </ul>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 m-t-0">
        <div class="card-box ">
            <div class="tab-content m-t-20" id="myTabContent">
                <div role="tabpanel" class="tab-pane fade in active" id="inactive" aria-labelledby="inactive">
                    <div class=" custom_table">
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($ads) && count($ads)>0) @foreach($ads as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td class="text-center">
                                        <img class="img img-circle" width="50" height="50" src="{{ asset('laravel_code/storage/app/public/company_ad/'.$row->image) }}"
                                            alt="Profile" />
                                    </td>
                                    <td>
                                        {{ucwords($row->title)}}
                                    </td>
                                    <td>
                                        @php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp
                                    </td>
                                    <td>
                                        @if($row->status == 0)
                                        <span class="label label-danger">Inactive</span>
                                        @else
                                        <span class="label label-success">Active</span>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group user_action_btn">
                                            <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                                <i class="zmdi zmdi-more"></i>
                                            </a>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="" title="Edit">
                                                    <i class="fa fa-edit text-info"></i> Edit
                                                </a>
                                                <a class="dropdown-item delete_assets_btn" href="javascript:void(0)" id="" title="Delete">
                                                    <i class="fa fa-trash text-danger"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane fade" id="assigned" aria-labelledby="assigned">
                    <div class=" custom_table">
                        <table id="datatable2" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Created At</th>
                                    <th>Assigned To</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($assigned) && count($assigned)>0) @foreach($assigned as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td class="text-center">
                                        <img class="img img-circle" width="50" height="50" src="{{ asset('laravel_code/storage/app/public/company_ad/'.$row->image) }}"
                                            alt="Profile" />
                                    </td>
                                    <td>
                                        {{ucwords($row->title)}}
                                    </td>
                                    <td>
                                        @php echo date_format(new DateTime($row->created_at), 'jS F Y g:ia'); @endphp
                                    </td>
                                    <td>
                                        @if($row->role_id == 2) {{$row->name}}(OAM) @else {{$row->name}}(FMC) @endif
                                    </td>
                                    <td style="text-align: center">
                                        <div class="btn-group user_action_btn">
                                            <a class="custm_btn" data-toggle="dropdown" aria-expanded="true">
                                                <i class="zmdi zmdi-more"></i>
                                            </a>
                                            <div class="dropdown-menu">
                                                <!-- <a class="dropdown-item edit_assigned_image" href="javascript:void(0)" id="{{$row->company_ad_id}}" title="Edit">
                                                    <i class="fa fa-edit text-info"></i> Edit
                                                </a> -->
                                                @if($row->status == 0)
                                                <a class="dropdown-item inactive_ad_btn" href="javascript:void(0)" id="{{$row->id}}" title="Active">
                                                    <i class="fa fa-check text-success"></i> Active
                                                </a>
                                                @else
                                                <a class="dropdown-item inactive_ad_btn" href="javascript:void(0)" id="{{$row->id}}" title="Inavtive">
                                                    <i class="fa fa-check text-danger"></i> Inavtive
                                                </a>
                                                @endif
                                                <a class="dropdown-item remove_assign_ad" href="javascript:void(0)" id="{{$row->id}}" title="Remove">
                                                    <i class="fa fa-trash text-danger"></i> Remove
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Update Assigned Ads -->
<div class="modal fade md_change_image in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title lead" id="md_heading">Update Ad</h4>
            </div>
            <div class="modal-body">
                <form action="{{url('')}}" method="POST" enctype="multipart/form-data" id="md_account_setting_form">
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Title
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-5">
                            <input required="" id="title" parsley-type="text" placeholder="Ad Title" value="" name="title" class="form-control" type="text">
                            <input type="hidden" name="ad_id" id="assign_ad_id">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 form-control-label">Image
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-sm-10">
                            <div style="position:relative;">
                                <a class='btn btn-success waves-effect waves-light' href='javascript:;'>
                                    Choose File..
                                    <input id="image" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        name="user_image" size="40" onchange='$("#upload-file-info").html($(this).val());'>
                                </a>
                                &nbsp;
                                <span class='label label-info' id="upload-file-info"></span>
                            </div>
                            <div class="m-t-10 image_area">
                                <p class="text-muted font-13 m-t-0"> Upload ad image in GIF, PNG or JPEG format max size 2MB</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button type="button" class="profie_update btn btn-success waves-effect waves-light save_company_ad">
                            Save
                        </button>
                    </div>
                    <div class="col-sm-1 ajax-pre-loader">
                        <img src="{{ URL::to('assets/images/loading.gif') }}" class="loading_gif" style="height: 26px !important;">
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

    <!-- Modal Box for Deletion confirmation -->
    <div class="modal fade md_delete_confirmation" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header alert alert-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p class="success-message">Are you sure you wish to delete this record ? </p>
                    <input type="hidden" name="user_uuid" id="ad_id">
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success delete-confirm md_ok_delete_btn">Ok</button>
                    <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->

@section('js')
<script>
    $(document).ready(function () {
        $('#datatable').DataTable();
        $('#datatable2').DataTable();

        $(document).on('click', '.edit_assigned_image', function () {

            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{url("/ads/change_status/")}}/' + id,
                data: { '_token': '{{ csrf_token() }}' },
                success: function (result) {
                    console.log(result);
                    if (result.flag == 1) {
                        $('#title').val(result.title);
                        var image_path = "{{asset('laravel_code/storage/app/public/company_ad/')}}/" + result.image;
                        $(".image_area").append('<img class="img img-thumbnail" name="user_image" height="100px" width="100px" src="' + image_path + '" />');
                        $('.md_change_image').modal('show');
                    }
                }
            });
        });

        $(document).on('click', '.inactive_ad_btn', function () {
            var id = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '{{url("/ads/change_status/")}}/' + id,
                data: { '_token': '{{ csrf_token() }}' },
                success: function (result) {
                    if (result.flag == 1) {
                        $('#msg-list').append('<li>Record has been saved successfully</li>');
                        $('.msg-box').addClass("alert-success").show();
                        $("html, .container").animate({ scrollTop: 0 }, 600);
                    }
                }
            });
        });

        $(document).on('click', '.remove_assign_ad', function () {
            var id = $(this).attr('id');
            $('#ad_id').val(id);
            $('.md_delete_confirmation').modal('show');
        });

        $(document).on('click', '.md_ok_delete_btn', function () {
            var ad_id = $('#ad_id').val();
            var url = "{{url('/ads/remove_assigned_ad/')}}/" + ad_id;
            window.location.href = url;
        });
    });



</script> @endsection @endsection

<!-- App title -->
<title>Synergic</title>

<!-- App CSS -->
<link href="{{ URL::to('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="{{ URL::to('assets/js/modernizr.min.j') }}"></script>

<div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">

        <div class="ex-page-content text-xs-center">
            <div class="text-error">4<span class="ion-sad"></span>4</div>
            <h3 class="text-uppercase text-white font-600">Page not Found</h3>
            <p class="text-white m-t-30">
                It's looking like you may have taken a wrong turn. Don't worry... it happens to
                the best of us. You might want to check your internet connection. Here's a little tip that might
                help you get back on track.
            </p>
            <br>
            <a class="btn btn-pink waves-effect waves-light" href="{{'home'}}"> Return Home</a>

        </div>

    </div>
    <!-- end wrapper page -->


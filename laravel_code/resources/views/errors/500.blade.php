
<!-- App title -->
<title>Synergic</title>

<!-- App CSS -->
<link href="{{ URL::to('assets/css/style.css') }}" rel="stylesheet" type="text/css"/>

<!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script src="{{ URL::to('assets/js/modernizr.min.j') }}"></script>
  <div class="account-pages"></div>
    <div class="clearfix"></div>
    <div class="wrapper-page">

        <div class="ex-page-content text-xs-center">
            <div class="text-error shadow">500</div>
            <h3 class="text-uppercase text-white font-600">Internal Server Error</h3>
            <p class="text-white m-t-30">
                Why not try refreshing your page? or you can contact <a href="" class="text-white text-uppercase"><b>support</b></a>
            </p>
            <br>
            <a class="btn btn-pink waves-effect waves-light" href="{{'home'}}"> Return Home</a>

        </div>


    </div>
    <!-- end wrapper page -->


    <script>
            var resizefunc = [];
        </script>
        
        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.nicescroll.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        
        <!-- Counter Up  -->
        <script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
        <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>
        
        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]-->
        <script src="assets/plugins/jquery-knob/jquery.knob.js"></script>
        
        
        <script type="text/javascript" src="assets/plugins/multiselect/js/jquery.multi-select.js"></script>
        <!-- Peity chart js -->
        <script src="assets/plugins/peity/jquery.peity.min.js"></script>
        
        
        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>
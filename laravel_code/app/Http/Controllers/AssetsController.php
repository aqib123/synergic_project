<?php

namespace App\Http\Controllers;
use App\Asset;
use GeneralFunctions;
use Validator;
use Auth;
use Excel;
use Carbon;

use Illuminate\Http\Request;

class AssetsController extends Controller
{
    public function add_assets(Request $request)
    {
        $data = [];
        if($request && $request->id != ''){
            $data['title'] = 'Edit Assets';
            $data['assets'] = \App\Asset::find($request->id);
        }else{
            $data['title'] = 'Add Assets';
        }
        return view('oam.add_assets',$data);
    }

    public function save_assets(Request $request){
        $user_id = Auth::user()->id;
        if(Auth::user()->parent_id != 0){
            $user_id = Auth::user()->parent_id;
        }

        if($request && $request->id != ''){
            $validator = Validator::make($request->all(), [
                'assets' => 'required|max:255'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'assets' => 'required|max:255'
            ]);
        }
        $assets = $request->input('assets');
        $assets_code=$this->generateNumber();
        if($validator->fails()) {
            if($request && $request->id != ''){
                return back()->withErrors($validator);
            }else{
                return back()->withErrors($validator)->withInput();
            }
        }
        if($request && $request->id != ''){
            $obj = Asset::find($request->id);
            $obj->updated_at = date('Y-m-d H:i:s', strtotime('now'));
            // check for Existence
            $check_data_exist = GeneralFunctions::check_data_exist('assets', $assets, 'name', $request->id);
        } 
        else{
            $obj = new Asset;
            // check for Existence
            $check_data_exist = GeneralFunctions::check_data_exist('assets', $assets, 'name');
        }
        if($check_data_exist == 1){
            return back()->withErrors(['Record Already Existed'])->withInput();
        }
        $obj->name = $assets;
        $obj->assets_code = $assets_code;
        $obj->user_id = $user_id;
        $obj->save();
        return back()->with('status','Record has been saved successfully');
        
    }
    

    public function import_csv_record_oam(Request $req)
    {
        //$this->generateBarcodeNumber();
        $count=0;
        $insertCount=0;
        $tempArray=[];
        $user_id = Auth::user()->id;
        if(Auth::user()->parent_id != 0){
            $user_id = Auth::user()->parent_id;
        }
        $validator = Validator::make($req->all(), [
            'csv_file' => 'required|mimes:csv,xlsx,xls',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        
        $localTemp=Excel::load($req->file('csv_file'))->get();
        $results = $localTemp->toArray();
        $mytime = Carbon\Carbon::now();
        foreach ($results as $key => $value) {
            $tempAsset=Asset::where('name', $value)->first();
            if(!$tempAsset && $value['asset_name']!='')
            {
                $randNumber=$this->generateNumber();
                $tempArray[$key]['name']=$value['asset_name'];
                $tempArray[$key]['created_at']=$mytime->toDateTimeString();
                $tempArray[$key]['updated_at']=$mytime->toDateTimeString();
                $tempArray[$key]['assets_code']=$randNumber;
                $tempArray[$key]['user_id']=$user_id;
                $insertCount++;
                }
                $count++;
            }
            Asset::insert(array_unique($tempArray, SORT_REGULAR));
            if($insertCount==0)
            {
               return back()->with('status', 'All Asset Already exist in our Records');           
           }
           elseif($count==$insertCount)
           {
            return back()->with('status', 'Record has been Saved successfully');
        }
        else
        {
            return back()->with('status', 'Record has been Saved successfully ,And Some Asset Already exist in our Records');
        }

    }
    public function generateNumber() 
    {
        $randNumber= rand ( 10000 , 99999);
        $tempAsset=\App\Building::whereRaw("find_in_set('$randNumber',assets_code)")->first();
        if($tempAsset)
        {
           return $this->generateNumber(); 
        }
            return $randNumber;
    }

        public function import_csv_record(Request $req)
        {
            $count=0;
            $insertCount=0;
            $tempArray=[];
            $user_id = Auth::user()->id;
            if(Auth::user()->parent_id != 0){
                $user_id = Auth::user()->parent_id;
            }
            $validator = Validator::make($req->all(), [
                'csv_file' => 'required|mimes:csv,xlsx,xls',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            }

            $localTemp=Excel::load($req->file('csv_file'))->get();
            $results = $localTemp->toArray();
            $mytime = Carbon\Carbon::now();
            foreach ($results as $key => $value) {
                $tempAsset=Asset::where('name', $value)->first();
                if(!$tempAsset && $value['asset_name']!='')
                {
                    $tempArray[$key]['name']=$value['asset_name'];
                    $tempArray[$key]['created_at']=$mytime->toDateTimeString();
                    $tempArray[$key]['updated_at']=$mytime->toDateTimeString();
                    $tempArray[$key]['user_id']=$user_id;
                    $insertCount++;
                }
                $count++;
            }
            Asset::insert(array_unique($tempArray, SORT_REGULAR));
            if($insertCount==0)
            {
               return back()->with('status', 'All Asset Already exist in our Records');           
           }
           elseif($count==$insertCount)
           {
            return back()->with('status', 'Record has been Saved successfully');
        }
        else
        {
            return back()->with('status', 'Record has been Saved successfully ,And Some Asset Already exist in our Records');
        }

    }

    public function assets()
    {
        $user_id = Auth::user()->id;
        if(Auth::user()->parent_id != 0){
            $user_id = Auth::user()->parent_id;
        }

        $data = [];
        $data['title'] = "All Assets";
        $assets = Asset::with('users')->where('soft_delete', 0)->get();
        // $assets = Asset::where('user_id', 123)->where('soft_delete', 0)->get();
        $data['assets'] = $assets->toArray();
        // dd($data);
        return view('oam.assets', $data);
    }
}

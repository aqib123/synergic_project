<?php
namespace App\Http\Controllers;
use Validator;
use GeneralFunctions;
use Illuminate\Http\Request;
use Auth;

class BuildingController extends Controller
{
    public function building_form(Request $request)
    {
        $data = [];
        $data['assets'] = \App\Asset::all();
        if($request && $request->id != ''){
            $data['title'] = 'Edit Building';
            $data['building'] = \App\Building::find($request->id);
        }else{
            $data['title'] = 'Add Building';
        }
        return view('oam.building_form',$data);

    }
    public function building_form_ajax(Request $request)
    {
        $assets=\App\Building::whereRaw("find_in_set('$request->assets',assets)")->first();
        $assets_code=$this->generateNumberBuilding();
        return response()->json(['data' =>$assets,'assets_code'=>$assets_code]);
    }

    public function save_building(Request $request)
    {
        $assets_array=[];
        $user_id = Auth::user()->id;
        if(Auth::user()->parent_id != 0){
            $user_id = Auth::user()->parent_id;
        }
            $data = [
                'name' => 'required|max:255',
                'address' => 'required|max:255',
                'assets.*' => 'required',
                'type' => 'required',
            ];

        if($request && $request->id != ''){
            if($request->type == "Commercial And Residential"){
                $data['floor'] = "required|numeric";
                $data['apartment'] = "required|numeric";
                $data['retail'] = "required|numeric";
                $data['office'] = "required|numeric";
            }
            if($request->type == "villa"){
                $data['residential_unit'] = "required|numeric";
                $data['commercial_unit'] = "required|numeric";
            }        
            if($request->type == "Commercial"){
                $data['floor'] = "required|numeric";
                $data['retail'] = "required|numeric";
                $data['office'] = "required|numeric";
            }
            if($request->type == "Residential"){
                $data['floor'] = "required|numeric";
                $data['apartment'] = "required|numeric";
            }
            $validator = Validator::make($request->all(), $data);
        }else{

            if($request->type == "Commercial And Residential"){
                $data['floor'] = "required|numeric";
                $data['apartment'] = "required|numeric";
                $data['retail'] = "required|numeric";
                $data['office'] = "required|numeric";
            }
            if($request->type == "villa"){
                $data['residential_unit'] = "required|numeric";
                $data['commercial_unit'] = "required|numeric";
            }  
            if($request->type == "Commercial"){
                $data['floor'] = "required|numeric";
                $data['retail'] = "required|numeric";
                $data['office'] = "required|numeric";
            }
            if($request->type == "Residential"){
                $data['floor'] = "required|numeric";
                $data['apartment'] = "required|numeric";
            }
            $validator = Validator::make($request->all(), $data);
        }
        if ($validator->fails()) {
            if($request->input('ajax')){
                $errors = GeneralFunctions::error_msg_serialize($validator->errors());
                return response()->json(['status' => 'error', 'msg_data' => $errors]);    
            }
            if($request && $request != ''){
                return back()->withErrors($validator)->withInput();
            }else{
                return back()->withErrors($validator)->withInput();
            }
        }else{
            
            $this->GetAssetsADDByOAMNotification($request->name,$request->assets, $user_id, $status = 0);
           
            if($request && $request->id != ''){
                $obj = \App\Building::find($request->id);
                $obj->updated_at = date('Y-m-d H:i:s', strtotime('now'));

                /* Check Makani Number Already Exist */
                $checkExistence = \App\Building::where('makani', $request->input('makani'))->where('id','!=', $request->id)->get();
                // dd($checkExistence);
                if(count($checkExistence->toArray()) > 0){
                    if($request->input('ajax')){
                        $errors = GeneralFunctions::error_msg_serialize($validator->errors());
                        return response()->json(['status' => 'error', 'msg_data' => ['Makani Number Already Mapped with Other Building, for more info please contact administration']]);    
                    }
                    return back()->withErrors(['Makani Number Already Mapped with Other Building, for more info please contact administration'])->withInput();
                }
            }else{
                $obj = new \App\Building;
                /* Check Makani Number Already Exist*/
                $checkExistence=[];
                if($request->input('makani'))
                {
                    $checkExistence = \App\Building::where('makani', $request->input('makani'))->get();
                    $checkExistence=$checkExistence->toArray();
                }
                
                if(count($checkExistence) > 0)
                {
                    if($request->input('ajax')){
                        $errors = GeneralFunctions::error_msg_serialize($validator->errors());
                        return response()->json(['status' => 'error', 'msg_data' => ['Makani Number Already Mapped with Other Building, for more info please contact administration']]);    
                    }
                    return back()->withErrors(['Makani Number Already Mapped with Other Building, for more info please contact administration'])->withInput();
                }
            }
            $obj->name = $request->name;
            $obj->address = $request->address;
            $obj->type = $request->type;
            $obj->makani = $request->makani;
            $obj->floor = $request->floor;
            $obj->retail = $request->retail;
            $obj->office = $request->office;
            $obj->apartment = $request->apartment;
            $obj->residential_unit = $request->residential_unit;
            $obj->commercial_unit = $request->commercial_unit;
            $obj->assets = implode(', ',$request->assets);
            $obj->assets_code = implode(', ',$request->codes);
            $obj->user_id = $user_id;
            $obj->save();

            if($request->input('ajax')){
               $getAllBuilding = \App\Building::where('user_id', $user_id)->get();
               $getAllBuilding = $getAllBuilding->toArray();
               return response()->json(['status' => 'success', 'msg_data' => 'Record has been saved successfully', 'all_buildings' => $getAllBuilding]);        
           }
           return back()->with('status','Record has been saved successfully');
       } 
   }

    public function generateNumberBuilding() 
    {
        $randNumber= rand ( 10000 , 99999);
        $tempAsset=\App\Building::whereRaw("find_in_set('randNumber',assets_code)")->first();
        if($tempAsset)
        {
           return $this->generateNumberBuilding(); 
        }
            return $randNumber; 
    }

     public function GetAssetsADDByOAMNotification($asset_name,$asset, $user_id, $status = 0)
    {
        // Notification Data
        $string = implode(" ", $asset);
        $notif_title = 'Asset Add by ' . $asset_name;
        $description = 'The following Asset '  . $string . ' is added by Company which was reviewed by You.';
        // $post_id = DB::getPdo()->lastInsertId();
        //$url            = 'job_preview?id=' . $job_id;
        $url = 'assets_form';
        $association_id = 1;
        $status         = 0;
        $user_id        = 1;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id);
        // Notification End
    }

   public function buildings()
   {
    $user_id = Auth::user()->id;
    if(Auth::user()->parent_id != 0){
        $user_id = Auth::user()->parent_id;
    }

    $data = [];
    $data['title'] = 'Communities';
    $data['building'] = \App\Building::where('user_id', $user_id)->where('soft_delete', 0)->get();
    return view('oam.buildings',$data);
}

public function delete_building($id)
{
    $obj = \App\Building::where('id', $id)->update(['soft_delete' => 1]);
        // $obj->where('id', $id)->delete();
    return back()->with('status','Record has been deleted successfully');
}

public function get_building_assets(Request $request)
{
    $selectedData = [];
    $output = '';
    $assets = \App\Building::where('id',$request->id)->first();
        // GeneralFunctions::ajax_debug($assets->assets);
    if(isset($assets)){
        $final_assets = explode(', ',$assets->assets);

        ob_start();

        $i=1;
        $count = 0;
             // GeneralFunctions::ajax_debug($final_assets);
        foreach($final_assets as $row){
            $assetName= \App\Asset::where('name', $row)->first();
                                // $selectedData[$count]['id'] = $count+1;
            $selectedData[$count] = $assetName->name;

            $output = $output.'<option>'.$assetName->name.'</option>';
            $count++;
            $i++;
        }
            // $output = ob_get_contents();
            // ob_end_clean();
            // echo $output;exit;
        return response()->json(['selectedData' => $selectedData , 'options' => $output, 'building_type' =>$assets->type ]);

    }else{
        echo 0;exit;
    }
}

public function get_assets(){
    $assets = \App\Building::where('id',1)->first();
    $final_assets = explode(', ',$assets->building_assets);
    ob_start();
    $i=1;
    $count = 0;
    foreach($final_assets as $row){
        $selectedData[$count]['id'] = $count+1;
        $selectedData[$count]['text'] = $row;

                    // $output .= '<option value="'.$i.'">'.$row.'</option>';
        ?>
        <!-- <option value="">Select Assets</option> -->
        <?php
        $count++;
                // $i++;
    }
    $output = ob_get_contents();
    ob_end_clean();
                // echo $output;exit;
    return response()->json(['selected_val' => $selectedData]);
}
}

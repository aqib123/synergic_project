<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AllUser;
use DB;
use Validator;
class UserRoleController extends Controller
{

    public function save_role(Request $request)
    {
        if($request && $request->id != ''){
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255',
                'description' => 'required|max:255'
            ]);
        }else{
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:255|unique:user_roles',
                'description' => 'required|max:255'
            ]);
        }

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }else{
            if($request && $request->id != ''){
                $obj = \App\UserRoles::find($request->id);
            }else{
                $obj = new \App\UserRoles;
            }
            $obj->name = $request->name;
            $obj->description = $request->description;
            $obj->save();

            return back()->with('status','User role has been added successfully');
        } 
    }

    public function destroy($id)
    {
        $obj = new \App\UserRoles;
        $obj->where('id', '=', $id)->delete();
        return back()->with('status','Record has been deleted successfully');
    }

    public function user_role(Request $request)
    {
        $data = [];
        if($request && $request->id != ''){
            $data['title'] = 'Edit Company Role';
            $data['role'] = \App\UserRoles::find($request->id);
        }else{
            $data['title'] = 'Add Company Role';
        }
        return view('user_role', $data);  
    }

    public function users()
    {
        $obj = new \App\UserRoles;
        $users = $boj->select('name', 'id')->get();
        return view('users', compact('users'));   
    }

    public function all_user_role()
    {
        $data = [];
        $data['title'] = 'Company Roles';
        $obj = new \App\UserRoles;
        $data['all_roles'] = $obj->get();
        return view('all_user_role', $data);   
    }

}

<?php

namespace App\Http\Controllers;
use App\User;
use GeneralFunctions;
use App\Company;
use App\TransactionDetails;
use DB;
use Session;
use Validator;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;

class GeneralController extends Controller
{

    public $paypalObject = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->paypalObject = new ExpressCheckout;
    }

    /* Subscription Acoount Section*/
    public function showRenewalAccount(Request $request)
    {
        $data = [];
        $getEmailAccount = $request->input('email');

        $getDetails = User::where('email', $getEmailAccount)->select('id','name','email','image')->first();
        $data['name'] = $getDetails->name;
        $data['email'] = $getDetails->email;
        $data['title'] = 'Renewal Account';
        $data['image'] = $getDetails['image'];
        $data['uuid'] = $getDetails['id'];

        return view('general.payment_gateway',$data);
    }

    public function payment_charges($id){
        $result = User::where('id', $id)->first();
        $generateInvoiceId = 'SYN'.uniqid();

        $data = [];
        $data['items'] = [
            [
                'name' => 'Account Subscription Renewal',
                'price' => 20.22,
                'qty' => 1
            ]
        ];

        $data['invoice_id'] = $generateInvoiceId;
        $data['invoice_description'] = "Order # ".$data['invoice_id']." Invoice";
        $data['return_url'] = url('/account/subscription/payment/success');
        $data['cancel_url'] = url('/account/subscription/payment/refused');

        $total = 0;
        foreach($data['items'] as $item) {
            $total += $item['price']*$item['qty'];
        }

        $data['total'] = $total;

        $options = [
            'BRANDNAME' => 'Synergics',
            // 'LOGOIMG' => 'https://example.com/mylogo.png',
            'CHANNELTYPE' => 'Merchant'
        ];

        $response = $this->paypalObject->addOptions($options)->setExpressCheckout($data);
        // dd($response);
        if($response['ACK'] == 'Success'){
            $sessionData = [
                'token' => $response['TOKEN'],
                // 'job_id' => $addRecord->id,
                'job_title' => 'Account Renewal',
                'invoice_id' => $generateInvoiceId,
                'data' => $data,
                'id' => $id,
                'email' => $result->email
            ];

            Session::put('subscription_payment_detail',$sessionData);
        }

         // This will redirect user to PayPal
        return redirect($response['paypal_link']);

    }

    public function payment_success(Request $req){
        $getPaymentDetails = Session::get('subscription_payment_detail');
        $user_id = $getPaymentDetails['id'];
        $email_address = $getPaymentDetails['email'];
        $token = $req->get('token');
        $PayerID = $req->get('PayerID');
        // Verify Express Checkout Token
        $response = $this->paypalObject->getExpressCheckoutDetails($getPaymentDetails['token']);
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
                // Perform transaction on PayPal
                $payment_status = $this->paypalObject->doExpressCheckoutPayment($getPaymentDetails['data'], $token, $PayerID);
                if ($payment_status['ACK'] == 'Failure') {
                    Session::flash('error', $payment_status['L_SHORTMESSAGE0'].' Insufficient amount');
                    $redirect_url = '/account/subscription/payment/refused' . $user_id;
                    return redirect($redirect_url)->with('error_msg','Insufficient amount');
                }else{
                    $data = [];
                   
                    $getCompanyDetails = Company::where('user_id', $user_id)->first();
                    $subscription_account = 2; // 2 is for Monthly Subscription. and  1 is for First 3 Month Subscription
                    $subscription_count = $getCompanyDetails['subscription_count'] + 1;
                    $date_subscription = 'date_'.$subscription_count;
                    $previous_subscription_dates = json_decode($getCompanyDetails->previous_subscription_dates, true);
                    $previous_subscription_dates[$date_subscription] = date('Y-m-d H:i:s');
                    $previous_subscription_dates = json_encode($previous_subscription_dates);


                    $data['subscription_account'] = $subscription_account;
                    $data['subscription_count'] = $subscription_count;
                    $data['previous_subscription_dates'] = $previous_subscription_dates;
                    $data['last_subscription_date'] = date('Y-m-d H:i:s');
                    $data['account_status'] = 1;
                    $data['account_expiration_date'] = date('Y-m-d H:i:s', strtotime('+1 months')); 

                    $users = Company::where('user_id', $user_id)->update($data);

                   // Add Payment Transaction in Table
                    $data = [
                        'invoice_title' => $getPaymentDetails['job_title'],
                        'user_id' => $user_id,
                        'invoice_number' => $getPaymentDetails['invoice_id'],
                        'method' => 'PAYPAL',
                        'status' => 2,
                        'amount' => $payment_status['PAYMENTINFO_0_AMT'],
                        'response_data' => json_encode($payment_status)
                    ];
                    TransactionDetails::create($data);
                    Session::forget('subscription_payment_detail');

                    return redirect('account/subscription/renewal?email='.$email_address)->with('status','Your Payment Has Been Completed. WelcomeYou can Now Login to your account');
                }
        }else{

        }

    }

    public function payment_refused(Request $req){
        $getPaymentDetails = Session::get('subscription_payment_detail');
        $user_id = $getPaymentDetails['id'];
        $email_address = $getPaymentDetails['email'];
        $getToken = $req->input('token');
        Session::forget('subscription_payment_detail');
        return redirect('account/subscription/renewal?email='.$email_address)->with('error_msg','Your Payment Has Been Refused.');
    }
    /* End of Subscription Acoount Section */
}

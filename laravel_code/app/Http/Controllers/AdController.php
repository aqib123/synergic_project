<?php
namespace App\Http\Controllers;
use \App\CompanyAd;
use \App\User;
use DB;
use Illuminate\Http\Request;

class AdController extends Controller
{
    public function ads(){
        $data = [];
        $data['title'] = "Ads";
        $data['oam'] = User::select('name', 'id')->where('parent_id', 0)->where('role_id', 2)->get();
        $data['fmc'] = User::select('name', 'id')->where('parent_id', 0)->where('role_id', 3)->get();
        $data['ads'] = CompanyAd::all(); 
        return view('ads', $data);
    }

    public function assign_ads(Request $request){

        if(!isset($request->all_oam) && 
        !isset($request->all_fmc) &&
        !isset($request->oam)  &&
        !isset($request->fmc))
        {
            return back()->with('error','Please select atleat one company to assign Ad');
        }else{
            $all_ids = array();
            if(isset($request->all_oam) && $request->all_oam == 1){

                $oam = User::select('id')->where('parent_id', 0)->where('role_id', 2)->get();
                if(isset($oam) && count($oam) > 0){
                    foreach($oam as $oam_id){
                        array_push($all_ids, $oam_id->id);
                    }
                }
            }else{
                if(isset($request->oam) && count($request->oam) > 0){
                    foreach($request->oam as $oam_id){
                        array_push($all_ids, $oam_id);
                    }
                }
            }

            if(isset($request->all_fmc) && $request->all_fmc == 1){

                $fmc = User::select('id')->where('parent_id', 0)->where('role_id', 3)->get();
                if(isset($fmc) && count($fmc) > 0){
                    foreach($fmc as $fmc_id){
                        array_push($all_ids, $fmc_id->id);
                    }
                }
            }else{
                if(isset($request->fmc) && count($request->fmc) > 0){
                    foreach($request->fmc as $fmc_id){
                        array_push($all_ids, $fmc_id);
                    }
                }
            }
            foreach($all_ids as $id){
                foreach($request->image as $img){
                    DB::table('assign_ads')->insert(['company_ad_id' => $img, 'user_id' =>$id, 'status' => 1]);
                }
            }
            return back()->with('status','Record has been saved successfully');
        }
    }

    public function delete_ad($id){
        $deleted = CompanyAd::where('id', $id)->delete();
        if($deleted){
            return back()->with('status','Record has been deleted successfully');
        }
    }

    public function ad_listing(){
        $data = [];
        $data['title'] = "Ads Listing";
        $data['ads'] = CompanyAd::all();
        $data['assigned'] = DB::table('assign_ads')
        ->select('assign_ads.*', 'users.name', 'users.role_id', 'company_ads.title', 'company_ads.image')
        ->join('users', 'users.id','=','assign_ads.user_id')
        ->join('company_ads', 'assign_ads.company_ad_id','=','company_ads.id')->get();
        return view('ads_listing', $data);
    }

    public function get_ad($id){
        $params = CompanyAd::find($id);
        return response()->json(['flag' => 1, 'image' => $params->image, 'title' => $params->title]);
    }

    public function change_status($id){
        $status = DB::table('assign_ads')->where('id', $id)->first(['status']);
        $status = $status->status;
        if($status == 0){
            $uc = 1;
        }else{
            $uc = 0;
        }
        $updated = DB::table('assign_ads')->where('id', $id)->update(['status' => $uc]);
        return response()->json(['flag' => 1]);
    }

    public function remove_assigned_ad($id){
        $updated = DB::table('assign_ads')->where('id', $id)->delete();
        return back()->with('status','Record has been deleted successfully');
    }

    public function sendSms(){
        //Edit below parameters with Your Details
        $user="ONLINIST"; //your account username
        $password="ONL18460"; //your account password
        $mobilenumbers="971563642945";  //For multiple mobile numbers use comma as separator
        $message = "test messgae from synergics"; //enter Your Message 
        $senderid="ONLINIST"; //Your senderid
        $messagetype="N"; //Type Of Your Message 
        $url="http://sms.bulk-sms-cloud.me/API_SendSMS.aspx";
        //domain name: Domain name Replace With Your Domain  
        $message = urlencode($message);
        $ch = curl_init(); 
        if (!$ch){die("Couldn't initialize a cURL handle");}
        $ret = curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);          
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, 
        "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


        //If you are behind proxy then please uncomment below line and provide your proxy ip with port.
        // $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");



        $curlresponse = curl_exec($ch); // execute
        if(curl_errno($ch))
            echo 'curl error : '. curl_error($ch);

        if (empty($ret)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
            //echo "<br>";
            echo $curlresponse;    //echo "Message Sent Succesfully" ;
        
        }
    }
}

?>

<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Building;
use App\Category;
use App\Company;
use App\CompanyAd;
use App\JobApply;
use App\JobCategory;
use App\JobPostJobCategory;
use App\Jobs;
use App\JobsReview;
use App\Job_Term_Condition;
use App\MailingContent;
use App\Notification;
use App\RatingReviews;
use App\ServiceCode;
use App\SubCategory;
use App\TermAndCondition;
use App\TransactionDetails;
use App\User;
use Auth;
use DateTime;
use DB;
use Excel;
use GeneralFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Session;
use Srmklive\PayPal\Services\ExpressCheckout;
use Validator;
use \App\OamSettings;

class JobController extends Controller
{
    public $paypalObject = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->paypalObject = new ExpressCheckout;
    }

    public function jobcat_form(Request $request)
    {
        $data               = [];
        $getCategories      = Category::get();
        $data['categories'] = $getCategories;
        if ($request && $request->id != '') {
            $data['title'] = 'Edit Job Category';
            $data['job']   = \App\JobCategory::find($request->id);
        } else {
            $data['title'] = 'Add Job Category';
        }
        return view('job.job_category', $data);
    }

    public function jobcat_save(Request $request)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }
        $validator = Validator::make($request->all(), [
            'category'     => 'required',
            'sub_category' => 'required',
            'service_code' => 'required',
            'service_name' => 'required',

        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            if ($request && $request->id != '') {
                $obj                   = \App\JobCategory::find($request->id);
                $checkDataAlreadyExist = \App\JobCategory::where('category_id', $request->category)->where('sub_category_id', $request->sub_category)->where('service_code', $request->service_code)->where('service_name', $request->service_name)->where('id', '!=', $request->id);

            } else {
                $obj                   = new \App\JobCategory;
                $checkDataAlreadyExist = \App\JobCategory::where('category_id', $request->category)->where('sub_category_id', $request->sub_category)->where('service_code', $request->service_code)->where('service_name', $request->service_name);
            }
            $checkDataAlreadyExist = $checkDataAlreadyExist->get();
            $checkDataAlreadyExist = $checkDataAlreadyExist->toArray();
            if (count($checkDataAlreadyExist) > 0) {
                return back()->withErrors(['Record Already Exist']);
            }
            $obj->category_id     = $request->category;
            $obj->sub_category_id = $request->sub_category;
            $obj->service_code    = $request->service_code;
            $obj->service_name    = $request->service_name;
            $obj->save();
            return back()->with('status', 'Record has been saved successfully');
        }
    }

    public function categories()
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }

        $data               = [];
        $data['title']      = 'Categories';
        $data['categories'] = JobCategory::with('category')->with('sub_category')->where('soft_delete', 0)->get();
        // dd($data['categories']->toArray());
        return view('job.categories', $data);
    }

    public function delete_cat($id)
    {
        $obj = \App\JobCategory::where('id', $id)->update(['soft_delete' => 1]);
        // $obj->where('id', '=', $id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    public function job_form(Request $request)
    {
        $data    = [];
        $user_id = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }
        $t_c                            = TermAndCondition::where('user_id', $user_id)->where('status', 1)->get()->toArray();
        $tc                             = TermAndCondition::where('posted_by', 'Admin')->where('status', 1)->get()->toArray();
        $data['t_c']                    = array_merge($t_c, $tc);
        $data['buildings']              = Building::where('user_id', $user_id)->where('soft_delete', 0)->get();
        $data['settings']               = OamSettings::first();
        $data['job_categories']         = JobCategory::where('soft_delete', 0)->get();
        $data['new_categories']         = Category::get();
        $data['new_categories']         = $data['new_categories']->toArray();
        $data['t_and_c']                = DB::table('varification_categories')->where('status', 3)->first();
        $data['assets']                 = Asset::all();
        $data['job_t_c']                = [];
        $data['job_categories_details'] = [];

        $data['staff_list'] = User::select('id', 'name')->where('parent_id', $user_id)->where('role_id', 2)->get();
        $data['staff_list'] = $data['staff_list']->toArray();

        if ($request && $request->id != '') {
            $data['title'] = 'Edit Job';
            if (Auth::user()->role_id == 1) {
                $userData           = Jobs::where('id', $request->id)->first();
                $data['t_c']        = TermAndCondition::where('user_id', $userData['user_id'])->where('status', 1)->get();
                $data['buildings']  = Building::where('user_id', $userData['user_id'])->where('soft_delete', 0)->get();
                $data['staff_list'] = User::select('id', 'name')->where('parent_id', $userData['user_id'])->where('role_id', 2)->get();
                $data['staff_list'] = $data['staff_list']->toArray();
            }
            $data['job_t_c'] = Job_Term_Condition::where('job_id', $request->id)->get()->toArray();
            foreach ($data['job_t_c'] as $key => $value) {
                Session::put($value['title'], $value);
            }

            $data['job_t_c']                = GeneralFunctions::convert_associative_array_into_simple($data['job_t_c']);
            $data['job_categories_details'] = JobPostJobCategory::with('job_category')->where('job_post_id', $request->id)->get();
            $data['job_categories_details'] = $data['job_categories_details']->toArray();
            foreach ($data['job_categories_details'] as $key => $value) {
                /*----------  Section For Population Sub Categories  ----------*/
                $getSubDirectories                                          = SubCategory::where('category_id', $value['job_category']['category_id'])->get();
                $data['job_categories_details'][$key]['get_sub_categories'] = $getSubDirectories->toArray();
                /*----------  Subsection For population of Service Name ----------*/
                $getServiceCode                                           = JobCategory::where('category_id', $value['job_category']['category_id'])->where('sub_category_id', $value['job_category']['sub_category_id'])->where('service_code', $value['job_category']['service_code'])->get();
                $data['job_categories_details'][$key]['get_service_name'] = $getServiceCode->toArray();
            }
            // dd($data['job_categories_details']);

            $data['job']      = Jobs::find($request->id);
            $data['b_assets'] = Building::find($data['job']['building']);
        } else {
            $data['title'] = 'Add Job';
        }
        if (Auth::user()->role_id == 1) {
            return view('oam.jobs.job_post', $data);
        }
        // Check Profile Completion
        $getProfileDetails = User::where('id', $user_id)->select('profile_strength')->first();
        if ($getProfileDetails->profile_strength == '100%') {
            return view('oam.jobs.job_post', $data);
        } else {
            return back()->with('profile_not_complete', 1);
        }
    }

    public function save_job(Request $request)
    {

        if ($request && $request->id != '') {
            $obj           = \App\Jobs::find($request->id);
            $admin_user_id = $obj->user_id;
        } else {
            $obj = new \App\Jobs;
        }
        $assets = implode(',', $request->assets);

        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id          = Auth::user()->id;
            $oam_staff_status = 0;
            $oam_staff_id     = 0;
        } else if (Auth::user()->parent_id != 0) {
            $oam_staff_status = 1;
            $oam_staff_id     = Auth::user()->id;
        }

        if (Auth::user()->role_id == 1) {
            $user_id = $admin_user_id;
        }

        $obj->job_title           = $request->job_title;
        $obj->building            = $request->building;
        $obj->contact_name        = json_encode(GeneralFunctions::array_format($request->focal_name));
        $obj->contact_email       = json_encode(GeneralFunctions::array_format($request->focal_email));
        $obj->job_type            = $request->job_type;
        $obj->building_type       = $request->building_type;
        $obj->assets              = $assets;
        $obj->purposal_validity   = $request->purposal_validity;
        $obj->payment_terms       = $request->payment_terms;
        $obj->staffing            = $request->staffing;
        $obj->uniform             = $request->uniform;
        $obj->oam_staff_status    = $oam_staff_status;
        $obj->oam_staff_id        = $oam_staff_id;
        $obj->user_id             = $user_id;
        $obj->contact_number      = json_encode(GeneralFunctions::array_format($request->contact_number));
        $obj->description         = $request->job_description;
        $obj->payment_term        = json_encode($request->input('term_description'));
        $obj->payment_percentages = json_encode($request->input('percentage'));
        $obj->staffing_status     = $request->staffing_value;
        $obj->uniform_status      = $request->uniform_value;
        $obj->job_expiry_days     = $request->job_expiry;
        $obj->job_status          = 2;
        $obj->save();
        $terms = $request->terms;
        if ($request && $request->id != '') {
            DB::table('job_term_conditions')->where('job_id', $request->id)->delete();
            JobPostJobCategory::where('job_post_id', $request->id)->delete();
            $job_id = $request->id;
            if ($obj->admin_approval == 3) {
                $this->GetJobUpdatetByOAMNotification($request->job_title, $job_id, $user_id, $status = 0);
            } else {
                $this->GetJobUpdatetNotification($request->job_title, $job_id, $user_id, $status = 0);
            }
        } else {
            $job_id = DB::getPdo()->lastInsertId();
            $this->NewJobNotification($request->job_title, $job_id, $user_id, $status = 0);
        }

 
        foreach ($request->input('service_name') as $key => $value) {
            JobPostJobCategory::create(['job_post_id' => $job_id, 'job_category_id' => $value]);
        }

        // For Posting New Job We will do the following steps to save the Terms and Conditions.
        if ($request->input('job_terms_selected')) {
            if (count($request->input('job_terms_selected')) > 0) {
                foreach ($request->input('job_terms_selected') as $key => $value) {
                    if ($value == 1 || $value == 2) {
                        $terms_condition_details = Session::get($key);

                        $dataArray               = [
                            'title'       => $terms_condition_details['title'],
                            'description' => $terms_condition_details['description'],
                            'job_id'      => $job_id,
                            'status'      => $value,
                        ];
                        DB::table('job_term_conditions')->insert($dataArray);
                        Session::forget($key);
                    }
                }
            }
        }
        return back()->with('status', 'Your Job Post has been sent to Administration for Approval, After Approvale Post will Updated.');
    }

    public function delete_job($id)
    {
        $obj = new Jobs;
        $obj->where('id', $id)->where('admin_approval', 0)->delete();
        return back()->with('status', 'Job has been removed successfully');
    }

    public function jobs()
    {
        $data          = [];
        $data['title'] = 'Posted Jobs list';
        $user_id       = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $creator          = User::where('id', $user_id)->first(['creator_name']);
        $image            = CompanyAd::where('id', 2)->first(['image']);
        $data['ad_image'] = $image->image;
        $data['creator']  = $creator->creator_name;
        $data['jobs']     = Jobs::with('user_tooltip')->where('user_id', $user_id)->withCount('jobApply')->orderBy('created_at', 'desc')->get();
        if (Auth::user()->role_id == 1) {
            $data['jobs'] = Jobs::withCount('jobApply')->orderBy('created_at', 'desc')->get();
            // dd($data['jobs']);
        }
 //dd($data);
        return view('oam.jobs.jobs', $data);
    }

    public function job_detail()
    {
        $data          = [];
        $data['title'] = 'Job Detail';
        return view('oam.jobs.job_detail', $data);
    }

    // Job List With Specific Job Category
    public function getJobList(Request $req)
    {
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        } else {
            $user_id = Auth::user()->parent_id;
        }
        $data['allJobs']      = [];
        $data['jobs']         = Jobs::with('job_categories')->where('admin_approval', 1)->orderBy('created_at', 'desc')->get();
        $data['jobs']         = $data['jobs']->toArray();
        $getCompanyCategories = User::with('job_categories')->with('companies')->where('id', $user_id)->first();
        if ($getCompanyCategories) {
            $getCompanyCategories = $getCompanyCategories->toArray();
            if ($getCompanyCategories['profile_strength'] != '100%') {
                $getCompanyCategories = [];
            } else {
                $data['allJobs'] = Jobs::where('admin_approval', 1)->orderBy('created_at', 'desc')->get()->toArray();
            }
        } else {
            $getCompanyCategories = [];
        }
        // Check the specific jobs related to given job categories.
        $dataArray = [];
        $count     = 0;
        if (count($getCompanyCategories) > 0) {
            foreach ($getCompanyCategories['job_categories'] as $key => $value) {
                // check Category in Jobs As well and Select those Jobs
                foreach ($data['jobs'] as $jobsKey => $jobsValue) {
                    foreach ($jobsValue['job_categories'] as $innerKey => $innerValue) {
                        if ($value['job_category_id'] == $innerValue['job_category_id']) {
                            $dataArray[$count]['id']              = $jobsValue['id'];
                            $dataArray[$count]['job_title']       = $jobsValue['job_title'];
                            $dataArray[$count]['date']            = $jobsValue['created_at'];
                            $dataArray[$count]['job_description'] = $jobsValue['description'];
                            $dataArray[$count]['count_views']     = $jobsValue['count_views'];
                            $count++;
                        }
                    }
                }
            }
        }
        $dataArray = $this->filterUniqueData($dataArray);
        $tempDataArray=[];
        foreach ($dataArray as $key => $row)
        {
            $tempDataArray[$key] = $row['date'];
        }
        array_multisort($tempDataArray,SORT_DESC, $dataArray);
        // $getJobList = $getJobList->toArray();
        $data['data']           = $dataArray;
        $data['title']          = 'Job List';
        $data['job_categories'] = JobCategory::all();
        $data['job_categories'] = $data['job_categories']->toArray();
        return view('fmc.jobs.job_listing', $data);
    }


    public function filterUniqueData($ArrayData)
    {
        $ArrayData = array_values(array_unique($ArrayData, SORT_REGULAR));
        return $ArrayData;
    }

    public function payment_page($id)
    {
        $user=Auth::user();
        // check if already applied for this Job
        $result  = Jobs::where('id', $id)->first();
        $counter = $result->count_views + 1;
        Jobs::where('id', $id)->update(['count_views' => $counter]);
        $data                    = [];
        $status                  = 0;
        $job_applied_status      = 0;
        $inspection_date         = '';
        $report_status           = 0;
        $uploaded_date           = '';
        $uploaded_path           = '';
        $site_visit_request      = null;
        $revise_quotation_status = 0;
        $quotation_amount        = '';
        $invoice_reporting_path  = '';
        $user_id                 = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $checkStatus = JobApply::where('job_id', $id)->where('user_id', $user_id)->get();
        $checkStatus = $checkStatus->toArray();
        if (count($checkStatus) > 0) {
            $status                  = 1;
            $job_applied_status      = $checkStatus[0]['status'];
            $inspection_date         = $checkStatus[0]['inspection_date'];
            $report_status           = $checkStatus[0]['report_status'];
            $uploaded_date           = $checkStatus[0]['uploaded_date'];
            $uploaded_path           = $checkStatus[0]['engineering_report_path'];
            $site_visit_request      = $checkStatus[0]['site_visit_request'];
            $revise_quotation_status = $checkStatus[0]['revise_quotation_status'];
            $quotation_amount        = $checkStatus[0]['quotation_amount'];
            $invoice_reporting_path  = $checkStatus[0]['invoice_report_path'];
        }
        $data['category']     = JobCategory::all();
        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();
        $data['detail']            = Jobs::with('JobApply')->where('id', $id)->first();
        // dd($data['detail']);
        $data['building']                = Building::where('id', $data['detail']['building'])->first();
        $data['title']                   = 'Job Details';
        $data['job_applied_status']      = $job_applied_status;
        $data['inspection_date']         = $inspection_date;
        $data['status']                  = $status;
        $data['report_status']           = $report_status;
        $data['uploaded_date']           = $uploaded_date;
        $data['getTermsAndConditions']   = Job_Term_Condition::where('job_id', $id)->get();
        $data['getTermsAndConditions']   = $data['getTermsAndConditions']->toArray();
        $data['uploaded_path']           = $uploaded_path;
        $data['site_visit_request']      = $site_visit_request;
        $data['revise_quotation_status'] = $revise_quotation_status;
        $data['quotation_amount']        = $quotation_amount;
        $data['invoice_reporting_path']  = $invoice_reporting_path;
        $data['user']=$user->role_id;
        $data['company_details'] = Company::where('user_id', GeneralFunctions::getMasterId())->first();
        $data['license_expiry_left'] = null;
        if ($data['company_details']->license_expiry != null) {
         
            $getNoOfDays                 = GeneralFunctions::difference_bwt_two_dated($data['company_details']->license_expiry, time());
            $data['license_expiry_left'] = $getNoOfDays;
        }
        // return view('fmc.jobs.job_description',['result' => $result, 'status' => $status, 'job_applied_status' => $job_applied_status, 'data' => $data]);
        return view('fmc.jobs.job_description', $data);
    }

    public function payment_charges($id)
    {
        $result            = Jobs::where('id', $id)->first();
        $generateInvoiceId = 'SYN' . uniqid();

        $data          = [];
        $data['items'] = [
            [
                'name'  => $result->job_title,
                'price' => 9.99,
                'qty'   => 1,
            ],
        ];

        $data['invoice_id']          = $generateInvoiceId;
        $data['invoice_description'] = "Order # " . $data['invoice_id'] . " Invoice";
        $data['return_url']          = url('/fmc/payment/success');
        $data['cancel_url']          = url('/fmc/payment/refused');

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['total'] = $total;

        $options = [
            'BRANDNAME'   => 'Synergics',
            // 'LOGOIMG' => 'https://example.com/mylogo.png',
            'CHANNELTYPE' => 'Merchant',
        ];

        $response = $this->paypalObject->addOptions($options)->setExpressCheckout($data);
        // dd($response);
        if ($response['ACK'] == 'Success') {
            $sessionData = [
                'token'      => $response['TOKEN'],
                // 'job_id' => $addRecord->id,
                'job_title'  => $result->job_title,
                'invoice_id' => $generateInvoiceId,
                'data'       => $data,
                'id'         => $id,
            ];

            Session::put('job_payment_detail', $sessionData);
        }

        // This will redirect user to PayPal
        return redirect($response['paypal_link']);

    }

    public function payment_success(Request $req)
    {
        $getPaymentDetails = Session::get('job_payment_detail');
        $job_id            = $getPaymentDetails['id'];
        $token             = $req->get('token');
        $PayerID           = $req->get('PayerID');
        // Verify Express Checkout Token
        $response = $this->paypalObject->getExpressCheckoutDetails($getPaymentDetails['token']);
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            // Perform transaction on PayPal
            $user_id = Auth::user()->parent_id;
            if (Auth::user()->parent_id == 0) {
                $user_id = Auth::user()->id;
            }
            $payment_status = $this->paypalObject->doExpressCheckoutPayment($getPaymentDetails['data'], $token, $PayerID);
            if ($payment_status['ACK'] == 'Failure') {
                Session::flash('error', $payment_status['L_SHORTMESSAGE0'] . ' Insufficient amount');
                $redirect_url = '/fmc/payment/refused' . $job_id;
                return redirect($redirect_url)->with('error_msg', 'Insufficient amount');
            } else {
                $dataAdding = [
                    'job_id'  => $getPaymentDetails['id'],
                    'user_id' => $user_id,
                    'status'  => 0,
                ];
                $addRecord = JobApply::create($dataAdding);

                // Add Payment Transaction in Table
                $data = [
                    'invoice_title'  => $getPaymentDetails['job_title'],
                    'user_id'        => $user_id,
                    'invoice_number' => $getPaymentDetails['invoice_id'],
                    'method'         => 'PAYPAL',
                    'status'         => 1,
                    'amount'         => $payment_status['PAYMENTINFO_0_AMT'],
                    'response_data'  => json_encode($payment_status),
                ];
                TransactionDetails::create($data);
                Session::forget('job_payment_detail');

                return redirect('fmc/payment_screen/' . $job_id)->with('status', 'Your Payment Has Been Completed. Now you can Apply for this Job');
            }
        } else {
        }
    }

    public function payment_refused(Request $req)
    {

        $getToken     = $req->input('token');
        $getRecord    = JobApply::where('response_token', $getToken)->select('job_id')->first();
        $deleteRecord = JobApply::where('response_token', $getToken)->delete();

        return redirect('fmc/payment_screen/' . $getRecord->job_id)->with('error_msg', 'Your Payment Has Been Refused.');
    }

    public function getProjectDetails(Request $req)
    {
        $getJobDetails = $result = Jobs::where('id', $req->input('data'))->with('companies')->first();
        return response()->json(['data' => $getJobDetails->toArray()]);
    }

    public function job_post_validation(Request $request)
    {
        $errorStatus = 0;
        $errors      = [];
        if ($request->page == 1) {
            $validationArray['job_title']         = 'required|max:255';
            $validationArray['purposal_validity'] = 'required|numeric|min:1';
            $validationArray['purposal_validity'] = 'required|numeric|min:1';
            $validationArray['job_reporting']     = 'required';
            $validationArray['job_type']          = 'required|max:255';
            $validationArray['job_description']   = 'required';
            $validationArray['job_expiry']        = 'required';
            $validator                            = Validator::make($request->all(), $validationArray);
            $errors= GeneralFunctions::error_msg_serialize($validator->errors());

            $errorCategory    = 1;
            $errorSubCategory = 1;
            $errorServiceCode = 1;
            $errorServiceName = 1;

            foreach ($request->input('category') as $key => $value) {
                if ($value == null) {
                    $errorCategory = 0;
                }
                if ($request->input('sub_category')[$key] == null) {
                    $errorSubCategory = 0;
                }

                if ($request->input('service_name')[$key] == null) {
                    $errorServiceName = 0;
                }
            }

            if ($errorSubCategory == 0) {
                array_push($errors, "Sub Category is required");
            }
            if ($errorServiceCode == 0) {
                array_push($errors, "Service Code is required");
            }
            if ($errorServiceName == 0) {
                array_push($errors, "Service Name is required");
            }
            if ($errorCategory == 0) {
                array_push($errors, "Category is required");
            }

          
            if (count($errors) > 0) {
                return response()->json(['status' => 'error', 'msg_data' => $errors]);
            }
           

            return response()->json(['status' => 'success']);
            //////
            // if ($validator->fails()) {
            //     // GeneralFunctions::ajax_debug($request->input('terms_and_conditions'));
            //     return response()->json(['status' => 'error', 'msg_data' => $errors]);
            // }
            // return response()->json(['status' => 'success']);
        } elseif ($request->page == 2) {
            $validationArray['assets']   = 'required|max:255';
            $validationArray['building'] = 'required';
            $validationArray['building'] = 'required';
            $validationArray['focal_name']     = 'required';
            $validationArray['focal_email']    = 'required';
            $validationArray['contact_number'] = 'required';
            $validator                   = Validator::make($request->all(), $validationArray);
            $errors                      = GeneralFunctions::error_msg_serialize($validator->errors());
            if ($validator->fails()) {
                // GeneralFunctions::ajax_debug($request->input('terms_and_conditions'));
                return response()->json(['status' => 'error', 'msg_data' => $errors]);
            }
            return response()->json(['status' => 'success']);
        } elseif ($request->page == 3) {
            $errorPercentage                  = 1;
            $errorDescription                 = 1;
            $validationArray['payment_terms'] = 'required';
            foreach ($request->input('percentage') as $key => $value) {
                if ($value == null) {
                    $errorPercentage = 0;
                }
                if ($request->input('term_description')[$key] == null) {
                    $errorDescription = 0;
                }
            }
            $validator = Validator::make($request->all(), $validationArray);
            $errors    = GeneralFunctions::error_msg_serialize($validator->errors());
            if ($validator->fails()) {
                // GeneralFunctions::ajax_debug($request->input('terms_and_conditions'));
                return response()->json(['status' => 'error', 'msg_data' => $errors]);
            }
            if (count($errors) > 0) {
                return response()->json(['status' => 'error', 'msg_data' => $errors]);
            }

            return response()->json(['status' => 'success']);
        } else {
             if ($request->input('terms_con_flag') == 0 && $request->id == '') {
                array_push($errors, "Please agree to terms and conditions first");
            }
            if (count($errors) > 0) {
                return response()->json(['status' => 'error', 'msg_data' => $errors]);
            }
              $job_categories_details = [];
            foreach ($request->input('category') as $key => $value) {
                $category                                          = Category::where('id', $value)->first();
                $job_categories_details[$key]['category_name']     = $category->name;
                $subCategory                                       = SubCategory::where('id', $request->input('sub_category')[$key])->first();
                $job_categories_details[$key]['sub_category_name'] = $subCategory->sub_category;

                $serviceName                                  = JobCategory::where('id', $request->input('service_name')[$key])->first();
                $job_categories_details[$key]['service_name'] = $serviceName->service_name;
            }
              return response()->json(['status' => 'success', 'data' => $request->all(), 'job_categories_details' => $job_categories_details]);
           
        }
    }

    public function job_listing()
    {
        return view('fmc.jobs.job_listing');
    }
    public function job_detail_fmc()
    {
        return view('fmc.jobs.job_detail');
    }
    public function job_review()
    {
        return view('oam.jobs.job_review');
    }
    public function admin_job_list()
    {
        $data          = [];
        $data['title'] = "All Jobs";
        $getAllJobs    = Jobs::with('companies')->orderBy('created_at', 'desc')->get();
        $data['jobs']  = $getAllJobs->toArray();
        return view('admin_job_list', $data);
    }

    public function job_preview()
    {
        $data                 = [];
        $job_id               = Input::get('id');
        $data['title']        = "Preview Job";
        $data['category']     = JobCategory::all();
        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $job_id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();
        //print_r($data['job_category_name']); exit;
        $data['detail']                = Jobs::where('id', $job_id)->first();
        $data['getTermsAndConditions'] = Job_Term_Condition::where('job_id', $job_id)->get();
        $data['getTermsAndConditions'] = $data['getTermsAndConditions']->toArray();
        $data['building']              = Building::where('id', $data['detail']['building'])->first();
        // dd($data);
        return view('job_preview', $data);
    }

    public function update_job_status(Request $request, $job_id, $user_id)
    {
        // dd($user_id);
        $data   = [];
        $reason = null;
        $data   = [
            'admin_approval' => 'required',
        ];

        if ($request->input('admin_approval') == 2) {
            $data['disapproved_reason'] = 'required';
            $reason                     = $request->input('disapproved_reason');
        }
        if ($request->input('admin_approval') == 1) {
            $reason = "";
        }

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user_email = User::with('companies')->where('id', $user_id)->first();

        $update_data = array(
            'admin_approval'      => $request->input('admin_approval'),
            'disapproved_reason'  => $reason,
            'admin_approval_date' => date('Y-m-d H:i:s', strtotime('now')),
        );
        $job_details = Jobs::where('id', $job_id)->update($update_data);
        $job_details = Jobs::where('id', $job_id)->first();

        // 1 for approved job and 2 for reject
        if ($request->input('admin_approval') == 1) {
            /*
            Title, Job ID, $job_details->user_id = Job Owner Company(OAM), $user_id = Association_id, status=0
             */

            $this->GetJobApprovedNotification($request->job_title, $job_id, $job_details->user_id, $status = 0);

        } else if ($request->input('admin_approval') == 2) {
            /*
            Title, Job ID, Job Owner Company(OAM), $job_details->user_id = $user_id = Association_id, status=0
             */
            $this->GetJobRejectNotification($request->job_title, $job_id, $job_details->user_id, $status = 0);
        }
        /*----------  Send Email Dynamically with Checking if Package is Active  ----------*/
        if ($request->input('admin_approval') == 1) {
            if (GeneralFunctions::checkingForPackageAccountCurrentlyForSystemService('email-service', 1, $job_details->user_id)) {
                // Get Mailing Content
                $getMailRecord = MailingContent::where('status', 1)->first();

                $data = [
                    'subject'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->title, $user_email->companies->name, $job_details->job_title),
                    'heading_details' => GeneralFunctions::replaceWordsWithStrings($getMailRecord->heading, $user_email->companies->name, $job_details->job_title),
                    'sub_heading'     => GeneralFunctions::replaceWordsWithStrings($getMailRecord->sub_heading, $user_email->companies->name, $job_details->job_title),
                    'heading'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->short_description, $user_email->companies->name, $job_details->job_title),
                    'content'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->full_description, $user_email->companies->name, $job_details->job_title),
                    'job_title'       => GeneralFunctions::replaceWordsWithStrings($job_details->job_title, $user_email->companies->name, $job_details->job_title),
                    'email'           => $user_email->email,
                    'company_name'    => $user_email->companies->name,
                ];
                $this->send_email_dynamically($data);
            }
        } else {
            if (GeneralFunctions::checkingForPackageAccountCurrentlyForSystemService('email-service', 2, $job_details->user_id)) {
                // Get Mailing Content
                $getMailRecord = MailingContent::where('status', 2)->first();

                $data = [
                    'subject'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->title, $user_email->companies->name, $job_details->job_title),
                    'heading_details' => GeneralFunctions::replaceWordsWithStrings($getMailRecord->heading, $user_email->companies->name, $job_details->job_title),
                    'sub_heading'     => GeneralFunctions::replaceWordsWithStrings($getMailRecord->sub_heading, $user_email->companies->name, $job_details->job_title),
                    'heading'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->short_description, $user_email->companies->name, $job_details->job_title),
                    'content'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->full_description, $user_email->companies->name, $job_details->job_title),
                    'job_title'       => GeneralFunctions::replaceWordsWithStrings($job_details->job_title, $user_email->companies->name, $job_details->job_title),
                    'email'           => $user_email->email,
                    'company_name'    => $user_email->companies->name,
                ];
                $this->send_email_dynamically($data);
            }
        }
        return redirect('admin_job_list')->with('status', 'Successfully Updated the status of Job');
    }

    public function apply_for_job(Request $req)
    {
        /* Applying for the Job after paying the charges */
        $status  = $req->input('status');
        $job_id  = $req->input('uuid');
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }

        $data['company']             = DB::table('company')->where('user_id', $user_id)->first();
        $data['license_expiry_left'] = null;
        if ($data['company']->license_expiry != null) {
            $getNoOfDays = GeneralFunctions::difference_bwt_two_dated($data['company']->license_expiry, time());
            if ($getNoOfDays <= 0) {
                return back()->withErrors(['Sorry, your trade license is expired. Please Validate the License first.']);
            }
        }

        $dataAdding = [
            'job_id'  => $job_id,
            'user_id' => $user_id,
            'status'  => $status,
        ];
        if ($req->site_visit_options == 2) {
            $dataAdding['site_visit_request'] = 1;
        } else {
            if ($req->hasFile('apply_engineeering_report')) {
                $report_path = $req->apply_engineeering_report->getClientOriginalName();
                $req->file('apply_engineeering_report')->storeAs('public/engineeering_report', $report_path);
                $dataAdding['engineering_report_path'] = $report_path;
                $dataAdding['report_status']           = 1;
            }
            $dataAdding['quotation_amount']             = $req->apply_quotation_amount;
            $dataAdding['uploaded_date']                = date('Y-m-d H:i:s', strtotime('now'));
            $dataAdding['proposal_validity_start_date'] = date('Y-m-d H:i:s', strtotime('now'));
        }
        $checkIfExist = JobApply::where('job_id', $job_id)->where('user_id', $user_id)->get();
        if (count($checkIfExist->toArray()) == 0) {
            $addRecord = JobApply::create($dataAdding);
        }
        $getCompanyDetails     = Company::where('user_id', $user_id)->select('name')->first();
        $get_oam_email_address = Jobs::with('users')->where('id', $job_id)->first();

        $data = [
            'subject'         => 'Job application from New Company',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Job Post Deatils',
            'heading_details' => 'Applied Application for Job',
            'job_title'       => $get_oam_email_address->job_title,
            'content'         => 'Company <u>' . $getCompanyDetails->name . '</u> has applied for the job <u>' . $get_oam_email_address->job_title . '</u> that was posted on ' . date_format(new DateTime($get_oam_email_address->created_at), 'jS F Y g:ia') . ' . You can check in Job details from the portal for full details.',
            'email'           => $get_oam_email_address->users->email,
            'company_name'    => $getCompanyDetails->name,
        ];
        $this->AppliedJobNotification($job_id, $get_oam_email_address->job_title, $get_oam_email_address->user_id);
        if ($status == 1) {
            $notif_title    = $getCompanyDetails->name . ' applied for job ' . $get_oam_email_address->job_title;
            $description    = 'Company applied on the following Job ' . $get_oam_email_address->job_title;
            $url            = 'oam/oam_job_details/' . $job_id;
            $association_id = 2;
            $status         = 0;
            $job_details    = Jobs::where('id', $job_id)->first();
            GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id, $job_id);
            $this->send_email_dynamically($data);
            //Change job status to " applied "
            // 0 => admin approved, 1 => Job complete, 2 => Pending for approval, 3 => applied, 4 => short list, 5 => assgin to FMC
            $job_status = $get_oam_email_address->job_status;
            if ($job_status != 1 || $job_status != 4 || $job_status != 5) {
                Jobs::where('id', $job_id)->update(['job_status' => 3]);
            }
            return redirect('fmc/payment_screen/' . $job_id)->with('status', 'Successfully! You have Applied for this Job. You will be prompt if you get short Listed for this Job');
        }
        return redirect('fmc/payment_screen/' . $job_id)->with('status', 'You have Declined this job');

    }

    public function send_email_dynamically(array $data)
    {
        Mail::send('emails.dummy', $data, function ($message) use ($data) {
            $message->from(env('COMPANY_EMAIL_ADDRESS', 'noreply@zilion.co'), 'Synergic');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });
    }

    public function oam_job_details($id)
    {
        $user=Auth::user();
        $data = [];
        $data['user']=$user->role_id;
        $jobApplicationRecord = JobApply::with('companies')->with('users')->where('job_id', $id)->where('status', '!=', 2)->get();
        // dd($jobApplicationRecord->toArray());

        $jobApplicationRecord = $jobApplicationRecord->toArray();
        $jobApplicationCount  = count($jobApplicationRecord);

        // Sort Listed For this Job
        $shortListCount       = JobApply::where('job_id', $id)->where('status', 3)->get();
        $shortListCount       = count($shortListCount->toArray());
        $jobApplicationRecord = JobApply::with('companies')->with('users')->where('job_id', $id)->orderBy('status', 'DESC')->get();

        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();

        $data['detail']                = Jobs::with('jobReview')->where('id', $id)->first();
        $data['building']              = Building::where('id', $data['detail']['building'])->first();
        $data['title']                 = 'Job Details';
        $data['jobApplicationRecord']  = $jobApplicationRecord;
        $data['jobApplicationCount']   = $jobApplicationCount;
        $data['getTermsAndConditions'] = Job_Term_Condition::where('job_id', $id)->get();
        $data['getTermsAndConditions'] = $data['getTermsAndConditions']->toArray();
        $data['shortListCount']        = $shortListCount;
        //dd($data);

        return view('fmc.jobs.job_description', $data);

    }

    public function short_list(Request $req)
    {
        $updateJobStatus = JobApply::where('user_id', $req->input('user_uuid'))->where('job_id', $req->input('job_id'))->update(['inspection_date' => $req->input('sl_inspection_date'), 'status' => 3]);

        $jobDetail         = Jobs::with('users')->where('id', $req->input('job_id'))->first();
        $getCompanyDetails = Company::with('users')->where('user_id', $req->input('user_uuid'))->first();
        /*
        Notification Data for short list FMC
         */
        $this->ShortListNotification($jobDetail->job_title, $jobDetail->id, $getCompanyDetails->user_id, $status = 0, $getCompanyDetails->user_id);
        if (GeneralFunctions::checkingForPackageAccountCurrentlyForSystemService('email-service', 3, $jobDetail->user_id)) {
            // Get Mailing Content
            $getMailRecord = MailingContent::where('status', 3)->first();

            $data = [
                'subject'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->title, $getCompanyDetails->name, $jobDetail->job_title),
                'heading_details' => GeneralFunctions::replaceWordsWithStrings($getMailRecord->heading, $getCompanyDetails->name, $jobDetail->job_title),
                'sub_heading'     => GeneralFunctions::replaceWordsWithStrings($getMailRecord->sub_heading, $getCompanyDetails->name, $jobDetail->job_title),
                'heading'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->short_description, $getCompanyDetails->name, $jobDetail->job_title),
                'content'         => GeneralFunctions::replaceWordsWithStrings($getMailRecord->full_description, $getCompanyDetails->name, $jobDetail->job_title),
                'job_title'       => GeneralFunctions::replaceWordsWithStrings($jobDetail->job_title, $getCompanyDetails->name, $jobDetail->job_title),
                'email'           => $getCompanyDetails->users->email,
                'company_name'    => $getCompanyDetails->name,
            ];
            $this->send_email_dynamically($data);
        }
        $data = [
            'subject'         => 'Short List for Job application',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Job Post Deatils',
            'heading_details' => 'Short List for Job',
            'job_title'       => $jobDetail->job_title,
            'content'         => 'Congratulationss! Company <u>' . $getCompanyDetails->name . '</u> u have been Short Listed for this job <u>' . $jobDetail->job_title . '</u> that was posted on ' . date_format(new DateTime($jobDetail->created_at), 'jS F Y g:ia') . ' . You can check in Job details from the portal for full details.',
            'email'           => $getCompanyDetails->users->email,
        ];

        $this->send_email_dynamically($data);
        //Change job status to " short list "
        // 0 => admin approved
        // 1 => Job complete,
        // 2 => Pending for approval,
        // 3 => applied,
        // 4 => short list,
        // 5 => assgin to FMC
        $job_status = $jobDetail->job_status;
        if ($job_status != 1 || $job_status != 5) {
            Jobs::where('id', $jobDetail->id)->update(['job_status' => 4]);
        }
        return back()->with('status', 'Successfully short listed Company ' . $req->input('sl_company_name'));
    }

    public function rejected_company(Request $req)
    {
        // dd($req->all());
        $updateJobStatus   = JobApply::where('user_id', $req->input('rl_user_uuid'))->where('job_id', $req->input('job_id'))->update(['status' => 5]);
        $jobDetail         = Jobs::with('users')->where('id', $req->input('job_id'))->first();
        $getCompanyDetails = Company::with('users')->where('user_id', $req->input('rl_user_uuid'))->first();

        $data = [
            'subject'         => 'Rejected for Job application',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Job Post Deatils',
            'heading_details' => 'Rejection for Job',
            'job_title'       => $jobDetail->job_title,
            'content'         => 'Sorry! Company <u>' . $getCompanyDetails->name . '</u> u have been Rejected for this job <u>' . $jobDetail->job_title . '</u> that was posted on ' . date_format(new DateTime($jobDetail->created_at), 'jS F Y g:ia') . ' . You can check in Job details from the portal for full details.',
            'email'           => $getCompanyDetails->users->email,
        ];

        $this->send_email_dynamically($data);
        return back()->with('status', 'Successfully Rejected the Company ' . $req->input('sl_company_name'));
    }

    public function upload_engineering_report(Request $req)
    {
        $report_path = '';
        if ($req->hasFile('engineeering_report')) {
            $report_path = $req->engineeering_report->getClientOriginalName();
            $req->file('engineeering_report')->storeAs('public/engineeering_report', $report_path);
            // $obj->image = $report_path;
        }
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }

        $updateJobStatus = JobApply::where('user_id', $user_id)->where('job_id', $req->input('job_id'))->update(['engineering_report_path' => $report_path, 'report_status' => 1, 'uploaded_date' => date('Y-m-d H:i:s', strtotime('now')), 'quotation_amount' => $req->input('quotation_amount'), 'proposal_validity_start_date' => date('Y-m-d H:i:s', strtotime('now'))]);
        return back()->with('status', 'Successfully Uploaded the Engineering Report ');
    }

    public function assign_company_job(Request $req)
    {
        $updateJobStatus   = JobApply::where('user_id', $req->input('assign_user_uuid'))->where('job_id', $req->input('job_id'))->update(['status' => 4]);
        $jobDetail         = Jobs::with('users')->where('id', $req->input('job_id'))->first();
        $getCompanyDetails = Company::where('user_id', $req->input('assign_user_uuid'))->first();

        $this->JobAssignedNotification($jobDetail->job_title, $jobDetail->id, $getCompanyDetails->user_id, $status = 0, $getCompanyDetails->user_id);

        $data = [
            'subject'         => 'Job application Approved',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Job Post Deatils',
            'heading_details' => 'Approval Application for Job',
            'job_title'       => $jobDetail->job_title,
            'content'         => 'Congratulationss! Company <u>' . $getCompanyDetails->name . '</u> u have been Approved for this job <u>' . $jobDetail->job_title . '</u> that was posted on ' . date_format(new DateTime($jobDetail->created_at), 'jS F Y g:ia') . ' . You can check in Job details from the portal for full details.',
            'email'           => $jobDetail->users->email,
        ];

        $this->send_email_dynamically($data);
        /*
        Change job status to " Assign "
        0 => admin approved,
        1 => Job complete,
        2 => Pending for approval,
        3 => applied,
        4 => short list,
        5 => assgin to FMC
         */
        $job_status = $jobDetail->job_status;
        if ($job_status != 1) {
            Jobs::where('id', $jobDetail->id)->update(['job_status' => 5]);
        }
        return back()->with('status', 'Successfully! You have Assigned the Job.');

    }

    public function award_company_job(Request $req)
    {
        $updateJobStatus   = JobApply::where('user_id', $req->input('awarding_user_uuid'))->where('job_id', $req->input('job_id'))->update(['status' => 8, 'job_working_start_date' => date('Y-m-d H:i:s', strtotime('now'))]);
        $jobDetail         = Jobs::with('users')->where('id', $req->input('job_id'))->first();
        $getCompanyDetails = Company::where('user_id', $req->input('awarding_user_uuid'))->first();
        // JobAssignedNotification
        $this->JobAwardNotification($jobDetail->job_title, $jobDetail->id, $getCompanyDetails->user_id, $status = 0, $getCompanyDetails->user_id);

        $data = [
            'subject'         => 'Job application Awarded',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Job Post Deatils',
            'heading_details' => 'Congratulations, You have beeen selected for the Job',
            'job_title'       => $jobDetail->job_title,
            'content'         => 'Congratulationss! Company <u>' . $getCompanyDetails->name . '</u> u have been Seleted for this job <u>' . $jobDetail->job_title . '</u> that was posted on ' . date_format(new DateTime($jobDetail->created_at), 'jS F Y g:ia') . ' . You can check in Job details from the portal for full details.',
            'email'           => $jobDetail->users->email,
        ];

        $this->send_email_dynamically($data);
        $job_status = $jobDetail->job_status;
        if ($job_status != 1) {
            Jobs::where('id', $jobDetail->id)->update(['job_status' => 5]);
        }
        return back()->with('status', 'Successfully! You have Assigned the Job.');
    }

    public function get_fmc_company_details($id)
    {
        $getCompanyDetails       = Company::with('users')->where('user_id', $id)->first();
        $data['company_details'] = $getCompanyDetails;
        $data['title']           = 'Company Profile';
        $data['job_category']    = explode(',', $getCompanyDetails->job_categories);

        foreach ($data['job_category'] as $key => $value) {

            $category = JobCategory::where('id', $value)->first();
            if ($category) {
                $data['job_category'][$key] = $category->service_name;
            }
        }
        $data['getAllReviews'] = RatingReviews::with('oam_company.users')->with('job')->where('fmc_company_id', $id)->orderBy('created_at', 'desc')->get();
        $data['getAllReviews'] = $data['getAllReviews']->toArray();
        //dd($data);
        return view('oam.fmc_profile_detail', $data);
    }
    public function get_fmc_company_details_ajax(Request $req)
    {
        $getCompanyDetails       = Company::with('users')->where('user_id', $req->user_id)->first();
        $data['company_details'] = $getCompanyDetails;
        $data['title']           = 'Company Profile';
        $data['job_category']    = explode(',', $getCompanyDetails->job_categories);

        foreach ($data['job_category'] as $key => $value) {

            $category = JobCategory::where('id', $value)->first();
            if ($category) {
                $data['job_category'][$key] = $category->service_name;
            }
        }
        $data['getAllReviews'] = RatingReviews::with('oam_company.users')->with('job')->where('fmc_company_id', $req->user_id)->orderBy('created_at', 'desc')->get();
        $data['getAllReviews'] = $data['getAllReviews']->toArray();
        $data['users']=$data['company_details']->users;
        
        return response()->json(['result' => $data]);
        
    }

    public function addJobsToFmcNotifications()
    {
        $company      = Company::all();
        $curr_job_cat = explode(',', $job_details->job_category);
        foreach ($company as $comp) {
            $comp_cat = explode(',', $comp->job_categories);
            foreach ($comp_cat as $one_cat) {
                if (in_array($curr_job_cat, $comp_cat)) {
                    echo "ok";
                }
            }
        }
    }

    public function GetJobApprovedNotification($job_title, $job_id, $company_id, $status = 0)
    {

        $fmcCompaniesArray = [];
        // Notification Data to OAM on Job Approved
        $notif_title    = 'Admin Approved ' . $job_title . ' Job';
        $description    = 'The following Job ' . $job_title . ' is approved by admin';
        $url            = 'oam/oam_job_details/' . $job_id;
        $association_id = 2;
        $status         = 0;
        $users_id       = $company_id;
        $job_details    = Jobs::where('id', $job_id)->first();
        $get_row        = Notification::where('association_id', 2)->where('user_ids', $company_id)->where('url', $url)->count();
        if ($get_row < 1) {
            GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        }

        $getFMCCompanies = User::with('companies')->where('parent_id', 0)->where('role_id', 3)->get();
        $getJobCat       = [];
        $getFMCCompanies = $getFMCCompanies->toArray();
        foreach ($getFMCCompanies as $key => $valueOuter) {
            $notif_title = 'New Job Posted ' . $job_title;
            $description = 'The following Job ' . $job_title . ' is approved by admin';
            $url         = 'fmc/payment_screen/' . $job_id;
            $status      = 0;
            $get_row     = Notification::where('association_id', 3)->where('user_ids', $valueOuter['id'])->where('url', $url)->count();
            if ($get_row < 1) {
                GeneralFunctions::notifications($notif_title, $description, $url, 3, $status, $valueOuter['id'], $job_id);
            }
        }

        // $getFMCCompanies = User::with('companies')->where('parent_id', 0)->where('role_id', 3)->get();
        // $getJobCat = [];
        // $getFMCCompanies = $getFMCCompanies->toArray();
        // foreach ($getFMCCompanies as $key => $valueOuter) {
        //     $getJobCat = explode(',',$valueOuter['companies']['job_categories']);
        //     $dataArray = [];
        //     $count = 0;
        //     foreach ($getJobCat as $keyInner => $valueInner) {
        //         if($getJobCat != null){
        //             $results = DB::select( DB::raw("SELECT id, job_title, description, created_at FROM jobs WHERE admin_approval = 1 AND id = $job_id AND find_in_set($valueInner,job_category) ORDER BY created_at DESC;"));

        //             if(count($results) > 0){
        //                 foreach ($results as $key => $value) {
        //                     $dataArray[$count]['id'] = $value->id;
        //                     $dataArray[$count]['job_title'] = $value->job_title;
        //                     $dataArray[$count]['date'] = $value->created_at;
        //                     $dataArray[$count]['job_description'] = $value->description;
        //                     $dataArray[$count]['fmc_company_id'] = $valueOuter['id'];
        //                     $count++;
        //                 }
        //             }
        //         }
        //     }
        //     $dataArray = $this->filterUniqueData($dataArray);
        //     if(count($dataArray) > 0){
        //         foreach ($dataArray as $key => $value) {
        //             $notif_title = 'New Job Posted '.$value['job_title'];
        //             $description = 'The following Job '.$value['job_title'].' is approved by admin';
        //             $url = 'fmc/payment_screen/'.$value['id'];
        //             $status = 0;
        //             $get_row = Notification::where('association_id', 3)->where('user_ids',$value['fmc_company_id'])->where('url', $url)->count();
        //             if($get_row < 1){
        //                 GeneralFunctions::notifications($notif_title, $description, $url, 3, $status, $value['fmc_company_id'], $job_id);
        //             }
        //         }
        //     }
        // }
    }

    public function GetJobRejectNotification($job_title, $job_id, $company_id, $status = 0)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Job Reject ' . $job_title;
        $description    = 'The following Job ' . $job_title . ' is rejected by admin';
        $url            = 'oam/oam_job_details/' . $job_id;
        $association_id = 2;
        $users_id       = $company_id;
        $status         = 0;
        $get_row        = Notification::where('association_id', 2)->where('user_ids', $users_id)->where('url', $url)->count();
        if ($get_row < 1) {
            GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        }
        // Notification End
    }

    public function ShortListNotification($job_title, $job_id, $company_id, $status = 0, $fmc_id)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Short list for job ' . $job_title;
        $description    = 'You are short list for the following Job ' . $job_title;
        $url            = 'fmc/payment_screen/' . $job_id;
        $association_id = 3;
        $users_id       = $fmc_id;
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        // Notification End
    }

    public function NewJobNotification($job_title, $job_id, $company_id, $status = 0)
    {

        // Notification Data
        $notif_title = 'New Job Posted ' . $job_title;
        $description = 'The following Job ' . $job_title . ' is pending for the approval';
        // $post_id = DB::getPdo()->lastInsertId();
        $url            = 'job_preview?id=' . $job_id;
        $association_id = 1;
        $status         = 0;
        $user_id        = 1;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id, $job_id);
        // Notification End
    }

    public function GetJobUpdatetByOAMNotification($job_title, $job_id, $company_id, $status = 0)
    {
        // Notification Data
        $notif_title = 'Job Updated ' . $job_title;
        $description = 'The following Job ' . $job_title . ' is updated by Company which was reviewed by You.';
        // $post_id = DB::getPdo()->lastInsertId();
        $url            = 'job_preview?id=' . $job_id;
        $association_id = 1;
        $status         = 0;
        $user_id        = 1;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id, $job_id);
        // Notification End
    }

    public function GetJobUpdatetNotification($job_title, $job_id, $company_id, $status = 0)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Job Updated ' . $job_title;
        $description    = 'The following Job ' . $job_title . ' is Updated by admin';
        $url            = 'oam/oam_job_details/' . $job_id;
        $association_id = 2;
        $users_id       = $company_id;
        $status         = 0;
        $get_row        = Notification::where('association_id', 2)->where('user_ids', $users_id)->where('url', $url)->count();
        if ($get_row < 1) {
            GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        }
        // Notification End
    }

    public function terms_and_conditions(Request $request)
    {
        $data = [];
        if ($request && $request->id != '') {
            $data['title']   = 'Edit Term And Conditions';
            $data['t_and_c'] = TermAndCondition::find($request->id);
        } else {
            $data['title'] = 'Add Term And Conditions';
        }
        return view('oam.terms_and_conditions', $data);
    }

    public function save_term_condition(Request $request)
    {
        if ($request && $request->id != '') {
            $validator = Validator::make($request->all(), [
                'title'       => 'required|max:255',
                'description' => 'required',

            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'title'       => 'required|max:255|unique:term_and_conditions',
                'description' => 'required|unique:term_and_conditions',
            ]);
        }
        if ($validator->fails()) {
            if ($request && $request->id != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        } else {
            if ($request && $request->id != '') {
                $obj             = TermAndCondition::find($request->id);
                $obj->updated_at = date('Y-m-d H:i:s', strtotime('now'));
            } else {
                $obj = new TermAndCondition;
            }

            $user_id = Auth::user()->parent_id;
            if (Auth::user()->parent_id == 0) {
                $user_id = Auth::user()->id;
            }
            $obj->user_id     = $user_id;
            $obj->title       = $request->title;
            $obj->status      = $request->status;
            $obj->posted_by   = "OAM";
            $obj->description = $request->description;
            $obj->save();

            return back()->with('status', 'Record has been saved successfully');
        }
    }

    public function term_conditions_list()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data          = [];
        $data['title'] = "Terms & Conditions";
        $t_c           = TermAndCondition::where('user_id', $user_id)->get()->toArray();
        //dd($t_c);
        $tc              = TermAndCondition::where('posted_by', 'Admin')->get()->toArray();
        $data['t_and_c'] = array_merge($t_c, $tc);

        return view('oam.term_and_conditions_list', $data);
    }
    /* Assigned Job List*/
    public function job_assigned_list(Request $request)
    {
        $data['title'] = 'Assigned jobs';
        $user_id       = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }
        $getAssignedJobsList = Jobs::with(['jobApply' => function ($query) {
            $query->where('status', 4);
        }])->whereHas('jobApply', function ($query) {
            $query->where('status', 4);
        })->where('user_id', $user_id)->get();
        $data['assigned_jobs'] = $getAssignedJobsList->toArray();
        //dd($data['assigned_jobs']);

        return view('oam.jobs.assigned_jobs_list', $data);
    }
     /* Assigned Job for FM List*/
    public function job_assigned_list_fm(Request $request)
    {
        $data['title'] = 'Assigned jobs';
        $user_id       = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }
         $data['assigned_jobs'] = JobApply::with('jobs')->where('user_id', $user_id)->where('status', 4)->orderBy('created_at', 'desc')->get();

       return view('fmc.jobs.assigned_jobs_list', $data);
    }

    public function 

    showJobDetails($id)
    {


        $data               = [];
        $status             = 0;
        $job_applied_status = 0;
        $inspection_date    = '';
        $report_status      = 0;
        $uploaded_date      = '';
        $checkStatus        = JobApply::where('job_id', $id)->where('user_id', $id)->get();
        $checkStatus        = $checkStatus->toArray();
        if (count($checkStatus) > 0) {
            $status             = 1;
            $job_applied_status = $checkStatus[0]['status'];
            $inspection_date    = $checkStatus[0]['inspection_date'];
            $report_status      = $checkStatus[0]['report_status'];
            $uploaded_date      = $checkStatus[0]['uploaded_date'];
        }
        $data['category']           = JobCategory::all();
        $data['detail']             = Jobs::with('JobApply')->where('id', $id)->first();
        $data['building']           = Building::where('id', $data['detail']['building'])->first();
        $data['title']              = 'Assigned Job Details';
        $data['job_applied_status'] = $job_applied_status;
        $data['inspection_date']    = $inspection_date;
        $data['status']             = $status;
        $data['report_status']      = $report_status;
        $data['uploaded_date']      = $uploaded_date;

        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();

        $jobApplicationRecord         = JobApply::with('companies')->with('users')->where('job_id', $id)->where('status', 4)->get();
        $data['jobApplicationRecord'] = $jobApplicationRecord->toArray();
        // return view('fmc.jobs.job_description',['result' => $result, 'status' => $status, 'job_applied_status' => $job_applied_status, 'data' => $data]);
        return view('oam.jobs.job_assigned_detail', $data);
    }
    public function saveReview(Request $request)
    {
        /* Update the Job Completion
        Change job status to " short list "
        0 => admin approved,
        1 => Job complete,
        2 => Pending for approval,
        3 => applied,
        4 => short list,
        5 => assgin to FMC
         */
        Jobs::where('id', $request->input('job_id'))->update(['job_status' => 1]);
        /* Add the Reviews and Ratings */
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        RatingReviews::create([
            'oam_company_id' => $user_id,
            'fmc_company_id' => $request->input('user_uuid'),
            'job_id'         => $request->input('job_id'),
            'ratings'        => $request->input('job_rating'),
            'review'         => $request->input('job_description'),
        ]);

        $job_id = $request->input('job_id');
        $fmc_id = $request->input('user_uuid');
        $job    = Jobs::where('id', $job_id)->first(['job_title']);
        $this->RatingReviewNotification($job_id, $fmc_id, $job->job_title);
        return back()->with('status', 'Successfully! Job is Completed.');
    }

    public function getJobsRelatedToCategories($id)
    {
        $dataArray = [];
        $count     = 0;
        $results   = DB::select(DB::raw("SELECT id, job_title, description, created_at FROM jobs WHERE admin_approval = 1 AND find_in_set($id,job_category) ORDER BY created_at DESC;"));
        if (count($results) > 0) {
            foreach ($results as $key => $value) {
                $dataArray[$count]['id']              = $value->id;
                $dataArray[$count]['job_title']       = $value->job_title;
                $dataArray[$count]['date']            = $value->created_at;
                $dataArray[$count]['job_description'] = $value->description;
                $count++;
            }
        }
        $dataArray = $this->filterUniqueData($dataArray);
        // $getJobList = $getJobList->toArray();

        $data['title']          = 'Job List';
        $data['data']           = $dataArray;
        $data['job_categories'] = JobCategory::get();
        $data['job_categories'] = $data['job_categories']->toArray();
        return view('fmc.jobs.job_listing', $data);
    }

    public function appliedJobs()
    {

    }

    public function get_terms_condition_content(Request $request)
    {
        // check From the JobTermAndCondition Table First
       
        $getResult = Job_Term_Condition::where('job_id', $request->job_id)->where('title', $request->title)->first();
        if ($getResult) {
            $data = $getResult->toArray();
        } else {
            $data = TermAndCondition::where('title', $request->title)->select('title', 'id', 'description')->first();
            if ($data) {
                $data = $data->toArray();
            }
        }

        Session::put($data['title'], $data);
        return response()->json(['result' => $data]);
    }

    public function update_term_condition_session(Request $request)
    {

        $params = array(
            'title'       => $request->title,
            'id'          => $request->id,
            'description' => $request->description,
        );
        Session::forget($request->title);
        Session::put($request->title, $params);

        return response()->json(['result' => $params]);
        //dd(Session::get($request->md_title));

    }

    public function delete_term_and_conditions($id)
    {
        TermAndCondition::where('id', $id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    public function sendJobReferenceRequest(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'ref_email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $getJobDetails = Jobs::where('id', $request->input('job_id'))->first();

        $data = [
            'subject'         => 'Related Job Reference from Synergics Company',
            'heading_details' => 'Related Job (' . $getJobDetails->job_title . ')',
            'sub_heading'     => 'Job Post Details',
            'heading'         => 'Job Description is been given below. Please check and Signup to Synergics for more info',
            'content'         => $getJobDetails->description,
            'job_title'       => $getJobDetails->job_title,
            'email'           => $request->input('ref_email'),
        ];
        $this->send_email_dynamically($data);
        return back()->with('status', 'Successfully! Email is sent');
    }
    /* Get Staff Record on Basis of Name Select in Job Posting For Point of Sale*/
    public function getStaffRecord(Request $request)
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $getResult = User::with('staff_members')->where('name', $request->input('name'))->where('role_id', 2)->where('parent_id', $user_id)->first();
        if ($getResult) {
            $getResult = $getResult->toArray();
        } else {
            $getResult = [];
        }

        return response()->json(['status' => 'success', 'result' => $getResult]);

    }
    /**
     *
     * New Changes to Upload on Live Server
     *
     */

    /*----------  Starting from here  ----------*/

    public function create_category(Request $request)
    {
        $data = [
            'md_category_name' => 'required|max:255',
        ];
        $rules = [
            'md_category_name.required' => 'Category Name is required',
            'md_category_name.max'      => 'Category Name should not be more than 255 characters',
            'md_category_name.unique'   => 'Category Name already exist in record',
        ];

        $validator = Validator::make($request->all(), $data, $rules);
        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        $checkNameExist = Category::where('name', $request->input('md_category_name'))->get();
        if (count($checkNameExist->toArray()) > 0) {
            return response()->json(['status' => 'error', 'msg_data' => ['Record Exist Already']]);
        }
        Category::create(['name' => $request->input('md_category_name')]);
        $getAllCategories = Category::all();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            foreach ($getAllCategories as $key => $value) {
                $data = $data . '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
            }
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function create_sub_category(Request $request)
    {
        $data = [
            'category_uuid'       => 'required',
            's_sub_category_name' => 'required|max:255',
        ];
        $rules = [
            'category_uuid.required'       => 'Catgeroy is required',
            's_sub_category_name.required' => 'Sub Category Name is required',
            's_sub_category_name.max'      => 'Sub Category Name should not be more than 255 characters',
        ];

        $validator = Validator::make($request->all(), $data, $rules);
        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        $checkNameExist = SubCategory::where('sub_category', $request->input('s_sub_category_name'))->get();
        if (count($checkNameExist->toArray()) > 0) {
            return response()->json(['status' => 'error', 'msg_data' => ['Record Exist Already']]);
        }
        SubCategory::create(['sub_category' => $request->input('s_sub_category_name'), 'category_id' => $request->input('category_uuid')]);
        $getAllCategories = SubCategory::where('category_id', $request->input('category_uuid'))->get();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            foreach ($getAllCategories as $key => $value) {
                $data = $data . '<option value="' . $value['id'] . '">' . $value['sub_category'] . '</option>';
            }
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function get_sub_category(Request $request)
    {
        $selectedOption = null;
        if ($request->input('req_id') != null) {
            $dataJobCat = JobCategory::where('id', $request->input('req_id'))->select('sub_category_id')->first();
            if ($dataJobCat) {
                $selectedOption = $dataJobCat->sub_category_id;
            }
        }
        $getAllCategories = SubCategory::where('category_id', $request->input('category_uuid'))->get();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            $data = $data . '<option value="">Select Option</option>';
            foreach ($getAllCategories as $key => $value) {
                if ($selectedOption == $value['id']) {
                    $data = $data . '<option value="' . $value['id'] . '" selected >' . $value['sub_category'] . '</option>';
                } else {
                    $data = $data . '<option value="' . $value['id'] . '">' . $value['sub_category'] . '</option>';
                }
                // $data = $data.'<option value="'.$value['id'].'">'.$value['sub_category'].'</option>';
            }
        } else {
            $data = $data . '<option value="">Select Option</option>';
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function send_job_review(Request $request, $job_id, $user_id)
    {
        // dd($user_id);
        $data   = [];
        $reason = null;
        $data   = [
            'admin_approval' => 'required',
        ];

        if ($request->input('admin_approval') == 3) {
            $data['job_review'] = 'required';
            $reason             = $request->input('job_review');
        }

        $validator = Validator::make($request->all(), $data);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user_email  = User::select('email')->where('id', $user_id)->first();
        $job_details = Jobs::where('id', $job_id)->first();
        $update_data = array(
            'admin_approval'   => $request->input('admin_approval'),
            'review_job_count' => $job_details->review_job_count + 1,
        );
        Jobs::where('id', $job_id)->update($update_data);
        JobsReview::create(['job_id' => $job_id, 'review_text' => $request->input('job_review')]);
        $this->GetJobReviewNotification($request->job_title, $job_id, $job_details->user_id, $status = 0);
        $data = [
            'subject'         => 'Job Post Review',
            'heading_details' => 'Review on Posted Job (' . $job_details->job_title . ')',
            'sub_heading'     => 'Job Post Deatils',
            'heading'         => 'Administartion Review on Posted Job',
            'content'         => $request->input('job_review'),
            'job_title'       => $job_details->job_title,
            'email'           => $user_email->email,
        ];
        $this->send_email_dynamically($data);
        //Jobs::where('id', $job_id)->update($data);
        return redirect('admin_job_list')->with('status', 'Successfully Updated the status of Job');
    }

    public function GetJobReviewNotification($job_title, $job_id, $company_id, $status = 0)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Job ' . $job_title . ' review by Administartion.';
        $description    = 'The following Job ' . $job_title . ' is marked with points';
        $url            = 'oam/oam_job_details/' . $job_id;
        $association_id = 2;
        $users_id       = $company_id;
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        // Notification End
    }
    /*----------  Ending here  ----------*/

    /*----------  Subsection for the Job Category Details  ----------*/
    public function get_job_category_details(Request $req)
    {
        $result     = null;
        $dataResult = ServiceCode::where('category_id', $req->input('category'))->where('sub_category_id', $req->input('sub_category_uuid'))->get();
        $data       = $dataResult->toArray();
        if (count($data) > 0) {
            $result = $dataResult[0];
        }
        return response()->json(['status' => 'success', 'result' => $result]);
    }

    public function create_service_code(Request $req)
    {
        $data = [
            's_service_code' => 'required|max:255',
        ];
        $rules = [
            's_service_code.required' => 'Service Code is required',
            's_service_code.max'      => 'Service Code should not be more than 255 characters',
        ];

        $validator = Validator::make($req->all(), $data, $rules);
        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        $checkNameExist = ServiceCode::where('service_code_name', $req->input('s_service_code'))->where('category_id', $req->service_category_uuid)->where('sub_category_id', $req->service_ub_category_uuid)->get();
        if (count($checkNameExist->toArray()) > 0) {
            return response()->json(['status' => 'error', 'msg_data' => ['Record Exist Already']]);
        }
        ServiceCode::create(['service_code_name' => $req->input('s_service_code'), 'category_id' => $req->service_category_uuid, 'sub_category_id' => $req->service_ub_category_uuid]);
        $getAllCategories = ServiceCode::all();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            foreach ($getAllCategories as $key => $value) {
                $data = $data . '<option value="' . $value['id'] . '">' . $value['service_code_name'] . '</option>';
            }
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function get_service_code(Request $req)
    {
        $selectedOption = null;
        if ($req->input('req_id') != null) {
            $dataJobCat = JobCategory::where('id', $req->input('req_id'))->select('service_name')->first();
            if ($dataJobCat) {
                $selectedOption = $dataJobCat->service_name;
            }
        }

        $getAllCategories = JobCategory::where('category_id', $req->category_uuid)->where('sub_category_id', $req->sub_category_uuid)->get();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            $data = $data . '<option value="">Select Option</option>';
            foreach ($getAllCategories as $key => $value) {
                if ($selectedOption == $value['id']) {
                    $data = $data . '<option value="' . $value['id'] . '" selected >' . $value['service_name'] . '</option>';
                } else {
                    $data = $data . '<option value="' . $value['id'] . '">' . $value['service_name'] . '</option>';
                }
                // $data = $data.'<option value="'.$value['id'].'">'.$value['sub_category'].'</option>';
            }
        } else {
            $data = $data . '<option value="">Select Option</option>';
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function get_service_name(Request $req)
    {
        $selectedOption = null;
        if ($req->input('req_id') != null) {
            $dataJobCat = JobCategory::where('id', $req->input('req_id'))->select('service_name')->first();
            if ($dataJobCat) {
                $selectedOption = $dataJobCat->service_code;
            }
        }

        $getAllCategories = JobCategory::where('category_id', $req->category_uuid)->where('sub_category_id', $req->sub_category_uuid)->where('service_code', $req->service_code)->get();
        $getAllCategories = $getAllCategories->toArray();
        $data             = '';
        if (count($getAllCategories) > 0) {
            $data = $data . '<option value="">Select Option</option>';
            foreach ($getAllCategories as $key => $value) {
                if ($selectedOption == $value['id']) {
                    $data = $data . '<option value="' . $value['id'] . '" selected >' . $value['service_name'] . '</option>';
                } else {
                    $data = $data . '<option value="' . $value['id'] . '">' . $value['service_name'] . '</option>';
                }
                // $data = $data.'<option value="'.$value['id'].'">'.$value['sub_category'].'</option>';
            }
        } else {
            $data = $data . '<option value="">Select Option</option>';
        }
        return response()->json(['status' => 'success', 'result' => $data]);
    }

    public function AppliedJobNotification($job_id, $job_title, $user_id)
    {
        // Notification Data
        $notif_title    = 'FMC applied on job ' . $job_title;
        $description    = 'The FMC applied on Job ' . $job_title;
        $url            = 'oam/oam_job_details_status/' . $job_id;
        $association_id = 2;
        $status         = 0;
        $user_id        = $user_id;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id, $job_id);
        // Notification End
    }

    public function RatingReviewNotification($job_id, $fmc_id, $job_title)
    {
        // Notification Data
        $notif_title    = 'An OAM company give you rating & reviews' . $job_title;
        $description    = 'An OAM campany give you rating and reviews on completion the job ' . $job_title;
        $url            = 'fmc/review/ratings/list/' . $fmc_id;
        $association_id = 3;
        $status         = 0;
        $user_id        = $fmc_id;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id, $job_id);
        // Notification End
    }

    public function short_description(Request $request)
    {
        $data           = [];
        $output         = '';
        $data['detail'] = Jobs::find($request->id);

        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $request->id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();

        $output = $output . "<tr>";
        if (isset($data['job_category_name']) && count($data['job_category_name']) > 0):
            foreach ($data['job_category_name'] as $cat_name):
                $output = $output . "<td>" . $cat_name->name . "</td>";
            endforeach;
        endif;
        $output = $output . "</tr>";

        $output = $output . "<tr>";
        if (isset($data['job_sub_category']) && count($data['job_sub_category']) > 0):
            foreach ($data['job_sub_category'] as $sub_cat_name):
                $output = $output . "<td>" . $sub_cat_name->sub_category . "</td>";
            endforeach;
        endif;
        $output = $output . "</tr>";

        $output = $output . "<tr>";
        if (isset($data['job_category']) && count($data['job_category']) > 0):
            foreach ($data['job_category'] as $cat_name):
                $output = $output . "<td>" . $cat_name['job_category']->service_name . "</td>";
            endforeach;
        endif;
        $output = $output . "</tr>";

        return response()->json(['html' => $output]);
    }

    public function get_cover_letter(Request $req)
    {
        $getCoverLetter = jobApply::where('id', $req->job_apply_id)->select('cover_letter')->first();
        $getCoverLetter = $getCoverLetter->toArray();
        return response()->json(['status' => 'success', 'result' => $getCoverLetter]);
    }

    public function get_term_condition_copy(Request $request)
    {
        $html = "";
        $data = TermAndCondition::where('posted_by', 'Admin')->where('id', $request->id)->first();
        return response()->json(['flag' => 1, 'title' => $data->title, 'description' => $data->description]);
    }

    public function submit_tc_copy(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title'       => 'required|unique:term_and_conditions',
            'description' => 'required|unique:term_and_conditions',
        ]);

        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['flag' => 0, 'errors' => $errors]);
        }
        $data = array(
            'title'       => $request->title,
            'description' => $request->description,
            'status'      => 1,
            'posted_by'   => 'OAM',
            'user_id'     => $request->user_id,
        );
        TermAndCondition::insert($data);
        return response()->json(['flag' => 1]);
    }

    public function change_tc_status(Request $request)
    {
        $id     = $request->input('id');
        $status = $request->input('status');
        TermAndCondition::where('id', $id)->update(['status' => $status]);
        return back()->with('status', 'Record has been saved successfully');
    }

    public function set_to_default()
    {
        TermAndCondition::where('user_id', Auth::user()->id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    public function renew_job($id)
    {
        $data = [];

        $jobApplicationRecord = JobApply::with('companies')->with('users')->where('job_id', $id)->where('status', '!=', 2)->get();
        // dd($jobApplicationRecord->toArray());

        $jobApplicationRecord = $jobApplicationRecord->toArray();
        $jobApplicationCount  = count($jobApplicationRecord);

        // Sort Listed For this Job
        $shortListCount       = JobApply::where('job_id', $id)->where('status', 3)->get();
        $shortListCount       = count($shortListCount->toArray());
        $jobApplicationRecord = JobApply::with('companies')->with('users')->where('job_id', $id)->get();

        $data['job_category'] = JobPostJobCategory::with('job_category')->where('job_post_id', $id)->get();
        $job_category_ids     = array();
        $sub_category_ids     = array();
        $service_code_ids     = array();
        foreach ($data['job_category'] as $cat) {
            $category_id     = $cat['job_category']->category_id;
            $sub_category_id = $cat['job_category']->sub_category_id;
            $service_code    = $cat['job_category']->service_code;

            array_push($job_category_ids, $category_id);
            array_push($sub_category_ids, $sub_category_id);
            array_push($service_code_ids, $service_code);
        }
        $data['job_category_name'] = Category::select('name')->whereIn('id', $job_category_ids)->get();
        $data['job_sub_category']  = SubCategory::select('sub_category')->whereIn('id', $sub_category_ids)->get();
        $data['job_service_code']  = ServiceCode::select('service_code_name')->whereIn('id', $service_code_ids)->get();

        $data['detail']                = Jobs::with('jobReview')->where('id', $id)->first();
        $data['building']              = Building::where('id', $data['detail']['building'])->first();
        $data['title']                 = 'Job Details';
        $data['jobApplicationRecord']  = $jobApplicationRecord;
        $data['jobApplicationCount']   = $jobApplicationCount;
        $data['getTermsAndConditions'] = Job_Term_Condition::where('job_id', $id)->get();
        $data['getTermsAndConditions'] = $data['getTermsAndConditions']->toArray();
        $data['shortListCount']        = $shortListCount;
        $data['renew_job_for_further'] = true;

        return view('fmc.jobs.job_description', $data);
    }

    public function site_visit_date_request(Request $req)
    {
        $updateJobStatus = JobApply::where('user_id', $req->input('sv_user_uuid'))->where('job_id', $req->input('job_id'))->update(['inspection_date' => $req->input('sv_inspection_date'), 'status' => 6, 'site_visit_request' => 2]);

        $jobDetail         = Jobs::with('users')->where('id', $req->input('job_id'))->first();
        $getCompanyDetails = Company::with('users')->where('user_id', $req->input('sv_user_uuid'))->first();
        /*
        Notification Data for Site Visit Request
         */
        $this->SiteVisitNotification($jobDetail->job_title, $jobDetail->id, $getCompanyDetails->user_id, $status = 0, $getCompanyDetails->user_id);

        return back()->with('status', 'Successfully Assigned Site Visit Date');
    }

    public function SiteVisitNotification($job_title, $job_id, $company_id, $status = 0, $fmc_id)
    {
        $notif_title    = 'Assigned Site Visit Date';
        $description    = 'You are a Site visit date for the following Job ' . $job_title;
        $url            = 'fmc/payment_screen/' . $job_id;
        $association_id = 3;
        $users_id       = $fmc_id;
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
    }

    public function reviseQuotation(Request $req)
    {
        /* Applying for the Job after paying the charges */
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        // Get Job First
        $getJob               = JobApply::where('job_id', $req->job_id)->where('user_id', $user_id)->first();
        $proposalHistoryArray = [];
        $count                = 1;
        if ($getJob->proposal_validity_history != null) {
            $proposalHistoryArray = json_decode($getJob->proposal_validity_history, true);
        }
        $proposalHistoryArray['day_' . count($proposalHistoryArray)] = date('Y-m-d H:i:s', strtotime('now'));

        $dataAdding = [
            'job_id'                       => $req->job_id,
            'user_id'                      => $user_id,
            'revise_quotation_status'      => 0,
            'proposal_validity_start_date' => date('Y-m-d H:i:s', strtotime('now')),
            'proposal_validity_history'    => json_encode($proposalHistoryArray),
        ];

        if ($req->update_quotation_options == 1) {
            $dataAdding['quotation_amount'] = $req->revise_quotation_amount;
        }

        JobApply::where('job_id', $req->job_id)->where('user_id', $user_id)->update($dataAdding);
        return back()->with('status', 'Successfully Revised Quotation');
    }

    public function renew_job_post(Request $req)
    {
        Jobs::where('id', $req->job_id)->update([
            'renew_count' => DB::raw('renew_count+1'),
            'renew_date'  => date('Y-m-d H:i:s', strtotime('now')),
            'job_expiry'  => 0,
        ]);
        return redirect('oam/oam_job_details/' . $req->job_id)->with('status', 'Successfully Renew Job');
    }

    /*===============================================================
    =            Bulk Importing for Job Category Section            =
    ===============================================================*/

    public function import_csv_record(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'csv_file' => 'required|mimes:csv,xlsx,xls',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        Excel::load($req->file('csv_file'), function ($reader) {
            $results = $reader->toArray();
            foreach ($results as $key => $value) {
                // Add Into Category Data
                // First Check If already Existed Data
                $checkDataAlreadyExist = Category::where('name', $value['category'])->get()->toArray();
                if (count($checkDataAlreadyExist) > 0) {
                    $category_id = $checkDataAlreadyExist[0]['id'];
                } else {
                    $category    = Category::create(['name' => $value['category']]);
                    $category_id = $category->id;
                }
                // Add Into Sub Category Data
                // First Check If already Existed Data
                $checkSubDataAlreadyExist = SubCategory::where('sub_category', $value['subcategory'])->get()->toArray();
                if (count($checkSubDataAlreadyExist) > 0) {
                    $sub_category_id = $checkSubDataAlreadyExist[0]['id'];
                } else {
                    $sub_category    = SubCategory::create(['sub_category' => $value['subcategory'], 'category_id' => $category_id]);
                    $sub_category_id = $sub_category->id;
                }
                // Add Into Job Category Data
                // First Check If already Existed Data
                $checkJobDataAlreadyExist = JobCategory::where('category_id', $category_id)->where('sub_category_id', $sub_category_id)->where('service_code', $value['servicecode'])->where('service_name', $value['servicename'])->get()->toArray();
                if (count($checkJobDataAlreadyExist) > 0) {
                    $job_category_id = $checkJobDataAlreadyExist[0]['id'];
                } else {
                    $job_category    = JobCategory::create(['category_id' => $category_id, 'sub_category_id' => $sub_category_id, 'service_code' => $value['servicecode'], 'service_name' => $value['servicename']]);
                    $job_category_id = $job_category->id;
                }
            }
        });
        return back()->with('status', 'Record has been Saved successfully');
    }
    /*=====  End of Bulk Importing for Job Category Section  ======*/

    /*============================================================================
    =            Section for Job Awarded Notification and SMS Service            =
    ============================================================================*/
    public function JobAssignedNotification($job_title, $job_id, $company_id, $status = 0, $fmc_id)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Congratulations, Company has offered you job (' . $job_title . ')';
        $description    = 'Please accept or reject the job, if you are interested';
        $url            = 'fmc/payment_screen/' . $job_id;
        $association_id = 3;
        $users_id       = $fmc_id;
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
        // Notification End
    }

    public function JobAwardNotification($job_title, $job_id, $company_id, $status = 0, $fmc_id)
    {

        // Notification Data on Job Reject
        $notif_title    = 'Congratulations, Company has Seleted you for the job (' . $job_title . ')';
        $description    = 'Please start working on it as per details';
        $url            = 'fmc/payment_screen/' . $job_id;
        $association_id = 3;
        $users_id       = $fmc_id;
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $job_id);
    }

    /*=====  End of Section for Job Awarded Notification and SMS Service  ======*/

    /*=========================================================
    =            Section for WorkOrder and Invoice            =
    =========================================================*/

    public function request_workorder_and_invoice(Request $req)
    {
        $dataAdding              = [];
        $data['jobApplyDetails'] = [];
        $getJobApplyRecord       = JobApply::with('jobs')->where('job_id', $req->job_id)->where('user_id', GeneralFunctions::getMasterId())->first();
        if ($getJobApplyRecord) {
            $data['jobApplyDetails'] = $getJobApplyRecord->toArray();
        }
        if ($req->hasFile('invoice_report')) {
            $report_path = $req->invoice_report->getClientOriginalName();
            $req->file('invoice_report')->storeAs('public/invoice_report', date('YmdHis', strtotime('now')) . '_' . $report_path);
            $dataAdding['invoice_report_path'] = date('YmdH:i:s', strtotime('now')) . '_' . $report_path;
        }
        $dataAdding['work_order_and_upload_date'] = date('Y-m-d H:i:s', strtotime('now'));
        $dataAdding['status']                     = 7;
        $saveRecord                               = JobApply::where('job_id', $req->job_id)->where('user_id', GeneralFunctions::getMasterId())->update($dataAdding);

        // Notification Data on WorkOrder Demand
        $notif_title    = 'Workorder Request and Invoice uploaded for (' . $data['jobApplyDetails']['jobs']['job_title'] . ')';
        $description    = 'Please Review Invoie to give permit for job';
        $url            = 'oam/oam_job_details/' . $data['jobApplyDetails']['job_id'];
        $association_id = 2;
        $users_id       = $data['jobApplyDetails']['jobs']['user_id'];
        $status         = 0;
        GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $data['jobApplyDetails']['job_id']);
        return back()->with('status', 'Record has been Saved successfully');
    }

    /*=====  End of Section for WorkOrder and Invoice  ======*/
}

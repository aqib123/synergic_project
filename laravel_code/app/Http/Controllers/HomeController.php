<?php

namespace App\Http\Controllers;

use App\Asset;
use App\Category;
use App\Company;
use App\FmcJobCategory;
use App\JobApply;
use App\JobCategory;
use App\Jobs;
use App\SubCategory;
use App\TypeOfInsurance;
use App\User;
use Auth;
use DB;
use GeneralFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Session;
use Validator;

class HomeController extends Controller
{

    public function index()
    {
        $data          = [];
        $data['title'] = "Synergic360 - " . Auth::user()->name;
        $month         = date('Y-m-d H:i:s', strtotime('-1 month'));
        $week          = date('Y-m-d H:i:s', strtotime('-1 week'));
        $year          = date('Y-m-d H:i:s', strtotime('-1 year'));
        $today         = date('Y-m-d H:i:s', strtotime('now'));

        // WEEKLY STATS

        //Total jobs posted in one week
        $data['weekly_total_jobs'] = DB::table('jobs')
            ->whereBetween('created_at', [$week, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('DATE(created_at) as day'))
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total applied jobs in one week
        $data['weekly_applied_jobs'] = DB::table('job_apply')
            ->where('status', 1)->whereBetween('created_at', [$week, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('DATE(created_at) as day'))
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total pending jobs in one week
        $data['weekly_pending_jobs'] = DB::table('jobs')
            ->where('admin_approval', 0)->whereBetween('created_at', [$week, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('DATE(created_at) as day'))
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //END WEEKLY STATS

        //MONTHLY STATS

        //Total jobs posted in one month
        $data['monthly_total_jobs'] = DB::table('jobs')
            ->whereBetween('created_at', [$month, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) as month'))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total applied jobs in one month
        $data['monthly_applied_jobs'] = DB::table('job_apply')
            ->where('status', 1)->whereBetween('created_at', [$month, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) as month'))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total pending jobs in one month
        $data['monthly_pending_jobs'] = DB::table('jobs')
            ->where('admin_approval', 0)->whereBetween('created_at', [$month, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) as month'))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //END MONTHLY STATS

        //YEARLY STATS

        //Total jobs posted in one year
        $data['yearly_total_jobs'] = DB::table('jobs')
            ->whereBetween('created_at', [$year, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('YEAR(created_at) as year'))
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total applied jobs in one year
        $data['yearly_applied_jobs'] = DB::table('job_apply')
            ->where('status', 1)->whereBetween('created_at', [$year, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('YEAR(created_at) as year'))
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //Total pending jobs in one year
        $data['yearly_pending_jobs'] = DB::table('jobs')
            ->where('admin_approval', 0)->whereBetween('created_at', [$year, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('YEAR(created_at) as year'))
            ->groupBy(DB::raw('YEAR(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //END YEARLY STATS

        $data['oma_week']  = User::where('role_id', 2)->where('parent_id', 0)->whereBetween('created_at', [$week, $today])->count();
        $data['oam_month'] = User::where('role_id', 2)->where('parent_id', 0)->whereBetween('created_at', [$month, $today])->count();
        $data['oam_year']  = User::where('role_id', 2)->where('parent_id', 0)->whereBetween('created_at', [$year, $today])->count();
        // FMC STATS
        $data['fmc_week']  = User::where('role_id', 3)->where('parent_id', 0)->whereBetween('created_at', [$week, $today])->count();
        $data['fmc_month'] = User::where('role_id', 3)->where('parent_id', 0)->whereBetween('created_at', [$month, $today])->count();
        $data['fmc_year']  = User::where('role_id', 3)->where('parent_id', 0)->whereBetween('created_at', [$year, $today])->count();

        $data['oam']             = User::where('role_id', 2)->where('parent_id', 0)->count(); //Count OAM Campanies
        $data['fmc']             = User::where('role_id', 3)->where('parent_id', 0)->count(); //Count FMC Campanies
        $data['weekly_approval'] = Jobs::whereBetween('created_at', [$week, $today])->where('admin_approval', 0)->count(); //Count all pending jobs in previous one week

        $getAllJobs           = Jobs::where('admin_approval', 0)->with('companies')->orderBy('created_at', 'desc')->limit(10)->get();
        $pending_jobs         = Jobs::where('admin_approval', 0)->count();
        $pending_users        = User::where('status', 0)->where('parent_id', 0)->count();
        $data['query_count']  = $pending_jobs + $pending_users;
        $data['pending_jobs'] = $getAllJobs->toArray();
        return view('home', $data);
    }

    public function update_profile(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'     => 'required|max:255',
            'email'    => 'required|email',
            'password' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            $obj = User::find($request->id);
            if ($request->hasFile('user_image')) {
                $user_image = $request->user_image->getClientOriginalName();
                $request->file('user_image')->storeAs('public/user_image', $user_image);
                $obj->image = $user_image;
            }
            if ($request->hasFile('licence_image')) {
                $company_image = $request->licence_image->getClientOriginalName();
                $request->file('licence_image')->storeAs('public/company_image', $company_image);
                $data['license'] = $company_image;
            }
            $obj->name     = $request->name;
            $obj->email    = $request->email;
            $obj->password = Hash::make($request->password);
            $obj->save();

            $data['billing_address'] = $request->input('billing_address');
            $data['office_address']  = $request->input('office_address');
            DB::table('company')->where('user_id', $request->id)->update($data);

            return back()->with('status', 'Record has been saved successfully');
        }
    }

    public function show_profile()
    {
        $data               = [];
        $data['title']      = "User Profile";
        $id                 = Input::get('id');
        $data['profile']    = User::find($id);
        $data['role']       = DB::table('user_roles')->where('id', $data['profile']['role_id'])->select('name as role')->first();
        $data['company']    = DB::table('company')->where('user_id', $id)->first();
        $data['categories'] = JobCategory::get();
        return view('users.complete_profile', $data);
    }

    public function fmc_home()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $getCalenderData    = JobApply::with('jobs')->where('user_id', $user_id)->whereNotNull('inspection_date')->get();
        $getCalenderData    = $getCalenderData->toArray();
        $calanderDataSource = [];

        // dd($getCalenderData);
        foreach ($getCalenderData as $key => $value) {
            //$dateFormat = explode("/",$value['inspection_date']);
            // $inspectionDate = $dateFormat[2].'-'.$dateFormat[0].'-'.$dateFormat[1];
            $inspectionDate                    = date('d-m-Y', strtotime($value['inspection_date']));
            $calanderDataSource[$key]['title'] = $value['jobs']['job_title'] . ' Inspection Date';
            $calanderDataSource[$key]['start'] = $inspectionDate;
            $calanderDataSource[$key]['id']    = $value['jobs']['id'];
        }
        $data          = [];
        $data['users'] = User::count();
        $month         = date('Y-m-d H:i:s', strtotime('-1 month')); //One month back form today
        $week          = date('Y-m-d H:i:s', strtotime('-1 week')); //One week back form today
        $year          = date('Y-m-d H:i:s', strtotime('-1 year')); //One year back form today
        $today         = date('Y-m-d H:i:s', strtotime('now')); //Today

        $data['apply_week_jobs']  = JobApply::whereBetween('created_at', [$week, $today])->where('status', 4)->count();
        $data['apply_month_jobs'] = JobApply::whereBetween('created_at', [$month, $today])->where('status', 4)->count();
        $data['apply_year_jobs']  = JobApply::whereBetween('created_at', [$year, $today])->where('status', 4)->count();

        $data['shortlist_apply_week_jobs']  = JobApply::whereBetween('created_at', [$week, $today])->where('status', 3)->count();
        $data['shortlist_apply_month_jobs'] = JobApply::whereBetween('created_at', [$month, $today])->where('status', 3)->count();
        $data['shortlist_apply_year_jobs']  = JobApply::whereBetween('created_at', [$year, $today])->where('status', 3)->count();
        $data['week_jobs']  = Jobs::whereBetween('created_at', [$week, $today])->count();
        $data['month_jobs'] = Jobs::whereBetween('created_at', [$month, $today])->count();
        $data['year_jobs']  = Jobs::whereBetween('created_at', [$year, $today])->count();

        $data['allJobs']      = [];
        $data['my_jobs']         = Jobs::with('job_categories')->where('admin_approval', 1)->orderBy('created_at', 'desc')->get();
        $data['my_jobs']         = $data['my_jobs']->toArray();
        $getCompanyCategories = User::with('job_categories')->with('companies')->where('id', $user_id)->first();
        if ($getCompanyCategories) {
            $getCompanyCategories = $getCompanyCategories->toArray();
            if ($getCompanyCategories['profile_strength'] != '100%') {
                $getCompanyCategories = [];
            } else {
                $data['allJobs'] = Jobs::where('admin_approval', 1)->orderBy('created_at', 'desc')->get()->toArray();
            }
        } else {
            $getCompanyCategories = [];
        }
        // Check the specific jobs related to given job categories.
        $dataArray = [];
        $count     = 0;
        if (count($getCompanyCategories) > 0) {
            foreach ($getCompanyCategories['job_categories'] as $key => $value) {
                // check Category in Jobs As well and Select those Jobs
                foreach ($data['my_jobs'] as $jobsKey => $jobsValue) {
                    foreach ($jobsValue['job_categories'] as $innerKey => $innerValue) {
                        if ($value['job_category_id'] == $innerValue['job_category_id']) {
                            $dataArray[$count]['id']              = $jobsValue['id'];
                            $dataArray[$count]['job_title']       = $jobsValue['job_title'];
                            $dataArray[$count]['date']            = $jobsValue['created_at'];
                            $dataArray[$count]['job_description'] = $jobsValue['description'];
                            $dataArray[$count]['count_views']     = $jobsValue['count_views'];
                            $count++;
                        }
                    }
                }
            }
        }
        $dataArray = $this->filterUniqueData($dataArray);
        $tempDataArray=[];
        foreach ($dataArray as $key => $row)
        {
            $tempDataArray[$key] = $row['date'];
        }
        array_multisort($tempDataArray,SORT_DESC, $dataArray);
        $data['data']           = $dataArray;



        // $data['total_jobs'] = Jobs::count();
        $data['total_jobs']=Jobs::with('job_categories')->where('admin_approval', 1)->count();
        $data['jobs']       = Jobs::with('jobApply')->orderBy('created_at', 'desc')->where('admin_approval', 1)->limit(5)->get();
        // dd($data['jobs']->toArray());
        $data['myjobs']     = JobApply::with('jobs')->where('user_id', $user_id)->orderBy('created_at', 'desc')->limit(5)->get();
        $data['job_apply']=$data['myjobs']->count();
        // $data['job_apply'] = JobApply::where('user_id', $user_id)->whereIn('status', [1, 3, 4])->count();

        $data['assigned_jobs_list'] = JobApply::with('jobs')->where('user_id', $user_id)->where('status', 4)->orderBy('created_at', 'desc')->limit(5)->get();
        // $data['myjobs'] = Jobs::with('jobApply')->orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->limit(5)->get();

        $start_curr_month = date('Y-m-01 H:i:s', strtotime($today)); //Start date of current month
        $end_curr_month   = date('Y-m-t H:i:s', strtotime($today)); //End date of current month

        $start_curr_year = date("Y-m-d H:i:s", strtotime('first day of January ' . date('Y-m-d H:i:s')));
        $end_curr_year   = date("Y-m-d H:i:s", strtotime('last day of December ' . date('Y-m-d H:i:s')));

        // This year applied jobs
        $data['curr_year_jobs']  = Jobs::where('admin_approval', 1)->where('user_id', $user_id)->whereBetween('created_at', [$start_curr_year, $end_curr_year])->count(); //Monthly
        $data['curr_month_jobs'] = Jobs::where('admin_approval', 1)->where('user_id', $user_id)->whereBetween('created_at', [$start_curr_month, $end_curr_month])->count(); //Yearly
        // Previous year applied jobs
        $data['pre_year_jobs']  = Jobs::where('admin_approval', 1)->where('user_id', $user_id)->whereBetween('created_at', [$month, $today])->count(); //Monthly
        $data['pre_month_jobs'] = Jobs::where('admin_approval', 1)->where('user_id', $user_id)->whereBetween('created_at', [$year, $today])->count(); //Yearly

        //last month job assign
        $data['last_month'] = DB::table('job_apply')
            ->where('status', 4)->where('user_id', $user_id)
            ->whereBetween('created_at', [$month, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('DAY(created_at) day'))
            ->groupBy(DB::raw('Day(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        $start_last_month = date('Y-m-d', strtotime('first day of last month'));
        $end_last_month   = date('Y-m-d', strtotime('last day of last month'));

        //Last year job assign
        $data['last_year'] = DB::table('job_apply')
            ->where('status', 4)->where('user_id', $user_id)
            ->whereBetween('created_at', [$year, $today])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) month'))
            ->groupBy(DB::raw('Day(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        //print_r($data['last_year']);exit;

        $data['current_year_jobs'] = DB::table('job_apply')
            ->where('status', 4)->where('user_id', $user_id)
            ->whereBetween('created_at', [$start_curr_year, $end_curr_year])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) month'))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->orderBy('created_at', 'desc')->get();

        $start_last_year = date("d-m-y", strtotime("last year January 1st"));
        $end_last_year   = date("d-m-y", strtotime("last year December 31st"));

        $data['last_year_jobs'] = DB::table('job_apply')
            ->where('status', 4)->where('user_id', $user_id)
            ->whereBetween('created_at', [$start_last_year, $end_last_year])
            ->select(
                DB::raw('COUNT(id) as count'),
                DB::raw('MONTH(created_at) month'))
            ->groupBy(DB::raw('MONTH(created_at)'))
            ->orderBy('created_at', 'desc')->get();
        //print_r($data['current_year_jobs']);exit;

        $data['title']   = "HOME";
        $getSubscription = null;
        if (Session::has('sub_msg')) {
            $getSubscription = session('sub_msg');
            Session::forget('sub_msg');
        }
        $data['sub_msg']            = $getSubscription;
        $data['calanderDataSource'] = json_encode($calanderDataSource);
        $data['user_tc']            = DB::table('varification_categories')->where('status', 11)->first();
        $data['staff']              = User::with('staff_members.user_roles')->where('parent_id', $user_id)->limit(5)->get();
        $data['staff_count']        = User::where('parent_id', $user_id)->count();
        $data['site_visit']         = JobApply::select('job_apply.job_id', 'jobs.description', 'jobs.oam_staff_id', 'jobs.job_title', 'users.name', 'job_apply.inspection_date')
            ->where('job_apply.user_id', GeneralFunctions::getMasterId())
            ->join('jobs', 'jobs.id', '=', 'job_apply.job_id')->join('users', 'job_apply.user_id', '=', 'users.id')->where('job_apply.site_visit_request', 1)->orderBy('job_apply.created_at', 'desc')->limit(10)->get();
        // dd($data['staff']);
        $data['qoute_submitted'] = JobApply::select('job_apply.job_id', 'job_apply.created_at', 'jobs.oam_staff_id', 'jobs.description', 'jobs.job_title', 'job_apply.quotation_amount')
            ->where('job_apply.user_id', GeneralFunctions::getMasterId())->whereNotNull('quotation_amount')
            ->join('jobs', 'jobs.id', '=', 'job_apply.job_id')
            ->orderBy('job_apply.created_at', 'desc')->limit(10)->get();
        // $data['assigned_jobs'] = Jobs::where('user_id', GeneralFunctions::getMasterId())->where('job_status', 5)->count();
        $data['assigned_jobs'] = jobApply::where('user_id', GeneralFunctions::getMasterId())->whereIn('status', [4])->count();
        $data['short_list']    = jobApply::where('user_id', GeneralFunctions::getMasterId())->whereIn('status', [3])->count();
      
      // dd($data);
        return view('fmc_home', $data);

    }
    public function filterUniqueData($ArrayData)
    {
        $ArrayData = array_values(array_unique($ArrayData, SORT_REGULAR));
        return $ArrayData;
    }


    public function oam_home()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data                      = [];
        $creator                   = User::where('id', $user_id)->first(['creator_name']);
        $data['creator']           = $creator->creator_name;
        $data['title']             = "Synergic360 - " . Auth::User()->name;
        $data['strength']          = User::where('id', $user_id)->select(['profile_strength'])->first();
        $data['staff']             = User::with('staff_members.user_roles')->where('parent_id', $user_id)->limit(5)->get();
        $data['total_jobs']        = Jobs::where('user_id', $user_id)->count();
        $data['not_approved_jobs'] = Jobs::where('user_id', $user_id)->where('admin_approval', 0)->count();
        $data['pending_jobs']      = Jobs::with('user_tooltip', 'users')->where('user_id', $user_id)->where('admin_approval', 0)->orderBy('created_at', 'desc')->limit(10)->get();
        $data['expired_jobs']      = Jobs::with('user_tooltip', 'users')->where('user_id', $user_id)->where('job_expiry', 1)->orderBy('created_at', 'desc')->limit(10)->get();
        $data['reviewed_jobs']     = Jobs::with('user_tooltip', 'users')->where('user_id', $user_id)->where('review_job_count', '>', 0)->orderBy('created_at', 'desc')->limit(10)->get();
        $data['jobs']              = Jobs::with('user_tooltip', 'users')->withCount('jobApply')->orderBy('created_at', 'desc')->where('user_id', $user_id)->where('admin_approval', 1)->limit(10)->get();
        $data['job_apply']         = Jobs::where('jobs.user_id', $user_id)->join('job_apply', 'job_apply.job_id', '=', 'jobs.id')->count();
        // $data['assigned_jobs']     = Jobs::where('user_id', $user_id)->where('job_status', 5)->orderBy('created_at', 'desc')->limit(10)->get();

        $data['assigned_jobs']=Jobs::with(['jobApply' => function ($query) { $query->where('status', 4); }])->whereHas('jobApply', function ($query) {$query->where('status', 4);})->where('user_id', $user_id)->orderBy('created_at', 'desc')->limit(10)->get();

        $data['site_visit'] = JobApply::select('job_apply.job_id', 'jobs.description', 'jobs.oam_staff_id', 'jobs.job_title', 'users.name', 'job_apply.inspection_date')
            ->where('jobs.user_id', $user_id)->whereNotNull('inspection_date')
            ->join('jobs', 'jobs.id', '=', 'job_apply.job_id')
            ->join('users', 'job_apply.user_id', '=', 'users.id')
            ->orderBy('job_apply.created_at', 'desc')->limit(10)->get();

        $data['qoute_submitted'] = JobApply::select('job_apply.job_id', 'job_apply.created_at', 'jobs.oam_staff_id', 'jobs.description', 'jobs.job_title', 'job_apply.quotation_amount')
            ->where('jobs.user_id', $user_id)->whereNotNull('quotation_amount')
            ->join('jobs', 'jobs.id', '=', 'job_apply.job_id')
            ->orderBy('job_apply.created_at', 'desc')->limit(10)->get();

        $data['staffData'] = User::with('staff_members.user_roles')->where('parent_id', $user_id)->get();
        $data['staffData'] = $data['staffData']->toArray();
        //dd($data['staffData']);
        $data['users']   = count($data['staffData']);
        $data['user_tc'] = DB::table('varification_categories')->where('status', 11)->first();
        //dd($data);
        return view('oam_home', $data);
    }

    public function oam_profile()
    {
        $data            = [];
        $data['title']   = "Profile Settings";
        $data['profile'] = User::find(Auth::user()->id);
        $user_id         = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data['company']             = DB::table('company')->where('user_id', $user_id)->first();
        $data['license_expiry_left'] = null;
        if ($data['company']->license_expiry != null) {
            $getNoOfDays                 = GeneralFunctions::difference_bwt_two_dated($data['company']->license_expiry, time());
            $data['license_expiry_left'] = $getNoOfDays;
        }
        return view('oam.user_profile', $data);
    }

    public function fmc_profile()
    {
        $data            = [];
        $data['title']   = "User Profile";
        $data['profile'] = User::find(Auth::user()->id);
        $user_id         = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data['new_categories']         = Category::get();
        $data['job_categories_details'] = [];
        $data['new_categories']         = $data['new_categories']->toArray();
        $data['company']                = DB::table('company')->where('user_id', $user_id)->first();

        $data['job_categories_details'] = FmcJobCategory::with('job_category')->where('fmc_id', $user_id)->get();
        $data['job_categories_details'] = $data['job_categories_details']->toArray();
        foreach ($data['job_categories_details'] as $key => $value) {
            /*----------  Section For Population Sub Categories  ----------*/
            $getSubDirectories                                          = SubCategory::where('category_id', $value['job_category']['category_id'])->get();
            $data['job_categories_details'][$key]['get_sub_categories'] = $getSubDirectories->toArray();

            /*----------  Subsection For population of Service Name ----------*/
            $getServiceCode                                           = JobCategory::where('category_id', $value['job_category']['category_id'])->where('sub_category_id', $value['job_category']['sub_category_id'])->where('service_code', $value['job_category']['service_code'])->get();
            $data['job_categories_details'][$key]['get_service_name'] = $getServiceCode->toArray();
        }
        return view('fmc.user_profile', $data);
    }

    public function update_oam_profile(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'creator_name'        => 'required',
            'creator_email'       => 'required|email',
            'creator_designation' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            if ($request && $request != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        } else {
            $obj                      = User::find($request->id);
            $obj->creator_name        = $request->creator_name;
            $obj->creator_email       = $request->creator_email;
            $obj->creator_designation = $request->creator_designation;
            $obj->save();
            return back()->with('status', 'Record has been saved successfully');
        }
    }

    public function update_fmc_profile(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'creator_name'        => 'required',
            'creator_email'       => 'required|email',
            'creator_designation' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            if ($request && $request != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        } else {
            $obj                      = User::find(GeneralFunctions::getMasterId());
            $obj->creator_email       = $request->creator_email;
            $obj->creator_name        = $request->creator_name;
            $obj->creator_designation = $request->creator_designation;
            $obj->save();
            return back()->with('status', 'Record has been saved successfully');
        }
    }

    public function assets_form(Request $request)
    {
        $data = [];
        if ($request && $request->id != '') {
            $data['title']  = 'Edit Assets';
            $data['assets'] = Asset::find($request->id);
        } else {
            $data['title'] = 'Add Assets';
        }
        return view('assets_form', $data);
    }

    public function save_assets(Request $request)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }

        if ($request && $request->id != '') {
            $validator = Validator::make($request->all(), [
                'assets' => 'required|max:255',
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'assets' => 'required|max:255',
            ]);
        }
        $assets      = $request->input('assets');
        $assets_code = $request->input('assets_code');
        if ($validator->fails()) {
            if ($request && $request->id != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        }
        if ($request && $request->id != '') {
            $obj             = Asset::find($request->id);
            $obj->updated_at = date('Y-m-d H:i:s', strtotime('now'));
            // check for Existence
            $check_data_exist = GeneralFunctions::check_data_exist('assets', $assets, 'name', $request->id);
        } else {
            $obj = new Asset;
            // check for Existence
            $check_data_exist = GeneralFunctions::check_data_exist('assets', $assets, 'name');
        }
        if ($check_data_exist == 1) {
            return back()->withErrors(['Record Already Existed'])->withInput();
        }
        $obj->name    = $assets;
        $obj->user_id = $user_id;
        $obj->save();
        return back()->with('status', 'Record has been saved successfully');
    }

    public function all_assets()
    {
        $data           = [];
        $data['title']  = "All Assets";
        $assets         = Asset::where('soft_delete', 0)->get();
        $data['assets'] = $assets->toArray();
        return view('assets', $data);
    }

    public function delete_assets($id)
    {
        $obj = Asset::where('id', $id)->update(['soft_delete' => 1]);
        //$obj->where('id', $id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    public function user_management()
    {
        return view('oam.user_management');
    }

    public function getCompanies($association)
    {
        return redirect('/users?status=' . $association);
    }
    public function new_settings()
    {
        return view('new_settings');
    }

    public function update_user_password(Request $request)
    {

        $validationArray = [
            'old_password'         => 'required|min:6|max:20',
            'new_password'         => 'required|min:6|max:20',
            'confirm_new_password' => 'required|same:new_password',
        ];
        $validator    = Validator::make($request->all(), $validationArray);
        $errors       = GeneralFunctions::error_msg_serialize($validator->errors());
        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $get_user     = User::find($request->user_id);

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'message' => $errors]);
        } else {
            $passwordIsOk = password_verify($old_password, $get_user->password);
            if (isset($old_password) && $passwordIsOk != 1) {
                array_push($errors, "Old password does not match our records");
                return response()->json(['status' => 'erorr', 'message' => $errors]);
            } else {
                $data = array('password' => Hash::make($new_password));
                User::where('id', $request->user_id)->update($data);
                return response()->json(['status' => 'success']);
            }
        }
    }

    public function comapny_details_info(Request $req)
    {
        $data                        = [];
        $data['title']               = "Company Details";
        $data['profile']             = User::find(Auth::user()->id);
        $user_id                     = GeneralFunctions::getMasterId();
        $data['company']             = DB::table('company')->where('user_id', $user_id)->first();
        $data['type_of_insurance']   = TypeOfInsurance::get()->toArray();
        $data['license_expiry_left'] = null;
        if ($data['company']->license_expiry != null) {
            $getNoOfDays                 = GeneralFunctions::difference_bwt_two_dated($data['company']->license_expiry, time());
            $data['license_expiry_left'] = $getNoOfDays;
        }
        /**
         *
         * Job Categories
         *
         */
        if (Auth::user()->role_id == 3) {

            $data['new_categories']         = Category::with('sub_categories.job_categories')->get();
            $data['job_categories_details'] = [];
            $data['parentCategory']         = [];
            $data['serviceName']            = [];
            $data['sub_category']           = [];
            $data['new_categories']         = $data['new_categories']->toArray();
            $data['job_categories_details'] = FmcJobCategory::with('job_category.category')->where('fmc_id', $user_id)->get();
            $data['job_categories_details'] = $data['job_categories_details']->toArray();
            foreach ($data['job_categories_details'] as $key => $value) {
                /*----------  Section For Population Sub Categories  ----------*/
                array_push($data['parentCategory'], $value['job_category']['category']['id']);
                array_push($data['serviceName'], $value['job_category_id']);
                array_push($data['sub_category'], $value['job_category']['sub_category_id']);
            }
        }
        $getCompanyDetails       = Company::with('users')->where('user_id', $user_id)->first();
        $data['company_details'] = $getCompanyDetails;
          // dd($data['company']);
        return view('company_details', $data);
    }

    public function save_company_details(Request $request)
    {

          
        $validationArray = [
            'name'            => 'required|max:255',
            'email'           => ['required', Rule::unique('users')->ignore($request->email_hidden, 'email')],
            'contact_no'      => 'required|digits:9|numeric',
            'billing_address' => 'required|max:255',
            'office_address'  => 'required|max:255',
            'contact_alter'   => 'required|digits:9|numeric',
            'email_alter'     => 'required|max:255',
            'licence_number'  => 'required|alpha_num|max:15',
            'license_expiry'  => 'required',
            'no_of_emp'       => 'required',
            'about_company'   => 'max:300',
            'insurance_type.*'  =>'required',
            'policy_no.*'  =>'required',


        ];
        if ($request->trn_number != '') {
            $validationArray['trn_number'] = [Rule::unique('company')->ignore($request->trn_number, 'trn_number')];
        }
    
        if ($request->company_website_hidden != '') {
            $validationArray['company_website'] = ['regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', Rule::unique('company')->ignore($request->company_website_hidden, 'company_website')];
        }

        $validator = Validator::make($request->all(), $validationArray);
        if ($validator->fails()) {
            if ($request && $request != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        } else {
            if (Auth::user()->role_id == 3) {
                $job_categories = [];
                if ($request->category) {
                    foreach ($request->category as $key => $value) {
                        if (is_array($value)) {
                            // dd($value);
                            foreach ($value['sub_category'] as $sub_key => $sub_value) {
                                if (is_array($sub_value)) {
                                    foreach ($sub_value['service_name'] as $service_key => $service_value) {
                                        $services['fmc_id']          = GeneralFunctions::getMasterId();
                                        $services['job_category_id'] = $service_key;
                                        $services['created_at']      = date('Y-m-d H:i:s', strtotime('now'));
                                        $services['updated_at']      = date('Y-m-d H:i:s', strtotime('now'));
                                        array_push($job_categories, $services);
                                    }
                                } else {
                                    return back()->withErrors(['Please Select the Service Names of Checked Sub Categories']);
                                }
                            }
                        } else {
                            return back()->withErrors(['Please Select the Sub Category of Checked Buissnes Activities']);
                        }
                    }

                }
                FmcJobCategory::where('fmc_id', GeneralFunctions::getMasterId())->delete();
                if (count($job_categories) > 0) {
                    FmcJobCategory::insert($job_categories);
                }
            }
            $obj = User::find(GeneralFunctions::getMasterId());
            if ($request->hasFile('user_image')) {
                $user_image = time() . $request->user_image->getClientOriginalName();
                $request->file('user_image')->storeAs('public/user_image', $user_image);
                $obj->image = $user_image;
            }
            $obj->name             = $request->name;
            $obj->email            = $request->email;
            $obj->profile_strength = "100%";
            $obj->save();
            $data = array(
                'billing_address'     => $request->input('billing_address'),
                'office_address'      => $request->input('office_address'),
                'contact_no'          => '971-' . $request->input('contact_no'),
                'licence_number'      => $request->input('licence_number'),
                'license_expiry'      => $request->input('license_expiry'),
                'email_alter'         => $request->input('email_alter'),
                'contact_alter'       => '971-' . $request->input('contact_alter'),
                'updated_at'          => date('Y-m-d H:i:s', strtotime('now')),
                'no_of_emp'           => $request->no_of_emp,
                'about_company'       => $request->about_company,
                'policy_number'       => json_encode(GeneralFunctions::array_format($request->policy_no)),
                'company_website'     => $request->company_website,
                'trn_number'          => $request->trn_number,
                'insurance_type'      => json_encode(GeneralFunctions::array_format($request->insurance_type)),
                // Bank Details in Company
                'bank_name'           => $request->bank_name,
                'bank_account_title'  => $request->bank_account_title,
                'bank_account_no'     => $request->bank_account_no,
                'bank_branch_address' => $request->bank_branch_address,
                'bank_iban_no'        => $request->bank_iban_no,
                'bank_swift_code'     => $request->bank_swift_code,
            );
            if ($request->hasFile('licence_image')) {
                $company_image = time() . $request->licence_image->getClientOriginalName();
                $request->file('licence_image')->storeAs('public/company_image', $company_image);
                $data['license'] = $company_image;
            }
            if ($request->hasFile('upload_certfication')) {
                $certification_array=[];
                $company_upload=company::where('user_id',Auth::user()->id)->first();
                $company_upload=json_decode( $company_upload->certification_path);
              
                foreach ($request->file('upload_certfication') as $key => $value) 
                {
                    $certification_file = time() .$value->getClientOriginalName();
                    $value->storeAs('public/company_image', $certification_file);
                    
                    if($company_upload)
                    {
                    
                        if (!array_key_exists($key, $company_upload)) {
                             $company_upload[$key+1]=$certification_file;
                        }
                        else
                        {
                            $company_upload[$key]=$certification_file;
                        }
                    }
                    else
                    {
                        $company_upload[$key]=$certification_file;
                    }



                }
                $data['certification_path'] = json_encode(GeneralFunctions::array_format($company_upload));
                   // dd($data);
                
            }


            DB::table('company')->where('user_id', GeneralFunctions::getMasterId())->update($data);

            return back()->with('status', 'Record has been saved successfully');
        }
    }

    /*==============================================
    =            Insurance Type Section            =
    ==============================================*/
    /**
     *
     * Show List of Type of Insurance
     *
     */
    public function insurance_list(Request $req)
    {
        $data['record'] = TypeOfInsurance::get()->toArray();
        $data['title']  = 'List of Type of Insurance';
        return view('super_admin.type_of_insurance.list', $data);
    }

    /**
     *
     * View for showing the Add Screen
     *
     */
    public function insurance_screen(Request $req)
    {
        $data['title'] = 'Type of Insurance Screen';
        if ($req->id && $req->id != '') {
            $data['edit_record'] = TypeOfInsurance::where('id', $req->id)->first()->toArray();
        }
        return view('super_admin.type_of_insurance.add_insurance', $data);
    }

    /**
     *
     * Save Record for Insurance Record
     *
     */
    public function save_insurance_type(Request $req)
    {
        $validationArray = [
            'name' => ['required', 'max:255', Rule::unique('insurance_type')->ignore($req->id, 'id')],
        ];

        $validator = Validator::make($req->all(), $validationArray);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        /*----------  Section for recording Data  ----------*/
        $createRecord = TypeOfInsurance::create(['name' => $req->name, 'remarks' => $req->remarks]);
        return back()->with('status', 'Record has been saved successfully');

    }

    /**
     *
     * Delete Record
     *
     */
    public function delete_insurance_record($id)
    {
        try {
            $deleteRecord = TypeOfInsurance::where('id', $id)->delete();
            return back()->with('status', 'Record has been deleted successfully');
        } catch (Exception $e) {
            return back()->withErrors(['Sorry, you cannot delete record. This record is linked']);
        }
    }

    /*=====  End of Insurance Type Section  ======*/

}

<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use DB;
use Validator;
use Mail;
use App\UserRoles;
use App\User;
use App\UserVerification;
use App\OamSettings;
use App\Company;
use GeneralFunctions;
use Session;
use App\StaffMember;
use App\Role;
use Illuminate\Support\Facades\Hash;
class StaffController extends Controller
{
	public function staff_form(Request $request){

		$data = [];
		$data['title'] = "Add User";
		$data['roles'] = [];
		$data['record'] = [];
		if($request && $request->id != ''){
			$getRecord = User::with('staff_members')->where('id', $request->id)->first();
			if($getRecord){
				$data['record'] = $getRecord->toArray();
			}
		}
		$data['roles'] = Role::where('owner_id', Auth::user()->id)->get();
		$data['roles'] = $data['roles']->toArray();
		return view('users.admin_staff.staff_form', $data);
	}

	public function staff_validation(Request $request){
		if($request && $request->id != ''){
			$validationArray = [
				'name' => 'required|max:255',
				'phone_number' => 'required',
				'email' => 'required',
				'role' => 'required',
			];
		}else{
			$validationArray = [
				'name' => 'required|max:255',
				'phone_number' => 'required',
				'email' => 'required|unique:users',
				'role' => 'required',
			];
		}
        $validator = Validator::make($request->all(), $validationArray);
        $errors = GeneralFunctions::error_msg_serialize($validator->errors());
        if(count($errors) > 0){
           return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        // GeneralFunctions::ajax_debug();
        return response()->json(['status' => 'success', 'data' => $request->all()]); 
	}

	public function staff_insert(Request $request){
		
		// Adding Temporary Password for the staff
		$uniqueId = substr(uniqid(rand(), true), 2, 2);
		$password = $request->input('email').$uniqueId;

		if($request && $request->id != ''){
			$userInstance = User::find($request->id);

			// Attaching Roles and User
			$updateUserData = User::where('id', $request->id)->update(['name' =>$request->name, 'email' => $request->email, 'password' => Hash::make($password) ]);
			$updateStaffData = StaffMember::where('user_id', $request->id)->update(['mobile_number' =>$request->phone_number, 'home_number' => $request->home_number ]);

			$userInstance->role()->detach();
			$userInstance->role()->attach([$request->input('role')]);


			$data = [
	            'subject' => 'Account Changes',
	            'heading' => 'Synergic',
	            'sub_heading' => 'Account has Been Created by the Company Owner',
	            'heading_details' => 'Profile Updated By Owner',
	            'job_title' => 'Account Updated',
	            'content' => 'Your Account has been Updated by the Company. You can login from portal to strat the tasks. <br><b>User Email : '.$request->input('email').'</b><br> <b>Password : </b>'.$password,
	            'email'=>$request->input('email')
	        ];

	        GeneralFunctions::send_email_dynamically($data);
			return back()->with('status','Record has been saved successfully');
		}

		// Adding User Data
		// Check if User Already Exist
		$userCheck = User::where('email',$request->email)->get();
		if(count($userCheck) > 0){
			return back()->withErrors(['User Already Exist in Company'])->withInput();
		}
		$userData = [
			'name' => $request->input('name'),
			'password' => Hash::make($password),
			'role_id' => Auth::user()->role_id,
			'parent_id' => Auth::user()->id,
			'two_factor_approval' => 1,
			'admin_approval' => 1,
			'email' => $request->input('email'),
			'status' => 1
		];

		$userDetails = $userSave = User::create($userData);

		// Save record in Staff Detail Screen 
		$staffDetail = [
			'mobile_number' => $request->input('phone_number'),
			'home_number' => $request->input('home_number'),
			'role_id' => $request->input('role'),
			'user_id' => $userSave->id
		];

		$getCompanyDetails = Company::where('user_id', Auth::user()->id)->first();
		$company = 'Synergics Administration Team';
		if($getCompanyDetails){
			$company = $getCompanyDetails->name;
		}

		StaffMember::create($staffDetail);
		$userInstance = User::find($userDetails->id);

		// Attaching Roles and User
		$userInstance->role()->attach([$request->input('role')]);

		$data = [
            'subject' => 'New Account',
            'heading' => 'Synergic',
            'sub_heading' => 'Account has Been Created by the Company Owner',
            'heading_details' => 'Approval Application for Job',
            'job_title' => 'New Member',
            'content' => 'Congratulationss! for becoming part of <u>'.$company.'</u> u have been a new member. You can login from portal to strat the tasks. <br><b>User Email : '.$request->input('email').'</b><br> <b>Password : </b>'.$password,
            'email'=>$request->input('email')
        ];

        GeneralFunctions::send_email_dynamically($data);

		return back()->with('status','Record has been saved successfully');
	}

	public function staff_list(){
		$data = [];
		$data['title'] = "User List";
		$data['staff'] = [];
		$data['staff'] = User::with('staff_members.user_roles')->where('parent_id', Auth::user()->id)->get();
		// dd($data['staff']->toArray());
		if(isset($data['staff'])){
			$data['staff'] = $data['staff']->toArray();
		}
		return view('users.admin_staff.staff_list', $data);
	}

	public function update_staff_status(Request $req){
		$getRequestStatus = $req->input('account_status');
        $user_uuid = $req->input('user_id');

        $reason = null;
        $getUserDetails = User::where('id', $user_uuid)->first();
        $subject = 'Your Account has been Activated';
        $heading_details = '';

        if($getRequestStatus == 2){
            $reason = $req->input('reason');
            $subject = 'Your Account has been Deactivated by Company';
        }

        $updateUserStatus = User::where('id',$user_uuid)->update(['status' => $getRequestStatus]);
        $data = [
            'subject' => $subject,
            'heading' => 'Synergic',
            'sub_heading' => 'User Account Details',
            'heading_details' => $heading_details,
            'job_title' => '',
            'content' => $reason,
            'email'=>$getUserDetails->email
        ];
        GeneralFunctions::send_email_dynamically($data);
        return back()->with('status','Account has been updated with status');
	}

}
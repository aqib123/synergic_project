<?php

namespace App\Http\Controllers;

use App\MailingContent;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use \App\Notification;
use GeneralFunctions;

class NotificationController extends Controller
{
    public function get_admin_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $output = '';
        /* Notification List for last 10 records for Admin */
        $notifications = Notification::limit(5)->where('association_id', Auth::user()->role_id)->where('user_ids', 1)->orderBy('created_at', 'desc')->get();
        // GeneralFunctions::ajax_debug($notifications);
        $counter = 0;
        if (count($notifications) > 0) {
            foreach ($notifications as $row) {
                if ($row->drop_down_status == 0) {
                    $output = $output . '<a style="background-color:#99bcda;" href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                    $counter++;
                } else {
                    $output = $output . '<a href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                }
            }

            $updateTotalNotificationData = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', 1)->update(['drop_down_status' => 1]);
            return response()->json(['html' => $output, 'counter' => $counter]);
        } else {
            $output = '<a class="dropdown-item notify-item">
            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>`
            <p class="notify-details"><b>No Notifications</b></p>
            </a>';
            return response()->json(['html' => $output, 'counter' => count($notifications)]);
        }
    }

    public function get_oam_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $output = '';
        /* Notification List for last 10 records for Admin */
        $notifications = Notification::limit(5)->where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->orderBy('created_at', 'desc')->get();

        $counter = 0;
        if (count($notifications) > 0) {
            foreach ($notifications as $row) {
                if ($row->drop_down_status == 0) {
                    $output = $output . '<a style="background-color:#99bcda;" href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                    $counter++;
                } else {
                    $output = $output . '<a href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                }
            }

            $updateTotalNotificationData = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->update(['drop_down_status' => 1]);
            return response()->json(['html' => $output, 'counter' => $counter]);
        } else {
            $output = '<a class="dropdown-item notify-item">
            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>`
            <p class="notify-details"><b>No Notifications</b></p>
            </a>';
            return response()->json(['html' => $output, 'counter' => count($notifications)]);
        }
    }

    public function get_fmc_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $output = '';
        /* Notification List for last 10 records for Admin */
        $notifications = Notification::limit(5)->where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->orderBy('created_at', 'desc')->get();

        $counter = 0;
        if (count($notifications) > 0) {
            foreach ($notifications as $row) {
                if ($row->drop_down_status == 0) {
                    $output = $output . '<a style="background-color:#039CFD;" href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                    $counter++;
                } else {
                    $output = $output . '<a href="' . url($row->url) . '" class="dropdown-item notify-item">
                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                    <p class="notify-details">' . $row->title . '<small class="text-muted">' . date_format(new DateTime($row->created_at), "jS F Y g:ia") . '</small></p>
                    </a>';
                }
            }

            $updateTotalNotificationData = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->update(['drop_down_status' => 1]);
            return response()->json(['html' => $output, 'counter' => $counter]);
        } else {
            $output = '<a class="dropdown-item notify-item">
            <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>`
            <p class="notify-details"><b>No Notifications</b></p>
            </a>';
            return response()->json(['html' => $output, 'counter' => count($notifications)]);
        }
    }

    public function get_admin_instant_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $notification = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', 1)->where('status', 0)->first();
        if ($notification) {
            $update         = Notification::find($notification->id);
            $update->status = 1;
            $update->save();
            return response()->json(['flag' => 1, 'title' => $notification->title, 'description' => $notification->description]);
        } else {
            return response()->json(['flag' => 0]);
        }
    }

    public function get_oam_instant_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $notification = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->where('status', 0)->first();
        if ($notification) {
            $update         = Notification::find($notification->id);
            $update->status = 1;
            $update->save();
            return response()->json(['flag' => 1, 'title' => $notification->title, 'description' => $notification->description]);
        } else {
            return response()->json(['flag' => 0]);
        }
    }

    public function get_fmc_instant_notification()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $notification = Notification::where('association_id', Auth::user()->role_id)->where('user_ids', $user_id)->where('status', 0)->first();
        if ($notification) {
            $update         = Notification::find($notification->id);
            $update->status = 1;
            $update->save();
            return response()->json(['flag' => 1, 'title' => $notification->title, 'description' => $notification->description]);
        } else {
            return response()->json(['flag' => 0]);
        }
    }

    public function admin_all_notifications()
    {
        $data                 = [];
        $data['title']        = "All Notifications";
        $data['notification'] = Notification::where('user_ids', 1)->get();
        return view('all_notifications', $data);
    }

    public function oam_all_notifications()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data                 = [];
        $data['title']        = "All Notifications";
        $data['notification'] = Notification::where('association_id', 2)->where('user_ids', $user_id)->get();
        return view('all_notifications', $data);
    }

    public function fmc_all_notifications()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $data                 = [];
        $data['title']        = "All Notifications";
        $data['notification'] = Notification::where('association_id', 3)->where('user_ids', $user_id)->get();
        return view('all_notifications', $data);
    }

    public function admin_delete_notification($id)
    {
        $obj = new Notification;
        $obj->where('id', $id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    /*==================================================
    =            Email Notification Section            =
    ==================================================*/
    public function show_email_list(Request $req)
    {
        $data['record'] = MailingContent::get()->toArray();
        $data['title']  = 'Email Notification List';
        return view('super_admin.email_notifications.list', $data);
    }

    public function edit_email_content(Request $req)
    {
        $data['record'] = MailingContent::where('id', $req->id)->first()->toArray();
        $data['title']  = 'Email Notification (' . $data['record']['title'] . ')';
        return view('super_admin.email_notifications.content', $data);
    }

    public function change_email_status(Request $req){
        MailingContent::where('id', $req->id)->update(['active_status' => $req->status]);
        return back()->with('status', 'Record has been updated successfully');
    }
    
    /*=====  End of Email Notification Section  ======*/
    
}

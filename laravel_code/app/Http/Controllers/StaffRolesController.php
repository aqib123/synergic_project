<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use DB;
use Validator;
use Mail;
use App\UserRoles;
use App\User;
use App\UserVerification;
use App\OamSettings;
use App\Company;
use App\Screens;
use GeneralFunctions;
use Session;
use App\Role;
use App\Permission;
use App\StaffMember;
use Illuminate\Support\Facades\Hash;
class StaffRolesController extends Controller
{
	public function roles_screen(Request $request){

		$data['roles_details'] = [];
		$data['title'] = "Add User Role";
		$data['role_info'] = [];
		if($request && $request->id != ''){

			$getRolesData = Role::with('permissions')->where('id', $request->id)->first();
			if($getRolesData){
				$data['role_info'] = $getRolesData->toArray();
				$data['roles_details'] = $this->assemble_permission_array($getRolesData->toArray());
			}	
		}	

		$data['screens'] = Screens::where('association_id', Auth::user()->role_id)->get();
		$data['screens'] = $data['screens']->toArray();
		return view('users.roles.roles_screen', $data);
	}

	public function roles_permission_save(Request $req){
		// Adding Roles in the Database Table

		if($req && $req->id != ''){
			$updateRoleData = Role::where('id', $req->id)->update(['name' =>$req->name, 'description' => $req->description ]);
			$rolesInstance = Role::find($req->id);

			// First Detach All the Permissions.
			$rolesInstance->permissions()->detach();
			// dd($req->input('permissions'));
			if(isset($req->permissions)){
				foreach ($req->input('permissions') as $key => $value) {
					$getScreenId = Screens::where('code',$key)->first();
					foreach ($value as $innerKey => $valueKey) {
						$getPermission = Permission::where('name', $innerKey)->first();
						$rolesInstance->permissions()->attach([$getPermission->id => ['screen_id'=>$getScreenId->id]]);
					}	
				}
			}
			return back()->with('status','Record has been updated successfully');
		}

		$checkRoleExistAlready = Role::where('owner_id', Auth::user()->id)->where('name', $req->input('name'))->get();
		if(count($checkRoleExistAlready) > 0){
			return back()->with('errors',['Role Name Already Exist. Please Change it to new Name']);
		}
		$owner = new Role();
		$owner->name         = $req->input('name');
		$owner->display_name = $req->input('name');// optional
		$owner->description  = $req->input('description'); // optional
		$owner->owner_id  = Auth::user()->id;
		$owner->save();
		if(isset($req->permissions)){
			foreach ($req->input('permissions') as $key => $value) {
				$getScreenId = Screens::where('code',$key)->first();
				foreach ($value as $innerKey => $valueKey) {
					$getPermission = Permission::where('name', $innerKey)->first();
					$owner->permissions()->attach([$getPermission->id => ['screen_id'=>$getScreenId->id]]);
				}	
			}
		}

		return back()->with('status','Record has been saved successfully');
	}

	public function roles_permission_validation(Request $request){
		$validationArray = [
            'name' => 'required|max:255',
            'description' => 'required',
        ];
        $validator = Validator::make($request->all(), $validationArray);
        $errors = GeneralFunctions::error_msg_serialize($validator->errors());
        if(count($errors) > 0){
           return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        // GeneralFunctions::ajax_debug();
        return response()->json(['status' => 'success', 'data' => $request->all()]);
	}

	public function show_roles_list(){
		$data['title'] = 'Roles List';
		$data['roles'] = Role::where('owner_id', Auth::user()->id)->get();
		$data['roles'] = $data['roles']->toArray();

		return view('users.roles.roles_list', $data);
	}

	public function assemble_permission_array(array $data){
		$assembleArray = [];
		$permissions = [1 => 'add', 2=> 'edit', 3=>'delete', 4=>'view'];
		foreach ($data['permissions'] as $key => $value) {
			$getScreenCode = Screens::where('id',$value['screen_id'])->first();
			$getPermissionCode = $permissions[$value['pivot']['permission_id']];
			$assembleArray[$getScreenCode->code][$getPermissionCode] = true;
		}

		return $assembleArray;
	}

}
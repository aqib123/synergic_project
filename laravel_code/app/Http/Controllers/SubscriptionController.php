<?php

namespace App\Http\Controllers;

use App\SubscriptionPackages;
use App\SubscriptionServices;
use App\SystemServices;
use GeneralFunctions;
use Illuminate\Http\Request;
use Validator;

class SubscriptionController extends Controller
{
    /**
     *
     * Section for Listing of the Subscription Packages
     *
     */

    public function subscription_list(Request $req)
    {
        // 1) Get List From the Subscription Table
        $data['record'] = SubscriptionPackages::get()->toArray();
        $data['title']  = 'Subscription Packages List';
        return view('super_admin.subscription_package.list')->with($data);
    }

    /**
     *
     * Section for add Package details
     *
     */
    public function package_view(Request $req)
    {
        $data['system_services'] = SystemServices::get()->toArray();
        $data['title']           = 'Add Subscription';
        if ($req->id && $req->id != null) {
            $data['edit_record'] = SubscriptionPackages::with('services')->where('id', $req->id)->first()->toArray();
        }
        return view('super_admin.subscription_package.add_package')->with($data);
    }

    /**
     *
     * Section for Validation of (Add Pcakage Form)
     *
     */
    public function package_validate(Request $req)
    {
        $validationArray = [
            'services' => 'required',
            'amount'   => 'required|min:1|numeric',
        ];
        if ($req->id != '' && $req->id) {
            $validationArray['name'] = 'required|unique:subscription_packages,package_title,' . $req->id . ',id';
        } else {
            $validationArray['name'] = 'required|unique:subscription_packages,package_title';
        }

        $validator = Validator::make($req->all(), $validationArray);
        $errors    = GeneralFunctions::error_msg_serialize($validator->errors());
        if (count($errors) > 0) {
            return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        // GeneralFunctions::ajax_debug();
        return response()->json(['status' => 'success', 'data' => $req->all()]);
    }

    /**
     *
     * Section for Adding Package
     *
     */
    public function save_package(Request $req)
    {
        $record = [
            'package_title'       => $req->name,
            'package_description' => $req->description,
            'total_amount'        => $req->amount,
        ];

        if ($req->id && $req->id != '') {
            SubscriptionPackages::where('id', $req->id)->update($record);
        } else {
            $record['status'] = 1;
            $subscription     = SubscriptionPackages::create($record);
            $req->id          = $subscription->id;
        }
        SubscriptionServices::where('subscription_id', $req->id)->delete();
        $servicesRecord = [];
        $i              = 0;
        foreach ($req->services as $key => $value) {
            $servicesRecord[$i] = [
                'subscription_id' => $req->id,
                'service_id'      => $key,
                'created_at'      => date("Y-m-d H:i:s"),
                'updated_at'      => date("Y-m-d H:i:s"),
            ];
            $i++;
        }
        SubscriptionServices::insert($servicesRecord);
        return back()->with('status', 'Record has been saved successfully');
    }

    /**
     *
     * Section for Delete Package
     *
     */
    public function delete_package($id)
    {
        try {
            $deleteRecord = SubscriptionPackages::where('id', $id)->delete();
            return back()->with('status', 'Record has been deleted successfully.');
        } catch (\Exception $e) {
            return back()->with('error_msg', 'Record is referenced in other Screens');
        }
    }

    /**
     *
     * Sectionn for changing the package
     *
     */
    public function change_status(Request $req)
    {
        SubscriptionPackages::where('id', $req->package_id)->update(['status' => $req->status_id]);
        return back()->with('status', 'Record has been saved successfully');
    }
}

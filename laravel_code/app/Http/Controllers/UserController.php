<?php

namespace App\Http\Controllers;

use App\Company;
use App\CompanyAd;
use App\Jobs;
use App\OamSettings;
use App\SubscriptionPackages;
use App\TermAndCondition;
use App\User;
use App\UserRoles;
use App\UserVerification;
use Auth;
use DB;
use GeneralFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Session;
use Validator;

class UserController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth', ['except' => ['email_varification']);
    }

    public function user_save(Request $request)
    {

        if ($request && $request->id != '') {
            $validator = Validator::make($request->all(), [
                'role_id'  => 'required',
                'email'    => 'required|email',
                'password' => 'required|max:255',
                'name'     => 'required|max:255',
                // 'contact_no' => 'required|max:255',
                // 'billing_address' => 'required|max:255',
                // 'office_address' => 'required|max:255'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'role_id'  => 'required',
                'email'    => 'required|email|unique:users',
                'password' => 'required|max:255',
                'name'     => 'required|max:255',
            ]);
        }
        if ($validator->fails()) {
            if ($request && $request->id != '') {
                return back()->withErrors($validator);
            } else {
                return back()->withErrors($validator)->withInput();
            }
        } else {
            if ($request && $request->id != '') {
                $obj = \App\User::find($request->id);
            } else {
                $obj = new \App\User;
            }
            if ($request->hasFile('user_image')) {
                $user_image = $request->user_image->getClientOriginalName();
                $request->file('user_image')->storeAs('public/user_image', $user_image);
                $obj->image = $user_image;
            }
            $obj->role_id  = $request->role_id;
            $obj->name     = $request->name;
            $obj->email    = $request->email;
            $obj->password = Hash::make($request->password);
            $obj->save();

            if ($request->role_id == 2 || $request->role_id == 3) {
                $data = array(
                    'billing_address' => $request->input('billing_address'),
                    'office_address'  => $request->input('office_address'),
                    'contact_no'      => $request->input('contact_no'),
                    'licence_number'  => $request->input('licence_number'),
                    'license_expiry'  => $request->input('license_expiry'),
                    'email_alter'     => $request->input('email_alter'),
                    'contact_alter'   => $request->input('contact_alter'),
                    'updated_at'      => date('Y-m-d H:i:s', strtotime('now')),
                );
                if ($request->role_id == 3) {
                    $arr_cat                = $request->input('job_categories');
                    $data['job_categories'] = implode(',', $arr_cat);
                }
                if ($request->hasFile('licence_image')) {
                    $company_image = $request->licence_image->getClientOriginalName();
                    $request->file('licence_image')->storeAs('public/company_image', $company_image);
                    $data['license'] = $company_image;
                }
                DB::table('company')->where('user_id', $request->id)->update($data);
            }
            if ($request->role_id == 2) {
                return redirect('users?status=OAM')->with('status', 'Record has been saved successfully');
            } else if ($request->role_id == 3) {
                return redirect('users?status=FMC')->with('status', 'Record has been saved successfully');
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /* Users
    -----------------------------------------------------------------------*/
    public function users()
    {
        $data          = [];
        $data['title'] = 'Companies';
        $data['users'] = DB::table('users as u')
            ->leftjoin('user_roles as r', 'r.id', '=', 'u.role_id')
            ->leftjoin('company as c', 'c.user_id', '=', 'u.id')
            ->select('u.*', 'r.name as user_role_name', 'c.reference_url')
            ->where('u.parent_id', 0)
            ->orderBy('u.created_at', 'desc')
            ->get();
        return view('users.users', $data);
    }

    /* Users
    -----------------------------------------------------------------------*/
    public function user_form(Request $request)
    {
        $data = [];
        if ($request && $request->id != '') {
            $data['title']      = 'Edit Company';
            $data['user']       = \App\User::find($request->id);
            $data['role']       = DB::table('user_roles')->where('id', $data['user']['role_id'])->select('name as role')->first();
            $data['company']    = DB::table('company')->where('user_id', $request->id)->first();
            $data['categories'] = \App\JobCategory::get();
        } else {
            $data['title'] = 'Add Company';
        }
        $data['roles'] = DB::table('user_roles')->select('name', 'id')->get();
        return view('users.user_form', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = new \App\User;
        $obj->where('id', '=', $id)->delete();
        return back()->with('status', 'Record has been deleted successfully');
    }

    public function add_user()
    {
        $users = DB::table('user_roles')->select('name', 'id')->get();
        return view('add_user', compact('users'));
    }

    public function user_profile()
    {
        $data          = [];
        $data['title'] = 'Update Profile';
        return view('users.user_profile', $data);
    }

    public function user_registration(Request $request)
    {

        // Checking if the Account is already rejected for Some Reason.
        $checking        = User::where('email', $request->email)->where('status', 2)->get();
        $validationRules = [
            'role_id'     => 'required',
            'contact_no'  => 'required',
            'name'        => 'required|max:255',
            'first_name'  => 'required|alpha',
            'last_name'   => 'required|alpha',
            'designation' => 'required',
            'password'    => 'required',
            'user_name'   => 'required|regex:/^[a-z0-9_\-]+$/',
        ];

        if (count($checking) == 0) {
            $validationRules['email'] = 'required|email|unique:users';
        }

        $validator = Validator::make($request->all(), $validationRules);
        // dd('comming herer');
        if ($validator->fails()) {
            // dd($validator->fails());
            return back()->withErrors($validator)->withInput();
        } else {
            /*===================================================
            =            Check if User Already Exist            =
            ===================================================*/
            $checking = User::where('user_name', $request->user_name)->get()->toArray();
            if (count($checking) > 0) {
                return back()->withErrors(['User name already taken'])->withInput();
            }
            /*=====  End of Check if User Already Exist  ======*/
            if (count($checking) == 0) {
                $obj = new \App\User;
            } else {
                $getUser = User::where('email', $request->email)->first();
                // $obj = new \App\User::find($getUser->id);
            }

            $obj->user_name           = $request->user_name;
            $obj->role_id             = $request->role_id;
            $obj->name                = $request->name;
            $obj->email               = $request->email;
            $obj->password            = Hash::make($request->password);
            $obj->parent_id           = 0;
            $obj->profile_strength    = "30%";
            $obj->creator_email       = $request->email;
            $obj->creator_name        = $request->first_name . ' ' . $request->last_name;
            $obj->creator_designation = $request->designation;
            $obj->save();

            if (count($checking) == 0) {
                $last_id = DB::getPdo()->lastInsertId();
            } else {
                $last_id = $getUser->id;
            }
            $data = array(
                'user_id'    => $last_id,
                'name'       => $request->name,
                'contact_no' => '971' . $request->contact_no,
            );
            // update Record With Company Url
            User::where('id', $obj->id)->update(['company_url' => GeneralFunctions::generateRefUrl($obj->id)]);
            //job categories only for FMC which have id 3
            if ($request->role_id == 3) {
                //$arr_cat = $request->job_categories;
                //$data['job_categories'] = implode(',', $arr_cat);
                $data['subscription_account']        = 1; // 1 => For 3 Months and 2 for Monthly Bases
                $data['subscription_email_count']    = 0; // 1 => For 1st time 10 days before expiring 2 => 1 Day before.
                $data['subscription_count']          = 1; // subscription count
                $data['previous_subscription_dates'] = json_encode(['date_1' => date('Y-m-d H:i:s')]);
                $data['last_subscription_date']      = date('Y-m-d H:i:s');
                $data['account_status']              = 1;
                $data['account_expiration_date']     = date('Y-m-d H:i:s', strtotime('+3 months'));
            }
            if ($request->reference_url != null) {
                $data['reference_status'] = 1;
                $data['reference_url']    = $request->reference_url;
            }
            $getTrialVersionPackage          = SubscriptionPackages::where('trial_package', 1)->first();
            $data['subscription_package_id'] = $getTrialVersionPackage->id;
            $users                           = DB::table('company')->insert($data);

            /* Two Factor Authentication Email */
            $data = [
                'email'    => $request->input('email'),
                'password' => $request->input('password'),
            ];

            // Notification Data
            $notif_title    = 'New Registration ' . $request->name;
            $description    = 'The following Registration for Company ' . $request->name . ' is pending for the approval';
            $post_id        = DB::getPdo()->lastInsertId();
            $url            = '/users?status=OAM';
            $association_id = 1;
            $status         = 0;
            $user_id        = 1;

            GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $user_id);
            // dd(env('COMPANY_EMAIL'));
            Mail::send('emails.two_factor_varification', $data, function ($message) use ($data) {
                $message->from(env('COMPANY_EMAIL_ADDRESS'), 'Synergic');
                $message->to($data['email']);
                $message->subject('Account Varification');
            });

            return redirect('/login')->with('status', 'Registration was successful');
        }
    }

    public function email_varification(Request $req)
    {
        $updateEmailVarification = \App\User::where('email', $req->input('email'))->update(['two_factor_auth' => 1]);

        /* Login to Account After Varification */
        $user = [
            'email'    => $req->input('email'),
            'password' => $req->input('password'),
        ];

        $getUserVarification = UserVerification::first();
        $getUserVarification = $getUserVarification->toArray();

        if ($getUserVarification['status'] == 1) {
            $updateStatus = User::where('email', $req->input('email'))->update(['status' => 1]);
        }

        if (Auth::attempt(['email' => $req->input('email'), 'password' => $req->input('password'), 'status' => 1])) {

            $userRoles = UserRoles::where('id', Auth::user()->role_id)->first();
            if ($userRoles->name == 'Admin') {
                return redirect('/home');
            }
            if ($userRoles->name == 'OAM') {
                return redirect('/oam/home');
            }
            if ($userRoles->name == 'FMC') {
                return redirect('/fmc/home');
            }
        } else {
            return redirect('/login')->with('error_msg', 'Pending for Admin Approval');
        }
    }

    public function user_login(Request $req)
    {

        $params = session::get('popup');
        $job_id = session::get('job_id');

        $validator = Validator::make($req->all(), [
            // 'email' => 'required|email',
            'password'  => 'required',
            'user_name' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        if (Auth::attempt(['user_name' => $req->input('user_name'), 'password' => $req->input('password')])) {
            $sessionData = [
                'email'     => Auth::user()->email,
                'role_id'   => Auth::user()->role_id,
                'user_name' => Auth::user()->user_name,
            ];
            session($sessionData);
            $userRoles = UserRoles::where('id', Auth::user()->role_id)->first();

            //Check the acount Validity before login
            if (Auth::user()->parent_id == 0) {
                $user_id         = Auth::user()->id;
                $checkActivation = User::where('id', $user_id)->select('status')->first();
                if ($checkActivation->status == 3) {
                    Auth::logout();
                    return back()->with('error_msg', 'Your Company is Been Suspended, Your Activations is currently disabled. Please contact administration for more information');
                }
                if ($checkActivation->status == 2) {
                    Auth::logout();
                    return back()->with('error_msg', 'Your Company is Been Banned, Your Activations is currently disabled. Please contact administration for more information');
                }

            } else {
                $user_id = Auth::user()->parent_id;
                // For Staff Of Specific Companies to Check Parent Company Active or Not
                $checkActivation = User::where('id', $user_id)->select('status')->first();
                if ($checkActivation->status == 3) {
                    Auth::logout();
                    return back()->with('error_msg', 'Your Owner Company is Been Suspended, Your Activations is currently disabled. Please contact administration for more information');
                }
                if ($checkActivation->status == 2) {
                    Auth::logout();
                    return back()->with('error_msg', 'Your Owner Company is Been Banned, Your Activations is currently disabled. Please contact administration for more information');
                }

                // Check Staff Account Active Or Not
                $checkAccountActivation = User::where('id', Auth::user()->id)->select('status')->first();
                if ($checkAccountActivation->status == 2) {
                    Auth::logout();
                    return back()->with('error_msg', 'Your Account is been Deactivated');
                }
            }
            if ($userRoles->name == 'Admin') {
                if (isset($params) && $params == 1) {
                    return redirect('/oam/oam_job_details/' . $job_id);
                } else {
                    return redirect('/home');
                }

            }
            if ($userRoles->name == 'OAM') {
                // Check if Company Url is Present there.
                $checkIfCompanyUrlExist = User::where('id', Auth::user()->id)->where('parent_id', 0)->whereNull('company_url')->first();
                if ($checkIfCompanyUrlExist) {
                    User::where('id', Auth::user()->id)->update(['company_url' => GeneralFunctions::generateRefUrl(Auth::user()->id)]);
                }
                if (isset($params) && $params == 1) {
                    return redirect('/oam/oam_job_details/' . $job_id);
                } else {
                    return redirect('/oam/home');
                }
            }
            if ($userRoles->name == 'FMC') {

                $getCompanyDetails = Company::where('user_id', $user_id)->first();
                $getCompanyDetails = $getCompanyDetails->toArray();
                // dd($getCompanyDetails['last_subscription_date']);
                if ($getCompanyDetails['account_status'] == 2) {
                    Auth::logout();
                    return redirect('account/subscription/renewal?email=' . Auth::user()->email);
                    // return back()->with('error_msg','Your Subscription for this Account has been Expired please Pay the Payment first for Activation of the Account.');
                }
                // Now check if Account is activated Check for expiration date is near
                $getSubscriptionAccountType = $getCompanyDetails['subscription_account']; // 1 => 3 Months , 2 => 1 Month only
                $getNoOfDays                = GeneralFunctions::difference_bwt_two_dated($getCompanyDetails['account_expiration_date'], time());
                // if($getSubscriptionAccountType == 1){
                //
                // }
                // else{

                // }
                $subscriptionMsg = null;
                if ($getNoOfDays <= 7) {
                    $subscriptionMsg = 'You Subscription will be expired within ' . $getNoOfDays . ' days. Please pay the amount for validity of the account';
                }
                $sessionData = [
                    'sub_msg' => $subscriptionMsg,
                ];
                session($sessionData);
                if (isset($params) && $params == 1) {
                    return redirect('/fmc/payment_screen/' . $job_id);
                } else {
                    return redirect('/fmc/home');
                }
            }

        } else {
            return back()->with('error', 'These credentials do not match our records.');
        }

    }

    /* User Setting Page*/

    public function user_settings(Request $req)
    {
        $data                 = [];
        $data['title']        = 'User Settings';
        $getAllData           = UserVerification::first();
        $data['all_roles']    = UserRoles::all();
        $getAllData           = ($getAllData != null) ? $getAllData->toArray() : [];
        $data['user_setting'] = $getAllData;
        $data['t_and_c']      = DB::table('varification_categories')->where('status', 3)->first();
        $data['user_tc']      = DB::table('varification_categories')->where('status', 11)->first();

        return view('users.user_settings', $data);
    }

    public function change_verification_setting(Request $req)
    {
        UserVerification::where('name', 'Verification data')->update(['status' => $req->input('varification_option')]);
        return back()->with('status', 'Record has been saved successfully');
    }

    /* OAM Settings */

    public function oam_settings_view(Request $req)
    {
        $data             = [];
        $data['title']    = 'OAM Settings';
        $getAllData       = OamSettings::first();
        $getAllData       = ($getAllData != null) ? $getAllData->toArray() : [];
        $data['settings'] = $getAllData;
        return view('oam.settings', $data);
    }

    public function save_oam_setting(Request $req)
    {

        $saveRecord = OamSettings::first();
        $dataSet    = array(
            't_and_c1'  => $req->input('t_and_c1'),
            't_and_c2'  => $req->input('t_and_c2'),
            't_and_c3'  => $req->input('t_and_c3'),
            't_and_c4'  => $req->input('t_and_c4'),
            't_and_c5'  => $req->input('t_and_c5'),
            't_and_c6'  => $req->input('t_and_c6'),
            't_and_c7'  => $req->input('t_and_c7'),
            't_and_c8'  => $req->input('t_and_c8'),
            't_and_c9'  => $req->input('t_and_c9'),
            't_and_c10' => $req->input('t_and_c10'),
            't_and_c11' => $req->input('t_and_c11'),
            't_and_c12' => $req->input('t_and_c12'),
            't_and_c13' => $req->input('t_and_c13'),
            't_and_c14' => $req->input('t_and_c14'),
            't_and_c15' => $req->input('t_and_c15'),
            't_and_c16' => $req->input('t_and_c16'),
            't_and_c17' => $req->input('t_and_c17'),
            't_and_c18' => $req->input('t_and_c18'),
            't_and_c19' => $req->input('t_and_c19'),
            't_and_c20' => $req->input('t_and_c20'),
            't_and_c21' => $req->input('t_and_c21'),
            't_and_c22' => $req->input('t_and_c22'),
            't_and_c23' => $req->input('t_and_c23'),
            't_and_c24' => $req->input('t_and_c24'),
            't_and_c25' => $req->input('t_and_c25'),
            't_and_c26' => $req->input('t_and_c26'),
            't_and_c27' => $req->input('t_and_c27'),
            't_and_c28' => $req->input('t_and_c28'),
            't_and_c29' => $req->input('t_and_c29'),
            't_and_c30' => $req->input('t_and_c30'),
            't_and_c31' => $req->input('t_and_c31'),
            't_and_c32' => $req->input('t_and_c32'),
            't_and_c33' => $req->input('t_and_c33'),
            't_and_c34' => $req->input('t_and_c34'),
            't_and_c35' => $req->input('t_and_c35'),
            't_and_c36' => $req->input('t_and_c36'),
            't_and_c37' => $req->input('t_and_c37'),
            't_and_c38' => $req->input('t_and_c38'),
            't_and_c39' => $req->input('t_and_c39'),
            't_and_c40' => $req->input('t_and_c40'),
        );
        if ($saveRecord != null) {
            $dataSet['updated_at'] = date('Y-m-d H:i:s', strtotime('now'));
            OamSettings::where('id', $saveRecord->id)->update($dataSet);
        } else {
            OamSettings::create($dataSet);
        }
        return back()->with('status', 'Record has been saved successfully');
    }

    public function array_format($arrayData)
    {
        $data = [];
        $i    = 0;
        foreach ($arrayData as $key => $value) {
            if ($value != null) {
                $data[$i] = $value;
                $i++;
            }
        }

        return $data;
    }

    public function termsAndConditions(Request $req)
    {
        return view('termsandconditions');
    }

    public function save_termCondition(Request $request)
    {
        $errors    = [];
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        $count = str_word_count($request->description);

        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['flag' => 0, 'errors' => $errors]);
        } else if ($count < 250) {
            array_push($errors, "Synergic terms and conditions should not be less then 250 words");
            return response()->json(['flag' => 0, 'errors' => $errors]);
        } else {
            $data = array(
                'name'        => 'Terms and Conditions',
                'status'      => 3,
                'description' => $request->description,
            );

            $description = DB::table('varification_categories')->where('status', 3)->first();
            if (isset($description) && !empty($description)) {
                DB::table('varification_categories')->where('status', 3)->update($data);
            } else {
                DB::table('varification_categories')->insert($data);
            }
            return response()->json(['flag' => 1]);
        }
    }

    public function login_form(Request $request)
    {

        $params = "";
        $job_id = "";
        $params = $request->input('popup_login');
        $job_id = $request->input('job_id');
        if (isset($params) && $params != '') {
            $request->session()->put('popup', $params);
        }
        if (isset($job_id) && $job_id != '') {
            $request->session()->put('job_id', $job_id);
        }
        return view('auth.login');
    }
    public function user_registration_form()
    {
        return view('auth.register');
    }

    public function user_registration_form_with_reference($id)
    {
        // get Reference Company From Database
        $data['result'] = [];
        $data['url']    = [];
        $record         = User::with('companies')->where('company_url', url('register/reference/' . $id))->first();
        if ($record) {
            $data['result'] = $record->toArray();
            $data['url']    = url('register/reference/' . $id);
        }
        return view('auth.register', $data);
    }

    public function user_approval_by_admin(Request $req)
    {
        // Approving or Disaproving User Registration

        $getRequestStatus = $req->input('status');
        $user_uuid        = $req->input('user_id');

        $reaseon = null;
        if ($getRequestStatus == 2) {
            $reaseon        = $req->input('reason');
            $getUserDetails = User::where('id', $user_uuid)->first();
            $data           = [
                'subject'         => 'Rejected for User Account Approval',
                'heading'         => 'Synergic',
                'sub_heading'     => 'User Account Details',
                'heading_details' => 'Rejection for User Approval from Admin',
                'job_title'       => 'Account Rejection',
                'content'         => $reaseon,
                'email'           => $getUserDetails->email,
            ];
            GeneralFunctions::send_email_dynamically($data);
        }
        $updateUserStatus = User::where('user_id', $user_uuid)->update(['status' => $getRequestStatus]);
        return back()->with('status', 'Account has been updated with status');
    }

    public function user_approval_by_admin_validation(Request $req)
    {
        $validationRules = [
            'status' => 'required',
        ];
        if ($req->input('status') == 2) {
            $validationRules['reason'] = $req->input('reason');
        }
        $validator = Validator::make($req->all(), $validationRules);

        $errors = GeneralFunctions::error_msg_serialize($validator->errors());
        if (count($errors) > 0) {
            return response()->json(['status' => 'error', 'msg_data' => $errors]);
        }
        return response()->json(['status' => 'success']);
    }
    public function logout()
    {
        //$request->session()->forget('email');
        Auth::logout();
        return redirect('/');
    }

    public function updateUserStatus(Request $req)
    {
        $getRequestStatus = $req->input('account_status');
        $user_uuid        = $req->input('user_id');

        $reason          = null;
        $getUserDetails  = User::where('id', $user_uuid)->first();
        $subject         = 'Your Account has been Approved';
        $heading_details = '';

        if ($getRequestStatus == 2) {
            $reason  = $req->input('reason');
            $subject = 'Your Account has been Rejected by Administration';
        }
        if ($getRequestStatus == 3) {
            $reason  = $req->input('reason');
            $subject = 'Your Account has been Suspended';
        }
        $updateUserStatus = User::where('id', $user_uuid)->update(['status' => $getRequestStatus]);
        $data             = [
            'subject'         => $subject,
            'heading'         => 'Synergic',
            'sub_heading'     => 'User Account Details',
            'heading_details' => $heading_details,
            'job_title'       => '',
            'content'         => $reason,
            'email'           => $getUserDetails->email,
        ];
        GeneralFunctions::send_email_dynamically($data);
        return back()->with('status', 'Account has been updated with status');
    }

    public function user_forget_password(Request $req)
    {
        return view('auth.forget_password', ['forget_password' => 1]);
    }

    public function forget_password_proccess(Request $req)
    {
        $validator = Validator::make($req->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        // check if email exist in the database
        $checkEmail = User::where('email', $req->input('email'))->get();
        $checkEmail = $checkEmail->toArray();
        if (count($checkEmail) == 0) {
            return back()->with('error_msg', 'Email Address not exist in the System. Please enter the right email address');
        }
        // Send Email to the Client for changing the Credentials.

        $data = [
            'subject'         => 'Request for Changing Credentials',
            'heading'         => 'Synergic',
            'sub_heading'     => 'Forget Password',
            'heading_details' => '',
            'job_title'       => 'Are you sure you want to change the Password',
            'content'         => 'For changing password please click the button bellow and enter the new password there',
            'email'           => $req->input('email'),
            'button'          => 'Forget Password',
            'action_url'      => 'user/change/to/new/password',
        ];
        GeneralFunctions::send_email_dynamically($data);
        return back()->with('status', 'Email for password change request has been sent.');
    }

    public function new_password_screen(Request $req)
    {
        $data['email'] = $req->input('email');
        return view('auth.new_password_form', ['forget_password' => 0, 'email' => $req->input('email')]);
    }

    public function new_password_implementation(Request $req)
    {
        $validationArray = [
            'password' => 'required|confirmed',
            'email'    => 'required|email',
        ];
        $customMessages = [
            'password.required'  => 'User Password is required.',
            'password.confirmed' => 'Password does not match.',
            'email.required'     => 'Token Expired. Please load the page from email address again',
        ];
        $validator = Validator::make($req->all(), $validationArray, $customMessages);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }

        $changePassword = User::where('email', $req->input('email'))->update(['password' => Hash::make($req->password)]);
        return redirect('/login')->with('status', 'You have Successfully change the password of this account. You can now login with new credentials');
    }

    public function newsletter()
    {
        $data                       = [];
        $data['subscription']       = DB::table('mailing_content')->where('status', 5)->first();
        $data['signup']             = DB::table('mailing_content')->where('status', 1)->first();
        $data['change_credentials'] = DB::table('mailing_content')->where('status', 3)->first();
        $data['app_disapp']         = DB::table('mailing_content')->where('status', 2)->first();
        $data['newsletter']         = DB::table('mailing_content')->where('status', 4)->first();
        $data['title']              = "Mailing Content";
        $data['oam']                = User::select('name', 'id')->where('parent_id', 0)->where('role_id', 2)->get();
        $data['fmc']                = User::select('name', 'id')->where('parent_id', 0)->where('role_id', 3)->get();
        return view('emarketing', $data);
    }

    public function save_newsletter(Request $request)
    {

        if (!isset($request->all_oam) &&
            !isset($request->all_fmc) &&
            !isset($request->oam_email) &&
            !isset($request->fmc_email)) {
            return back()->with('alert-danger', 'Please select atleat one company to send email');
        } else {
            $all_email = array();
            if (isset($request->all_oam) && $request->all_oam == 1) {

                $oam = User::select('email')->where('parent_id', 0)->where('role_id', 2)->get();
                if (isset($oam) && count($oam) > 0) {
                    foreach ($oam as $email1) {
                        array_push($all_email, $email1->email);
                    }
                }
            } else {
                if (isset($request->oam_email) && count($request->oam_email) > 0) {
                    foreach ($request->oam_email as $email1) {
                        array_push($all_email, $email1);
                    }
                }
            }

            if (isset($request->all_fmc) && $request->all_fmc == 1) {

                $fmc = User::select('email')->where('parent_id', 0)->where('role_id', 3)->get();
                if (isset($fmc) && count($fmc) > 0) {
                    foreach ($fmc as $email2) {
                        array_push($all_email, $email2->email);
                    }
                }
            } else {
                if (isset($request->fmc_email) && count($request->fmc_email) > 0) {
                    foreach ($request->fmc_email as $email2) {
                        array_push($all_email, $email2);
                    }
                }
            }
            //$sample_emails = array('umarsalman70@gmail.com', 'rizwan.ayubi296@gmail.com', 'formanite.yaqoob92@gmail.com');
            $data = [
                'subject'         => 'Synergic360 Newsletter',
                'heading_details' => $request->short_description,
                'sub_heading'     => $request->sub_heading,
                'heading'         => $request->heading,
                'content'         => $request->full_description,
                'email'           => $all_email,
            ];
            GeneralFunctions::send_newsletter_dynamically($data);
            return back()->with('alert-success', 'Email has been sent successfully');
        }
    }

    public function fmc_account_details(Request $req)
    {
        $user_id = Auth::user()->id;
        if (Auth::user()->parent_id != 0) {
            $user_id = Auth::user()->parent_id;
        }
        $getCompanyDetails       = Company::with('users')->where('user_id', $user_id)->first();
        $data['company_details'] = $getCompanyDetails;
        $data['title']           = 'Company Details';
        return view('fmc.account_details', $data);
    }

    public function save_mailing_content(Request $request)
    {

        $data = [
            'short_description' => $request->short_description,
            'sub_heading'       => $request->sub_heading,
            'heading'           => $request->heading,
            'full_description'  => $request->full_description,
            'status'            => $request->status,
            'title'             => $request->title,
        ];
        //print_r($data['title']); exit;
        $is_exists = DB::table('mailing_content')->where('status', $request->status)->first();
        if ($is_exists) {
            $data['updated_at'] = date('Y-m-d H:i:s', strtotime('now'));
            DB::table('mailing_content')->where('status', $request->status)->update($data);
        } else {
            DB::table('mailing_content')->insert($data);
        }
        return response()->json(['flag' => 1]);
    }

    public function check_user(Request $request)
    {

        $path = url('/');
        if (Auth::check()) {

            $role_id = Auth::User()->role_id;

            $user_id = Auth::User()->parent_id;
            if (Auth::User()->parent_id == 0) {
                $user_id = Auth::user()->id;
            }
            $my_job = Jobs::where('user_id', $user_id)->where('id', $request->id)->first();
            if (isset($my_job) && $my_job != '') {
                if (isset($role_id) && $role_id != '') {
                    if ($role_id == 1 || $role_id == 2) {
                        $url = $path . '/oam/oam_job_details/' . $request->id;
                        return response()->json(['status' => 1, 'url' => $url]);
                    } else if ($role_id == 3) {
                        $url = $path . '/fmc/payment_screen/' . $request->id;
                        return response()->json(['status' => 1, 'url' => $url]);
                    }
                }
            } else {
                return response()->json(['status' => 2]);
            }
        } else {
            //$url = $path.'/login';
            return response()->json(['status' => 0]);
        }
    }

    public function submit_user_term_condition(Request $request)
    {
        if ($request->agree_term_condition == 0) {
            return response()->json(['flag' => 0, 'response' => 'Please agree with synergic360 fair usage terms and conditions']);
        } else {
            $user_id = Auth::user()->parent_id;
            if (Auth::user()->parent_id == 0) {
                $user_id = Auth::user()->id;
            }
            User::where('id', $user_id)->update(['synergic_term_condition' => 1]);
            return response()->json(['flag' => 1, 'response' => 'You have successfully agreed with synergic360 fair usage terms and conditions']);
        }
    }

    public function user_tc_content(Request $request)
    {

        $errors    = [];
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        $count = str_word_count($request->description);
        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['flag' => 0, 'errors' => $errors]);
        } else if ($count < 50) {
            array_push($errors, "Detail for fair usage terms and conditions should not be less then 50 words");
            return response()->json(['flag' => 0, 'errors' => $errors]);
        } else {
            $description = $request->description;
            $data        = array(
                'name'        => 'User terms and conditions',
                'status'      => 11,
                'description' => $description,
            );
            //check if terms and conditions for synergic users exists or not
            $is_exists = DB::table('varification_categories')->where('status', 11)->first();
            if ($is_exists) {
                DB::table('varification_categories')->where('status', 11)->update($data);
            } else {
                DB::table('varification_categories')->insert($data);
            }
            return response()->json(['flag' => 1]);
        }
    }

    public function DefaultTC_for_job(Request $request)
    {

        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        $validator = Validator::make($request->all(), [
            'title'       => 'required|unique:term_and_conditions',
            'description' => 'required',
        ]);
        $count = str_word_count($request->description);

        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['flag' => 0, 'errors' => $errors]);
        } else if ($count < 50) {
            array_push($errors, "Detail for this term and condition should not be less then 50 words");
            return response()->json(['flag' => 0, 'errors' => $errors]);
        }
        if (Auth::user()->role_id == 1) {
            $status = "Admin";
        } else {
            $ststus = "OAM";
        }
        $data = array(
            'title'       => $request->title,
            'status'      => 1,
            'user_id'     => $user_id,
            'posted_by'   => 'Admin',
            'description' => $request->description,
        );
        TermAndCondition::insert($data);
        return response()->json(['flag' => 1]);
    }

    public function save_ad_image(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'title'      => 'required:unique:company_ads',
            'user_image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($validator->fails()) {
            $errors = GeneralFunctions::error_msg_serialize($validator->errors());
            return response()->json(['flag' => 0, 'errors' => $errors]);
        }
        if ($request->hasFile('user_image')) {
            $user_image = time() . $request->user_image->getClientOriginalName();
            $uploaded   = $request->file('user_image')->storeAs('public/company_ad', $user_image);
            $obj        = new CompanyAd;
            $obj->title = $request->title;
            $obj->image = $user_image;
            if ($obj->save() && $uploaded) {
                return response()->json(['flag' => 1]);
            }
        }
    }

}

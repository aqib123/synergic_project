<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/**
 *
 * Routes Without Authentications
 *
 */
Route::get('/', function () {
    return view('Login/index');
});

/**
 *
 * Authenticated Routes
 *
 */

  Route::post('/login/user', ['uses'=>'LoginController@getUser']);

  Route::group(['middleware' => ['admin']], function(){

	Route::group(['middlewareGroups' => ['web']], function () {
		/*=============================================
		=            Section For Dashboard            =
		=============================================*/
		
		Route::get('/dashboard', function () {
	    	return view('Dashboard/index');
		});
		
		/*=====  End of Section For Dashboard  ======*/
		
		/*==================================================
		=            Section For Login / Logout            =
		==================================================*/

		Route::get('/logout', ['uses'=>'LoginController@getLogout']);
		Route::get('auth/logout', 'Auth\AuthController@logout');
		Route::get('logout', [ 'uses' => 'Auth\AuthController@getLogout', 'as' => 'logout' ]);
		/*=====  End of Section For Login / Logout  ======*/

		/*==============================================
		=            Section For Categories Brand      =
		==============================================*/
		
		Route::get('/category/brand', ['uses'=>'BrandController@getView']);
		Route::post('category/brand/insert', ['uses'=>'BrandController@addData']);	
		Route::post('category/brand/edit', ['uses'=>'BrandController@editData']);
		Route::post('category/brand/delete', ['uses'=>'BrandController@deleteData']);
		/*=====  End of Section For Categories  ======*/

		/*======================================================
		=            Section For Categories Product            =
		======================================================*/

		Route::get('/category/product', ['uses'=>'ProductController@getView']);
		Route::post('category/product/insert', ['uses'=>'ProductController@addData']);	
		Route::post('category/product/edit', ['uses'=>'ProductController@editData']);
		Route::post('category/product/delete', ['uses'=>'ProductController@deleteData']);
		/*=====  End of Section For Categories Product  ======*/

		/*=================================================
		=            Section For Expense Page             =
		=================================================*/

		Route::get('/page/expense', ['uses'=>'ExpenseController@getView']);
		Route::post('page/expense/insert', ['uses'=>'ExpenseController@addData']);
		Route::post('page/expense/edit', ['uses'=>'ExpenseController@editData']);
		Route::post('page/expense/delete', ['uses'=>'ExpenseController@deleteData']);
		/*=====  End of Section For Expense Page   ======*/

		/*=================================================
		=            Section For Expense Type Page        =
		=================================================*/

		Route::get('/page/expense/type', ['uses'=>'ExpenseTypeController@getView']);
		Route::post('page/expense/type/insert', ['uses'=>'ExpenseTypeController@addData']);
		Route::post('page/expense/type/edit', ['uses'=>'ExpenseTypeController@editData']);
		Route::post('page/expense/type/delete', ['uses'=>'ExpenseTypeController@deleteData']);
		/*=====  End of Section For Expense Type Page   ======*/

		/*=================================================
		=            Section For Remaining Inventory Page =
		=================================================*/

		Route::get('/page/inventory/remaining', ['uses'=>'RemainingInventoryController@getView']);
		// Route::post('page/expense/type/insert', ['uses'=>'ExpenseTypeController@addData']);
		Route::post('/page/inventory/remaining/edit', ['uses'=>'RemainingInventoryController@editData']);
		Route::post('/page/inventory/remaining/delete', ['uses'=>'RemainingInventoryController@deleteData']);
		/*=====  End of Section For Remaining Inventory Page   ======*/
		
		/*=============================================================
		=            Section For Customer Bill (Point of Sale)        =
		==============================================================*/

		Route::get('/page/pointofsale/customer/bill', ['uses'=>'CustomerBillController@getView']);
		Route::post('/page/pointofsale/customer/bill/insert', ['uses'=>'CustomerBillController@addData']);
		// Route::post('page/expense/type/edit', ['uses'=>'ExpenseTypeController@editData']);
		// Route::post('page/expense/type/delete', ['uses'=>'ExpenseTypeController@deleteData']);
		/*=====  End of Section For Customer Bill (Point of Sale)   ======*/		
		
		/*=============================================
		=            Section For Add Inventory        =
		=============================================*/
		
		Route::get('/page/pointofsale/inventory',['uses'=>'InventoryController@getView']);
		Route::post('/page/pointofsale/inventory/insert', ['uses'=>'InventoryController@addData']);
		
		/*=====  End of Section For Add Items  ======*/
		
		/*==================================================
		=            Section For Vendor Portion            =
		==================================================*/
		Route::get('/page/vendor/view', ['uses'=>'VendorController@getView']);
		Route::post('/page/vendor/view/edit', ['uses'=>'VendorController@editData']);
		Route::post('/page/vendor/view/delete', ['uses'=>'VendorController@deleteData']);
		Route::post('/page/vendor/view/add', ['uses'=>'VendorController@addData']);

		/* Accounts Details */
		Route::post('/page/vendor/view/accounts/cash', ['uses'=>'VendorController@PayCash']);
		Route::post('/page/vendor/view/accounts/cheque', ['uses'=>'VendorController@PayThroughCheque']);		
		
		
		/*=====  End of Section For Vendor Portion  ======*/

		/*============================================================
		=            Section For Vendor Stock Information            =
		============================================================*/
		Route::get('/page/vendor/stock/view', ['uses'=>'VendorStockController@getView']);
		Route::post('/page/vendor/stock/view/edit', ['uses'=>'VendorStockController@editData']);
		Route::post('/page/vendor/stock/view/delete', ['uses'=>'VendorStockController@deleteData']);
		
		/*=====  End of Section For Vendor Stock Information  ======*/

		/*============================================================
		=            Section For Vendor Total Bill Information       =
		============================================================*/
		Route::get('/page/vendor/totalBill/view', ['uses'=>'VendorTotalBillController@getView']);
		Route::post('/page/vendor/totalBill/view/edit', ['uses'=>'VendorTotalBillController@editData']);
		Route::post('/page/vendor/totalBill/view/delete', ['uses'=>'VendorTotalBillController@deleteData']);
		
		/*=====  End of Section For Vendor Stock Information  ======*/

		/*=========================================
		=     Section For Sales Details           =
		=========================================*/
		
		Route::get('/page/sales/view', ['uses'=>'SalesController@getView']);
		Route::post('/page/sales/view/edit', ['uses'=>'SalesController@editData']);
		Route::post('/page/sales/view/delete', ['uses'=>'SalesController@deleteData']);
		
		/*=====  End of Section For Sales  ======*/

		/*============================================================
		=            Section For Sales Total Bill Information       =
		============================================================*/
		Route::get('/page/sales/totalBill/view', ['uses'=>'SalesTotalBillController@getView']);
		Route::post('/page/sales/totalBill/view/edit', ['uses'=>'SalesTotalBillController@editData']);
		Route::post('/page/sales/totalBill/view/delete', ['uses'=>'SalesTotalBillController@deleteData']);
		
		/*=====  End of Section For Sales Stock Information  ======*/

		/*============================================================
		=            Section For Sales Reprts					     =
		============================================================*/
		Route::get('/page/sales/reports/monthly/view', ['uses'=>'SalesReportController@getMonthlyView']);
		Route::post('/page/sales/reports/monthly/view/search', ['uses'=>'SalesReportController@getMonthlySearch']);

		Route::get('/page/sales/reports/daily/view', ['uses'=>'SalesReportController@getDailyView']);
		Route::post('/page/sales/reports/daily/view/search', ['uses'=>'SalesReportController@getDailySearch']);
		
		/*=====  End of Section For Sales Stock Information  ======*/
		
		/*============================================================
		=            Section For Profile                        =
		============================================================*/
		Route::get('/page/profile/view', ['uses'=>'ProfileController@getView']);
		Route::post('/page/profile/edit', ['uses'=>'ProfileController@editData']);
		Route::post('page/profile/edit/password', ['uses'=>'ProfileController@editPasswordData']);
		///page/profile/edit/password
		/*=====  End of Section For Profile  ======*/

		/* Custome Controller For Custome Functions */
		Route::get('/getCustomerName', ['uses'=>'CustomeController@getCustomerName']);
		Route::get('/itemDetails', ['uses'=>'CustomeController@getItemDetails']);
		Route::get('/show_details/{id}', ['uses'=>'SalesTotalBillController@getSalesDetails', 'as' => 'switch'] );

		// Route::get('/page/profile/view', ['uses'=>'@getView']);

		Route::get('/page/income_statement/view', ['uses'=>'SalesReportController@getIncomeStatementView']);
		Route::post('/page/income_statement/view/edit', ['uses'=>'SalesReportController@getIncomeStatementSearch']);

		/*=================================================
		=            Remaining Inventory Worth            =
		=================================================*/
		
		Route::get('/page/remaining/inventory/worth', ['uses'=>'RemainingInventoryWorthController@getView']);
		
		/*=====  End of Remaining Inventory Worth  ======*/
		
		
		
		/* Print Order Bill */
		Route::get('page/pointofsale/customer/bill/print', function () {
    		return view('PointofScreen/bill');
		});
		
	});

});


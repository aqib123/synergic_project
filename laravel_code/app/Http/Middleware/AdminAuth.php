<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use View;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            if(Auth::user()->role_id == 1){
                return $next($request);
            }
            else{
                return redirect('/');
            }
        }
        else{
            return redirect('/');
        }
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use View;

class FmcAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            if(Auth::user()->role_id == 3){
                return $next($request);
            }
            else{
                Auth::logout();
                return redirect('/');
            }
        }
        else{
            return redirect('/');
        }
    }
}

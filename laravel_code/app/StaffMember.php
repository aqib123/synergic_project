<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffMember extends Model
{
	protected $table = 'staff_contact_details';
	protected $fillable = [
        'mobile_number','home_number', 'user_id', 'role_id'
    ];

    public function user_roles()
    {
        return $this->hasOne(Role::class, 'id', 'role_id'); 
    }    
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','name', 'email', 'password','two_factor_auth','admin_approval','status', 'parent_id','rejected_reason', 'rejected_counts','company_url','user_name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role()
    {
        return $this->belongsToMany(User::class,'role_user', 'role_id','user_id');
    }

    public function staff_members()
    {
        return $this->hasOne(StaffMember::class, 'user_id', 'id'); 
    }

    public function companies()
    {
        return $this->hasOne(Company::class, 'user_id', 'id'); 
    }
    
    public function job_categories(){
        return $this->hasMany(FmcJobCategory::class, 'fmc_id', 'id');
    }

}

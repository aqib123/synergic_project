<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVerification extends Model
{
    protected $table = 'varification_categories';
    protected $fillable = ['name', 'status'];
}
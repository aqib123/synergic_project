<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobsReview extends Model
{
	protected $table = 'job_review_details';
	protected $fillable = ['job_id', 'review_text'];

	
}

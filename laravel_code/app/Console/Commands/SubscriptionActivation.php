<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GeneralFunctions;
use DB;
use Mail;
use App\JobApply;
use App\Jobs;
use App\Users;

class SubscriptionActivation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SubscriptionActivation:deActivateAccount';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'De Activate The Subscription Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // Implementation of Revise Quotation
        
        /*======================================
        =            Implementation            =
        ======================================*/
        
        $getAppliedJobs = JobApply::with('jobs')->where('status','!=', 5)->whereNotNull('quotation_amount')->get();
        $getAppliedJobs = $getAppliedJobs->toArray();


        foreach ($getAppliedJobs as $key => $value) {
            $proposalValidityDays = '+'.$value['jobs']['purposal_validity'].' days';
            $getQuotationRevisedDate = date('Y-m-d H:i:s', strtotime($proposalValidityDays, strtotime($value['proposal_validity_start_date'])));
            if(GeneralFunctions::difference_bwt_two_dated($getQuotationRevisedDate, time()) == 0 ){

                // Update Revise Quotation Status in Database
               
                JobApply::where('id', $value['id'])->update(['revise_quotation_status' => 1]);
                $notif_title = 'Revise Quotation';
                $description = 'Please Revise the Quotation for Job'.$value['jobs']['job_title'];
                $url = 'fmc/payment_screen/'.$value['job_id'];
                $association_id = 3;
                $status = 0;
                $users_id = $value['user_id'];
                GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $value['job_id']);      
            }
        }
        
        /*=====  End of Implementation  ======*/


        $getCompanies = DB::table('company')->get();
        $getCompanies = $getCompanies->toArray();

        foreach ($getCompanies as $key => $value) {
            $getAccountExpirationDate = $value->account_expiration_date;
            $getNoOfDays = GeneralFunctions::difference_bwt_two_dated($value->account_expiration_date,time());
            $getUserId = $value->user_id;
            $getUserInformation = DB::table('users')->where('id',$getUserId)->first();
            if($getUserInformation){
                $userEmail = $getUserInformation->email;

                if($getUserInformation->role_id == 3){
                    if($getNoOfDays <= 0){
                        if($value->account_status != 2 ){
                            /* De Activating Account Because of Non Payment till Now */
                            $accountDeactiveInfo = [
                                'account_status' => 2,
                            ];
                            $getCompanies = DB::table('company')->where('id', $value->id)->update($accountDeactiveInfo);

                            /*  */
                            $data = [
                                'email' => $userEmail,
                            ];
                            Mail::send('emails.subs_account_renewal',$data, function ($message) use ($data){
                                $message->from(env('COMPANY_EMAIL_ADDRESS'),'Synergic');
                                $message->to($data['email']);
                                $message->subject('Synergics Account Subscription Expired');
                            });
                        }
                    }
                }
            }
        }

        // Implementation of Sending Warning Notification and Email to OAM Company before Jobe Expiry (3 days before)
        
        /*======================================
        =            Implementation            =
        ======================================*/
        
        $getAllApprovedJobs = DB::table('jobs')->where('admin_approval', 1)->get();
        $getAllApprovedJobs = $getAllApprovedJobs->toArray();

        foreach ($getAllApprovedJobs as $key => $value) {

            
            $expiryDaysInNumbers = $value->job_expiry_days;
            if($value->job_expiry_days == 0){
                $expiryDaysInNumbers = 30;
                if($value->job_type == 'Breakdown Maintenance'){
                    $expiryDaysInNumbers = 7;
                }
            }
            if($value->renew_count != 0){
                $expiryDaysInNumbers = $expiryDaysInNumbers * ($value->renew_count+1);
            }
            $jobExpiryDate = '+'.$expiryDaysInNumbers.' days';

            
            $getJobExpiryDate = date('Y-m-d H:i:s', strtotime($jobExpiryDate, strtotime($value->created_at)));
            if(GeneralFunctions::difference_bwt_two_dated($getJobExpiryDate, time()) == 4){
                // Add to Notifications
                $notif_title = 'Job Expiry Warning';
                $description = 'Warning, Please Renew the Job'.$value->job_title.'. It will be Expired within 4 days';
                $url = 'oam/oam_job_details/renew_job/'.$value->id;
                $association_id = 2;
                $status = 0;
                $users_id = $value->user_id;
                GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $value->id);      
            }
            if(GeneralFunctions::difference_bwt_two_dated($getJobExpiryDate, time()) <= 0){
                $localVariable=jobs::where('id',$value->id)->first();
                if($localVariable->job_expiry == 0 )
                { 
                    Jobs::where('id', $value->id)->update(['job_expiry' => 1]);
                    // Add to Notifications
                    $notif_title = 'Job has been Expired';
                    $description = 'Warning, Please Renew the Job'.$value->job_title.'. It is Expired Now';
                    $url = 'oam/oam_job_details/renew_job/'.$value->id;
                    $association_id = 2;
                    $status = 0;
                    $users_id = $value->user_id;
                    GeneralFunctions::notifications($notif_title, $description, $url, $association_id, $status, $users_id, $value->id);      
                }
            }
        }
        
        /*=====  End of Implementation  ======*/
    }
}
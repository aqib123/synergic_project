<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $table = 'sub_category';
	protected $fillable = ['category_id', 'sub_category'];

	public function job_categories(){
		return $this->hasMany(JobCategory::class, 'sub_category_id', 'id');
	}

}

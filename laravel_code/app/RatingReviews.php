<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingReviews extends Model
{
    protected $table = 'job_rating_review';
    protected $fillable = ['job_id', 'fmc_company_id', 'oam_company_id', 'ratings', 'review'];

    public function oam_company(){
		return $this->hasOne(Company::class, 'user_id', 'oam_company_id');
	}

	public function job(){
		return $this->hasOne(Jobs::class, 'id', 'job_id');
	}
}

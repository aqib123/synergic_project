<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermAndCondition extends Model
{
    protected $table = 'term_and_conditions';
    protected $fillable = [
        'title','description', 'job_id'
    ];
}

<?php
namespace App\Helpers;

use App\JobApply;
use App\User;
use Auth;
use DB;
use Mail;
use App\MailingContent;
use App\SystemServices;
use App\Company;

class GeneralFunctions
{
    public static function error_msg_serialize($errorList)
    {
        $errorData = $errorList;
        $errorData = $errorData->toArray();
        $errors    = [];
        $i         = 0;
        foreach ($errorData as $key => $value) {
            $errors[$i] = $value[0];
            $i++;
        }

        return $errors;
    }

    public static function ajax_debug($data)
    {
        echo '<pre>';
        print_r($data);
        die();
    }

    public static function check_data_exist($table_name, $value, $column_name, $edit_id = null)
    {
        $checkExistence = DB::table($table_name)->where($column_name, $value)->get();
        if ($edit_id) {
            $checkExistence = DB::table($table_name)->where($column_name, $value)->where('id', '!=', $edit_id)->get();
        }
        $checkExistence = $checkExistence->toArray();
        if (count($checkExistence) > 0) {
            return 1;
        }
        return 2;
    }

    public static function difference_bwt_two_dated($datetime1, $datetime2)
    {

        // dd($datetime2);
        $date2 = strtotime($datetime1);

        $datediff = $date2 - $datetime2;

        return round($datediff / (60 * 60 * 24));
    }

    public static function send_email_dynamically(array $data)
    {
        // dd($data);
        Mail::send('emails.dummy', $data, function ($message) use ($data) {
            $message->from(env('COMPANY_EMAIL_ADDRESS', 'noreply@zilion.co'), 'Synergic');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });
    }

    public static function send_newsletter_dynamically(array $data)
    {
        // dd($data);
        Mail::send('emails.newsletter_template', $data, function ($message) use ($data) {
            $message->from(env('COMPANY_EMAIL_ADDRESS', 'noreply@zilion.co'), 'Synergic');
            $message->to($data['email']);
            $message->subject($data['subject']);
        });
    }

    /* Permissions Working */
    public static function check_view_permission($url)
    {
        return GeneralFunctions::permissions($url, 4);
    }

    public static function check_edit_permission($url)
    {
        return GeneralFunctions::permissions($url, 2);
    }

    public static function check_add_permission($url)
    {
        return GeneralFunctions::permissions($url, 1);
    }

    public static function check_delete_permission($url)
    {
        return GeneralFunctions::permissions($url, 3);
    }

    public static function permissions($url, $permission)
    {
        if (Auth::user()->parent_id != 0) {
            $getRoleData         = DB::table('staff_contact_details')->where('user_id', Auth::user()->id)->first();
            $getScreenPermission = DB::table('screens_details')->where('url', $url)->first();
            if (!$getScreenPermission) {
                return false;
            }
            $screenId = $getScreenPermission->id;
            $roleId   = $getRoleData->role_id;

            $data = DB::table('permission_role')->where('screen_id', $screenId)->where('role_id', $roleId)->where('permission_id', $permission)->get();
            if (count($data->toArray()) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function notifications($title, $des, $url, $association_id, $status, $user_ids)
    {
        $data = [
            'title'          => $title,
            'description'    => $des,
            'url'            => $url,
            'association_id' => $association_id,
            'status'         => $status,
            'user_ids'       => $user_ids,

        ];
        
        DB::table('notifications')->insert($data);
    }

    public static function days_remaining_in_expiration($purposal_validity, $admin_approval_date)
    {
        if ($admin_approval_date != null) {
            $getExpiryDate = date('Y-m-d', strtotime($admin_approval_date . ' + ' . $purposal_validity . ' days'));
            $datediff      = strtotime($getExpiryDate) - time();
            if ($datediff < 0) {
                return 'Job Post Expired';
            } else {
                return 'Days Left ' . round($datediff / (60 * 60 * 24));
            }

        } else {
            return 'Pending For Approval';
        }
        // GeneralFunctions::difference_bwt_two_dated($getExpiryDate, time());
    }
    public static function short_list($id)
    {
        $short_list_count = JobApply::where('job_id', $id)->where('status', 3)->count();
        return 'Short Listed ' . $short_list_count;
    }

    public static function convert_associative_array_into_simple(array $arrayData)
    {
        $newDataArray = [];
        foreach ($arrayData as $key => $value) {
            array_push($newDataArray, $value['title']);
        }
        return $newDataArray;
    }

    public static function get_terms_condition_status($terms_title, $job_id)
    {
        // dd($job_id);
        $getResult = DB::table('job_term_conditions')->where('job_id', $job_id)->where('title', $terms_title)->first();
        if ($getResult) {
            $getResult = $getResult->status;
        } else {
            $getResult = 0;
        }
        return $getResult;
    }

    public static function getJobCategories(array $dataArray)
    {
        $resultArray = [];
        if (count($dataArray) > 0) {
            foreach ($dataArray as $key => $value) {
                $getResult = DB::table('job_categories')->where('id', $value)->first();
                if ($getResult) {
                    array_push($resultArray, $getResult->title);
                }
            }
        }
        // dd($resultArray);
        return $resultArray;
    }

    public static function array_format($arrayData)
    {
        $data = [];
        $i    = 0;
        foreach ($arrayData as $key => $value) {
            if ($value != null) {
                $data[$i] = $value;
                $i++;
            }
        }

        return $data;
    }

    public static function generateRefUrl($id)
    {
        $url = url('register/reference/' . md5($id));
        return $url;
    }

    public static function getRefernceCompany($url)
    {
        $company_name = '';
        if ($url != null) {
            $getCompanyName = User::with('companies')->where('company_url', $url)->first();
            $company_name   = '<a class="custom_links user_name_link" href="show_profile?id="' . $getCompanyName->id . '">' . ucwords($getCompanyName->companies->name) . '</a>';
        }
        return $company_name;
    }

    public static function getMasterId()
    {
        $user_id = Auth::user()->parent_id;
        if (Auth::user()->parent_id == 0) {
            $user_id = Auth::user()->id;
        }
        return $user_id;
    }

    public static function checkValueInAssociativeArray($needle, $array)
    {
        // dd($needle);
        foreach ($array as $key => $value) {
            if ($value['service_id'] == $needle) {
                return true;
            }
        }

        return false;
    }

    //Like service code = sms-service and $status = 1 which define which sms job pos sms or etc
    public static function checkingForPackageAccountCurrentlyForSystemService($service_code, $status, $user_id){
        $record = [];
        $result = Company::with('subscription_package.services')->where('user_id', $user_id)->first();
        if($result){
            $record = $result->toArray();
        }
        foreach ($record['subscription_package']['services'] as $key => $value) {
            $checkIfServiceActive = SystemServices::where('codes', $service_code)->where('id', $value['service_id'])->get()->toArray();
            if(count($checkIfServiceActive) > 0){
                if($service_code == 'email-service'){
                    $checkAdminActivation = MailingContent::where('status', $status)->where('active_status', 1)->first();
                    if($checkAdminActivation){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public static function replaceWordsWithStrings($text, $company_name, $job_title){
        $replacingWords = ['{{$company_name}}', '{{$job_title}}'];
        $replacedWith = ['<b>'.$company_name.'</b>', '<b>'.$job_title.'</b>'];
        $text = str_replace($replacingWords, $replacedWith, $text);
        return $text; 
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetails extends Model
{
	protected $table = "transaction_details";
	protected $fillable = ['invoice_title', 'invoice_number', 'method', 'status', 'amount', 'user_id', 'response_data'];
}
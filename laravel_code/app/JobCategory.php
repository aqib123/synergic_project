<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCategory extends Model
{
    protected $fillable = [
        'soft_delete','category_id', 'sub_category_id', 'service_code','service_name'
    ];

	public function category(){
		return $this->hasOne(Category::class, 'id', 'category_id');
	}

	public function sub_category(){
		return $this->hasOne(SubCategory::class, 'id', 'sub_category_id');
	}

	public function service_code_fun(){
		return $this->hasOne(ServiceCode::class, 'id', 'service_code');
	}
}

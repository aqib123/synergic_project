<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemServices extends Model
{
	protected $table = "system_services";
	protected $fillable = ['service_name', 'service_description'];

	public function companies(){
		return $this->hasOne(Company::class, 'user_id', 'user_id');	
	}
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeOfInsurance extends Model
{
    protected $table    = 'insurance_type';
    protected $fillable = ['name', 'remarks'];
}

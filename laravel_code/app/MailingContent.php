<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingContent extends Model
{
    protected $table    = 'mailing_content';
    protected $fillable = [
        'title', 'heading', 'sub_heading', 'short_description', 'full_description', 'status',
    ];
}

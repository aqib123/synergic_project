<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
	public function companies(){
		return $this->hasOne(Company::class, 'user_id', 'user_id');	
	}

	public function users(){
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function jobApply(){
		return $this->hasMany(JobApply::class, 'job_id', 'id');
	}

	public function AppliedJobs() {
        return $this->hasMany(JobApply::class, 'job_id', 'id')->where('status', 4);
    }
	
	public function jobReview(){
		return $this->hasMany(JobsReview::class, 'job_id', 'id');
	}
	
	public function job_categories(){
        return $this->hasMany(JobPostJobCategory::class, 'job_post_id', 'id');
	}
	
	public function user_tooltip(){
        return $this->belongsTo(User::class, 'oam_staff_id', 'id');
    }
    
}

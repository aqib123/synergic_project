<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCode extends Model
{
	protected $table = 'service_code';
	protected $fillable = ['category_id', 'sub_category_id', 'service_code_name'];
}

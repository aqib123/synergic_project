<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApply extends Model
{
    protected $table    = "job_apply";
    protected $fillable = ['job_id', 'user_id', 'status', 'cover_letter', 'quotation_amount', 'engineering_report_path', 'report_status', 'uploaded_date', 'site_visit_request', 'proposal_validity_start_date', 'proposal_validity_history', 'revise_quotation_status', 'invoice_report_path', 'work_order_and_upload_date', 'job_working_start_date'];

    public function companies()
    {
        return $this->hasOne(Company::class, 'user_id', 'user_id');
    }
    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function jobs()
    {
        return $this->hasOne(Jobs::class, 'id', 'job_id');
    }
}

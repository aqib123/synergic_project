<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table    = 'company';
    protected $fillable = ['user_id', 'name', 'contact_no', 'job_categories', 'license', 'billing_address', 'office_address', 'licence_number', 'license_expiry', 'email_alter', 'contact_alter', 'subscription_account', 'subscription_email_count', 'subscription_count', 'previous_subscription_dates', 'last_subscription_date', 'account_status', 'account_expiration_date', 'reference_status', 'reference_url', 'no_of_emp', 'about_company', 'certification_path', 'policy_number', 'company_website', 'subscription_package_id', 'trn_number', 'insurance_type', 'bank_name', 'bank_account_title', 'bank_account_no', 'bank_branch_address', 'bank_iban_no', 'bank_swift_code'];

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function subscription_package()
    {
        return $this->hasOne(SubscriptionPackages::class, 'id', 'subscription_package_id');
    }
}

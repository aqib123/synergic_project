<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPostJobCategory extends Model
{
	protected $fillable = ['job_category_id', 'job_post_id'];

	public function job_category(){
		return $this->hasOne(JobCategory::class, 'id', 'job_category_id');
	}
}

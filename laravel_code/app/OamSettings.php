<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class OamSettings extends Model
{
    protected $table = 'settings_oam';
    protected $fillable = ['staffing', 'uniform', 'assets', 'buildings'];
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FmcJobCategory extends Model
{
	protected $fillable = ['job_category_id', 'fmc_id'];
	protected $table = 'fmc_job_category';

	public function job_category(){
		return $this->hasOne(JobCategory::class, 'id', 'job_category_id');
	}
}

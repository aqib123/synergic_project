<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPackages extends Model
{
    protected $table    = "subscription_packages";
    protected $fillable = ['package_title', 'package_description', 'total_amount', 'status', 'trial_package'];

    public function services()
    {
        return $this->hasMany(SubscriptionServices::class, 'subscription_id', 'id');
    }
}

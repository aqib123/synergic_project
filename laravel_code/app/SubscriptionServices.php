<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionServices extends Model
{
    protected $table    = "subscription_services";
    protected $fillable = ['subscription_id', 'service_id'];

}

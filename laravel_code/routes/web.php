<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('auth.login');
});

// Auth::routes();

Route::group(['middleware' => ['admin']], function () {
    Route::group(['middlewareGroups' => ['web']], function () {

        Route::get('/home', 'HomeController@index')->name('home');

        /* Routes For Super Admin Define*/

        //Ad routes
        Route::get('/ads', 'AdController@ads');

        Route::get('newsletter', 'UserController@newsletter');
        Route::post('save_newsletter', 'UserController@save_newsletter');
        Route::post('save_mailing_content', 'UserController@save_mailing_content');

        Route::get('admin_job_list', 'JobController@admin_job_list');
        Route::get('job_preview', 'JobController@job_preview');
        Route::post('update_job_status/{job_id}/{user_id}', 'JobController@update_job_status');
        Route::post('send_job_review/{job_id}/{user_id}', 'JobController@send_job_review');

        /*=============================================================
        =            Section Assets Bulk Import Data            =
         =============================================================*/

        Route::post('bulk/import/assets', 'AssetsController@import_csv_record');

        /*=====  End of Section Assets Bulk Import Data  ======*/

        /* User routes start*/
        Route::get('users', 'UserController@users');
        Route::get('user_form', 'UserController@user_form');
        Route::post('user_save', 'UserController@user_save');
        Route::get('edit_user/{id}', 'UserController@edit');
        Route::post('update_user/{id}', 'UserController@update');
        Route::get('delete_user/{id}', 'UserController@destroy');
        Route::get('add_user', 'UserController@add_user');
        Route::get('user_profile', 'UserController@user_profile');
        Route::post('update_profile/{id}', 'HomeController@update_profile');
        Route::get('show_profile', 'HomeController@show_profile');
        Route::post('user_approval', 'UserController@user_approval');
        /* User routes end*/

        /* User role routes start*/
        Route::get('/all_user_role', 'UserRoleController@all_user_role');
        Route::post('save_role', 'UserRoleController@save_role');
        Route::post('update/{id}', 'UserRoleController@update');
        Route::get('delete_role/{id}', 'UserRoleController@destroy');
        Route::get('edit/{id}', 'UserRoleController@edit');
        Route::get('user_role', 'UserRoleController@user_role');
        /* User role routes end*/

        /* job category routes start*/
        Route::get('jobcat_form', 'JobController@jobcat_form');
        Route::post('jobcat_save', 'JobController@jobcat_save');
        Route::get('categories', 'JobController@categories');
        Route::get('delete_cat/{id}', 'JobController@delete_cat');
        /* job category routes end*/

        Route::get('settings', 'UserController@user_settings');
        Route::post('user_tc_content', 'UserController@user_tc_content');
        Route::post('DefaultTC_for_job', 'UserController@DefaultTC_for_job');
        Route::post('/settings/update', 'UserController@change_verification_setting');
        Route::get('/termandconditions', 'UserController@termsAndConditions');
        Route::post('/save_termCondition', 'UserController@save_termCondition');
        Route::post('/save_ad_image', 'UserController@save_ad_image');
        Route::get('/assets_form', 'HomeController@assets_form');
        Route::post('save_assets', 'HomeController@save_assets');
        Route::get('all_assets', 'HomeController@all_assets');
        Route::get('delete_assets/{id}', 'HomeController@delete_assets');

        /* Staff Members of Admin Section */
        Route::get('staff/form', 'StaffController@staff_form');
        Route::post('check_staff_validations', 'StaffController@staff_validation');
        Route::post('/staff/member/save', 'StaffController@staff_insert');
        Route::get('staff/member/list', 'StaffController@staff_list');

        /* Roles Section of Admin Panel */
        Route::get('staff/roles', 'StaffRolesController@roles_screen');
        Route::post('/add/role_permissions', 'StaffRolesController@roles_permission_save');
        Route::post('/roles/permission_validation', 'StaffRolesController@roles_permission_validation');
        Route::get('/roles/list', 'StaffRolesController@show_roles_list');

        /* Get All Oam Companies */
        Route::get('get/oam/companies/{association}', 'HomeController@getCompanies');
        Route::post('/update/account/status', 'UserController@updateUserStatus');
        /*
        Notification Routes
         */
        Route::get('admin_all_notifications', 'NotificationController@admin_all_notifications');
        Route::get('admin_delete_notification/{id}', 'NotificationController@admin_delete_notification');
        Route::get('new_settings', 'HomeController@new_settings');

        /* Job Category Routes*/
        Route::post('/create/category/name', 'JobController@create_category');
        Route::post('/create/sub/category/name', 'JobController@create_sub_category');
        Route::get('/sub/category', 'JobController@get_sub_category');
        Route::get('/service_code/category', 'JobController@get_service_code');
        Route::post('/create/service_code/name', 'JobController@create_service_code');
        /* jobs */
        Route::get('jobs', 'JobController@jobs');
        Route::get('job_form', 'JobController@job_form');
        Route::post('/job_form_validation', 'JobController@job_post_validation');
        Route::post('save_job', 'JobController@save_job');
        Route::get('jobs', 'JobController@jobs');
        Route::get('delete_job/{id}', 'JobController@delete_job');
        Route::get('get_building_assets', 'BuildingController@get_building_assets');
        Route::post('save_building', 'BuildingController@save_building');
        Route::post('get_terms_condition_content', 'JobController@get_terms_condition_content');
        Route::post('update_term_condition_session', 'JobController@update_term_condition_session');
        Route::post('get_staff_record', 'JobController@getStaffRecord');
        Route::get('oam_job_details/{id}', 'JobController@oam_job_details');

        Route::get('/get/job_category_details', 'JobController@get_job_category_details');
        Route::get('/service_name/category', 'JobController@get_service_name');

        Route::get('/get/cover_letter', 'JobController@get_cover_letter');
        /*----------  Company Details Route  ----------*/

        /*=======================================================
        =            Section For Subscription Module            =
        =======================================================*/
        Route::get('subscription/packages', 'SubscriptionController@subscription_list');
        Route::get('add/package', 'SubscriptionController@package_view');
        Route::post('/add_package/validation', 'SubscriptionController@package_validate');
        Route::post('/save/record/subscription', 'SubscriptionController@save_package');
        Route::get('/delete/record/subscription/{id}', 'SubscriptionController@delete_package');
        Route::post('/subscription/change/status/', 'SubscriptionController@change_status');
        /*=====  End of Section For Subscription Module  ======*/

        /*=========================================================
        =            Section for Notifications Setting            =
        =========================================================*/
        Route::get('/emails/list', 'NotificationController@show_email_list');
        Route::get('edit/email/content', 'NotificationController@edit_email_content');
        Route::get('change/status/email/content', 'NotificationController@change_email_status');
        /*=====  End of Section for Notifications Setting  ======*/

        /*=============================================================
        =            Section Job Category Bulk Import Data            =
        =============================================================*/
        Route::post('/bulk/import/job_category', 'JobController@import_csv_record');

        /*=====  End of Section Job Category Bulk Import Data  ======*/

        /*=====================================================
        =            Section for Type Of Insurance            =
        =====================================================*/
        Route::get('/type/insurance/list', 'HomeController@insurance_list');
        Route::post('/save/type/insurance', 'HomeController@save_insurance_type');
        Route::get('screen/type_of_insurance', 'HomeController@insurance_screen');
        Route::get('/delete/type/insurance/{id}', 'HomeController@delete_insurance_record');
        /*=====  End of Section for Type Of Insurance  ======*/

    });
});

/* End Routes For Super Admin Define*/

/* FMC Dashboard Portal Prefix*/

/**
Note : Add All FMC view Files under fmc folder in Resoureces > View
 **/

Route::group(['middleware' => ['fmc']], function () {
    Route::group(['middlewareGroups' => ['web']], function () {
        Route::group(['prefix' => 'fmc'], function () {
            Route::get('/home', 'HomeController@fmc_home');
            Route::get('fmc_profile', 'HomeController@fmc_profile');
            Route::post('update_user_password', 'HomeController@update_user_password');
            Route::post('update_fmc_profile/{id}', 'HomeController@update_fmc_profile');

            // Apply For Job
            Route::get('/job_list', 'JobController@getJobList');
            Route::get('/payment_screen/{id}', 'JobController@payment_page');
            Route::get('/payment/charges/{id}', 'JobController@payment_charges');
            Route::get('payment/success', 'JobController@payment_success');
            Route::get('payment/refused', 'JobController@payment_refused');
            Route::get('/get/job/details', 'JobController@getProjectDetails');
            Route::get('/job_listing', 'JobController@job_listing');
            Route::get('/job_detail', 'JobController@job_detail_fmc');
            Route::post('job/apply', 'JobController@apply_for_job');
            Route::get('/job_detail', 'JobController@job_detail');
            Route::post('upload/engineering/report', 'JobController@upload_engineering_report');
              /* Show the Implementation of the Assigned Jobs and there reviews */
            Route::get('show_assigned_jobs', 'JobController@job_assigned_list_fm');

            /* Staff Members of Admin Section */
            Route::get('staff/form', 'StaffController@staff_form');
            Route::post('check_staff_validations', 'StaffController@staff_validation');
            Route::post('/staff/member/save', 'StaffController@staff_insert');
            Route::get('staff/member/list', 'StaffController@staff_list');

            /* Roles Section of FMC Panel */
            Route::get('staff/roles', 'StaffRolesController@roles_screen');
            Route::post('/add/role_permissions', 'StaffRolesController@roles_permission_save');
            Route::post('/roles/permission_validation', 'StaffRolesController@roles_permission_validation');
            Route::get('/roles/list', 'StaffRolesController@show_roles_list');

            Route::get('get_fmc_notification', 'NotificationController@get_fmc_notification');
            Route::get('review/ratings/list/{id}', 'JobController@get_fmc_company_details');
            Route::get('get/related/jobs/{id}', 'JobController@getJobsRelatedToCategories');
            Route::post('/company/job/reference', 'JobController@sendJobReferenceRequest');
            /*
            Notification Routes
             */
            Route::get('fmc_all_notifications', 'NotificationController@fmc_all_notifications');
            Route::get('subscription/account/details', 'UserController@fmc_account_details');

            Route::get('/sub/category', 'JobController@get_sub_category');
            Route::get('/get/job_category_details', 'JobController@get_job_category_details');
            Route::get('/service_code/category', 'JobController@get_service_code');
            Route::post('/create/service_code/name', 'JobController@create_service_code');
            Route::get('/service_name/category', 'JobController@get_service_name');

            Route::get('/get/cover_letter', 'JobController@get_cover_letter');

            Route::post('/company/job/revise/quotation', 'JobController@reviseQuotation');
            Route::get('/company/account/details', 'HomeController@comapny_details_info');
            Route::post('/company_details/update', 'HomeController@save_company_details');
            Route::post('/company/job/workorder/invoide', 'JobController@request_workorder_and_invoice');
        });
    });
});
/* End FMC */

/* OAM Dashboard Portal Prefix*/
Route::group(['middleware' => ['oam']], function () {
    Route::group(['middlewareGroups' => ['web']], function () {
        Route::group(['prefix' => 'oam'], function () {
            Route::get('/home', 'HomeController@oam_home');
            Route::get('/settings', 'UserController@oam_settings_view');
            Route::post('/settings/update', 'UserController@save_oam_setting');
            Route::get('oam_profile', 'HomeController@oam_profile');
            Route::post('update_oam_profile/{id}', 'HomeController@update_oam_profile');
            Route::post('update_user_password', 'HomeController@update_user_password');
            Route::get('user_management', 'HomeController@user_management');

            // Job Post Routes
            Route::get('job_form', 'JobController@job_form');
            Route::post('/job_form_validation', 'JobController@job_post_validation');
            Route::post('save_job', 'JobController@save_job');
            Route::get('jobs', 'JobController@jobs');
            Route::get('delete_job/{id}', 'JobController@delete_job');
            Route::get('get_assets_list', 'BuildingController@get_assets');
            Route::get('get_building_assets', 'BuildingController@get_building_assets');
            Route::get('job_detail', 'JobController@job_detail');
            // Add Buildings
            Route::get('buildings', 'BuildingController@buildings');
            Route::get('building_form', 'BuildingController@building_form');
            Route::post('building_form_ajax', 'BuildingController@building_form_ajax');
            Route::post('save_building', 'BuildingController@save_building');
            Route::get('delete_building/{id}', 'BuildingController@delete_building');

            // Assets
            Route::get('assets', 'AssetsController@assets');
            Route::get('add_assets', 'AssetsController@add_assets');
            Route::post('save_assets', 'AssetsController@save_assets');
            Route::get('delete_assets/{id}', 'HomeController@delete_assets');
            Route::get('oam_job_details/{id}', 'JobController@oam_job_details');
            Route::post('shortlist/fmc/company', 'JobController@short_list');
            Route::post('rejected/fmc/company', 'JobController@rejected_company');
            Route::post('assing/fmc/company/job', 'JobController@assign_company_job');
            Route::post('awarding/fmc/company/job', 'JobController@award_company_job');
            Route::get('fmc/company/details/{id}', 'JobController@get_fmc_company_details');
            Route::post('company/details', 'JobController@get_fmc_company_details_ajax');
            Route::post('company/job/reference', 'JobController@sendJobReferenceRequest');
            /*=============================================================
            =            Section Assets Bulk Import Data            =
            =============================================================*/

            Route::post('/bulk/import/assets', 'AssetsController@import_csv_record_oam');

            /*=====  End of Section Assets Bulk Import Data  ======*/

            /* Staff Members of Admin Section */
            Route::get('staff/form', 'StaffController@staff_form');
            Route::post('check_staff_validations', 'StaffController@staff_validation');
            Route::post('/staff/member/save', 'StaffController@staff_insert');
            Route::get('staff/member/list', 'StaffController@staff_list');

            /* Roles Section of Admin Panel */
            Route::get('staff/roles', 'StaffRolesController@roles_screen');
            Route::post('/add/role_permissions', 'StaffRolesController@roles_permission_save');
            Route::post('/roles/permission_validation', 'StaffRolesController@roles_permission_validation');
            Route::get('/roles/list', 'StaffRolesController@show_roles_list');

            /* Notification Routes */
            Route::get('get_oam_notification', 'NotificationController@get_oam_notification');
            /* Job review */
            Route::get('job_review', 'JobController@job_review');

            /* Terms And Conditions */
            Route::get('terms_and_conditions', 'JobController@terms_and_conditions');
            Route::post('save_term_condition', 'JobController@save_term_condition');
            Route::get('term_conditions_list', 'JobController@term_conditions_list');
            Route::post('get_term_condition_copy', 'JobController@get_term_condition_copy');
            Route::get('change_tc_status', 'JobController@change_tc_status');
            Route::get('set_to_default', 'JobController@set_to_default');
            Route::post('submit_tc_copy', 'JobController@submit_tc_copy');
            Route::get('delete_term_and_conditions/{id}', 'JobController@delete_term_and_conditions');
            Route::post('get_terms_condition_content', 'JobController@get_terms_condition_content');
            Route::post('update_term_condition_session', 'JobController@update_term_condition_session');

            /* Show the Implementation of the Assigned Jobs and there reviews */
            Route::get('show_assigned_jobs', 'JobController@job_assigned_list');
            Route::get('oam_job_details_status/{id}', 'JobController@showJobDetails');
            Route::post('job/fmc/company/rating', 'JobController@saveReview');
            /*
            Notification Routes
             */
            Route::get('oam_all_notifications', 'NotificationController@oam_all_notifications');
            Route::post('get_staff_record', 'JobController@getStaffRecord');
            Route::get('/sub/category', 'JobController@get_sub_category');
            Route::get('/get/job_category_details', 'JobController@get_job_category_details');
            Route::get('/service_code/category', 'JobController@get_service_code');
            Route::post('/create/service_code/name', 'JobController@create_service_code');
            Route::get('/service_name/category', 'JobController@get_service_name');

            Route::get('/get/cover_letter', 'JobController@get_cover_letter');

            Route::get('/oam_job_details/renew_job/{id}', 'JobController@renew_job');
            Route::post('site_visit_date/fmc/company', 'JobController@site_visit_date_request');
            Route::post('renew/job/expiry', 'JobController@renew_job_post');
            Route::get('/company/account/details', 'HomeController@comapny_details_info');
            Route::post('/company_details/update', 'HomeController@save_company_details');
        });
    });
});
/* End OAM */

/*
General Implementation Routes
 */
Route::get('account/subscription/renewal', 'GeneralController@showRenewalAccount');
Route::get('account/subscription/payment/charges/{id}', 'GeneralController@payment_charges');
Route::get('account/subscription/payment/success', 'GeneralController@payment_success');
Route::get('account/subscription/payment/refused', 'GeneralController@payment_refused');
Route::get('signup/email/varification', 'UserController@email_varification');
Route::get('/login', 'UserController@login_form');
Route::post('/login/user', 'UserController@user_login');
Route::get('/forget_password', 'UserController@user_forget_password');
Route::get('logout', 'UserController@logout');
Route::get('/register', 'UserController@user_registration_form');
Route::post('user_registration', 'UserController@user_registration');

Route::get('/get_admin_instant_notification', 'NotificationController@get_admin_instant_notification');
Route::get('/get_oam_instant_notification', 'NotificationController@get_oam_instant_notification');
Route::get('/get_fmc_instant_notification', 'NotificationController@get_fmc_instant_notification');

Route::get('get_admin_notification', 'NotificationController@get_admin_notification');
Route::get('get_oam_notification', 'NotificationController@get_oam_notification');
Route::get('get_fmc_notification', 'NotificationController@get_fmc_notification');

Route::post('update/staff/account/status', 'StaffController@update_staff_status');
Route::post('forget/password', 'UserController@forget_password_proccess');
Route::get('user/change/to/new/password', 'UserController@new_password_screen');
Route::post('new/password/implementation', 'UserController@new_password_implementation');

Route::get('check_user', 'UserController@check_user');
Route::get('short_description', 'JobController@short_description');
Route::get('register/reference/{id}', 'UserController@user_registration_form_with_reference');
Route::post('submit_user_term_condition', 'UserController@submit_user_term_condition');
